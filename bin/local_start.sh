#!/bin/sh

set -ex
export $(cat .env | xargs)
python3.9 src/manage.py migrate

uwsgi \
    --http :8810 \
    --http-keepalive \
    --module antwoorden_cms.wsgi \
    --static-map /static=/app/static \
    --static-map /media=/app/media \
    --chdir src \
    --enable-threads \
    --post-buffering=8192 \
    --buffer-size=65535
