from copy import copy

from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from .models import Gemeente, Hyperlink, SiteConfiguration, UtterResponse


class UtterResponseForm(forms.ModelForm):
    class Meta:
        model = UtterResponse
        fields = ("response",)
        labels = {"response": _("Antwoord")}

    def __init__(self, *args, **kwargs):
        self.gemeente = kwargs.pop("gemeente", "")
        self.is_staged = kwargs.pop("is_staged", "")
        super().__init__(*args, **kwargs)

        self.fields["response"].label = f"Antwoord Gem {self.gemeente}"

        # The help_text from the utter metadata will be used instead
        self.fields["response"].help_text = ""

    def clean(self):
        # Validate that utter does not exist yet for municipality
        if self.gemeente and self.initial.get("utter"):
            if UtterResponse.objects.filter(
                gemeente__name=self.gemeente,
                utter=self.initial["utter"],
                is_staged=self.is_staged,
            ).exists():
                raise ValidationError(_("Dit antwoord bestaat al voor deze gemeente"))


class HyperlinkForm(forms.ModelForm):
    class Meta:
        model = Hyperlink
        fields = (
            "name",
            "label",
            "url",
            "title",
        )
        labels = {
            "name": _("Machinenaam"),
        }

    def __init__(self, *args, **kwargs):
        required = kwargs.pop("required", [])
        self.gemeente = kwargs.pop("gemeente", "")
        self.is_staged = kwargs.pop("is_staged", "")

        super().__init__(*args, **kwargs)

        # In case of creating a custom Hyperlink (not used in default utters)
        if not self.instance.id and not self.initial.get("name"):
            return

        # Make `name` not editable when creating a predefined Hyperlink
        if self.initial.get("name"):
            self.fields["name"].disabled = True

        # Do not allow editing of name
        if self.instance.id:
            self.fields.pop("name")

        fields = copy(self.fields)

        site_config = SiteConfiguration.get_solo()

        for field in fields:
            if field == "label":
                continue

            # Only remove fields if specified in site configuration
            if field != "name" and field not in required:
                if site_config.hyperlinks_show_only_used_attributes:
                    self.fields.pop(field)
            else:
                self.fields[field].required = True

    def clean(self):
        data = self.cleaned_data

        # Validate that hyperlink does not exist yet for municipality
        if self.gemeente and not self.instance.pk:
            if Hyperlink.objects.filter(
                gemeente=self.gemeente,
                name=data.get("name", self.initial["name"]),
                is_staged=self.is_staged,
            ).exists():
                raise ValidationError(_("Deze variabele bestaat al voor deze gemeente"))


class GemeenteAdminForm(forms.ModelForm):
    class Meta:
        model = Gemeente
        fields = "__all__"
        widgets = {
            "main_widget_color": forms.widgets.TextInput(attrs={"type": "color"}),
        }


class HyperlinksImportForm(forms.Form):
    import_file = forms.FileField(
        label=_("bestand"),
        required=True,
        help_text=_("Het YAML bestand met de hyperlinks."),
    )
