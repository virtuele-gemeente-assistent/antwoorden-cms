import json
import logging
from string import Formatter

from django.conf import settings
from django.contrib import admin, messages
from django.contrib.admin.views.decorators import staff_member_required
from django.core.cache import cache
from django.core.management import call_command
from django.http import HttpResponse, HttpResponseRedirect
from django.template.response import TemplateResponse
from django.urls import path, reverse
from django.utils.html import escape, format_html, format_html_join
from django.utils.translation import gettext_lazy as _

import requests
import yaml
from django_better_admin_arrayfield.admin.mixins import DynamicArrayMixin
from django_otp.decorators import otp_required
from ordered_model.admin import OrderedInlineModelAdminMixin, OrderedStackedInline
from solo.admin import SingletonModelAdmin

from antwoorden_cms.config.admin import import_subjects_view

from .filters import LatestModifiedUtterFilter
from .forms import GemeenteAdminForm, HyperlinksImportForm
from .models import (
    Channel,
    Gemeente,
    GemeenteDomain,
    Hyperlink,
    LivechatConfiguration,
    SiteConfiguration,
    Story,
    UtterResponse,
    VariableMetadata,
)
from .utils import (
    export_hyperlinks,
    get_local_utters,
    import_hyperlinks,
    import_stories,
)

logger = logging.getLogger(__name__)


class ChannelAdmin(admin.ModelAdmin):
    pass


admin.site.register(Channel, ChannelAdmin)


class LivechatConfigInline(OrderedStackedInline):
    model = LivechatConfiguration
    extra = 1
    fields = (
        "order",
        "name",
        "identifier",
        "type",
        "avatar_url",
        "avatar",
        "move_up_down_links",
    )
    readonly_fields = (
        "order",
        "move_up_down_links",
    )
    ordering = ("order",)


class GemeenteDomainInline(OrderedStackedInline):
    model = GemeenteDomain
    extra = 1
    fields = (
        "order",
        "domain",
        "gem_default_enabled",
        "direct_handover",
        "enable_indexing",
        "environment",
        "indexer",
        "move_up_down_links",
    )
    readonly_fields = (
        "order",
        "move_up_down_links",
    )
    ordering = ("order",)


class GemeenteAdmin(OrderedInlineModelAdminMixin, DynamicArrayMixin, admin.ModelAdmin):
    inlines = [
        GemeenteDomainInline,
        LivechatConfigInline,
    ]
    form = GemeenteAdminForm
    list_display = (
        "name",
        "slug",
    )
    fieldsets = (
        (
            None,
            {
                "fields": (
                    "name",
                    "slug",
                    "municipality_logo",
                    "website_image",
                    "contact_url",
                    "phone_number",
                    "possible_organisations_for_handover",
                    "supports_handover",
                    "livechat_enabled",
                ),
            },
        ),
        (
            "Widget styling",
            {
                "fields": (
                    "main_widget_color",
                    "custom_widget_css",
                ),
            },
        ),
        (
            "Widget metadata",
            {
                "fields": (
                    "widget_title",
                    "widget_subtitle",
                    "popup_message",
                    "widget_avatar",
                ),
            },
        ),
    )


admin.site.register(Gemeente, GemeenteAdmin)


def render_newlines(response):
    """
    Replace newlines with linebreaks, while keeping the variable placeholders
    in place
    """
    try:
        format_vars = {
            fn: f"{{{fn}}}"
            for _, fn, _, _ in Formatter().parse(response)
            if fn is not None
        }
        return format_html(escape(response).replace("\n", "</br>"), **format_vars)
    except ValueError:
        return response


@otp_required
@staff_member_required
def import_default_responses(request):
    try:
        response = requests.get(
            settings.DOMAIN_URL,
            headers={"Accept": "application/json"},
            timeout=settings.REQUESTS_TIMEOUT_LONG,
        )
        if response.status_code == 200:
            logger.info("Importing default responses from rasa endpoint")
            yaml_data = yaml.safe_load(response.text)
            call_command(
                "import_default_responses", stream=json.dumps(yaml_data), override=True
            )
        response = requests.get(
            settings.LOCAL_YML_URL, timeout=settings.REQUESTS_TIMEOUT_LONG
        )
        if response.status_code == 200:
            logger.info("Importing variable metadata from gitlab")
            call_command(
                "import_hyperlink_metadata", stream=response.content, override=True
            )
        messages.add_message(
            request,
            messages.SUCCESS,
            _("Default responses and variable metadata were successfully imported"),
        )
    except Exception as e:
        logger.exception(
            f"Failed to import default responses and hyperlink metadata from gitlab: {e}"
        )
        messages.add_message(
            request,
            messages.ERROR,
            _(
                "Something went wrong while importing default responses and variable metadata: {error}".format(
                    error=str(e)
                ),
            ),
        )

    try:
        import_stories()
        messages.add_message(
            request,
            messages.SUCCESS,
            _("Stories were successfully imported"),
        )
    except Exception as e:
        logger.exception(f"Failed to import stories: {e}")
        messages.add_message(
            request,
            messages.ERROR,
            _("Something went wrong while importing stories: {error}").format(
                error=str(e)
            ),
        )

    return HttpResponseRedirect(reverse("admin:utters_utterresponse_changelist"))


class UtterResponseAdmin(admin.ModelAdmin):
    list_display = (
        "utter",
        "gemeente",
        "get_questions",
        "get_central_default",
        "get_local_default",
        "get_response",
        "modified_on",
        "is_staged",
        "to_be_deleted",
    )
    list_filter = (
        LatestModifiedUtterFilter,
        "is_staged",
        "to_be_deleted",
        "gemeente",
    )
    search_fields = (
        "utter",
        "response",
    )
    # inlines = [ButtonInline]
    change_list_template = "admin/utterresponse_change_list.html"

    def get_questions(self, obj):
        local_utter = cache.get("admin_local_utters", {}).get(obj.utter, {})
        questions = local_utter.get("questions", [])
        return format_html_join(
            "\n", "<li>{}</li>", ((question,) for question in questions)
        )

    get_questions.short_description = _("Questions")

    def get_central_default(self, obj):
        local_utter = cache.get("admin_local_utters", {}).get(obj.utter, {})
        central_default = local_utter.get("default", "")
        if "\n" in central_default:
            return render_newlines(central_default)
        return central_default

    get_central_default.short_description = _("Central default")

    def get_local_default(self, obj):
        local_default = UtterResponse.objects.filter(
            gemeente=None, utter=obj.utter
        ).first()
        local_response = getattr(local_default, "response", "")
        if "\n" in local_response:
            return render_newlines(local_response)
        return local_response

    get_local_default.short_description = _("Central local default")

    def get_response(self, obj):
        if "\n" in obj.response:
            return render_newlines(obj.response)
        return obj.response

    get_response.short_description = _("Response")

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path(
                "import_default_responses/",
                import_default_responses,
                name="import_default_responses",
            ),
        ]
        return my_urls + urls

    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}

        # Cache the local utter data, used for aggregation
        local_utter_data = {}
        local_utters = get_local_utters()
        for utter_name, data in local_utters.items():
            if data.get("metadata", False):
                local_utter_data[utter_name] = {
                    "questions": data["metadata"]["questions"],
                    "default": data["default"][0]["text"],
                }
        cache.set("admin_local_utters", local_utters)

        return super().changelist_view(request, extra_context=extra_context)


admin.site.register(UtterResponse, UtterResponseAdmin)


@otp_required
@staff_member_required
def import_hyperlinks_view(request):
    # Disable importing on production
    if settings.ENVIRONMENT == "production":
        messages.add_message(
            request,
            messages.ERROR,
            _("Importing hyperlinks is not allowed on production"),
        )
        return HttpResponseRedirect(reverse("admin:utters_hyperlink_changelist"))

    form = HyperlinksImportForm(request.POST, request.FILES)
    if "_import_hyperlinks" in request.POST:
        form = HyperlinksImportForm(request.POST, request.FILES)
        if form.is_valid():
            try:
                import_file = form.cleaned_data["import_file"]
                import_data = yaml.safe_load(import_file.read())
                import_hyperlinks(import_data, delete_existing=True)
                messages.add_message(
                    request,
                    messages.SUCCESS,
                    _("Hyperlinks were successfully imported"),
                )
                return HttpResponseRedirect(
                    reverse("admin:utters_hyperlink_changelist")
                )
            except Exception as exc:
                messages.add_message(request, messages.ERROR, exc)
    else:
        form = HyperlinksImportForm()

    return TemplateResponse(request, "admin/hyperlink_import.html", {"form": form})


@otp_required
@staff_member_required
def export_hyperlinks_view(request):
    try:
        logger.info("Exporting hyperlinks")

        return_response = HttpResponse(content_type="application/x-yaml")
        return_response["Content-Disposition"] = "attachment;filename=local.yml"
        hyperlink_data = export_hyperlinks()

        yaml.dump(
            hyperlink_data,
            return_response,
            sort_keys=False,
            encoding="utf-8",
            allow_unicode=True,
            width=float("inf"),
        )
        messages.add_message(
            request, messages.SUCCESS, _("Hyperlinks were successfully exported")
        )
        return return_response
    except Exception as e:
        logger.exception(f"Failed to export local responses: {e}")
        messages.add_message(
            request,
            messages.ERROR,
            _("Something went wrong while exporting hyperlinks"),
        )
    return HttpResponseRedirect(reverse("admin:utters_hyperlink_changelist"))


class HyperlinkAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "is_staged",
        "gemeente",
        "label",
        "url",
    )
    list_filter = (
        "gemeente",
        "is_staged",
    )
    search_fields = (
        "name",
        "label",
        "url",
    )
    change_list_template = "admin/hyperlink_change_list.html"

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.prefetch_related("gemeente")

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path(
                "import_hyperlinks/", import_hyperlinks_view, name="import_hyperlinks"
            ),
            path(
                "export_hyperlinks/", export_hyperlinks_view, name="export_hyperlinks"
            ),
        ]
        return my_urls + urls


admin.site.register(Hyperlink, HyperlinkAdmin)


class VariableMetadataAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "is_staged",
        "category",
        "verbose_name",
        "help_text",
    )
    list_filter = (
        "category",
        "is_staged",
    )
    search_fields = (
        "name",
        "category",
        "verbose_name",
        "help_text",
    )


admin.site.register(VariableMetadata, VariableMetadataAdmin)


class SiteConfigurationAdmin(DynamicArrayMixin, SingletonModelAdmin):
    change_form_template = "admin/siteconfig_change_form.html"

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path("import-subjects/", import_subjects_view, name="import_subjects"),
        ]
        return my_urls + urls


admin.site.register(SiteConfiguration, SiteConfigurationAdmin)


@admin.register(Story)
class StoryAdmin(admin.ModelAdmin):
    list_display = (
        "identifier",
        "name",
        "category",
        "description",
        "date_created",
        "is_staging",
    )
    search_fields = (
        "identifier",
        "name",
        "description",
    )
    list_filter = (
        "category",
        "is_staging",
    )
