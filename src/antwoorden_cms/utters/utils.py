import json
import logging
import re
from collections import OrderedDict, defaultdict
from functools import reduce
from typing import Dict, List, Optional, Tuple

from django.conf import settings
from django.core.cache import cache
from django.db import transaction
from django.db.models import Count, Q, QuerySet
from django.urls import reverse

import requests
import yaml

from antwoorden_cms.config.models import OrganisationStorySettings

from .constants import (
    CENTRAL_RESPONSES_KEY,
    DOMAIN_TAG_KEY,
    ETAG_CACHE_KEY,
    LOCAL_RESPONSES_METADATA_KEY,
    RESPONSES_REFRESH_KEY,
)
from .dataclasses import Story, UtterData, search_stories
from .fetchers import FunctionalStoriesFetcher
from .models import (
    Gemeente,
    Hyperlink,
    SiteConfiguration,
    Story as StoryModel,
    UtterResponse,
    VariableMetadata,
)

logger = logging.getLogger(__name__)


def get_nested_item(data, keys):
    return reduce(lambda seq, key: seq[key], keys, data)


def transform_responses_data(responses: Dict):
    """
    Since the local response metadata is now included in domain.yml, this has
    to be transformed to a more usable data format

    See: https://gitlab.com/virtuele-gemeente-assistent/gem/-/issues/2907#note_777524786
    """
    central_responses = {}
    local_responses_metadata = {}
    for utter_name, utter_data in responses.items():
        central_responses[utter_name] = utter_data

        if "_lo_" in utter_name:
            for entry in utter_data:
                if entry.get("channel") == "metadata" and "custom" in entry:
                    local_responses_metadata[utter_name] = entry["custom"]

    return central_responses, local_responses_metadata


def update_cache(responses: dict, new_etag: str) -> bytes:
    """
    Transform the response data and update the cache with new values.

    Returns:
        The JSON-encoded domain content as bytes.
    """
    # Transform responses for central and local caching.
    central_responses, transformed_data = transform_responses_data(
        responses.get("responses", {})
    )

    # Encode all parts as JSON bytes.
    domain_content_bytes = json.dumps(responses).encode("utf-8")
    central_responses_bytes = json.dumps(central_responses).encode("utf-8")
    transformed_data_bytes = json.dumps(transformed_data).encode("utf-8")

    # Update cache with new ETag and data.
    cache.set(ETAG_CACHE_KEY, new_etag, timeout=None)
    cache.set(DOMAIN_TAG_KEY, new_etag, timeout=None)
    cache.set(CENTRAL_RESPONSES_KEY, central_responses_bytes, timeout=None)
    cache.set(LOCAL_RESPONSES_METADATA_KEY, transformed_data_bytes, timeout=None)

    return domain_content_bytes


def _retrieve_responses(headers: dict) -> Tuple[Optional[dict], Optional[str], bool]:
    """
    Retrieve responses from the server using the given headers.

    Returns:
        A tuple (responses, new_etag, not_modified_flag):
          - If the server returns a 304 status, not_modified_flag is True and responses is None.
          - If the server returns a 302, the function fetches the response from the redirected URL.
          - For a 200 status, the JSON response is parsed (if possible).
    """
    response = requests.get(
        settings.DOMAIN_URL,
        headers=headers,
        timeout=settings.REQUESTS_TIMEOUT_SHORT,
        allow_redirects=False,
    )
    new_etag = response.headers.get("ETag")
    location = response.headers.get("Location")

    if response.status_code == 304:
        # Content has not changed.
        # Flag a refresh so that repeated calls within a short interval do not trigger new requests.
        cache.set(RESPONSES_REFRESH_KEY, True, timeout=settings.REQUESTS_TIMEOUT_LONG)
        return {}, new_etag, True

    elif response.status_code == 302:
        logger.info("Local utters not found in cache, retrieving from central...")
        try:
            central_response = requests.get(
                location,
                headers=headers,
                timeout=settings.REQUESTS_TIMEOUT_SHORT,
                allow_redirects=True,
            )
            responses = yaml.safe_load(central_response.text)
        except Exception as e:
            logger.error("Error retrieving central response: %s", e)
            return {}, new_etag, False

    elif response.status_code == 200:
        # NOTE: This part is used for testing to avoid replacing mocks
        responses = response.json()
    else:
        logger.error(
            "Failed to retrieve local utters metadata, status code: %s",
            response.status_code,
        )
        return {}, new_etag, False

    return responses, new_etag, False


def cache_utter_metadata() -> Tuple[bool, bytes]:
    """
    Retrieve and cache metadata for utterances.

    Returns:
        A tuple (cache_updated, domain_content_bytes):
          - cache_updated is True if the cache was refreshed.
          - domain_content_bytes contains the updated domain content as JSON bytes.
    """
    domain_content_bytes = b""
    # Check if necessary cache keys exist (matching what tests set).
    cache_exists = (
        LOCAL_RESPONSES_METADATA_KEY in cache
        and DOMAIN_TAG_KEY in cache
        and CENTRAL_RESPONSES_KEY in cache
    )

    # Skip processing if a recent refresh has been flagged.
    if cache_exists and cache.get(RESPONSES_REFRESH_KEY):
        logger.info("Refresh key exists for local utters. Using cache.")
        return False, domain_content_bytes

    headers = {"Accept": "application/json"}
    cached_etag = cache.get(ETAG_CACHE_KEY)
    logger.info("Retrieving local utters metadata for cached etag: %s", cached_etag)

    if cache_exists and cached_etag:
        headers["If-None-Match"] = cached_etag
    else:
        logger.info("Local utters cache not found, retrieving from central...")

    responses, new_etag, not_modified = _retrieve_responses(headers)
    if not_modified:
        logger.info("Local utters not modified, using cached version.")
        return False, domain_content_bytes
    if responses is None:
        return False, domain_content_bytes

    logger.info("Domain has changed, updating cache...")
    domain_content_bytes = update_cache(responses, new_etag)
    # Flag a refresh so that repeated calls within a short interval do not trigger new requests.
    cache.set(RESPONSES_REFRESH_KEY, True, timeout=settings.REQUESTS_TIMEOUT_LONG)

    return True, domain_content_bytes


def get_central_utters(cache_only=True):
    """
    Retrieve central utter responses for the appropriate environment
    """
    if not cache_only:
        try:
            cache_utter_metadata()
        except (
            requests.exceptions.RequestException,
            json.decoder.JSONDecodeError,
        ) as e:
            logger.exception(
                f"Exception occurred while retrieving local utter metadata: {e}"
            )
            return {}

    key = CENTRAL_RESPONSES_KEY

    cached_data = cache.get(key)

    if cached_data is not None:
        if isinstance(cached_data, bytes):
            return json.loads(cached_data.decode("utf-8"))
        else:
            return cached_data


def get_local_utters() -> dict:
    """
    Retrieve local utter metadata for the appropriate environment
    """
    try:
        cache_utter_metadata()
    except Exception as e:
        logger.exception(
            f"Exception occurred while retrieving local utter metadata: {e}"
        )
        return {}

    key = LOCAL_RESPONSES_METADATA_KEY

    cached_data = cache.get(key)

    if cached_data is not None:
        if isinstance(cached_data, bytes):
            return json.loads(cached_data.decode("utf-8"))
        else:
            return cached_data


def get_non_overridden_utters(gemeente_slug, required=True):
    local_metadata = get_local_utters()

    local_utters = UtterResponse.objects.filter(
        gemeente__slug=gemeente_slug,
        is_staged=False,
    ).values_list("utter", flat=True)

    editable_utters = {}
    for utter_name, utter in local_metadata.items():
        if utter_name not in local_utters:
            if utter.get("metadata", None):
                if utter["metadata"].get("required", False) == required:
                    # continue
                    editable_utters[utter_name] = utter

    return editable_utters


def get_categories():
    local_utters = get_local_utters()

    categories = OrderedDict()
    for utter_name, utter in local_utters.items():
        if utter.get("metadata", None):
            category = utter["metadata"].get("category", "overig")
            if category in categories:
                categories[category] += 1
            else:
                categories[category] = 1

    return dict(categories)


def get_variable_categories():
    # Exclude category hyperlinks, because this category has its own view
    category_data = (
        VariableMetadata.objects.filter(~Q(category="hyperlinks"), is_staged=False)
        .values_list("category")
        .annotate(count=Count("category"))
    )
    return dict(category_data)


def get_local_utter_data(utter_name):
    local_utters = get_local_utters()

    return local_utters.get(utter_name, {})


# def get_hyperlinks(gemeente_slug, category=None):
#     """
#     Returns all of the hyperlinks+attrs used in responses for a given gemeente
#     """
#     gemeente = Gemeente.objects.get(slug=gemeente_slug)

#     names = []
#     if category:
#         names = VariableMetadata.objects.filter(category=category).values_list("name", flat=True)

#     utter_data = get_local_utters(
#         gemeente_slug=gemeente_slug,
#         return_all=True
#     )
#     utters = [utter["utter"] for utter in utter_data]

#     # Retrieve utters/responses for which the current municipality has
#     # specific responses
#     specific_utterresponses = UtterResponse.objects.filter(
#         utter__in=utters, gemeente=gemeente
#     )
#     specific_utters = specific_utterresponses.values_list("utter", flat=True)
#     specific_responses = specific_utterresponses.values_list("response", flat=True)

#     # Retrieve the remaining default utters (the ones not overridden by
#     # the current municipality)
#     default_responses = UtterResponse.objects.filter(
#         gemeente=None
#     ).values_list("response", flat=True)

#     responses = specific_responses | default_responses

#     # Extract the hyperlinks from the responses that do not exist for this
#     # municipality
#     all_hyperlinks = defaultdict(list)
#     for response in responses:
#         for match in re.findall(HYPERLINK_NAME_REGEX, response):
#             path = match[1:-1]
#             split_path = path.split('.')
#             if len(split_path) != 2:
#                 continue

#             name, attr = split_path

#             if category:
#                 # This hyperlinks is not present in the given category
#                 if name not in names:
#                     continue

#             if attr not in all_hyperlinks[name]:
#                 all_hyperlinks[name].append(attr)

#     return dict(all_hyperlinks)


def get_hyperlinks(category=None):
    """
    Returns all of the hyperlinks for which metadata is defined
    """
    filters = ~Q(category="hyperlinks")
    if category:
        # kwargs["category"] = category
        filters = Q(category=category)

    names = VariableMetadata.objects.filter(filters).values_list("name", flat=True)

    all_hyperlinks = defaultdict(list)
    for name in names:
        all_hyperlinks[name] = []

    return dict(all_hyperlinks)


def get_missing_variables(gemeente_slug, category=None):
    """
    Returns all of the missing hyperlinks for the gemeente
    """
    gemeente = Gemeente.objects.get(slug=gemeente_slug)
    all_hyperlinks = get_hyperlinks(category=category)

    hyperlinks = Hyperlink.objects.filter(gemeente=gemeente).values_list(
        "name", flat=True
    )

    # Extract the hyperlinks from the responses that do not exist for this
    # municipality
    missing_hyperlinks = defaultdict(list)
    for name, attrs in all_hyperlinks.items():
        if name not in hyperlinks:
            missing_hyperlinks[name] = attrs

    return dict(missing_hyperlinks)


def get_required_hyperlink_attrs(hyperlink):
    gemeente = hyperlink.gemeente
    utter_data = get_local_utters()
    utters = utter_data.keys()

    # Retrieve utters/responses for which the current municipality has
    # specific responses
    specific_utterresponses = UtterResponse.objects.filter(
        utter__in=utters, gemeente=gemeente
    )
    specific_utters = specific_utterresponses.values_list("utter", flat=True)
    specific_responses = specific_utterresponses.values_list("response", flat=True)

    # Retrieve the remaining default utters (the ones not overridden by
    # the current municipality)
    default_responses = UtterResponse.objects.filter(
        ~Q(utter__in=specific_utters), gemeente=None
    ).values_list("response", flat=True)

    responses = specific_responses | default_responses

    # Extract the required attributes for the given Hyperlink
    required_attrs = []
    for response in responses:
        for match in re.findall(
            r"<{}[A-z0-9\.\_\-]*>".format(hyperlink.name), response
        ):
            path = match[1:-1]
            split_path = path.split(".")
            if len(split_path) != 2:
                continue

            name, attr = split_path
            if attr not in required_attrs and attr != "label":
                required_attrs.append(attr)

    return required_attrs


def export_hyperlinks():
    """
    Aggregate hyperlinks by name (same format as in local.yml)
    """
    hyperlinks = Hyperlink.objects.order_by("name").prefetch_related("gemeente")
    result = defaultdict(dict)

    for hyperlink in hyperlinks:
        hyperlink_data = {}
        for attr in ["label", "title", "url"]:
            if getattr(hyperlink, attr):
                hyperlink_data[attr] = getattr(hyperlink, attr)
        result[hyperlink.name][hyperlink.gemeente.slug] = hyperlink_data

    return {"hyperlinks": dict(result)}


@transaction.atomic
def import_hyperlinks(import_data, delete_existing=False):
    """
    Read an imported YAML file: delete existing hyperlinks and create hyperlinks
    that do not exist yet
    """
    if delete_existing:
        Hyperlink.objects.filter(gemeente__isnull=False).delete()

    gemeentes = {gemeente.slug: gemeente for gemeente in Gemeente.objects.all()}
    hyperlinks = import_data["hyperlinks"]

    to_create = []
    for hyperlink_name, gemeente_data in hyperlinks.items():
        for gemeente_slug, data in gemeente_data.items():
            # Create gemeente if it does not exist yet
            if gemeente_slug not in gemeentes:
                created_gemeente = Gemeente.objects.create(
                    name=gemeente_slug.capitalize(), slug=gemeente_slug
                )
                gemeentes[gemeente_slug] = created_gemeente

            instance = Hyperlink(
                name=hyperlink_name,
                label=data.get("label", ""),
                url=data.get("url", ""),
                title=data.get("title", ""),
                gemeente=gemeentes[gemeente_slug],
            )
            to_create.append(instance)

    Hyperlink.objects.bulk_create(to_create)


def get_dynamic_hyperlinks():
    """
    Returns all of the hyperlink names that are used in default utters
    """
    # utters = UtterResponse.objects.filter(gemeente=None, is_staged=is_staged)

    hyperlinks = set()
    metadata = VariableMetadata.objects.filter(
        category="hyperlinks", is_staged=False
    ).values_list("name", flat=True)

    for hyperlink in metadata:
        # matches = re.findall(MARKDOWN_LINK_REGEX, utter.response)
        # for label, url, hyperlink_name in matches:
        #     # Ignore hyperlinks that already exist on prod
        #     if is_staged and hyperlink_name in prod_hyperlinks:
        #         continue

        hyperlinks.add(hyperlink)

    return sorted(hyperlinks)


def get_missing_dynamic_hyperlinks(gemeente_slug):
    """
    Returns all of the missing dynamic hyperlinks for the gemeente
    """
    gemeente = Gemeente.objects.get(slug=gemeente_slug)
    all_hyperlinks = get_dynamic_hyperlinks()

    hyperlinks = set(
        Hyperlink.objects.filter(gemeente=gemeente).values_list("name", flat=True)
    )

    return list(set(all_hyperlinks) - hyperlinks)


def collect_local_metadata(gemeente: Gemeente):
    # TODO replace with queryset that contains either defaults or overrides
    utter_data = get_local_utters()
    res = {}

    overrides = UtterResponse.objects.filter(gemeente=gemeente, is_staged=False)
    overrides = queryset_to_dict(overrides)

    default_utters = UtterResponse.objects.filter(gemeente=None, is_staged=False)
    default_utters = queryset_to_dict(default_utters)

    for utter_name, data in utter_data.items():
        kwargs = dict(
            local_override_prod=overrides.get(utter_name),
            local_default_prod=default_utters.get(utter_name),
        )
        res[utter_name] = UtterData(
            utter_name=utter_name,
            metadata=data,
            gemeente=gemeente,
            **kwargs,
        )

    return res


def retrieve_story_categories() -> List[str]:
    """
    Retrieve the story categories for a given environment

    In case of staging, only the categories that do not exist on prod will be returned
    """
    fetcher = FunctionalStoriesFetcher()
    categories = fetcher.retrieve_story_categories()

    return categories


def retrieve_story_categories_for_dashboard(
    gemeente_slug,
    request,
):
    results = defaultdict(dict)

    categories = (
        StoryModel.objects.filter(is_staging=False)
        .values_list("category", flat=True)
        .distinct()
        .order_by()
    )

    for category in categories:
        story_ids = StoryModel.objects.filter(
            category=category, is_staging=False
        ).values_list("identifier", flat=True)
        num_stories = story_ids.count()
        num_ids = story_ids.distinct("identifier").count()

        # Call stories API
        # HOTFIX: retrieve data internally for now, because calling its own API causes
        # performance issues
        story_settings = OrganisationStorySettings.objects.filter(
            organisation__slug=gemeente_slug
        )
        story_settings_for_category = [
            setting.story_id
            for setting in story_settings
            if setting.story_id in story_ids
        ]

        if len(story_settings_for_category) < num_ids:
            results[category]["action_required"] = True
        results[category]["num_stories"] = num_stories

    return dict(results)


def retrieve_stories_for_category(
    category: str,
) -> List[Dict]:
    fetcher = FunctionalStoriesFetcher()
    story_data = fetcher.retrieve_stories(category)
    return story_data


def retrieve_stories(
    category: str,
    gemeente: Gemeente,
    search_query: Optional[str] = None,
) -> List[Story]:
    local_utter_data = collect_local_metadata(gemeente)
    central_utter_data = get_central_utters(cache_only=False)

    fetcher = FunctionalStoriesFetcher()
    story_data = fetcher.retrieve_stories(category)

    stories = [
        Story(
            local_utter_data=local_utter_data,
            central_utter_data=central_utter_data,
            story_name=data["story_name"],
            story_id=data["story_id"],
            conversation=data["conversation"],
            metadata=data["metadata"],
        )
        for data in story_data
    ]

    if search_query:
        stories = search_stories(search_query, stories)

    return stories


@transaction.atomic
def import_stories():
    config = SiteConfiguration.get_solo()
    story_objects = []
    categories = retrieve_story_categories()
    for category in categories:
        if category in config.disabled_story_categories:
            continue

        story_data = retrieve_stories_for_category(category)
        for story in story_data:
            description = story.get("metadata", {}).get("description", "")
            story_objects.append(
                StoryModel(
                    name=story["story_name"],
                    identifier=story["story_id"],
                    category=category,
                    description=description,
                )
            )

    StoryModel.objects.all().delete()
    StoryModel.objects.bulk_create(story_objects)


def queryset_to_dict(qs: QuerySet, key_name: str = "utter") -> dict:
    return {getattr(x, key_name): x for x in qs}


# TODO not in use yet
def get_organisations(request):
    api_url = getattr(
        settings,
        "ORGANISATION_API_URL",
        request.build_absolute_uri(reverse("antwoorden_cms:organisation_list")),
    )

    try:
        response = requests.get(api_url, timeout=settings.REQUESTS_TIMEOUT_SHORT)
        return response.json()
    except requests.RequestException:
        logger.exception(f"Failed to retrieve organisations from {api_url}")
        return []
