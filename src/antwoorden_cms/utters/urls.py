from django.urls import path

from .apps import AppConfig
from .views import (
    DynamicHyperlinkListView,
    DynamicRequiredHyperlinkListView,
    GemeenteView,
    HomePageView,
    HyperlinkCreateView,
    HyperlinkDeleteView,
    HyperlinkDetailView,
    HyperlinkEditableListView,
    HyperlinkListView,
    HyperlinkUpdateView,
    StoriesListView,
    UtterResponseChangeListView,
    UtterResponseCreatedListView,
    UtterResponseCreateView,
    UtterResponseDeleteView,
    UtterResponseDetailView,
    UtterResponseListView,
    UtterResponseRequiredEditableListView,
    UtterResponseUpdateView,
)

app_name = AppConfig.__name__

urlpatterns = [
    path("", HomePageView.as_view(), name="home"),
    # Lists of yet to be edited responses
    # path('<slug:gemeente_slug>/antwoorden/editable/', UtterResponseEditableListView.as_view(), name='antwoord_list_editable'),
    path(
        "<slug:gemeente_slug>/antwoorden/editable/required",
        UtterResponseRequiredEditableListView.as_view(),
        name="antwoord_list_editable_required",
    ),
    # Municipality specific responses
    path("<slug:gemeente_slug>/", GemeenteView.as_view(), name="gemeente_detail"),
    path(
        "<slug:gemeente_slug>/antwoorden/",
        UtterResponseListView.as_view(),
        name="antwoord_list",
    ),
    path(
        "<slug:gemeente_slug>/antwoorden/create/",
        UtterResponseCreateView.as_view(),
        name="antwoord_create",
    ),
    path(
        "<slug:gemeente_slug>/antwoorden/<int:antwoord_pk>/",
        UtterResponseDetailView.as_view(),
        name="antwoord_read",
    ),
    path(
        "<slug:gemeente_slug>/antwoorden/<int:antwoord_pk>/update",
        UtterResponseUpdateView.as_view(),
        name="antwoord_update",
    ),
    path(
        "<slug:gemeente_slug>/antwoorden/<int:antwoord_pk>/delete/",
        UtterResponseDeleteView.as_view(),
        name="antwoord_delete",
    ),
    path(
        "<slug:gemeente_slug>/antwoorden/changed",
        UtterResponseChangeListView.as_view(),
        name="antwoord_changed_overview",
    ),
    path(
        "<slug:gemeente_slug>/antwoorden/created",
        UtterResponseCreatedListView.as_view(),
        name="antwoord_created_overview",
    ),
    path(
        "<slug:gemeente_slug>/stories/<str:category>",
        StoriesListView.as_view(),
        name="stories_list",
    ),
    # Variables pages
    path(
        "<slug:gemeente_slug>/hyperlinks/",
        HyperlinkListView.as_view(),
        name="hyperlink_list",
    ),
    path(
        "<slug:gemeente_slug>/hyperlinks/editable/",
        HyperlinkEditableListView.as_view(),
        name="hyperlink_list_editable",
    ),
    path(
        "<slug:gemeente_slug>/hyperlinks/create",
        HyperlinkCreateView.as_view(),
        name="hyperlink_create",
    ),
    path(
        "<slug:gemeente_slug>/hyperlinks/<int:hyperlink_pk>/",
        HyperlinkDetailView.as_view(),
        name="hyperlink_read",
    ),
    path(
        "<slug:gemeente_slug>/hyperlinks/<int:hyperlink_pk>/update",
        HyperlinkUpdateView.as_view(),
        name="hyperlink_update",
    ),
    path(
        "<slug:gemeente_slug>/hyperlinks/<int:hyperlink_pk>/delete/",
        HyperlinkDeleteView.as_view(),
        name="hyperlink_delete",
    ),
    # Dynamic hyperlinks
    path(
        "<slug:gemeente_slug>/dynamic-hyperlinks/",
        DynamicHyperlinkListView.as_view(),
        name="dynamic_hyperlink_list",
    ),
    path(
        "<slug:gemeente_slug>/dynamic-required-hyperlinks/",
        DynamicRequiredHyperlinkListView.as_view(),
        name="dynamic_required_hyperlink_list",
    ),
]
