from django.apps import AppConfig


class UttersConfig(AppConfig):
    name = "antwoorden_cms.utters"
