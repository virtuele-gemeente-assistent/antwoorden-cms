import logging

from django.conf import settings
from django.urls import reverse
from django.views.generic import (
    CreateView,
    DeleteView,
    DetailView,
    ListView,
    TemplateView,
    UpdateView,
)

from two_factor.views import OTPRequiredMixin

from antwoorden_cms.config.models import OrganisationStorySettings
from antwoorden_cms.statistics.models import Graph

from ..dataclasses import UtterData
from ..forms import UtterResponseForm
from ..mixins import ObjectPermissionMixin, RedirectMixin, SearchMixin, SortingMixin
from ..models import Gemeente, SiteConfiguration, UtterResponse
from ..utils import (
    get_categories,
    get_dynamic_hyperlinks,
    get_local_utter_data,
    get_local_utters,
    get_missing_dynamic_hyperlinks,
    get_missing_variables,
    get_non_overridden_utters,
    get_variable_categories,
    queryset_to_dict,
    retrieve_stories,
    retrieve_story_categories_for_dashboard,
)

logger = logging.getLogger(__name__)


class HomePageView(TemplateView):
    template_name = "home.html"

    def get(self, request, *args, **kwargs):
        # Always switch back to prod
        request.session["environment"] = "production"
        request.session["chatbot_url"] = settings.CHATBOT_URL
        return super().get(request, *args, **kwargs)


class GemeenteView(DetailView):
    model = Gemeente
    slug_url_kwarg = "gemeente_slug"

    permission_required = "utters.gemeente_read"

    template_name = "utters/gemeente_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["categories"] = get_categories()
        # context["num_local_utters"] = len(get_non_overridden_utters(self.kwargs['gemeente_slug'], required=False))
        context["num_local_utters_required"] = len(
            get_non_overridden_utters(
                self.kwargs["gemeente_slug"],
                required=True,
            )
        )
        # context["gemeente_antwoorden"] = UtterResponse.objects.filter(gemeente=context["object"])

        context["num_variables_missing"] = len(
            get_missing_variables(self.kwargs["gemeente_slug"])
        )
        context["variable_categories"] = get_variable_categories()

        context["num_hyperlinks_missing"] = len(
            get_missing_dynamic_hyperlinks(self.kwargs["gemeente_slug"])
        )
        context["num_hyperlinks"] = len(get_dynamic_hyperlinks())
        # context["hyperlinks"] = Hyperlink.objects.filter(gemeente__slug=self.kwargs['gemeente_slug'])

        context["story_categories"] = retrieve_story_categories_for_dashboard(
            self.kwargs["gemeente_slug"], self.request
        )

        context["graph_definitions"] = Graph.objects.all()

        # Feature flag: only show configured categories
        config = SiteConfiguration.get_solo()
        context["story_categories"] = {
            category: data
            for category, data in context["story_categories"].items()
            if category not in config.disabled_story_categories
        }

        context["categories"] = {
            category: count
            for category, count in context["categories"].items()
            if category in config.enabled_utter_categories
        }

        return context


class UtterResponseRequiredEditableListView(
    OTPRequiredMixin, ObjectPermissionMixin, TemplateView
):
    template_name = "utters/antwoord_list_editable.html"

    permission_required = "utters.utter_read"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["utterresponse_list"] = list(
            get_non_overridden_utters(
                self.kwargs["gemeente_slug"], required=True
            ).items()
        )
        for utter_name, data in context["utterresponse_list"]:
            data["response"] = data["default"][0]["text"]
        context["required_utters"] = True
        return context


class UtterResponseListView(
    SortingMixin, SearchMixin, OTPRequiredMixin, ObjectPermissionMixin, ListView
):
    template_name = "utters/antwoord_list.html"
    model = UtterResponse
    default_sort_param = "utter"
    default_sort_order = "asc"

    permission_required = "utters.utter_read"

    def get_queryset(self):
        qs = super().get_queryset()
        is_staged = False
        return qs.filter(
            gemeente__slug=self.kwargs["gemeente_slug"], is_staged=is_staged
        ).order_by("pk")

    def aggregate_utter_data(self):
        gemeente = Gemeente.objects.get(slug=self.kwargs["gemeente_slug"])

        # Retrieve local utters for current gemeente
        utter_data = get_local_utters()
        res = []

        category = self.request.GET.get("category", "")

        overrides = queryset_to_dict(self.object_list)
        default_utters = queryset_to_dict(
            UtterResponse.objects.filter(gemeente=None, is_staged=False)
        )
        for utter_name, data in utter_data.items():
            entry = UtterData(
                utter_name=utter_name,
                metadata=data,
                gemeente=gemeente,
                local_default_prod=default_utters.get(utter_name),
                local_override_prod=overrides.get(utter_name),
            )

            # Filter utters by category from metadata
            if category and category != entry.metadata["metadata"].get(
                "category", "overig"
            ):
                continue

            res.append(entry)
        return res


class StoriesListView(OTPRequiredMixin, ObjectPermissionMixin, TemplateView):
    template_name = "stories/stories_list.html"

    permission_required = "utters.utter_read"

    def get_context_data(self, **kwargs):
        search_query = self.request.GET.get("search", "").lower()
        gemeente = Gemeente.objects.get(slug=self.kwargs["gemeente_slug"])

        context = super().get_context_data(**kwargs)
        context["stories"] = retrieve_stories(
            self.kwargs["category"],
            gemeente=gemeente,
            search_query=search_query,
        )

        story_settings_mapping = {
            setting.story_id: setting
            for setting in OrganisationStorySettings.objects.filter(
                organisation=gemeente
            )
        }
        context["story_settings"] = story_settings_mapping
        context["search_query"] = search_query

        # TODO currently a configurable list per organisation
        # context["handover_options"] = get_organisations(self.request)
        context["handover_options"] = gemeente.possible_organisations_for_handover

        return context


class UtterResponseChangeListView(
    SortingMixin, SearchMixin, OTPRequiredMixin, ObjectPermissionMixin, ListView
):
    """
    List of local utters and the dates on which their defaults have changed
    """

    template_name = "utters/utters_changed_overview.html"
    model = UtterResponse
    default_sort_param = "modified_on"
    default_sort_order = "desc"

    permission_required = "utters.utter_read"

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(
            gemeente__slug=self.kwargs["gemeente_slug"], is_staged=False
        ).order_by("pk")

    def aggregate_utter_data(self):
        gemeente = Gemeente.objects.get(slug=self.kwargs["gemeente_slug"])

        # Retrieve local utters for current gemeente
        utter_data = get_local_utters()
        res = []

        overrides = queryset_to_dict(self.object_list)
        default_utters = queryset_to_dict(
            UtterResponse.objects.filter(gemeente=None, is_staged=False)
        )
        for utter_name, data in utter_data.items():
            local_override = overrides.get(utter_name)
            if not local_override:
                continue

            entry = UtterData(
                utter_name=utter_name,
                metadata=data,
                gemeente=gemeente,
                local_override_prod=local_override,
                local_default_prod=default_utters.get(utter_name),
                # local_override_staging=None,
                # local_default_staging=None,
            )

            res.append(entry)
        return res


class UtterResponseCreatedListView(
    SortingMixin, SearchMixin, OTPRequiredMixin, ObjectPermissionMixin, ListView
):
    """
    List of local utters and the dates on which they were created
    """

    template_name = "utters/utters_created_overview.html"
    model = UtterResponse
    default_sort_param = "created"
    default_sort_order = "desc"

    permission_required = "utters.utter_read"

    def get_queryset(self):
        qs = super().get_queryset()
        is_staged = False
        return qs.filter(
            gemeente__slug=self.kwargs["gemeente_slug"], is_staged=is_staged
        ).order_by("pk")

    def aggregate_utter_data(self):
        gemeente = Gemeente.objects.get(slug=self.kwargs["gemeente_slug"])

        # Retrieve local utters for current gemeente
        utter_data = get_local_utters()
        res = []

        utter_names = self.object_list.values_list("utter", flat=True)
        default_utters = queryset_to_dict(
            UtterResponse.objects.filter(gemeente=None, is_staged=False)
        )
        for utter_name, data in utter_data.items():
            # Local variant of utter exists
            if utter_name in utter_names:
                continue

            entry = UtterData(
                utter_name=utter_name,
                metadata=data,
                gemeente=gemeente,
                local_default_prod=default_utters.get(utter_name),
                # local_override_prod=self.object_list.filter(utter=utter_name).first(),
            )

            if not entry.metadata["metadata"].get("created"):
                continue

            res.append(entry)
        return res


class UtterResponseCreateView(
    RedirectMixin, OTPRequiredMixin, ObjectPermissionMixin, CreateView
):
    template_name = "utters/antwoord_create.html"
    model = UtterResponse
    form_class = UtterResponseForm

    permission_required = "utters.utter_write"

    def get_success_url(self):
        success_url = super().get_success_url()
        if success_url:
            return success_url

        context = self.get_context_data()
        category = context["utter_data"].get("metadata", {}).get("category", "overig")
        base_url = reverse(
            "gemeentes:antwoord_list",
            kwargs={"gemeente_slug": self.kwargs["gemeente_slug"]},
        )
        return f"{base_url}?category={category}"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["utter_data"] = get_local_utter_data(
            self.request.GET.get("utter_name"),
        )
        context["required_utter"] = (
            context["utter_data"].get("metadata", {}).get("required", False)
        )
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["initial"] = {
            "utter": self.request.GET.get("utter_name"),
        }
        kwargs["gemeente"] = self.kwargs["gemeente_slug"].capitalize()
        kwargs["is_staged"] = False
        return kwargs

    def form_valid(self, form):
        self.object = form.save()
        self.object.gemeente = Gemeente.objects.get(slug=self.kwargs["gemeente_slug"])
        self.object.utter = self.request.GET.get("utter_name")
        self.object.is_staged = False
        return super().form_valid(form)


class UtterResponseDetailView(OTPRequiredMixin, ObjectPermissionMixin, DetailView):
    template_name = "utters/antwoord_detail.html"
    model = UtterResponse
    pk_url_kwarg = "antwoord_pk"

    permission_required = "utters.utter_read"

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(gemeente__slug=self.kwargs["gemeente_slug"])


class UtterResponseUpdateView(
    RedirectMixin, OTPRequiredMixin, ObjectPermissionMixin, UpdateView
):
    template_name = "utters/antwoord_update.html"
    model = UtterResponse
    form_class = UtterResponseForm
    pk_url_kwarg = "antwoord_pk"

    permission_required = "utters.utter_write"

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(gemeente__slug=self.kwargs["gemeente_slug"])

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["gemeente"] = self.kwargs["gemeente_slug"].capitalize()
        return kwargs

    def form_valid(self, form):
        self.object = form.save()
        # When editing a staging override, it is no longer scheduled to be deleted
        self.object.to_be_deleted = False
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["utter_data"] = get_local_utter_data(
            context["object"].utter,
        )
        return context

    def get_success_url(self):
        if self.request.is_ajax():
            return ""

        success_url = super().get_success_url()
        if success_url:
            return success_url

        context = self.get_context_data()
        category = context["utter_data"].get("metadata", {}).get("category", "overig")
        base_url = reverse(
            "gemeentes:antwoord_list",
            kwargs={"gemeente_slug": self.kwargs["gemeente_slug"]},
        )
        return f"{base_url}?category={category}"


class UtterResponseDeleteView(
    RedirectMixin, OTPRequiredMixin, ObjectPermissionMixin, DeleteView
):
    model = UtterResponse
    pk_url_kwarg = "antwoord_pk"

    permission_required = "utters.utter_write"

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(gemeente__slug=self.kwargs["gemeente_slug"])

    def get(self, request, *args, **kwargs):
        self.request.session["redirect_url"] = self.request.META.get("HTTP_REFERER")
        return self.post(request, *args, **kwargs)

    def get_success_url(self):
        success_url = super().get_success_url()
        if success_url:
            return success_url

        utter_data = get_local_utter_data(self.object.utter)
        category = utter_data.get("metadata", {}).get("category", "overig")
        base_url = reverse(
            "gemeentes:antwoord_list",
            kwargs={"gemeente_slug": self.kwargs["gemeente_slug"]},
        )
        return f"{base_url}?category={category}"
