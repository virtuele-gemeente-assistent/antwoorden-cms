from django.db.models import Q
from django.urls import reverse
from django.views.generic import (
    CreateView,
    DeleteView,
    DetailView,
    ListView,
    TemplateView,
    UpdateView,
)

from two_factor.views import OTPRequiredMixin

from ..forms import HyperlinkForm
from ..mixins import ObjectPermissionMixin, RedirectMixin
from ..models import Gemeente, Hyperlink, VariableMetadata
from ..utils import (  # get_required_hyperlink_attrs,
    get_dynamic_hyperlinks,
    get_missing_dynamic_hyperlinks,
    get_missing_variables,
)


class HyperlinkListView(OTPRequiredMixin, ObjectPermissionMixin, ListView):
    template_name = "utters/hyperlink_list.html"
    model = Hyperlink
    default_sort_param = "name"
    default_sort_order = "asc"

    permission_required = "utters.hyperlink_read"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["category"] = self.request.GET.get("category")
        context["sort"] = self.request.GET.get("sort", self.default_sort_param)
        context["order"] = self.request.GET.get("order", self.default_sort_order)
        context["search_query"] = self.request.GET.get("search", "").lower()
        return context

    def get_queryset(self):
        qs = super().get_queryset()
        is_staged = False
        qs = qs.filter(is_staged=is_staged)

        hyperlinks = qs.filter(gemeente__slug=self.kwargs["gemeente_slug"]).order_by(
            "pk"
        )

        kwargs = {}
        if self.request.GET.get("category"):
            kwargs = {"category": self.request.GET["category"]}

        metadata = VariableMetadata.objects.filter(is_staged=is_staged, **kwargs)
        search_filter = self.request.GET.get("search")
        if search_filter:
            names = qs.filter(
                Q(gemeente__slug=self.kwargs["gemeente_slug"])
                & (Q(label__icontains=search_filter) | Q(url__icontains=search_filter))
            ).values_list("name", flat=True)
            # filter for verbose name matches, but include hyperlinks with matches
            metadata = metadata.filter(
                Q(name__in=names) | Q(verbose_name__icontains=search_filter)
            )

        res = []
        for meta in metadata:
            if hyperlinks.filter(name=meta.name).exists():
                hyperlink = hyperlinks.filter(name=meta.name).last()
                res.append((meta.verbose_name, hyperlink))
            else:
                res.append((meta.verbose_name, meta.name, []))

        sorting_param = self.request.GET.get("sort", self.default_sort_param)
        order = self.request.GET.get("order", self.default_sort_order)

        def sort_hyperlinks(elem):
            # Local variant exists
            if len(elem) == 2:
                if sorting_param == "name":
                    return elem[0]
                elif sorting_param == "label" or sorting_param == "url":
                    return getattr(elem[1], sorting_param, "")
            # No local variant exists
            elif len(elem) == 3:
                if sorting_param == "name":
                    return elem[0]
                elif sorting_param == "label" or sorting_param == "url":
                    return ""

        res = sorted(res, key=sort_hyperlinks, reverse=order == "desc")

        return res


class HyperlinkEditableListView(OTPRequiredMixin, ObjectPermissionMixin, TemplateView):
    template_name = "utters/hyperlink_list_editable.html"

    permission_required = "utters.hyperlink_read"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        metadata = VariableMetadata.objects.filter()

        missing = get_missing_variables(self.kwargs["gemeente_slug"])

        missing_hyperlinks = []
        for meta in metadata:
            if meta.name in missing:
                missing_hyperlinks.append(
                    (meta.verbose_name, meta.name, missing[meta.name])
                )
        context["missing_hyperlinks"] = missing_hyperlinks

        return context


class DynamicHyperlinkListView(OTPRequiredMixin, ObjectPermissionMixin, ListView):
    template_name = "utters/dynamic_hyperlink_list.html"
    model = Hyperlink
    default_sort_param = "name"
    default_sort_order = "asc"

    permission_required = "utters.hyperlink_read"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["sort"] = self.request.GET.get("sort", self.default_sort_param)
        context["order"] = self.request.GET.get("order", self.default_sort_order)
        is_staged = False
        metadata = VariableMetadata.objects.filter(
            is_staged=is_staged, category="hyperlinks"
        )
        context["metadata"] = {x.name: x.verbose_name for x in metadata}
        return context

    def get_queryset(self):
        qs = super().get_queryset()
        is_staged = False
        qs = qs.filter(is_staged=is_staged)

        hyperlink_names = get_dynamic_hyperlinks()

        hyperlinks = qs.filter(
            gemeente__slug=self.kwargs["gemeente_slug"], name__in=hyperlink_names
        ).order_by("pk")
        existing_hyperlink_names = hyperlinks.values_list("name", flat=True)

        res = [
            (
                hyperlinks.filter(name=name).last()
                if name in existing_hyperlink_names
                else Hyperlink(name=name, gemeente=None)
            )
            for name in hyperlink_names
        ]

        sorting_param = self.request.GET.get("sort", self.default_sort_param)
        order = self.request.GET.get("order", self.default_sort_order)

        res = sorted(
            res, key=lambda x: getattr(x, sorting_param, ""), reverse=order == "desc"
        )

        return res


class DynamicRequiredHyperlinkListView(
    OTPRequiredMixin, ObjectPermissionMixin, ListView
):
    template_name = "utters/dynamic_required_hyperlink_list.html"
    model = Hyperlink
    default_sort_param = "name"
    default_sort_order = "asc"

    permission_required = "utters.hyperlink_read"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["sort"] = self.request.GET.get("sort", self.default_sort_param)
        context["order"] = self.request.GET.get("order", self.default_sort_order)
        is_staged = False
        metadata = VariableMetadata.objects.filter(
            is_staged=is_staged, category="hyperlinks"
        )
        context["metadata"] = {x.name: x.verbose_name for x in metadata}
        return context

    def get_queryset(self):
        hyperlink_names = get_missing_dynamic_hyperlinks(self.kwargs["gemeente_slug"])

        res = [Hyperlink(name=name, gemeente=None) for name in hyperlink_names]

        sorting_param = self.request.GET.get("sort", self.default_sort_param)
        order = self.request.GET.get("order", self.default_sort_order)

        res = sorted(
            res, key=lambda x: getattr(x, sorting_param, ""), reverse=order == "desc"
        )

        return res


class HyperlinkCreateView(
    RedirectMixin, OTPRequiredMixin, ObjectPermissionMixin, CreateView
):
    template_name = "utters/hyperlink_create.html"
    fields = (
        "name",
        "label",
        "url",
    )

    permission_required = "utters.hyperlink_write"

    def get_form_class(self):
        return HyperlinkForm

    def get_success_url(self):
        success_url = super().get_success_url()
        if success_url:
            return success_url

        url = reverse(
            "gemeentes:hyperlink_list",
            kwargs={"gemeente_slug": self.kwargs["gemeente_slug"]},
        )
        context = self.get_context_data()
        if context.get("metadata"):
            category = context["metadata"].category
            return f"{url}?category={category}"
        return url

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["initial"] = {"name": self.request.GET.get("name")}
        # kwargs["required"] = dict(self.request.GET).get("required", [])
        kwargs["required"] = []
        kwargs["gemeente"] = Gemeente.objects.get(slug=self.kwargs["gemeente_slug"])
        kwargs["is_staged"] = False
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.GET.get("name"):
            context["metadata"] = VariableMetadata.objects.filter(
                name=self.request.GET.get("name")
            ).first()
            context["category"] = getattr(context["metadata"], "category", None)
        return context

    def form_valid(self, form):
        self.object = form.save()
        self.object.gemeente = Gemeente.objects.get(slug=self.kwargs["gemeente_slug"])
        self.object.is_staged = False
        return super().form_valid(form)


class HyperlinkDetailView(OTPRequiredMixin, ObjectPermissionMixin, DetailView):
    template_name = "utters/hyperlink_detail.html"
    model = Hyperlink
    pk_url_kwarg = "hyperlink_pk"

    permission_required = "utters.hyperlink_read"

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(gemeente__slug=self.kwargs["gemeente_slug"])


class HyperlinkUpdateView(
    RedirectMixin, OTPRequiredMixin, ObjectPermissionMixin, UpdateView
):
    template_name = "utters/hyperlink_update.html"
    model = Hyperlink
    form_class = HyperlinkForm
    pk_url_kwarg = "hyperlink_pk"

    permission_required = "utters.hyperlink_write"

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["required"] = []
        kwargs["gemeente"] = self.kwargs["gemeente_slug"].capitalize()

        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["metadata"] = VariableMetadata.objects.filter(
            name=self.object.name
        ).first()
        context["category"] = getattr(context["metadata"], "category", None)
        return context

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(gemeente__slug=self.kwargs["gemeente_slug"])

    def get_success_url(self):
        success_url = super().get_success_url()
        if success_url:
            return success_url

        url = reverse(
            "gemeentes:hyperlink_list",
            kwargs={"gemeente_slug": self.kwargs["gemeente_slug"]},
        )
        context = self.get_context_data()
        if context.get("metadata"):
            category = context["metadata"].category
            return f"{url}?category={category}"
        return url


class HyperlinkDeleteView(OTPRequiredMixin, ObjectPermissionMixin, DeleteView):
    model = Hyperlink
    pk_url_kwarg = "hyperlink_pk"

    permission_required = "utters.hyperlink_write"

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(gemeente__slug=self.kwargs["gemeente_slug"])

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)

    def get_success_url(self):
        url = reverse(
            "gemeentes:hyperlink_list",
            kwargs={"gemeente_slug": self.kwargs["gemeente_slug"]},
        )
        metadata = VariableMetadata.objects.filter(name=self.object.name)
        if metadata.exists():
            category = metadata.first().category
            return f"{url}?category={category}"
        return url
