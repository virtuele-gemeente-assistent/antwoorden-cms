from django import template
from django.template.defaultfilters import stringfilter

from ..models import UtterResponse

register = template.Library()


@register.filter
@stringfilter
def format_response(value, gemeente):
    return UtterResponse.format_hyperlinks(value, gemeente)
