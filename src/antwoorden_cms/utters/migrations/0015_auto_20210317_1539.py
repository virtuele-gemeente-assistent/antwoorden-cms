# Generated by Django 2.2.11 on 2021-03-17 14:39

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("utters", "0014_siteconfiguration"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="gemeente",
            options={
                "permissions": (
                    (
                        "gemeente_read",
                        "Permissie om inhoud van gemeente dashboard in te zien",
                    ),
                    ("utter_read", "Permissie om inhoud van antwoorden in te zien"),
                    (
                        "utter_write",
                        "Permissie om inhoud van antwoorden aan te maken/wijzigen/verwijderen",
                    ),
                    (
                        "hyperlink_read",
                        "Permissie om inhoud van hyperlinks/leges in te zien",
                    ),
                    (
                        "hyperlink_write",
                        "Permissie om inhoud van hyperlinks/leges aan te maken/wijzigen/verwijderen",
                    ),
                    (
                        "statistics_read",
                        "Permissie om inhoud van statistieken in te zien",
                    ),
                    (
                        "dialog_read",
                        "Permissie om inhoud van gevoerde gesprekken met Gem in te zien",
                    ),
                )
            },
        ),
    ]
