# Generated by Django 2.2.11 on 2020-12-08 13:43

import sys

from django.conf import settings
from django.contrib.sites.models import Site
from django.db import migrations


def set_site(apps, schema_editor):
    # Site object is required for the QR code generation (django-otp)
    domain = "localhost"
    if settings.ALLOWED_HOSTS:
        domain = settings.ALLOWED_HOSTS[0]

    if "test" not in sys.argv:
        Site.objects.get_or_create(
            domain=domain, name=f"Antwoorden CMS ({settings.ENVIRONMENT})"
        )


class Migration(migrations.Migration):

    dependencies = [
        ("sites", "0001_initial"),
        ("utters", "0011_obilivechatconfiguration"),
    ]

    operations = [migrations.RunPython(set_site, migrations.RunPython.noop)]
