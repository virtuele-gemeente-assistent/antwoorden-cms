# Generated by Django 2.2.11 on 2020-10-28 10:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("utters", "0009_hyperlink_title"),
    ]

    operations = [
        migrations.CreateModel(
            name="VariableMetadata",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "name",
                    models.CharField(
                        help_text="De machinenaam van de variabele", max_length=512
                    ),
                ),
                (
                    "category",
                    models.CharField(
                        blank=True,
                        help_text="De categorie waartoe deze variabele behoort",
                        max_length=255,
                    ),
                ),
                (
                    "verbose_name",
                    models.CharField(
                        blank=True,
                        help_text="De volledige naam van deze variabele",
                        max_length=512,
                    ),
                ),
                (
                    "help_text",
                    models.TextField(
                        blank=True,
                        help_text="De help text die bij het aanmaken/bewerken van deze variabele verschijnt",
                    ),
                ),
            ],
            options={
                "verbose_name": "Variabelen metadata",
            },
        ),
    ]
