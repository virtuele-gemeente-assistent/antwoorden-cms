# Generated by Django 2.2.11 on 2020-11-18 09:36

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("utters", "0010_variablemetadata"),
    ]

    operations = [
        migrations.CreateModel(
            name="ObiLivechatConfiguration",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "guid",
                    models.CharField(
                        help_text="GUID van de OBI4Wan omgeving voor de gekoppelde gemeente.",
                        max_length=255,
                    ),
                ),
                (
                    "avatar_url",
                    models.URLField(
                        blank=True,
                        help_text="URL van de avatar die gebruikt wordt voor livechat.",
                    ),
                ),
                (
                    "gemeente",
                    models.OneToOneField(
                        help_text="Gemeente waaraan deze OBI4Wan configuratie gekoppeld is.",
                        on_delete=django.db.models.deletion.CASCADE,
                        to="utters.Gemeente",
                    ),
                ),
            ],
        ),
    ]
