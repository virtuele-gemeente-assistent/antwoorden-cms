# Generated by Django 3.2.13 on 2022-10-19 09:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("utters", "0037_siteconfiguration_story_toggling_enabled"),
    ]

    operations = [
        migrations.AddField(
            model_name="gemeente",
            name="website_image",
            field=models.ImageField(
                blank=True,
                help_text="Een afbeelding van de website die getoond wordt voor niet ingelogde gebruikers van het CMS.",
                null=True,
                upload_to="",
                verbose_name="Website afbeelding",
            ),
        ),
    ]
