import json
from unittest.mock import patch

from django.conf import settings
from django.core.cache import cache
from django.core.management import call_command
from django.test import TestCase, override_settings

import requests_mock

from antwoorden_cms.utters.constants import LOCAL_RESPONSES_METADATA_KEY

DOMAIN_URL = f"{settings.DOMAIN_URL}"
STATUS_URL = f"{settings.CHATBOT_URL}/status"

STATUS_RESPONSE = {"fingerprint": {"domain": "1234"}}
STAGING_STATUS_RESPONSE = {"fingerprint": {"domain": "4321"}}


class CheckImportsTestCase(TestCase):
    @override_settings(ENVIRONMENT="production")
    def test_check_imports_production_model_changed(self):
        cache.set(LOCAL_RESPONSES_METADATA_KEY, "")

        response1 = {
            "responses": {
                "utter_lo_bla": [{"custom": {"default": "1"}, "channel": "metadata"}]
            }
        }
        encoded1 = json.dumps(response1).encode("utf-8")

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=response1)
            m.get(STATUS_URL, json=STATUS_RESPONSE)
            m.get(settings.LOCAL_YML_URL, text="")

            with patch("django.core.management.call_command") as mock_command:
                call_command("check_imports")

                commands_executed = mock_command.call_args_list

        self.assertEqual(len(commands_executed), 2)
        # self.assertEqual(len(commands_executed), 5)

        (
            # apply_staged_changes,
            import_prod_responses,
            import_prod_metadata,
        ) = commands_executed

        # self.assertIn("apply_staged_changes", apply_staged_changes[0])
        # self.assertEqual(len(apply_staged_changes[0]), 1)
        # self.assertEqual(len(apply_staged_changes[1]), 0)

        self.assertIn("import_default_responses", import_prod_responses[0])
        self.assertEqual(import_prod_responses[1]["stream"], encoded1)
        self.assertEqual(import_prod_responses[1]["override"], True)
        self.assertEqual(import_prod_responses[1].get("is_staged", False), False)

        self.assertIn("import_hyperlink_metadata", import_prod_metadata[0])
        self.assertEqual(import_prod_metadata[1]["stream"], b"")
        self.assertEqual(import_prod_metadata[1]["override"], True)
        self.assertEqual(import_prod_metadata[1].get("is_staged", False), False)

        # Regression https://gitlab.com/virtuele-gemeente-assistent/gem/-/issues/3665
        self.assertEqual(
            json.loads(cache.get(LOCAL_RESPONSES_METADATA_KEY).decode("utf-8")),
            {"utter_lo_bla": {"default": "1"}},
        )

    @override_settings(ENVIRONMENT="production")
    def test_check_imports_production_model_not_changed(self):
        cache.set(LOCAL_RESPONSES_METADATA_KEY, {})

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json={})
            m.get(STATUS_URL, json=STATUS_RESPONSE)
            m.get(settings.LOCAL_YML_URL, text="")

            with patch("django.core.management.call_command") as mock_command:
                call_command("check_imports")

                commands_executed = mock_command.call_args_list

        self.assertEqual(len(commands_executed), 0)
        self.assertEqual(cache.get(LOCAL_RESPONSES_METADATA_KEY), {})
