from unittest.mock import patch

from django.conf import settings
from django.core.cache import cache
from django.test import TestCase
from django.urls import reverse
from django.utils.translation import gettext as _

import requests_mock
from django_webtest import WebTest

from antwoorden_cms.config.tests.factories import OrganisationStorySettingsFactory

from ..constants import CENTRAL_RESPONSES_KEY, LOCAL_RESPONSES_METADATA_KEY
from ..models import SiteConfiguration
from ..utils import retrieve_stories, retrieve_story_categories
from .factories import GemeenteFactory, UtterResponseFactory
from .mixins import TwoFactorTestCaseMixin
from .utils import mock_retrieve_stories, mock_retrieve_story_categories

LOCAL_RESPONSES_DATA = {
    "utter_lo_product_verhuismelding_kind_inschrijven_inwonend": [{"text": "default1"}],
    "utter_lo_product_paspoort_kosten": [{"text": "default2"}],
    "utter_lo_product_id-kaart_kosten": [
        {
            "text": "default3",
            "buttons": [
                {"title": "Button1", "payload": "/button1"},
                {"title": "Button2", "payload": "/button2"},
            ],
        }
    ],
    "utter_lo_product_verhuismelding_kind_inschrijven_uitwonend_minderjarig": {
        "text": "verhuizen default one"
    },
    "utter_lo_product_verhuismelding_kind_inschrijven_uitwonend_meerderjarig": {
        "text": "verhuizen default two"
    },
}

RESPONSES_DATA = {
    "utter_la_product_rijbewijs_algemeen-q": [{"text": "landelijk antwoord1"}],
    "utter_la_overig_kindjongerdan18-q": [{"text": "landelijk antwoord2"}],
    **LOCAL_RESPONSES_DATA,
}

END2END_RESULTS_URL = settings.MEDIA_CONTAINER_URL + settings.STORIES_PATH


def mock_cache():
    cache.set(CENTRAL_RESPONSES_KEY, RESPONSES_DATA)
    cache.set(
        LOCAL_RESPONSES_METADATA_KEY,
        LOCAL_RESPONSES_DATA,
    )


class RetrieveStoryTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.gemeente = GemeenteFactory.create(name="dongen", slug="dongen")
        cls.default1 = UtterResponseFactory.create(
            utter="utter_lo_product_verhuismelding_kind_inschrijven_inwonend",
            response="default1",
        )
        cls.default2 = UtterResponseFactory.create(
            utter="utter_lo_product_paspoort_kosten",
            response="default2",
        )
        cls.default3 = UtterResponseFactory.create(
            utter="utter_lo_product_id-kaart_kosten",
            response="default3",
        )
        cls.override = UtterResponseFactory.create(
            utter="utter_lo_product_id-kaart_kosten",
            gemeente=cls.gemeente,
            response="override",
        )
        cls.verhuizen_default1 = UtterResponseFactory.create(
            utter="utter_lo_product_verhuismelding_kind_inschrijven_uitwonend_minderjarig",
            response="verhuizen default one",
        )
        cls.verhuizen_default2 = UtterResponseFactory.create(
            utter="utter_lo_product_verhuismelding_kind_inschrijven_uitwonend_meerderjarig",
            response="verhuizen default two",
        )

    def setUp(self):
        super().setUp()

        mocker = requests_mock.Mocker()
        mocker.start()
        self.addCleanup(mocker.stop)
        self.requests_mocker = mocker

        self.end2end_results_url = END2END_RESULTS_URL

    def mock_stories_with_local_files(self):

        categories = mock_retrieve_story_categories()
        self.requests_mocker.get(self.end2end_results_url, json=categories)

        for category in categories:
            self.requests_mocker.get(
                self.end2end_results_url + f"{category}.yml",
                json=mock_retrieve_stories(category),
            )

    def test_retrieve_story_categories_media_container(self):

        self.requests_mocker.get(
            self.end2end_results_url,
            json=[
                {
                    "name": "verhuizen.yml",
                    "type": "file",
                    "mtime": "Thu, 21 Apr 2022 12:18:29 GMT",
                    "size": 42383,
                },
                {
                    "name": "paspoort.yml",
                    "type": "file",
                    "mtime": "Thu, 21 Apr 2022 12:18:29 GMT",
                    "size": 42383,
                },
                {
                    "name": "_extra_test_file.yml",
                    "type": "file",
                    "mtime": "Thu, 21 Apr 2022 12:18:29 GMT",
                    "size": 42383,
                },
            ],
        )
        categories = retrieve_story_categories()

        self.assertEqual(categories, ["verhuizen", "paspoort"])

    @patch("antwoorden_cms.utters.utils.cache_utter_metadata", side_effect=mock_cache)
    def test_retrieve_stories_for_category(self, mock_metadata):

        self.mock_stories_with_local_files()

        stories = retrieve_stories("paspoort", self.gemeente)

        self.assertEqual(len(stories), 3)

        story1, story2, story3 = stories

        self.assertEqual(story1.story_name, "Rijbewijs algemeen")
        self.assertEqual(
            story1.user_messages,
            [
                "Ik wil informatie over een rijbewijs",
                "Ik heb een vraag over een rijbewijs",
                "Hulp nodig met rijbewijs",
            ],
        )
        self.assertEqual(
            story1.bot_messages,
            [
                {
                    "text": [{"string": "landelijk antwoord1"}],
                    "buttons": [
                        {
                            "title": "Eerste rijbewijs aanvragen",
                            "payload": '/aanvragen_hoe_1ekeer{"product":"rijbewijs"}',
                        },
                        {
                            "title": "Rijbewijs verlengen of categorie toevoegen",
                            "payload": '/verlengen_hoe{"product":"rijbewijs"}',
                        },
                        {
                            "title": "Gezondheidsverklaring",
                            "payload": '/aanvragen_hoe{"gezondheidsverklaring":"gezondheidsverklaring","product":"rijbewijs"}',
                        },
                        {"title": "Iets anders", "payload": "/inform_nee"},
                    ],
                }
            ],
        )

        self.assertEqual(
            story2.story_name,
            "Kosten - paspoort",
        )
        self.assertEqual(
            story2.user_messages,
            [
                "Wat kost een paspoort?",
                "Hoe duur is een paspoort?",
                "Hoeveel moet ik betalen voor een paspoort?",
            ],
        )
        self.assertEqual(
            story2.bot_messages,
            [{"text": [{"string": "default2", "instance": self.default2}]}],
        )

        self.assertEqual(
            story3.story_name,
            "Kosten - id-kaart",
        )
        self.assertEqual(
            story3.user_messages,
            [
                "Wat kost een id-kaart?",
                "Hoe duur is een id-kaart?",
                "Hoeveel moet ik betalen voor een id-kaart?",
            ],
        )
        self.assertEqual(
            story3.bot_messages,
            [{"text": [{"string": "override", "instance": self.override}]}],
        )

    @patch("antwoorden_cms.utters.utils.cache_utter_metadata", side_effect=mock_cache)
    def test_retrieve_stories_for_category_search_bot_message(self, mock_metadata):

        self.mock_stories_with_local_files()

        # Search on override
        stories = retrieve_stories("paspoort", self.gemeente, search_query="antwoord1")

        self.assertEqual(len(stories), 1)
        self.assertEqual(
            stories[0].story_name,
            "Rijbewijs algemeen",
        )

        # Search on default
        stories = retrieve_stories("paspoort", self.gemeente, search_query="default2")

        self.assertEqual(len(stories), 1)
        self.assertEqual(
            stories[0].story_name,
            "Kosten - paspoort",
        )

        # Search on default that was overridden
        stories = retrieve_stories("paspoort", self.gemeente, search_query="default3")

        self.assertEqual(len(stories), 0)

    @patch("antwoorden_cms.utters.utils.cache_utter_metadata", side_effect=mock_cache)
    def test_retrieve_stories_for_category_search_user_message(self, mock_metadata):

        self.mock_stories_with_local_files()

        stories = retrieve_stories("paspoort", self.gemeente, search_query="kost")

        self.assertEqual(len(stories), 2)
        self.assertEqual(
            stories[0].story_name,
            "Kosten - paspoort",
        )
        self.assertEqual(
            stories[1].story_name,
            "Kosten - id-kaart",
        )

    @patch("antwoorden_cms.utters.utils.cache_utter_metadata", side_effect=mock_cache)
    def test_retrieve_stories_with_chain(self, mock_metadata):
        self.mock_stories_with_local_files()

        stories = retrieve_stories("verhuizen", self.gemeente)

        self.assertEqual(len(stories), 1)
        self.assertEqual(
            stories[0].story_name,
            "Verhuizing doorgeven namens thuiswonend kind",
        )

        chains = stories[0].chains

        self.assertEqual(len(chains), 3)

        self.assertEqual(len(chains[0]["messages"]), 2)
        self.assertEqual(len(chains[1]["messages"]), 3)
        self.assertEqual(len(chains[2]["messages"]), 3)

        # first message should be the same in all
        first_message = {
            "user": [
                "Ik wil het adres van mijn oudste wijzigen",
                "Kan ik mijn kinderen bij mij inschrijven?",
                "verhuizing kind gezag",
            ],
            "bot": [{"text": [{"string": "Staat je kind nu bij jou ingeschreven?"}]}],
        }
        self.assertEqual(chains[0]["messages"][0], first_message)
        self.assertEqual(chains[1]["messages"][0], first_message)
        self.assertEqual(chains[1]["messages"][0], first_message)

        # Second message should be different between the first and second two chains
        second_message_ja = {
            "user": ["ja"],
            "bot": [{"text": [{"string": "default1", "instance": self.default1}]}],
        }
        self.assertEqual(chains[0]["messages"][1], second_message_ja)
        second_message_nee = {
            "user": ["nee"],
            "bot": [{"text": [{"string": "Is je kind jonger dan 18?"}]}],
        }
        self.assertEqual(chains[1]["messages"][1], second_message_nee)
        self.assertEqual(chains[2]["messages"][1], second_message_nee)

        # Second message should be different
        third_message_ja = {
            "user": ["ja"],
            "bot": [
                {
                    "text": [
                        {
                            "string": "verhuizen default one",
                            "instance": self.verhuizen_default1,
                        }
                    ]
                }
            ],
        }
        self.assertEqual(chains[1]["messages"][2], third_message_ja)
        third_message_nee = {
            "user": ["nee"],
            "bot": [
                {
                    "text": [
                        {
                            "string": "verhuizen default two",
                            "instance": self.verhuizen_default2,
                        }
                    ]
                }
            ],
        }
        self.assertEqual(chains[2]["messages"][2], third_message_nee)


class StoriesListWebTest(TwoFactorTestCaseMixin, WebTest):
    @classmethod
    def setUpTestData(cls):
        cls.gemeente = GemeenteFactory.create(name="dongen", slug="dongen")
        cls.default1 = UtterResponseFactory.create(
            utter="utter_lo_product_verhuismelding_kind_inschrijven_inwonend",
            response="default1",
        )
        cls.default2 = UtterResponseFactory.create(
            utter="utter_lo_product_paspoort_kosten",
            response="default2",
        )
        cls.default3 = UtterResponseFactory.create(
            utter="utter_lo_product_id-kaart_kosten",
            response="default3",
        )
        cls.override = UtterResponseFactory.create(
            utter="utter_lo_product_id-kaart_kosten",
            gemeente=cls.gemeente,
            response="override",
        )
        cls.verhuizen_default1 = UtterResponseFactory.create(
            utter="utter_lo_product_verhuismelding_kind_inschrijven_uitwonend_minderjarig",
            response="verhuizen default one",
        )
        cls.verhuizen_default2 = UtterResponseFactory.create(
            utter="utter_lo_product_verhuismelding_kind_inschrijven_uitwonend_meerderjarig",
            response="verhuizen default two",
        )

    def setUp(self):
        super().setUp()

        self.user.gemeentes.add(self.gemeente)

        # Should not be used, because different organisation
        OrganisationStorySettingsFactory.create(
            story_id="rijbewijs-algemeen",
            enabled=True,
            organisation=GemeenteFactory.create(name="amsterdam"),
        )

        OrganisationStorySettingsFactory.create(
            story_id="kosten-paspoort",
            enabled=True,
            organisation=self.gemeente,
        )
        OrganisationStorySettingsFactory.create(
            story_id="kosten-id-kaart",
            enabled=False,
            organisation=self.gemeente,
        )

        mocker = requests_mock.Mocker()
        mocker.start()
        self.addCleanup(mocker.stop)
        self.requests_mocker = mocker

        api_uri = reverse(
            "antwoorden_cms:story_settings_list",
            kwargs={"organisation": self.gemeente.slug},
        )
        host = self.app.extra_environ["HTTP_HOST"]
        api_url = f"http://{host}{api_uri}"

        self.requests_mocker.get(api_url, json=self.app.get(api_uri).json)

        verhuizen_category = "verhuizen"
        self.requests_mocker.get(
            f"{END2END_RESULTS_URL}{verhuizen_category}.yml",
            json=mock_retrieve_stories(verhuizen_category),
        )

        paspoort_category = "paspoort"
        self.requests_mocker.get(
            f"{END2END_RESULTS_URL}{paspoort_category}.yml",
            json=mock_retrieve_stories(paspoort_category),
        )

    @patch("antwoorden_cms.utters.utils.cache_utter_metadata", side_effect=mock_cache)
    def test_stories_list(self, mock_metadata):
        """
        Test stories list, verify that the appropriate texts are shown
        """
        url = reverse(
            "gemeentes:stories_list",
            kwargs={"gemeente_slug": self.gemeente.slug, "category": "paspoort"},
        )

        response = self.app.get(url, user=self.user)

        # Story toggling disabled by default
        enable_buttons = response.html("input", {"class": "toggle-story"})

        self.assertEqual(len(enable_buttons), 0)

        descriptions = response.html("h5", {"class": "story-description"})

        self.assertEqual(len(descriptions), 3)

        story_rows = response.html("div", {"class": "story-row"})

        self.assertEqual(len(story_rows), 3)

        story1, story2, story3 = story_rows

        # Story 1
        self.assertEqual(
            descriptions[0].text, "Beschrijving: Vraag over rijbewijs, bijvoorbeeld:"
        )
        user_messages = story1.find_all("ul", {"class": "story-user-messages"})
        examples1 = user_messages[0].find_all("li")
        self.assertEqual(examples1[0].text, "Ik wil informatie over een rijbewijs")
        self.assertEqual(examples1[1].text, "Ik heb een vraag over een rijbewijs")
        self.assertEqual(examples1[2].text, "Hulp nodig met rijbewijs")

        bot_messages = story1.find_all("div", {"class": "utter-text"})
        self.assertEqual(bot_messages[0].text, "landelijk antwoord1")

        buttons = story1.find_all("div", {"class": "widget-button"})
        self.assertEqual(len(buttons), 4)
        self.assertEqual(buttons[0].text, "Eerste rijbewijs aanvragen")
        self.assertEqual(buttons[1].text, "Rijbewijs verlengen of categorie toevoegen")
        self.assertEqual(buttons[2].text, "Gezondheidsverklaring")
        self.assertEqual(buttons[3].text, "Iets anders")

        # Story 2
        self.assertEqual(
            descriptions[1].text, "Beschrijving: Kosten paspoort, bijvoorbeeld:"
        )
        user_messages = story2.find_all("ul", {"class": "story-user-messages"})
        examples1 = user_messages[0].find_all("li")
        self.assertEqual(examples1[0].text, "Wat kost een paspoort?")
        self.assertEqual(examples1[1].text, "Hoe duur is een paspoort?")
        self.assertEqual(
            examples1[2].text, "Hoeveel moet ik betalen voor een paspoort?"
        )

        bot_messages = story2.find_all("div", {"class": "utter-text"})
        self.assertEqual(bot_messages[0].text, "default2")

        # Story 3
        self.assertEqual(
            descriptions[2].text, "Beschrijving: Kosten ID-kaart, bijvoorbeeld:"
        )
        user_messages = story3.find_all("ul", {"class": "story-user-messages"})
        examples1 = user_messages[0].find_all("li")
        self.assertEqual(examples1[0].text, "Wat kost een id-kaart?")
        self.assertEqual(examples1[1].text, "Hoe duur is een id-kaart?")
        self.assertEqual(
            examples1[2].text, "Hoeveel moet ik betalen voor een id-kaart?"
        )

        bot_messages = story3.find_all("div", {"class": "utter-text"})
        self.assertEqual(bot_messages[0].text, "override")

    @patch(
        "antwoorden_cms.utters.models.SiteConfiguration.get_solo",
        return_value=SiteConfiguration(
            story_toggling_enabled=True,
        ),
    )
    @patch("antwoorden_cms.utters.utils.cache_utter_metadata", side_effect=mock_cache)
    def test_stories_list_javascript_settings_correct(self, mock_metadata, mock_conf):
        """
        Test stories list, verify that the appropriate texts are shown
        """
        url = reverse(
            "gemeentes:stories_list",
            kwargs={"gemeente_slug": self.gemeente.slug, "category": "paspoort"},
        )

        response = self.app.get(url, user=self.user)

        enable_buttons = response.html("select", {"class": "toggle-story"})

        self.assertEqual(len(enable_buttons), 3)

        # By default disabled
        enable_buttons_attrs1 = enable_buttons[0].attrs
        selected_option1 = enable_buttons[0].find("option", {"selected": True})
        self.assertEqual(selected_option1.attrs["value"], "empty")
        self.assertEqual(enable_buttons_attrs1["data-story-id"], "rijbewijs-algemeen")
        self.assertNotIn("data-has-obj", enable_buttons_attrs1)

        enable_buttons_attrs2 = enable_buttons[1].attrs
        selected_option2 = enable_buttons[1].find("option", {"selected": True})
        self.assertEqual(selected_option2.attrs["value"], "enabled")
        self.assertEqual(enable_buttons_attrs2["data-story-id"], "kosten-paspoort")
        self.assertEqual(enable_buttons_attrs2["data-has-obj"], "true")

        enable_buttons_attrs3 = enable_buttons[2].attrs
        selected_option3 = enable_buttons[2].find("option", {"selected": True})
        self.assertEqual(selected_option3.attrs["value"], "disabled")
        self.assertEqual(enable_buttons_attrs3["data-story-id"], "kosten-id-kaart")
        self.assertEqual(enable_buttons_attrs3["data-has-obj"], "true")

        alt_titles_undefined = response.html(
            "div", {"class": "story-toggle-title-undefined"}
        )

        self.assertEqual(len(alt_titles_undefined), 3)
        self.assertEqual(
            alt_titles_undefined[0].attrs["class"], ["story-toggle-title-undefined"]
        )
        self.assertEqual(
            alt_titles_undefined[1].attrs["class"],
            ["story-toggle-title-undefined", "d-none"],
        )
        self.assertEqual(
            alt_titles_undefined[2].attrs["class"],
            ["story-toggle-title-undefined", "d-none"],
        )

    @patch("antwoorden_cms.utters.utils.cache_utter_metadata", side_effect=mock_cache)
    def test_stories_list_buttons(self, mock_metadata):
        """
        Test stories list, verify that the appropriate buttons for local utters are shown
        """
        url = reverse(
            "gemeentes:stories_list",
            kwargs={"gemeente_slug": self.gemeente.slug, "category": "paspoort"},
        )

        response = self.app.get(url, user=self.user)

        story_rows = response.html("div", {"class": "story-row"})

        self.assertEqual(len(story_rows), 3)

        buttons = story_rows[1].find_all("a")

        create_url = reverse(
            "gemeentes:antwoord_create", kwargs={"gemeente_slug": self.gemeente.slug}
        )

        self.assertEqual(len(buttons), 1)
        self.assertEqual(buttons[0].text.strip(), _("Lokale variant aanmaken"))
        self.assertEqual(
            buttons[0].attrs["href"],
            f"{create_url}?utter_name=utter_lo_product_paspoort_kosten",
        )

        buttons = story_rows[2].find_all("a")

        self.assertEqual(len(buttons), 2)
        self.assertEqual(buttons[0].text.strip(), _("Lokale variant aanpassen"))
        self.assertEqual(
            buttons[0].attrs["href"],
            reverse(
                "gemeentes:antwoord_update",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "antwoord_pk": self.override.pk,
                },
            ),
        )
        self.assertEqual(
            buttons[1].attrs["href"],
            reverse(
                "gemeentes:antwoord_delete",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "antwoord_pk": self.override.pk,
                },
            ),
        )

    @patch("antwoorden_cms.utters.utils.cache_utter_metadata", side_effect=mock_cache)
    def test_stories_list_chain(self, mock_metadata):
        url = reverse(
            "gemeentes:stories_list",
            kwargs={"gemeente_slug": self.gemeente.slug, "category": "verhuizen"},
        )

        response = self.app.get(url, user=self.user)
        story_rows = response.html("div", {"class": "story-row"})
        self.assertEqual(len(story_rows), 3)

        user_messages = [
            chain.find_all("ul", {"class": "story-user-messages"})
            for chain in story_rows
        ]
        bot_messages = [
            chain.find_all("div", {"class": "utter-text"}) for chain in story_rows
        ]

        # first message is same for all
        for i in range(3):
            with self.subTest(f"Chain {i} - first message"):

                examples1 = user_messages[i][0].find_all("li")
                self.assertEqual(
                    examples1[0].text, "Ik wil het adres van mijn oudste wijzigen"
                )
                self.assertEqual(
                    examples1[1].text, "Kan ik mijn kinderen bij mij inschrijven?"
                )
                self.assertEqual(examples1[2].text, "verhuizing kind gezag")

                self.assertEqual(
                    bot_messages[i][0].text, "Staat je kind nu bij jou ingeschreven?"
                )

        with self.subTest("Chain 1 - second message"):
            examples2 = user_messages[0][1].find_all("li")
            self.assertEqual(examples2[0].text, "ja")
            self.assertEqual(bot_messages[0][1].text, "default1")

        # second two are the same
        for i in range(1, 3):
            with self.subTest(f"Chain {i} - second message"):

                examples2 = user_messages[i][1].find_all("li")
                self.assertEqual(examples2[0].text, "nee")
                self.assertEqual(bot_messages[i][1].text, "Is je kind jonger dan 18?")

        with self.subTest("Chain 2 - third message"):

            examples3 = user_messages[1][2].find_all("li")
            self.assertEqual(examples3[0].text, "ja")
            self.assertEqual(bot_messages[1][2].text, "verhuizen default one")

        with self.subTest("Chain 3 - third message"):
            examples3 = user_messages[2][2].find_all("li")
            self.assertEqual(examples3[0].text, "nee")
            self.assertEqual(bot_messages[2][2].text, "verhuizen default two")
