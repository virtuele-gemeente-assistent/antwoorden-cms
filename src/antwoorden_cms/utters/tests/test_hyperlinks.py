from unittest import skip

from django.conf import settings
from django.test import TestCase, tag
from django.urls import reverse
from django.utils.translation import ugettext as _

import requests_mock
from django_webtest import WebTest

from ..models import Gemeente, Hyperlink, SiteConfiguration, UtterResponse
from .factories import (
    ChannelFactory,
    GemeenteFactory,
    HyperlinkFactory,
    UtterResponseFactory,
    VariableMetadataFactory,
)
from .mixins import TwoFactorTestCaseMixin
from .utils import grant_gemeente_perms

HEADERS = {"Content-Type": "application/json"}

LOCAL_UTTER_RESPONSES = {
    "responses": {
        "utter_lo_1": [
            {"text": "landelijke default"},
            {
                "channel": "metadata",
                "custom": {
                    "default": [
                        {
                            "text": "text1",
                        }
                    ],
                    "metadata": {
                        "required": False,
                        "questions": ["question1", "question2"],
                        "category": "verhuismelding",
                        "examples": ["example1"],
                    },
                },
            },
        ],
        "utter_lo_2": [
            {"text": "landelijke default"},
            {
                "channel": "metadata",
                "custom": {
                    "default": [
                        {
                            "text": "text2",
                        }
                    ],
                    "metadata": {
                        "required": False,
                        "questions": ["question1", "question2"],
                        "category": "verhuismelding",
                        "examples": ["example1", "example2"],
                    },
                },
            },
        ],
        "utter_lo_3": [
            {"text": "landelijke default"},
            {
                "channel": "metadata",
                "custom": {
                    "default": [
                        {
                            "text": "text3",
                        }
                    ],
                    "metadata": {
                        "required": True,
                        "category": "overig",
                        "examples": ["example2"],
                    },
                },
            },
        ],
        "utter_lo_4": [
            {"text": "landelijke default"},
            {
                "channel": "metadata",
                "custom": {
                    "default": [
                        {
                            "text": "text4",
                        }
                    ],
                    "metadata": {"required": True},
                },
            },
        ],
    }
}

DOMAIN_URL = settings.DOMAIN_URL


class HyperlinkTests(TestCase):
    def test_format_invalid_hyperlink_return_same_response(self):
        response = "[bla](missing_attribute)"
        gemeente = GemeenteFactory(name="Dongen")
        formatted = UtterResponse.format_hyperlinks(response, gemeente)
        self.assertEqual(formatted, response)

    def test_hyperlink_name_allow_digits(self):
        gemeente = GemeenteFactory(name="Dongen")
        HyperlinkFactory.create(
            name="some_link2", url="https://example.com", gemeente=gemeente
        )
        response = "[bla](<some_link2.url>)"
        formatted = UtterResponse.format_hyperlinks(response, gemeente)
        self.assertEqual(formatted, "[bla](https://example.com)")

    def test_hyperlink_name_allow_dash(self):
        gemeente = GemeenteFactory(name="Dongen")
        HyperlinkFactory.create(
            name="some-link", url="https://example.com", gemeente=gemeente
        )
        response = "[bla](<some-link.url>)"
        formatted = UtterResponse.format_hyperlinks(response, gemeente)
        self.assertEqual(formatted, "[bla](https://example.com)")

    def test_hyperlink_duplicates(self):
        gemeente = GemeenteFactory(name="Dongen")
        HyperlinkFactory.create(name="some-link", url="duplicate", gemeente=gemeente)
        HyperlinkFactory.create(
            name="some-link", url="https://example.com", gemeente=gemeente
        )
        response = "[bla](<some-link.url>)"
        formatted = UtterResponse.format_hyperlinks(response, gemeente)
        self.assertEqual(formatted, "[bla](https://example.com)")

    def test_hyperlink_bad_attribute(self):
        gemeente = GemeenteFactory(name="Dongen")
        HyperlinkFactory.create(
            name="some-link", url="https://example.com", gemeente=gemeente
        )
        response = "[bla](<some-link.urt>)"
        formatted = UtterResponse.format_hyperlinks(response, gemeente)
        self.assertEqual(formatted, "[bla](some-link.urt)")


class HyperlinkListTests(TwoFactorTestCaseMixin, WebTest):
    login_url = reverse("two_factor:login")

    def setUp(self):
        super().setUp()

        self.gemeente = GemeenteFactory(name="Amsterdam")
        self.user.gemeentes.add(self.gemeente)

        self.hyperlink_list_url = reverse(
            "gemeentes:hyperlink_list", kwargs={"gemeente_slug": self.gemeente.slug}
        )

    def test_hyperlink_list(self):
        hyperlink1 = HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )
        hyperlink2 = HyperlinkFactory(
            gemeente=self.gemeente, name="some_link2", url="url", label="label"
        )
        VariableMetadataFactory.create(name="some_link", verbose_name="Some Link")
        VariableMetadataFactory.create(name="some_link2", verbose_name="Some Link 2")

        response = self.app.get(self.hyperlink_list_url, user=self.user)

        colnames = response.html("th")
        self.assertIn("Naam", colnames[0].text)
        self.assertIn("Zichtbare tekst (label)", colnames[1].text)
        self.assertIn("Internetadres (url)", colnames[2].text)

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 2)
        for i, hyperlink in enumerate([hyperlink1, hyperlink2]):
            with self.subTest(hyperlink=hyperlink):
                cells = rows[i].find_all("td")
                if i == 0:
                    self.assertEqual(cells[0].text, "Some Link")
                    self.assertEqual(cells[1].text, "label")
                    self.assertEqual(cells[2].text, "url")
                elif i == 1:
                    self.assertEqual(cells[0].text, "Some Link 2")
                    self.assertEqual(cells[1].text, "label")
                    self.assertEqual(cells[2].text, "url")

                delete_url = reverse(
                    "gemeentes:hyperlink_delete",
                    kwargs={
                        "gemeente_slug": self.gemeente.slug,
                        "hyperlink_pk": hyperlink.pk,
                    },
                )
                update_url = reverse(
                    "gemeentes:hyperlink_update",
                    kwargs={
                        "gemeente_slug": self.gemeente.slug,
                        "hyperlink_pk": hyperlink.pk,
                    },
                )
                delete_button, update_button = rows[i].find_all("a")
                self.assertEqual(delete_url, delete_button.get("href"))
                self.assertEqual(update_url, update_button.get("href"))

    def test_hyperlink_list_show_unused_hyperlink(self):
        VariableMetadataFactory.create(
            name="some_unused_link", verbose_name="Some unused Link"
        )

        response = self.app.get(self.hyperlink_list_url, user=self.user)

        colnames = response.html("th")
        self.assertIn("Naam", colnames[0].text)
        self.assertIn("Zichtbare tekst (label)", colnames[1].text)
        self.assertIn("Internetadres (url)", colnames[2].text)

        rows = response.html("tr")[1:]

        # Unused hyperlinks should also show up
        self.assertEqual(len(rows), 1)

        cells = rows[0].find_all("td")
        self.assertEqual(cells[0].text, "Some unused Link")
        self.assertEqual(cells[1].text, "")
        self.assertEqual(cells[2].text, "")

        create_url = reverse(
            "gemeentes:hyperlink_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        create_button = rows[0].find("a")
        self.assertEqual(
            f"{create_url}?name=some_unused_link", create_button.get("href")
        )

    def test_hyperlink_list_duplicate_hyperlink_for_municipality(self):
        """
        Regression test for: https://gitlab.com/virtuele-gemeente-assistent/gem/-/issues/1282
        """
        HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )
        hyperlink2 = HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url2", label="label2"
        )
        VariableMetadataFactory.create(name="some_link", verbose_name="Some Link")

        response = self.app.get(self.hyperlink_list_url, user=self.user)

        rows = response.html("tr")[1:]

        self.assertEqual(len(rows), 1)

        delete_url = reverse(
            "gemeentes:hyperlink_delete",
            kwargs={"gemeente_slug": self.gemeente.slug, "hyperlink_pk": hyperlink2.pk},
        )
        update_url = reverse(
            "gemeentes:hyperlink_update",
            kwargs={"gemeente_slug": self.gemeente.slug, "hyperlink_pk": hyperlink2.pk},
        )
        delete_button, update_button = rows[0].find_all("a")
        self.assertEqual(delete_url, delete_button.get("href"))
        self.assertEqual(update_url, update_button.get("href"))

    def test_hyperlink_list_breadcrumb_category(self):
        HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )
        VariableMetadataFactory.create(
            name="some_link", verbose_name="Some Link", category="something"
        )

        response = self.app.get(
            f"{self.hyperlink_list_url}?category=something", user=self.user
        )

        breadcrumbs = response.html("li", {"class": "breadcrumb-item"})

        self.assertEqual(breadcrumbs[1].text.strip(), "Something")

    def test_hyperlink_list_update_and_create(self):
        site_config = SiteConfiguration.get_solo()
        site_config.hyperlinks_show_only_used_attributes = False
        site_config.save()

        hyperlink1 = HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )
        UtterResponseFactory.create(utter="utter_lo_bla", response="<some_link2.url>")
        VariableMetadataFactory.create(name="some_link", verbose_name="Some Link")
        VariableMetadataFactory.create(name="some_link2", verbose_name="Some Link 2")

        response = self.app.get(self.hyperlink_list_url, user=self.user)

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 2)

        delete_url = reverse(
            "gemeentes:hyperlink_delete",
            kwargs={"gemeente_slug": self.gemeente.slug, "hyperlink_pk": hyperlink1.pk},
        )
        update_url = reverse(
            "gemeentes:hyperlink_update",
            kwargs={"gemeente_slug": self.gemeente.slug, "hyperlink_pk": hyperlink1.pk},
        )
        delete_button, update_button = rows[0].find_all("a")
        self.assertEqual(delete_url, delete_button.get("href"))
        self.assertEqual(update_url, update_button.get("href"))

        # Check if there is a create button with the correct query params
        create_url = reverse(
            "gemeentes:hyperlink_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        query_string = "?name=some_link2"

        create_button = rows[1].find_all("a")[0]
        self.assertEqual(f"{create_url}{query_string}", create_button.get("href"))

    @skip("Feature not supported anymore")
    def test_hyperlink_list_update_and_create_hyperlink_dynamic_attrs(self):
        site_config = SiteConfiguration.get_solo()
        site_config.hyperlinks_show_only_used_attributes = True
        site_config.save()

        hyperlink1 = HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )
        UtterResponseFactory.create(utter="utter_lo_bla", response="<some_link2.url>")
        VariableMetadataFactory.create(name="some_link", verbose_name="Some Link")
        VariableMetadataFactory.create(name="some_link2", verbose_name="Some Link 2")

        response = self.app.get(self.hyperlink_list_url, user=self.user)

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 2)

        delete_url = reverse(
            "gemeentes:hyperlink_delete",
            kwargs={"gemeente_slug": self.gemeente.slug, "hyperlink_pk": hyperlink1.pk},
        )
        update_url = reverse(
            "gemeentes:hyperlink_update",
            kwargs={"gemeente_slug": self.gemeente.slug, "hyperlink_pk": hyperlink1.pk},
        )
        delete_button, update_button = rows[0].find_all("a")
        self.assertEqual(delete_url, delete_button.get("href"))
        self.assertEqual(update_url, update_button.get("href"))

        # Check if there is a create button with the correct query params
        create_url = reverse(
            "gemeentes:hyperlink_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        query_string = "?name=some_link2&required=url"

        create_button = rows[1].find_all("a")[0]
        self.assertEqual(f"{create_url}{query_string}", create_button.get("href"))

    def test_hyperlink_list_filter_category(self):
        hyperlink1 = HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )
        UtterResponseFactory.create(utter="utter_lo_bla", response="<some_link2.url>")
        VariableMetadataFactory.create(
            name="some_link", verbose_name="Some Link", category="verhuizen"
        )
        VariableMetadataFactory.create(
            name="some_link2", verbose_name="Some Link 2", category="paspoort"
        )

        response = self.app.get(
            f"{self.hyperlink_list_url}?category=verhuizen", user=self.user
        )

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 1)

        delete_url = reverse(
            "gemeentes:hyperlink_delete",
            kwargs={"gemeente_slug": self.gemeente.slug, "hyperlink_pk": hyperlink1.pk},
        )
        update_url = reverse(
            "gemeentes:hyperlink_update",
            kwargs={"gemeente_slug": self.gemeente.slug, "hyperlink_pk": hyperlink1.pk},
        )
        delete_button, update_button = rows[0].find_all("a")
        self.assertEqual(delete_url, delete_button.get("href"))
        self.assertEqual(update_url, update_button.get("href"))

    def test_hyperlink_list_login_required(self):
        response = self.app.get(self.hyperlink_list_url)
        self.assertEqual(response.status_code, 302)
        self.assertIn(self.login_url, response.location)

    def test_hyperlink_list_editable_required(self):
        site_config = SiteConfiguration.get_solo()
        site_config.hyperlinks_show_only_used_attributes = False
        site_config.save()

        gemeente2 = GemeenteFactory.create(name="Tilburg")
        HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )
        HyperlinkFactory(
            gemeente=gemeente2, name="some_other_link", url="url", label="label"
        )
        UtterResponseFactory.create(
            utter="utter_lo_1",
            response="[<some_link.label>](<some_link.url>)",
            gemeente=self.gemeente,
        )
        UtterResponseFactory.create(
            utter="utter_lo_2",
            response="[<some_other_link.label>](<some_other_link.url>)",
            gemeente=self.gemeente,
        )
        VariableMetadataFactory.create(
            name="some_link",
            verbose_name="Some Link",
        )
        VariableMetadataFactory.create(
            name="some_other_link",
            verbose_name="Some Other Link",
        )

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                reverse(
                    "gemeentes:hyperlink_list_editable",
                    kwargs={"gemeente_slug": self.gemeente.slug},
                ),
                user=self.user,
            )

        rows = response.html("tr")[1:]

        # some_link should not show up, because it already exists for current gemeente
        self.assertEqual(len(rows), 1)

        self.assertEqual(rows[0].find_all("td")[0].text, "Some Other Link")

        # Check if there is a create button with the correct query params
        create_url = reverse(
            "gemeentes:hyperlink_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        query_string = "?name=some_other_link"

        create_button = rows[0].find_all("a")[0]
        self.assertEqual(f"{create_url}{query_string}", create_button.get("href"))

    def test_hyperlink_list_editable_required_show_unused_hyperlinks(self):
        site_config = SiteConfiguration.get_solo()
        site_config.hyperlinks_show_only_used_attributes = False
        site_config.save()

        gemeente2 = GemeenteFactory.create(name="Tilburg")
        HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )
        HyperlinkFactory(
            gemeente=gemeente2, name="some_other_link", url="url", label="label"
        )
        UtterResponseFactory.create(
            utter="utter_lo_1",
            response="[<some_link.label>](<some_link.url>)",
            gemeente=self.gemeente,
        )
        UtterResponseFactory.create(
            utter="utter_lo_2",
            response="[<some_other_link.label>](<some_other_link.url>)",
            gemeente=self.gemeente,
        )
        VariableMetadataFactory.create(
            name="some_unused_link",
            verbose_name="Some unused Link",
        )

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                reverse(
                    "gemeentes:hyperlink_list_editable",
                    kwargs={"gemeente_slug": self.gemeente.slug},
                ),
                user=self.user,
            )

        rows = response.html("tr")[1:]

        # some_unused_link should show up, because metadata is defined for it
        self.assertEqual(len(rows), 1)

        self.assertEqual(rows[0].find_all("td")[0].text, "Some unused Link")

        # Check if there is a create button with the correct query params
        create_url = reverse(
            "gemeentes:hyperlink_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        query_string = "?name=some_unused_link"

        create_button = rows[0].find_all("a")[0]
        self.assertEqual(f"{create_url}{query_string}", create_button.get("href"))

    @skip("Feature not supported anymore")
    def test_hyperlink_list_editable_required_dynamic_hyperlink_attrs(self):
        site_config = SiteConfiguration.get_solo()
        site_config.hyperlinks_show_only_used_attributes = True
        site_config.save()

        gemeente2 = GemeenteFactory.create(name="Tilburg")
        HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )
        HyperlinkFactory(
            gemeente=gemeente2, name="some_other_link", url="url", label="label"
        )
        UtterResponseFactory.create(
            utter="utter_lo_1",
            response="[<some_link.label>](<some_link.url>)",
            gemeente=self.gemeente,
        )
        UtterResponseFactory.create(
            utter="utter_lo_2",
            response="[<some_other_link.label>](<some_other_link.url>)",
            gemeente=self.gemeente,
        )
        VariableMetadataFactory.create(
            name="some_link",
            verbose_name="Some Link",
        )
        VariableMetadataFactory.create(
            name="some_other_link",
            verbose_name="Some Other Link",
        )

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                reverse(
                    "gemeentes:hyperlink_list_editable",
                    kwargs={"gemeente_slug": self.gemeente.slug},
                ),
                user=self.user,
            )

        rows = response.html("tr")[1:]

        # some_link should not show up, because it already exists for current gemeente
        self.assertEqual(len(rows), 1)

        self.assertEqual(rows[0].find_all("td")[0].text, "Some Other Link")

        # Check if there is a create button with the correct query params
        create_url = reverse(
            "gemeentes:hyperlink_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        query_string = "?name=some_other_link&required=label&required=url"

        create_button = rows[0].find_all("a")[0]
        self.assertEqual(f"{create_url}{query_string}", create_button.get("href"))

    def test_hyperlink_list_editable_required_default(self):
        site_config = SiteConfiguration.get_solo()
        site_config.hyperlinks_show_only_used_attributes = False
        site_config.save()

        GemeenteFactory.create(name="Tilburg")
        UtterResponseFactory.create(
            utter="utter_lo_1",
            response="[<some_link.label>](<some_link.url>)",
            gemeente=None,
        )
        UtterResponseFactory.create(
            utter="utter_lo_2",
            response="[<some_other_link.label>](<some_other_link.url>)",
            gemeente=self.gemeente,
        )
        VariableMetadataFactory.create(
            name="some_link",
            verbose_name="Some Link",
        )
        VariableMetadataFactory.create(
            name="some_other_link",
            verbose_name="Some Other Link",
        )

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                reverse(
                    "gemeentes:hyperlink_list_editable",
                    kwargs={"gemeente_slug": self.gemeente.slug},
                ),
                user=self.user,
            )

        rows = response.html("tr")[1:]

        # Both links should show up
        self.assertEqual(len(rows), 2)

        # Hyperlink 1
        self.assertEqual(rows[0].find_all("td")[0].text, "Some Link")

        # Check if there is a create button with the correct query params
        create_url = reverse(
            "gemeentes:hyperlink_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        query_string = "?name=some_link"

        create_button = rows[0].find_all("a")[0]
        self.assertEqual(f"{create_url}{query_string}", create_button.get("href"))

        # Hyperlink 2
        self.assertEqual(rows[1].find_all("td")[0].text, "Some Other Link")

        # Check if there is a create button with the correct query params
        create_url = reverse(
            "gemeentes:hyperlink_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        query_string = "?name=some_other_link"

        create_button = rows[1].find_all("a")[0]
        self.assertEqual(f"{create_url}{query_string}", create_button.get("href"))

    @skip("Feature not supported anymore")
    def test_hyperlink_list_editable_required_default_dynamic_hyperlink_attrs(self):
        site_config = SiteConfiguration.get_solo()
        site_config.hyperlinks_show_only_used_attributes = True
        site_config.save()

        GemeenteFactory.create(name="Tilburg")
        UtterResponseFactory.create(
            utter="utter_lo_1",
            response="[<some_link.label>](<some_link.url>)",
            gemeente=None,
        )
        UtterResponseFactory.create(
            utter="utter_lo_2",
            response="[<some_other_link.label>](<some_other_link.url>)",
            gemeente=self.gemeente,
        )
        VariableMetadataFactory.create(
            name="some_link",
            verbose_name="Some Link",
        )
        VariableMetadataFactory.create(
            name="some_other_link",
            verbose_name="Some Other Link",
        )

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                reverse(
                    "gemeentes:hyperlink_list_editable",
                    kwargs={"gemeente_slug": self.gemeente.slug},
                ),
                user=self.user,
            )

        rows = response.html("tr")[1:]

        # Both links should show up
        self.assertEqual(len(rows), 2)

        # Hyperlink 1
        self.assertEqual(rows[0].find_all("td")[0].text, "Some Link")

        # Check if there is a create button with the correct query params
        create_url = reverse(
            "gemeentes:hyperlink_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        query_string = "?name=some_link&required=label&required=url"

        create_button = rows[0].find_all("a")[0]
        self.assertEqual(f"{create_url}{query_string}", create_button.get("href"))

        # Hyperlink 2
        self.assertEqual(rows[1].find_all("td")[0].text, "Some Other Link")

        # Check if there is a create button with the correct query params
        create_url = reverse(
            "gemeentes:hyperlink_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        query_string = "?name=some_other_link&required=label&required=url"

        create_button = rows[1].find_all("a")[0]
        self.assertEqual(f"{create_url}{query_string}", create_button.get("href"))

    def test_hyperlink_list_editable_required_default_also_required_when_override_exists(
        self,
    ):
        site_config = SiteConfiguration.get_solo()
        site_config.hyperlinks_show_only_used_attributes = False
        site_config.save()

        GemeenteFactory.create(name="Tilburg")
        UtterResponseFactory.create(
            utter="utter_lo_1",
            response="[<some_link.label>](<some_link.url>)",
            gemeente=None,
        )
        UtterResponseFactory.create(
            utter="utter_lo_2",
            response="[<some_other_link.label>](<some_other_link.url>)",
            gemeente=self.gemeente,
        )
        UtterResponseFactory.create(
            utter="utter_lo_1",
            response="[<some_link2.label>](<some_link2.url>)",
            gemeente=self.gemeente,
        )
        VariableMetadataFactory.create(
            name="some_link",
            verbose_name="Some Link",
        )
        VariableMetadataFactory.create(
            name="some_other_link",
            verbose_name="Some Other Link",
        )
        VariableMetadataFactory.create(
            name="some_link2",
            verbose_name="Some Link2",
        )

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                reverse(
                    "gemeentes:hyperlink_list_editable",
                    kwargs={"gemeente_slug": self.gemeente.slug},
                ),
                user=self.user,
            )

        rows = response.html("tr")[1:]

        # Both links should show up
        self.assertEqual(len(rows), 3)

        # Hyperlink 1
        self.assertEqual(rows[0].find_all("td")[0].text, "Some Link")

        # Check if there is a create button with the correct query params
        create_url = reverse(
            "gemeentes:hyperlink_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        query_string = "?name=some_link"

        create_button = rows[0].find_all("a")[0]
        self.assertEqual(f"{create_url}{query_string}", create_button.get("href"))

        # Hyperlink 2
        self.assertEqual(rows[1].find_all("td")[0].text, "Some Other Link")

        # Check if there is a create button with the correct query params
        create_url = reverse(
            "gemeentes:hyperlink_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        query_string = "?name=some_other_link"

        create_button = rows[1].find_all("a")[0]
        self.assertEqual(f"{create_url}{query_string}", create_button.get("href"))

        # Hyperlink 3
        self.assertEqual(rows[2].find_all("td")[0].text, "Some Link2")

        # Check if there is a create button with the correct query params
        create_url = reverse(
            "gemeentes:hyperlink_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        query_string = "?name=some_link2"

        create_button = rows[2].find_all("a")[0]
        self.assertEqual(f"{create_url}{query_string}", create_button.get("href"))

    @skip("Feature not supported anymore")
    def test_hyperlink_list_editable_required_default_also_required_when_override_exists_dynamic_hyperlink_attrs(
        self,
    ):
        site_config = SiteConfiguration.get_solo()
        site_config.hyperlinks_show_only_used_attributes = True
        site_config.save()

        GemeenteFactory.create(name="Tilburg")
        UtterResponseFactory.create(
            utter="utter_lo_1",
            response="[<some_link.label>](<some_link.url>)",
            gemeente=None,
        )
        UtterResponseFactory.create(
            utter="utter_lo_2",
            response="[<some_other_link.label>](<some_other_link.url>)",
            gemeente=self.gemeente,
        )
        UtterResponseFactory.create(
            utter="utter_lo_1",
            response="[<some_link2.label>](<some_link2.url>)",
            gemeente=self.gemeente,
        )
        VariableMetadataFactory.create(
            name="some_link",
            verbose_name="Some Link",
        )
        VariableMetadataFactory.create(
            name="some_other_link",
            verbose_name="Some Other Link",
        )
        VariableMetadataFactory.create(
            name="some_link2",
            verbose_name="Some Link2",
        )

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                reverse(
                    "gemeentes:hyperlink_list_editable",
                    kwargs={"gemeente_slug": self.gemeente.slug},
                ),
                user=self.user,
            )

        rows = response.html("tr")[1:]

        # Both links should show up
        self.assertEqual(len(rows), 3)

        # Hyperlink 1
        self.assertEqual(rows[0].find_all("td")[0].text, "Some Link")

        # Check if there is a create button with the correct query params
        create_url = reverse(
            "gemeentes:hyperlink_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        query_string = "?name=some_link&required=label&required=url"

        create_button = rows[0].find_all("a")[0]
        self.assertEqual(f"{create_url}{query_string}", create_button.get("href"))

        # Hyperlink 2
        self.assertEqual(rows[1].find_all("td")[0].text, "Some Other Link")

        # Check if there is a create button with the correct query params
        create_url = reverse(
            "gemeentes:hyperlink_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        query_string = "?name=some_other_link&required=label&required=url"

        create_button = rows[1].find_all("a")[0]
        self.assertEqual(f"{create_url}{query_string}", create_button.get("href"))

        # Hyperlink 3
        self.assertEqual(rows[2].find_all("td")[0].text, "Some Link2")

        # Check if there is a create button with the correct query params
        create_url = reverse(
            "gemeentes:hyperlink_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        query_string = "?name=some_link2&required=label&required=url"

        create_button = rows[2].find_all("a")[0]
        self.assertEqual(f"{create_url}{query_string}", create_button.get("href"))

    @skip("Editable hyperlink list view has is deprecated")
    def test_hyperlink_list_editable_with_defaults(self):
        gemeente2 = GemeenteFactory.create(name="Tilburg")
        hyperlink1 = HyperlinkFactory(
            gemeente=gemeente2, name="some_link", url="url", label="label"
        )
        hyperlink2 = HyperlinkFactory(
            gemeente=gemeente2, name="some_other_link", url="url", label="label"
        )
        UtterResponseFactory.create(
            utter="utter_lo_1",
            response="[<some_link.label>](<some_link.url>)",
            gemeente=None,
        )
        UtterResponseFactory.create(
            utter="utter_lo_2",
            response="[<some_other_link.label>](<some_other_link.url>)",
            gemeente=self.gemeente,
        )

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                reverse(
                    "gemeentes:hyperlink_list_editable",
                    kwargs={"gemeente_slug": self.gemeente.slug},
                ),
                user=self.user,
            )

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 2)

        for i, hyperlink in enumerate([hyperlink1, hyperlink2]):
            with self.subTest(hyperlink=hyperlink):
                # Check if there is a create button with the correct query params
                create_url = reverse(
                    "gemeentes:hyperlink_create",
                    kwargs={
                        "gemeente_slug": self.gemeente.slug,
                    },
                )
                query_string = f"?name={hyperlink.name}&required=label&required=url"

                create_button = rows[i].find_all("a")[0]
                self.assertEqual(
                    f"{create_url}{query_string}", create_button.get("href")
                )

    @skip("Editable hyperlink list view has is deprecated")
    def test_hyperlink_list_editable_login_required(self):
        response = self.app.get(
            reverse(
                "gemeentes:hyperlink_list_editable",
                kwargs={"gemeente_slug": self.gemeente.slug},
            ),
        )
        self.assertEqual(response.status_code, 302)
        self.assertIn(self.login_url, response.location)

    def test_hyperlink_list_search_verbose_name(self):

        VariableMetadataFactory.create(
            name="some_link", verbose_name="target one", category="verhuizen"
        )
        HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url one", label="label one"
        )
        VariableMetadataFactory.create(
            name="some_link2", verbose_name="target two", category="verhuizen"
        )
        VariableMetadataFactory.create(
            name="some_link3", verbose_name="miss three", category="verhuizen"
        )

        response = self.app.get(
            self.hyperlink_list_url, {"search": "target"}, user=self.user
        )

        rows = response.html("tr")[1:]

        self.assertEqual(len(rows), 2)

        # should include hyperlink without match
        self.assertEqual(rows[0].find_all("td")[0].text, "target one")
        self.assertEqual(rows[0].find_all("td")[1].text, "label one")
        self.assertEqual(rows[0].find_all("td")[2].text, "url one")

        self.assertEqual(rows[1].find_all("td")[0].text, "target two")
        self.assertEqual(rows[1].find_all("td")[1].text, "")
        self.assertEqual(rows[1].find_all("td")[2].text, "")

    def test_hyperlink_list_search_label(self):

        VariableMetadataFactory.create(
            name="some_link", verbose_name="Link one", category="verhuizen"
        )
        HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url one", label="target one"
        )
        VariableMetadataFactory.create(
            name="some_link2", verbose_name="Link two", category="verhuizen"
        )
        HyperlinkFactory(
            gemeente=self.gemeente, name="some_link2", url="url two", label="label two"
        )

        response = self.app.get(
            self.hyperlink_list_url, {"search": "target"}, user=self.user
        )

        rows = response.html("tr")[1:]

        self.assertEqual(len(rows), 1)

        self.assertEqual(rows[0].find_all("td")[0].text, "Link one")
        self.assertEqual(rows[0].find_all("td")[1].text, "target one")
        self.assertEqual(rows[0].find_all("td")[2].text, "url one")

    def test_hyperlink_list_search_url(self):

        VariableMetadataFactory.create(
            name="some_link", verbose_name="Link one", category="verhuizen"
        )
        HyperlinkFactory(
            gemeente=self.gemeente,
            name="some_link",
            url="www.target_url.nl",
            label="label one",
        )
        VariableMetadataFactory.create(
            name="some_link2", verbose_name="Link two", category="verhuizen"
        )
        HyperlinkFactory(
            gemeente=self.gemeente,
            name="some_link2",
            url="www.other_url.nl",
            label="label two",
        )

        response = self.app.get(
            self.hyperlink_list_url, {"search": "target"}, user=self.user
        )

        rows = response.html("tr")[1:]
        self.assertEqual(len(rows), 1)

        self.assertEqual(rows[0].find_all("td")[0].text, "Link one")
        self.assertEqual(rows[0].find_all("td")[1].text, "label one")
        self.assertEqual(rows[0].find_all("td")[2].text, "www.target_url.nl")

    def test_hyperlink_list_search_mix(self):
        VariableMetadataFactory.create(
            name="some_link", verbose_name="Link one", category="verhuizen"
        )
        HyperlinkFactory(
            gemeente=self.gemeente,
            name="some_link",
            url="www.tarGET_url.nl",
            label="label one",
        )
        VariableMetadataFactory.create(
            name="some_link2", verbose_name="Link two", category="verhuizen"
        )
        HyperlinkFactory(
            gemeente=self.gemeente,
            name="some_link2",
            url="www.other_url.nl",
            label="tArGet two",
        )
        VariableMetadataFactory.create(
            name="some_link3", verbose_name="Target Three", category="verhuizen"
        )

        response = self.app.get(
            self.hyperlink_list_url, {"search": "target"}, user=self.user
        )

        rows = response.html("tr")[1:]
        self.assertEqual(len(rows), 3)

        self.assertEqual(rows[0].find_all("td")[0].text, "Link one")
        self.assertEqual(rows[0].find_all("td")[1].text, "label one")
        self.assertEqual(rows[0].find_all("td")[2].text, "www.tarGET_url.nl")

        self.assertEqual(rows[1].find_all("td")[0].text, "Link two")
        self.assertEqual(rows[1].find_all("td")[1].text, "tArGet two")
        self.assertEqual(rows[1].find_all("td")[2].text, "www.other_url.nl")

        self.assertEqual(rows[2].find_all("td")[0].text, "Target Three")
        self.assertEqual(rows[2].find_all("td")[1].text, "")
        self.assertEqual(rows[2].find_all("td")[2].text, "")

    def test_hyperlink_list_search_odd_characters(self):
        """
        Regression test with examples that use colon :  and backslash / in hyperlink model
        """
        VariableMetadataFactory.create(
            name="some_link", verbose_name="Weird Example", category="verhuizen"
        )
        HyperlinkFactory(
            gemeente=self.gemeente,
            name="some_link",
            url="https://example.com",
            label="uur 14:00 one",
        )

        response = self.app.get(
            self.hyperlink_list_url, {"search": "14:00"}, user=self.user
        )

        self.assertEqual(len(response.html("tr")[1:]), 1)

        response = self.app.get(
            self.hyperlink_list_url, {"search": "https://example.com"}, user=self.user
        )
        self.assertEqual(len(response.html("tr")[1:]), 1)


class HyperlinkListSortingTests(TwoFactorTestCaseMixin, WebTest):
    login_url = reverse("two_factor:login")

    def setUp(self):
        super().setUp()

        self.gemeente = GemeenteFactory(name="Amsterdam")
        self.user.gemeentes.add(self.gemeente)

        self.hyperlink_list_url = reverse(
            "gemeentes:hyperlink_list", kwargs={"gemeente_slug": self.gemeente.slug}
        )

    def test_hyperlink_list_default_sort_by_name(self):
        HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )
        UtterResponseFactory.create(utter="utter_lo_bla", response="<some_link2.url>")
        VariableMetadataFactory.create(
            name="some_link", verbose_name="Some Link", category="verhuizen"
        )
        VariableMetadataFactory.create(
            name="some_link2", verbose_name="Some Link 2", category="paspoort"
        )

        response = self.app.get(f"{self.hyperlink_list_url}", user=self.user)

        rows = response.html("tr")[1:]

        self.assertEqual(len(rows), 2)

        self.assertEqual(rows[0].find_all("td")[0].text, "Some Link")
        self.assertEqual(rows[1].find_all("td")[0].text, "Some Link 2")

    def test_hyperlink_list_sort_by_name_desc(self):
        HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )
        UtterResponseFactory.create(utter="utter_lo_bla", response="<some_link2.url>")
        VariableMetadataFactory.create(
            name="some_link", verbose_name="Some Link", category="verhuizen"
        )
        VariableMetadataFactory.create(
            name="some_link2", verbose_name="Some Link 2", category="paspoort"
        )

        response = self.app.get(
            f"{self.hyperlink_list_url}?sort=name&order=desc", user=self.user
        )

        rows = response.html("tr")[1:]

        self.assertEqual(len(rows), 2)

        self.assertEqual(rows[0].find_all("td")[0].text, "Some Link 2")
        self.assertEqual(rows[1].find_all("td")[0].text, "Some Link")


class HyperlinkReadTests(TwoFactorTestCaseMixin, WebTest):
    login_url = reverse("two_factor:login")

    def setUp(self):
        super().setUp()

        self.gemeente = GemeenteFactory(name="Amsterdam")
        self.user.gemeentes.add(self.gemeente)

        self.hyperlink_list_url = reverse(
            "gemeentes:hyperlink_list", kwargs={"gemeente_slug": self.gemeente.slug}
        )

    def test_hyperlink_read(self):
        hyperlink = HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )
        response = self.app.get(
            reverse(
                "gemeentes:hyperlink_read",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "hyperlink_pk": hyperlink.pk,
                },
            ),
            user=self.user,
        )

        self.assertEqual(response.status_code, 200)

        self.assertIn(hyperlink.url, response.text)

        # Check if there are delete and update buttons
        delete_button, update_button = response.html.find(
            "div", {"id": "ui-view"}
        ).find_all("a", {"class": "btn"})
        delete_url = reverse(
            "gemeentes:hyperlink_delete",
            kwargs={"gemeente_slug": self.gemeente.slug, "hyperlink_pk": hyperlink.pk},
        )
        update_url = reverse(
            "gemeentes:hyperlink_update",
            kwargs={"gemeente_slug": self.gemeente.slug, "hyperlink_pk": hyperlink.pk},
        )
        self.assertEqual(delete_button.get("href"), delete_url)
        self.assertEqual(update_button.get("href"), update_url)

    def test_hyperlink_read_login_required(self):
        hyperlink = HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )
        response = self.app.get(
            reverse(
                "gemeentes:hyperlink_read",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "hyperlink_pk": hyperlink.pk,
                },
            ),
        )
        self.assertEqual(response.status_code, 302)
        self.assertIn(self.login_url, response.location)


class HyperlinkCreateTests(TwoFactorTestCaseMixin, WebTest):
    login_url = reverse("two_factor:login")

    def setUp(self):
        super().setUp()

        self.gemeente = GemeenteFactory(name="Amsterdam")
        self.user.gemeentes.add(self.gemeente)

        self.hyperlink_list_url = reverse(
            "gemeentes:hyperlink_list", kwargs={"gemeente_slug": self.gemeente.slug}
        )

    def test_hyperlink_create(self):
        ChannelFactory.create()
        create_url = reverse(
            "gemeentes:hyperlink_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        response = self.app.get(create_url, user=self.user)

        form = response.forms[1]
        form["name"] = "some_url"
        form["label"] = "label"
        form[
            "url"
        ] = "https://media1.tenor.com/images/d09906a9990dd5064e644e6acefedb6f/tenor.gif?itemid=13491038"
        form["title"] = "some title"

        response = form.submit()

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.location, self.hyperlink_list_url)

        self.assertEqual(Hyperlink.objects.count(), 1)

        hyperlink = Hyperlink.objects.last()

        self.assertEqual(hyperlink.gemeente, self.gemeente)
        self.assertEqual(hyperlink.name, "some_url")
        self.assertEqual(hyperlink.label, "label")
        self.assertEqual(
            hyperlink.url,
            "https://media1.tenor.com/images/d09906a9990dd5064e644e6acefedb6f/tenor.gif?itemid=13491038",
        )
        self.assertEqual(hyperlink.title, "some title")

    def test_hyperlink_create_mailto_link_invalid(self):
        ChannelFactory.create()
        create_url = reverse(
            "gemeentes:hyperlink_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        response = self.app.get(create_url, user=self.user)

        form = response.forms[1]
        form["name"] = "some_url"
        form["label"] = "label"
        form["url"] = "mailto:invalid"
        form["title"] = "some title"

        response = form.submit()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Hyperlink.objects.count(), 0)

    def test_hyperlink_create_mailto_link(self):
        ChannelFactory.create()
        create_url = reverse(
            "gemeentes:hyperlink_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        response = self.app.get(create_url, user=self.user)

        form = response.forms[1]
        form["name"] = "some_url"
        form["label"] = "label"
        form["url"] = "mailto:foo@bar.com"
        form["title"] = "some title"

        response = form.submit()

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.location, self.hyperlink_list_url)

        self.assertEqual(Hyperlink.objects.count(), 1)

        hyperlink = Hyperlink.objects.last()

        self.assertEqual(hyperlink.gemeente, self.gemeente)
        self.assertEqual(hyperlink.name, "some_url")
        self.assertEqual(hyperlink.label, "label")
        self.assertEqual(
            hyperlink.url,
            "mailto:foo@bar.com",
        )
        self.assertEqual(hyperlink.title, "some title")

    def test_hyperlink_create_already_exists(self):
        HyperlinkFactory.create(
            gemeente=self.gemeente,
            name="some_url",
            title="old",
        )
        create_url = reverse(
            "gemeentes:hyperlink_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        response = self.app.get(create_url, user=self.user)

        form = response.forms[1]
        form["name"] = "some_url"
        form["label"] = "label"
        form[
            "url"
        ] = "https://media1.tenor.com/images/d09906a9990dd5064e644e6acefedb6f/tenor.gif?itemid=13491038"
        form["title"] = "some title"

        response = form.submit()

        self.assertEqual(response.status_code, 200)

        error = response.html.find("div", {"class": "alert"})
        self.assertEqual(
            error.text.strip(), _("Deze variabele bestaat al voor deze gemeente")
        )

        self.assertEqual(Hyperlink.objects.count(), 1)

        hyperlink = Hyperlink.objects.last()

        self.assertEqual(hyperlink.gemeente, self.gemeente)
        self.assertEqual(hyperlink.name, "some_url")
        self.assertEqual(hyperlink.title, "old")

    def test_hyperlink_create_redirect_to_category_filtered_list(self):
        ChannelFactory.create()
        create_url = reverse(
            "gemeentes:hyperlink_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        VariableMetadataFactory.create(name="some_url", category="test")
        response = self.app.get(f"{create_url}?name=some_url", user=self.user)

        form = response.forms[1]
        form["label"] = "label"
        form[
            "url"
        ] = "https://media1.tenor.com/images/d09906a9990dd5064e644e6acefedb6f/tenor.gif?itemid=13491038"
        form["title"] = "some title"

        response = form.submit()

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.location, f"{self.hyperlink_list_url}?category=test")

        self.assertEqual(Hyperlink.objects.count(), 1)

        hyperlink = Hyperlink.objects.last()

        self.assertEqual(hyperlink.gemeente, self.gemeente)
        self.assertEqual(hyperlink.name, "some_url")
        self.assertEqual(hyperlink.label, "label")
        self.assertEqual(
            hyperlink.url,
            "https://media1.tenor.com/images/d09906a9990dd5064e644e6acefedb6f/tenor.gif?itemid=13491038",
        )
        self.assertEqual(hyperlink.title, "some title")

    def test_hyperlink_create_breadcrumb_category(self):
        ChannelFactory.create()
        create_url = reverse(
            "gemeentes:hyperlink_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        VariableMetadataFactory.create(name="some_url", category="test")
        response = self.app.get(f"{create_url}?name=some_url", user=self.user)

        breadcrumbs = response.html("li", {"class": "breadcrumb-item"})

        self.assertEqual(breadcrumbs[1].text.strip(), "Test")
        self.assertEqual(breadcrumbs[2].text.strip(), "Test aanmaken")

    def test_hyperlink_create_ignore_dynamic_required_fields(self):
        ChannelFactory.create()
        create_url = reverse(
            "gemeentes:hyperlink_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        response = self.app.get(
            f"{create_url}?name=some_url&required=url", user=self.user
        )

        form = response.forms[1]

        # Attempt to submit without url
        form["url"] = ""

        response = form.submit()

        self.assertEqual(response.status_code, 302)

        # error = response.html.find("p", {"id": "error_1_id_url"})
        # self.assertIsNotNone(error)

    def test_hyperlink_create_display_metadata(self):
        UtterResponseFactory.create(
            gemeente=self.gemeente,
            response="[<some_link.label>](<some_link.url>)",
            utter="utter_lo_1",
        )
        metadata = VariableMetadataFactory.create(
            name="some_link",
            verbose_name="Some Link",
            category="hyperlinks",
            help_text="hellup",
        )
        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            create_url = reverse(
                "gemeentes:hyperlink_create",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                },
            )
            response = self.app.get(
                f"{create_url}?name=some_link&required=label&required=url",
                user=self.user,
            )

        self.assertEqual(response.status_code, 200)

        metadata = response.html.find("table")

        rows = metadata.find_all("tr")

        self.assertIn("Some Link", rows[0].find_all("td")[-1].text)
        self.assertIn("hyperlinks", rows[1].find_all("td")[-1].text)

        help_text = response.html.find("p", {"id": "hint_id_response"})
        self.assertEqual(help_text.text, "hellup")

    def test_hyperlink_create_login_required(self):
        response = self.app.get(
            reverse(
                "gemeentes:hyperlink_create",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                },
            ),
        )
        self.assertEqual(response.status_code, 302)
        self.assertIn(self.login_url, response.location)

    def test_hyperlink_create_redirects(self):
        default_hyperlink_overview_uri = reverse(
            "gemeentes:hyperlink_list", kwargs={"gemeente_slug": self.gemeente.slug}
        )
        default_hyperlink_overview_url = (
            f"http://testserver{default_hyperlink_overview_uri}"
        )

        required_hyperlink_overview_uri = reverse(
            "gemeentes:hyperlink_list_editable",
            kwargs={"gemeente_slug": self.gemeente.slug},
        )
        required_hyperlink_overview_url = f"http://testserver{required_hyperlink_overview_uri}?category=verhuismelding"

        ChannelFactory.create()
        create_url = reverse(
            "gemeentes:hyperlink_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )

        for referrer in [
            default_hyperlink_overview_url,
            required_hyperlink_overview_url,
        ]:
            with self.subTest(referrer=referrer):
                Hyperlink.objects.all().delete()

                with requests_mock.Mocker() as m:
                    m.get(DOMAIN_URL, json={"utter_ask": {}}, headers=HEADERS)

                    response = self.app.get(
                        f"{create_url}?name=hyperlink_name",
                        user=self.user,
                        headers={"Referer": referrer},
                    )

                    form = response.forms[1]
                    form["label"] = "testlabel"

                    response = form.submit()

                self.assertEqual(response.status_code, 302)
                self.assertEqual(response.location, referrer)

                self.assertEqual(Hyperlink.objects.count(), 1)


class HyperlinkUpdateTests(TwoFactorTestCaseMixin, WebTest):
    login_url = reverse("two_factor:login")

    def setUp(self):
        super().setUp()

        self.gemeente = GemeenteFactory(name="Amsterdam")
        self.user.gemeentes.add(self.gemeente)

        self.hyperlink_list_url = reverse(
            "gemeentes:hyperlink_list", kwargs={"gemeente_slug": self.gemeente.slug}
        )

    def test_hyperlink_update(self):
        UtterResponseFactory.create(
            gemeente=self.gemeente,
            response="[<some_link.label>](<some_link.url>)",
            utter="utter_lo_1",
        )
        hyperlink = HyperlinkFactory(
            gemeente=self.gemeente,
            name="some_link",
            url="https://example.com",
            label="label",
        )
        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                reverse(
                    "gemeentes:hyperlink_update",
                    kwargs={
                        "gemeente_slug": self.gemeente.slug,
                        "hyperlink_pk": hyperlink.pk,
                    },
                ),
                user=self.user,
            )

            form = response.forms[1]
            form["label"] = "label2"
            form["url"] = "http://google.com"

            response = form.submit()

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.location, self.hyperlink_list_url)

        self.assertEqual(Hyperlink.objects.count(), 1)

        hyperlink.refresh_from_db()

        self.assertEqual(hyperlink.gemeente, self.gemeente)
        self.assertEqual(hyperlink.name, "some_link")
        self.assertEqual(hyperlink.label, "label2")
        self.assertEqual(hyperlink.url, "http://google.com")

    def test_hyperlink_update_redirect_to_category_filtered_list(self):
        UtterResponseFactory.create(
            gemeente=self.gemeente,
            response="[<some_link.label>](<some_link.url>)",
            utter="utter_lo_1",
        )
        hyperlink = HyperlinkFactory(
            gemeente=self.gemeente,
            name="some_link",
            url="https://example.com",
            label="label",
        )
        VariableMetadataFactory.create(name="some_link", category="test")
        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                reverse(
                    "gemeentes:hyperlink_update",
                    kwargs={
                        "gemeente_slug": self.gemeente.slug,
                        "hyperlink_pk": hyperlink.pk,
                    },
                ),
                user=self.user,
            )

            form = response.forms[1]
            form["label"] = "label2"
            form["url"] = "http://google.com"

            response = form.submit()

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.location, f"{self.hyperlink_list_url}?category=test")

        self.assertEqual(Hyperlink.objects.count(), 1)

        hyperlink.refresh_from_db()

        self.assertEqual(hyperlink.gemeente, self.gemeente)
        self.assertEqual(hyperlink.name, "some_link")
        self.assertEqual(hyperlink.label, "label2")
        self.assertEqual(hyperlink.url, "http://google.com")

    def test_hyperlink_update_breadcrumb_category(self):
        UtterResponseFactory.create(
            gemeente=self.gemeente,
            response="[<some_link.label>](<some_link.url>)",
            utter="utter_lo_1",
        )
        hyperlink = HyperlinkFactory(
            gemeente=self.gemeente,
            name="some_link",
            url="https://example.com",
            label="label",
        )
        VariableMetadataFactory.create(name="some_link", category="test")
        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                reverse(
                    "gemeentes:hyperlink_update",
                    kwargs={
                        "gemeente_slug": self.gemeente.slug,
                        "hyperlink_pk": hyperlink.pk,
                    },
                ),
                user=self.user,
            )

        breadcrumbs = response.html("li", {"class": "breadcrumb-item"})

        self.assertEqual(breadcrumbs[1].text.strip(), "Test")
        self.assertEqual(breadcrumbs[2].text.strip(), "Test bijwerken")

    def test_hyperlink_update_required_fields(self):
        UtterResponseFactory.create(
            gemeente=self.gemeente, response="<some_link.label>", utter="utter_lo_1"
        )
        hyperlink = HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="", label="label"
        )
        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                reverse(
                    "gemeentes:hyperlink_update",
                    kwargs={
                        "gemeente_slug": self.gemeente.slug,
                        "hyperlink_pk": hyperlink.pk,
                    },
                ),
                user=self.user,
            )

            form = response.forms[1]
            form["label"] = "label2"

            response = form.submit()

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.location, self.hyperlink_list_url)

        self.assertEqual(Hyperlink.objects.count(), 1)

        hyperlink.refresh_from_db()

        self.assertEqual(hyperlink.gemeente, self.gemeente)
        self.assertEqual(hyperlink.name, "some_link")
        self.assertEqual(hyperlink.label, "label2")

    def test_hyperlink_update_required_fields_fallback(self):
        hyperlink = HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="", label="label"
        )
        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                reverse(
                    "gemeentes:hyperlink_update",
                    kwargs={
                        "gemeente_slug": self.gemeente.slug,
                        "hyperlink_pk": hyperlink.pk,
                    },
                ),
                user=self.user,
            )

            form = response.forms[1]
            form["label"] = "label2"

            response = form.submit()

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.location, self.hyperlink_list_url)

        self.assertEqual(Hyperlink.objects.count(), 1)

        hyperlink.refresh_from_db()

        self.assertEqual(hyperlink.gemeente, self.gemeente)
        self.assertEqual(hyperlink.name, "some_link")
        self.assertEqual(hyperlink.label, "label2")

    def test_hyperlink_update_display_metadata(self):
        UtterResponseFactory.create(
            gemeente=self.gemeente,
            response="[<some_link.label>](<some_link.url>)",
            utter="utter_lo_1",
        )
        hyperlink = HyperlinkFactory(
            gemeente=self.gemeente,
            name="some_link",
            url="https://example.com",
            label="label",
        )
        metadata = VariableMetadataFactory.create(
            name="some_link",
            verbose_name="Some Link",
            category="hyperlinks",
            help_text="hellup",
        )
        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                reverse(
                    "gemeentes:hyperlink_update",
                    kwargs={
                        "gemeente_slug": self.gemeente.slug,
                        "hyperlink_pk": hyperlink.pk,
                    },
                ),
                user=self.user,
            )

        self.assertEqual(response.status_code, 200)

        metadata = response.html.find("table")

        rows = metadata.find_all("tr")

        self.assertIn("Some Link", rows[0].find_all("td")[-1].text)
        self.assertIn("hyperlinks", rows[1].find_all("td")[-1].text)

        help_text = response.html.find("p", {"id": "hint_id_response"})
        self.assertEqual(help_text.text, "hellup")

    def test_hyperlink_update_login_required(self):
        hyperlink = HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )
        response = self.app.get(
            reverse(
                "gemeentes:hyperlink_update",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "hyperlink_pk": hyperlink.pk,
                },
            ),
        )
        self.assertEqual(response.status_code, 302)
        self.assertIn(self.login_url, response.location)


class HyperlinkDeleteTests(TwoFactorTestCaseMixin, WebTest):
    login_url = reverse("two_factor:login")

    def setUp(self):
        super().setUp()

        self.gemeente = GemeenteFactory(name="Amsterdam")
        self.user.gemeentes.add(self.gemeente)

        self.hyperlink_list_url = reverse(
            "gemeentes:hyperlink_list", kwargs={"gemeente_slug": self.gemeente.slug}
        )

    def test_hyperlink_delete(self):
        hyperlink = HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )
        response = self.app.get(
            reverse(
                "gemeentes:hyperlink_delete",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "hyperlink_pk": hyperlink.pk,
                },
            ),
            user=self.user,
        )

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.location, self.hyperlink_list_url)

        self.assertEqual(Hyperlink.objects.count(), 0)

    def test_hyperlink_delete_redirect_to_category_filtered_list(self):
        hyperlink = HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )
        VariableMetadataFactory.create(name="some_link", category="verhuizen")
        response = self.app.get(
            reverse(
                "gemeentes:hyperlink_delete",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "hyperlink_pk": hyperlink.pk,
                },
            ),
            user=self.user,
        )

        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            response.location, f"{self.hyperlink_list_url}?category=verhuizen"
        )

        self.assertEqual(Hyperlink.objects.count(), 0)

    def test_hyperlink_delete_login_required(self):
        hyperlink = HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )
        response = self.app.get(
            reverse(
                "gemeentes:hyperlink_delete",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "hyperlink_pk": hyperlink.pk,
                },
            ),
        )
        self.assertEqual(response.status_code, 302)
        self.assertIn(self.login_url, response.location)


@tag("permissions")
class HyperlinkPermissionTests(TwoFactorTestCaseMixin, WebTest):
    login_url = reverse("two_factor:login")
    user_is_admin = False
    user_is_superuser = False

    def setUp(self):
        super().setUp()

        gemeente = GemeenteFactory(name="Amsterdam")
        self.user.gemeentes.add(gemeente)

        self.gemeente = GemeenteFactory(name="rotterdam")
        self.hyperlink_list_url = reverse(
            "gemeentes:hyperlink_list", kwargs={"gemeente_slug": self.gemeente.slug}
        )

    def test_hyperlink_list_no_permission(self):
        # Should not have any influence
        grant_gemeente_perms(
            "admin", self.user, self.gemeente, ["hyperlink_write", "utter_read"]
        )
        HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )

        response = self.app.get(self.hyperlink_list_url, user=self.user, status=403)

        self.assertEqual(response.status_code, 403)

    def test_hyperlink_list_no_permission_for_current_gemeente(self):
        # Should not have any influence
        grant_gemeente_perms(
            "admin",
            self.user,
            Gemeente.objects.get(name="Amsterdam"),
            ["hyperlink_read"],
        )
        HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )

        response = self.app.get(self.hyperlink_list_url, user=self.user, status=403)

        self.assertEqual(response.status_code, 403)

    def test_hyperlink_list_has_permission(self):
        HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )

        grant_gemeente_perms("admin", self.user, self.gemeente, ["hyperlink_read"])

        response = self.app.get(self.hyperlink_list_url, user=self.user)

        self.assertEqual(response.status_code, 200)

    def test_hyperlink_list_has_permission_read_only(self):
        grant_gemeente_perms("admin", self.user, self.gemeente, ["hyperlink_read"])

        HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )
        VariableMetadataFactory.create(name="some_link", verbose_name="Some Link")
        VariableMetadataFactory.create(name="some_link2", verbose_name="Some Link 2")

        response = self.app.get(self.hyperlink_list_url, user=self.user)

        colnames = response.html("th")
        self.assertIn("Naam", colnames[0].text)
        self.assertIn("Zichtbare tekst (label)", colnames[1].text)
        self.assertIn("Internetadres (url)", colnames[2].text)

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 2)

        # Update/delete buttons should not show up
        buttons = rows[0].find_all("a")
        self.assertEqual(buttons, [])

        # Create button should not show up
        buttons = rows[1].find_all("a")
        self.assertEqual(buttons, [])

    def test_hyperlink_list_editable_no_permission(self):
        # Should not have any influence
        grant_gemeente_perms(
            "admin", self.user, self.gemeente, ["hyperlink_write", "utter_read"]
        )
        HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )

        response = self.app.get(
            reverse(
                "gemeentes:hyperlink_list_editable",
                kwargs={"gemeente_slug": self.gemeente.slug},
            ),
            user=self.user,
            status=403,
        )

        self.assertEqual(response.status_code, 403)

    def test_hyperlink_list_editable_has_permission(self):
        HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )

        grant_gemeente_perms("admin", self.user, self.gemeente, ["hyperlink_read"])

        response = self.app.get(
            reverse(
                "gemeentes:hyperlink_list_editable",
                kwargs={"gemeente_slug": self.gemeente.slug},
            ),
            user=self.user,
        )

        self.assertEqual(response.status_code, 200)

    def test_hyperlink_create_no_permission(self):
        # Should not have any influence
        grant_gemeente_perms(
            "admin", self.user, self.gemeente, ["hyperlink_read", "utter_write"]
        )

        response = self.app.get(
            reverse(
                "gemeentes:hyperlink_create",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                },
            ),
            user=self.user,
            status=403,
        )

        self.assertEqual(response.status_code, 403)

    def test_hyperlink_create_has_permission(self):
        grant_gemeente_perms("admin", self.user, self.gemeente, ["hyperlink_write"])
        response = self.app.get(
            reverse(
                "gemeentes:hyperlink_create",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                },
            ),
            user=self.user,
        )

        self.assertEqual(response.status_code, 200)

    def test_hyperlink_update_no_permission(self):
        # Should not have any influence
        grant_gemeente_perms(
            "admin", self.user, self.gemeente, ["hyperlink_read", "utter_write"]
        )
        hyperlink = HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )
        response = self.app.get(
            reverse(
                "gemeentes:hyperlink_update",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "hyperlink_pk": hyperlink.pk,
                },
            ),
            user=self.user,
            status=403,
        )

        self.assertEqual(response.status_code, 403)

    def test_hyperlink_update_has_permission(self):
        grant_gemeente_perms("admin", self.user, self.gemeente, ["hyperlink_write"])
        hyperlink = HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                reverse(
                    "gemeentes:hyperlink_update",
                    kwargs={
                        "gemeente_slug": self.gemeente.slug,
                        "hyperlink_pk": hyperlink.pk,
                    },
                ),
                user=self.user,
            )

        self.assertEqual(response.status_code, 200)

    def test_hyperlink_delete_no_permission(self):
        # Should not have any influence
        grant_gemeente_perms(
            "admin", self.user, self.gemeente, ["hyperlink_read", "utter_write"]
        )

        hyperlink = HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )
        response = self.app.get(
            reverse(
                "gemeentes:hyperlink_delete",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "hyperlink_pk": hyperlink.pk,
                },
            ),
            user=self.user,
            status=403,
        )

        self.assertEqual(response.status_code, 403)

    def test_hyperlink_delete_has_permission(self):
        grant_gemeente_perms("admin", self.user, self.gemeente, ["hyperlink_write"])
        hyperlink = HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )
        response = self.app.get(
            reverse(
                "gemeentes:hyperlink_delete",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "hyperlink_pk": hyperlink.pk,
                },
            ),
            user=self.user,
        )

        self.assertEqual(response.status_code, 302)

    def test_hyperlink_read_no_permission(self):
        # Should have no influence
        grant_gemeente_perms("admin", self.user, self.gemeente, ["hyperlink_write"])

        hyperlink = HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )
        response = self.app.get(
            reverse(
                "gemeentes:hyperlink_read",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "hyperlink_pk": hyperlink.pk,
                },
            ),
            user=self.user,
            status=403,
        )

        self.assertEqual(response.status_code, 403)

    def test_hyperlink_read_has_permission_read_only(self):
        grant_gemeente_perms("admin", self.user, self.gemeente, ["hyperlink_read"])

        hyperlink = HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )
        response = self.app.get(
            reverse(
                "gemeentes:hyperlink_read",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "hyperlink_pk": hyperlink.pk,
                },
            ),
            user=self.user,
        )

        self.assertEqual(response.status_code, 200)

        self.assertIn(hyperlink.url, response.text)

        # Delete and update buttons should not appear
        buttons = response.html.find("div", {"id": "ui-view"}).find_all(
            "a", {"class": "btn"}
        )
        self.assertEqual(buttons, [])
