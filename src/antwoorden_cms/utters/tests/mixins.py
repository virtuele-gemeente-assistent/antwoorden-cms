from django.contrib.auth.models import Permission

from ..models import Gemeente
from .factories import GroupFactory, UserFactory


class TwoFactorTestCaseMixin:
    user_is_admin = True
    user_is_superuser = True

    def setUp(self):
        self.user = UserFactory.create(
            is_superuser=self.user_is_superuser,
            is_staff=self.user_is_admin,
            password="test",
        )

        # Create a group with all permissions
        if self.user_is_admin:
            group = GroupFactory.create(name="admin")
            gemeente_perms = Permission.objects.filter(
                codename__in=[p[0] for p in Gemeente._meta.permissions]
            )
            group.permissions.set(gemeente_perms)

            self.user.groups.add(group)

        self.device = self.user.staticdevice_set.create()
        self.device.token_set.create(token=self.user.get_username())

        self.app.set_cookie("sessionid", "initial")
        session = self.app.session
        session["otp_device_id"] = self.device.persistent_id
        session.save()
        self.app.set_cookie("sessionid", session.session_key)
