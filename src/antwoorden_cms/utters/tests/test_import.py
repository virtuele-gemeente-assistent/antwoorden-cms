from django.conf import settings
from django.urls import reverse

import requests_mock
from django_webtest import WebTest

from ..models import UtterResponse, VariableMetadata
from .factories import GemeenteFactory, UserFactory, UtterResponseFactory
from .mixins import TwoFactorTestCaseMixin

DOMAIN_URL = settings.DOMAIN_URL


class DefaultUtterImportTests(TwoFactorTestCaseMixin, WebTest):
    login_url = reverse("two_factor:login")

    def test_import_default_responses(self):
        data = {
            "responses": {
                "utter_lo_greet": [
                    {"text": "landelijke default"},
                    {
                        "channel": "metadata",
                        "custom": {"metadata": {}, "default": [{"text": "hallo"}]},
                    },
                ]
            }
        }

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=data)
            self.app.get(reverse("admin:import_default_responses"), user=self.user)

        utter = UtterResponse.objects.get()

        self.assertEqual(utter.utter, "utter_lo_greet")
        self.assertEqual(utter.response, "hallo")

    def test_import_default_responses_override(self):
        data = {
            "responses": {
                "utter_lo_greet": [
                    {"text": "landelijke default"},
                    {
                        "channel": "metadata",
                        "custom": {"metadata": {}, "default": [{"text": "hallo"}]},
                    },
                ]
            }
        }
        UtterResponseFactory.create(
            utter="utter_lo_greet", response="hoooooooi \U0001F984", gemeente=None
        )

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=data)
            self.app.get(reverse("admin:import_default_responses"), user=self.user)

        utter = UtterResponse.objects.get()

        self.assertEqual(utter.utter, "utter_lo_greet")
        self.assertEqual(utter.response, "hallo")

    def test_import_default_responses_dont_override_local_utters(self):
        data = {
            "responses": {
                "utter_lo_greet": [
                    {"text": "landelijke default"},
                    {
                        "channel": "metadata",
                        "custom": {"metadata": {}, "default": [{"text": "hallo"}]},
                    },
                ]
            }
        }
        UtterResponseFactory.create(
            utter="utter_lo_greet",
            response="hoooooooi \U0001F984",
            gemeente=GemeenteFactory(name="Dongen"),
        )

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=data)
            self.app.get(reverse("admin:import_default_responses"), user=self.user)

        self.assertEqual(UtterResponse.objects.count(), 2)

    def test_import_default_responses_login_required(self):
        data = {
            "responses": {
                "utter_lo_greet": [
                    {"text": "landelijke default"},
                    {
                        "channel": "metadata",
                        "custom": {"metadata": {}, "default": [{"text": "hallo"}]},
                    },
                ]
            }
        }
        UtterResponseFactory.create(
            utter="utter_lo_greet", response="hoooooooi \U0001F984"
        )

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=data)
            response = self.app.get(reverse("admin:import_default_responses"))

        self.assertEqual(response.status_code, 302)
        self.assertIn(self.login_url, response.url)

    def test_import_default_responses_staff_required(self):
        data = {
            "responses": {
                "utter_lo_greet": [
                    {"text": "landelijke default"},
                    {
                        "channel": "metadata",
                        "custom": {"metadata": {}, "default": [{"text": "hallo"}]},
                    },
                ]
            }
        }
        UtterResponseFactory.create(
            utter="utter_lo_greet", response="hoooooooi \U0001F984"
        )

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=data)
            response = self.app.get(
                reverse("admin:import_default_responses"), user=UserFactory.create()
            )

        self.assertEqual(response.status_code, 302)
        self.assertIn(self.login_url, response.url)

    def test_import_hyperlink_metadata(self):
        data = """%YAML 1.1\n---\nmunicipalities:\n- dongen\nresponses:\n\n  utter_greet:\n    default:\n    - text: hallo\nhyperlinks:\n\n  pagina_contact:\n    dongen:\n    label: hallo\n    metadata:\n      name: Pagina contact\n      category: hyperlinks\n      helptext: help"""

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json={"responses": {}})
            m.get(settings.LOCAL_YML_URL, text=data)
            self.app.get(reverse("admin:import_default_responses"), user=self.user)

        metadata = VariableMetadata.objects.get()

        self.assertEqual(metadata.name, "pagina_contact")
        self.assertEqual(metadata.verbose_name, "Pagina contact")
        self.assertEqual(metadata.category, "hyperlinks")
        self.assertEqual(metadata.help_text, "help")
