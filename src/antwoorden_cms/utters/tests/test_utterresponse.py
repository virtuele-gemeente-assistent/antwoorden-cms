from copy import deepcopy
from unittest import skip

from django.conf import settings
from django.core.cache import cache
from django.test import tag
from django.urls import reverse
from django.utils.translation import ugettext as _

import requests_mock
from django_webtest import WebTest
from freezegun import freeze_time

from ..models import Gemeente, UtterResponse
from .factories import (
    ChannelFactory,
    GemeenteFactory,
    HyperlinkFactory,
    UtterResponseFactory,
)
from .mixins import TwoFactorTestCaseMixin
from .utils import grant_gemeente_perms

HEADERS = {"Content-Type": "application/json"}

LOCAL_UTTER_RESPONSES = {
    "responses": {
        "utter_lo_1": [
            {"text": "landelijke default"},
            {
                "channel": "metadata",
                "custom": {
                    "default": [
                        {
                            "text": "text1",
                        }
                    ],
                    "metadata": {
                        "required": False,
                        "questions": ["question1", "question2"],
                        "category": "verhuismelding",
                        "examples": ["example1"],
                        "changed": "2021-01-01",
                        "created": "2020-05-05",
                    },
                },
            },
        ],
        "utter_lo_2": [
            {"text": "landelijke default"},
            {
                "channel": "metadata",
                "custom": {
                    "default": [
                        {
                            "text": "text2",
                        }
                    ],
                    "metadata": {
                        "required": False,
                        "questions": ["question1", "question2"],
                        "category": "verhuismelding",
                        "examples": ["example1", "example2"],
                        "changed": "2020-01-01",
                        "created": "2019-05-05",
                    },
                },
            },
        ],
        "utter_lo_3": [
            {"text": "landelijke default"},
            {
                "channel": "metadata",
                "custom": {
                    "default": [
                        {
                            "text": "text3",
                        }
                    ],
                    "metadata": {
                        "required": True,
                        "category": "overig",
                        "examples": ["example2"],
                        "changed": "2019-01-01",
                    },
                },
            },
        ],
        "utter_lo_4": [
            {"text": "landelijke default"},
            {
                "channel": "metadata",
                "custom": {
                    "default": [
                        {
                            "text": "text4",
                        }
                    ],
                    "metadata": {
                        "required": True,
                        "changed": "2018-01-01",
                    },
                },
            },
        ],
    }
}

DOMAIN_URL = settings.DOMAIN_URL


class UtterResponseListTests(TwoFactorTestCaseMixin, WebTest):
    login_url = reverse("two_factor:login")

    def setUp(self):
        super().setUp()

        self.gemeente = GemeenteFactory(name="Amsterdam")
        self.user.gemeentes.add(self.gemeente)

        self.antwoord_list_url = reverse(
            "gemeentes:antwoord_list", kwargs={"gemeente_slug": self.gemeente.slug}
        )

        # Ensure cache is emptied between tests
        cache.delete("domain_tag")

    @skip("Editable list view has been temporarily removed")
    def test_utterresponse_editable_list(self):
        self.utter1 = UtterResponseFactory(gemeente=self.gemeente, utter="utter_lo_2")

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                reverse(
                    "gemeentes:antwoord_list_editable",
                    kwargs={"gemeente_slug": self.gemeente.slug},
                ),
                user=self.user,
            )

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 1)

        LOCAL_UTTER_RESPONSES["utter_lo_1"]

        # Check if there is a link to the detail page
        detail_url = reverse(
            "gemeentes:antwoord_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        create_button = rows[0].find("a")
        self.assertIn(f"{detail_url}?utter_name=utter_lo_1", create_button.get("href"))

    @skip("Editable list view has been temporarily removed")
    def test_utterresponse_editable_list_login_required(self):
        response = self.app.get(
            reverse(
                "gemeentes:antwoord_list_editable",
                kwargs={"gemeente_slug": self.gemeente.slug},
            ),
        )
        self.assertEqual(response.status_code, 302)
        self.assertIn(self.login_url, response.location)

    def test_utterresponse_editable_list_required(self):
        self.utter1 = UtterResponseFactory(gemeente=self.gemeente, utter="utter_lo_4")

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                reverse(
                    "gemeentes:antwoord_list_editable_required",
                    kwargs={"gemeente_slug": self.gemeente.slug},
                ),
                user=self.user,
            )

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 1)

        LOCAL_UTTER_RESPONSES["responses"]["utter_lo_3"]

        # Check if there is a link to the detail page
        detail_url = reverse(
            "gemeentes:antwoord_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        create_button = rows[0].find("a")

        self.assertIn(
            f"{detail_url}?utter_name=utter_lo_3", container=create_button.get("href")
        )

    def test_utterresponse_editable_list_required_login_required(self):
        response = self.app.get(
            reverse(
                "gemeentes:antwoord_list_editable_required",
                kwargs={"gemeente_slug": self.gemeente.slug},
            ),
        )
        self.assertEqual(response.status_code, 302)
        self.assertIn(self.login_url, response.location)

    def test_utterresponse_local_list(self):
        utter1 = UtterResponseFactory.create(gemeente=self.gemeente, utter="utter_lo_1")
        utter2 = UtterResponseFactory.create(gemeente=self.gemeente, utter="utter_lo_2")

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(self.antwoord_list_url, user=self.user)

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 4)

        for i, utter in enumerate([utter1, utter2]):
            with self.subTest(utter=utter):
                question1, question2 = rows[i].find_all("li")
                self.assertEqual(question1.text, "question1")
                self.assertEqual(question2.text, "question2")

                # Check if there are delete and update buttons
                delete_url = reverse(
                    "gemeentes:antwoord_delete",
                    kwargs={
                        "gemeente_slug": self.gemeente.slug,
                        "antwoord_pk": utter.pk,
                    },
                )
                update_url = reverse(
                    "gemeentes:antwoord_update",
                    kwargs={
                        "gemeente_slug": self.gemeente.slug,
                        "antwoord_pk": utter.pk,
                    },
                )
                delete_button, update_button = rows[i].find_all("a")
                self.assertEqual(delete_url, delete_button.get("href"))
                self.assertEqual(update_url, update_button.get("href"))

        add_rows = rows[2:4]

        for i, utter in enumerate(list(LOCAL_UTTER_RESPONSES.keys())[2:4]):
            with self.subTest(utter=utter):
                # Check if there is a link to the detail page
                detail_url = reverse(
                    "gemeentes:antwoord_create",
                    kwargs={
                        "gemeente_slug": self.gemeente.slug,
                    },
                )
                create_button = add_rows[i].find("a")
                self.assertIn(
                    f"{detail_url}?utter_name={utter}", create_button.get("href")
                )

    def test_utterresponse_local_list_format_duplicate_hyperlinks(self):
        UtterResponseFactory.create(
            gemeente=self.gemeente, utter="utter_lo_1", response="<some_var.label>"
        )
        HyperlinkFactory.create(
            name="some_var",
            label="some_label1",
            gemeente=self.gemeente,
        )
        HyperlinkFactory.create(
            name="some_var",
            label="some_label2",
            gemeente=self.gemeente,
        )

        with requests_mock.Mocker() as m:
            m.get(
                DOMAIN_URL,
                json={
                    "responses": {
                        "utter_lo_1": LOCAL_UTTER_RESPONSES["responses"]["utter_lo_1"]
                    }
                },
                headers=HEADERS,
            )

            response = self.app.get(self.antwoord_list_url, user=self.user)

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 1)

        # Variable should be formatted with newest Hyperlink
        self.assertEqual(rows[0].findAll("td")[1].text, "some_label2")

    def test_utterresponse_local_list_filter_by_category(self):
        utter1 = UtterResponseFactory.create(gemeente=self.gemeente, utter="utter_lo_1")

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                f"{self.antwoord_list_url}?category=verhuismelding", user=self.user
            )

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 2)

        question1, question2 = rows[0].find_all("li")
        self.assertEqual(question1.text, "question1")
        self.assertEqual(question2.text, "question2")

        # Check if there are delete and update buttons
        delete_url = reverse(
            "gemeentes:antwoord_delete",
            kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": utter1.pk},
        )
        update_url = reverse(
            "gemeentes:antwoord_update",
            kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": utter1.pk},
        )
        delete_button, update_button = rows[0].find_all("a")
        self.assertEqual(delete_url, delete_button.get("href"))
        self.assertEqual(update_url, update_button.get("href"))

        # Check if there is a link to the detail page
        detail_url = reverse(
            "gemeentes:antwoord_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        create_button = rows[1].find("a")
        self.assertIn(f"{detail_url}?utter_name=utter_lo_2", create_button.get("href"))

    def test_utterresponse_local_list_search_query_on_questions(self):
        utter1 = UtterResponseFactory.create(gemeente=self.gemeente, utter="utter_lo_1")

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                f"{self.antwoord_list_url}?search=question1", user=self.user
            )

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 2)

        question1, question2 = rows[0].find_all("li")
        self.assertEqual(question1.text, "question1")
        self.assertEqual(question2.text, "question2")

        # Check if there are delete and update buttons
        delete_url = reverse(
            "gemeentes:antwoord_delete",
            kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": utter1.pk},
        )
        update_url = reverse(
            "gemeentes:antwoord_update",
            kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": utter1.pk},
        )
        delete_button, update_button = rows[0].find_all("a")
        self.assertEqual(delete_url, delete_button.get("href"))
        self.assertEqual(update_url, update_button.get("href"))

        question1, question2 = rows[1].find_all("li")
        self.assertEqual(question1.text, "question1")
        self.assertEqual(question2.text, "question2")

        # Check if there is a link to the detail page
        detail_url = reverse(
            "gemeentes:antwoord_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        create_button = rows[1].find("a")
        self.assertIn(f"{detail_url}?utter_name=utter_lo_2", create_button.get("href"))

    def test_utterresponse_local_list_search_query_on_central_response(self):
        utter1 = UtterResponseFactory.create(gemeente=self.gemeente, utter="utter_lo_1")

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                f"{self.antwoord_list_url}?search=text1", user=self.user
            )

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 1)

        question1, question2 = rows[0].find_all("li")
        self.assertEqual(question1.text, "question1")
        self.assertEqual(question2.text, "question2")

        # Check if there are delete and update buttons
        delete_url = reverse(
            "gemeentes:antwoord_delete",
            kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": utter1.pk},
        )
        update_url = reverse(
            "gemeentes:antwoord_update",
            kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": utter1.pk},
        )
        delete_button, update_button = rows[0].find_all("a")
        self.assertEqual(delete_url, delete_button.get("href"))
        self.assertEqual(update_url, update_button.get("href"))

    def test_utterresponse_local_list_search_query_on_examples(self):
        utter1 = UtterResponseFactory.create(gemeente=self.gemeente, utter="utter_lo_1")

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                f"{self.antwoord_list_url}?search=example1", user=self.user
            )

        rows = response.html("tr")[1:]

        # utter_lo_1 and utter_lo_2 should show up
        self.assertEqual(len(rows), 2)

        question1, question2 = rows[0].find_all("li")
        self.assertEqual(question1.text, "question1")
        self.assertEqual(question2.text, "question2")

        # Check if there are delete and update buttons
        delete_url = reverse(
            "gemeentes:antwoord_delete",
            kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": utter1.pk},
        )
        update_url = reverse(
            "gemeentes:antwoord_update",
            kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": utter1.pk},
        )
        delete_button, update_button = rows[0].find_all("a")
        self.assertEqual(delete_url, delete_button.get("href"))
        self.assertEqual(update_url, update_button.get("href"))

        question1, question2 = rows[1].find_all("li")
        self.assertEqual(question1.text, "question1")
        self.assertEqual(question2.text, "question2")

        # Check if there is a link to the detail page
        detail_url = reverse(
            "gemeentes:antwoord_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        create_button = rows[1].find("a")
        self.assertIn(f"{detail_url}?utter_name=utter_lo_2", create_button.get("href"))

    def test_utterresponse_local_list_search_query_on_local_default_response(self):
        UtterResponseFactory.create(
            gemeente=None, utter="utter_lo_2", response="some local default"
        )

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                f"{self.antwoord_list_url}?search=some+local+default", user=self.user
            )

        rows = response.html("tr")[1:]

        # utter_lo_2 should show up
        self.assertEqual(len(rows), 1)

        question1, question2 = rows[0].find_all("li")
        self.assertEqual(question1.text, "question1")
        self.assertEqual(question2.text, "question2")

        # Check if there is a link to the detail page
        detail_url = reverse(
            "gemeentes:antwoord_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        create_button = rows[0].find("a")
        self.assertIn(f"{detail_url}?utter_name=utter_lo_2", create_button.get("href"))

    def test_utterresponse_local_list_search_query_on_local_overridden_response(self):
        utter1 = UtterResponseFactory.create(
            gemeente=self.gemeente, utter="utter_lo_2", response="some local override"
        )

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                f"{self.antwoord_list_url}?search=some+local+override", user=self.user
            )

        rows = response.html("tr")[1:]

        # utter_lo_2 should show up
        self.assertEqual(len(rows), 1)

        question1, question2 = rows[0].find_all("li")
        self.assertEqual(question1.text, "question1")
        self.assertEqual(question2.text, "question2")

        # Check if there are delete and update buttons
        delete_url = reverse(
            "gemeentes:antwoord_delete",
            kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": utter1.pk},
        )
        update_url = reverse(
            "gemeentes:antwoord_update",
            kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": utter1.pk},
        )
        delete_button, update_button = rows[0].find_all("a")
        self.assertEqual(delete_url, delete_button.get("href"))
        self.assertEqual(update_url, update_button.get("href"))

    def test_utterresponse_local_list_search_query_on_local_overridden_response_formatted(
        self,
    ):
        utter1 = UtterResponseFactory.create(
            gemeente=self.gemeente,
            utter="utter_lo_2",
            response="some <gemeente.label> override",
        )
        HyperlinkFactory.create(
            gemeente=self.gemeente, name="gemeente", label="Utrecht"
        )

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                f"{self.antwoord_list_url}?search=Utrecht", user=self.user
            )

        rows = response.html("tr")[1:]

        # utter_lo_2 should show up
        self.assertEqual(len(rows), 1)

        question1, question2 = rows[0].find_all("li")
        self.assertEqual(question1.text, "question1")
        self.assertEqual(question2.text, "question2")

        # Check if there are delete and update buttons
        delete_url = reverse(
            "gemeentes:antwoord_delete",
            kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": utter1.pk},
        )
        update_url = reverse(
            "gemeentes:antwoord_update",
            kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": utter1.pk},
        )
        delete_button, update_button = rows[0].find_all("a")
        self.assertEqual(delete_url, delete_button.get("href"))
        self.assertEqual(update_url, update_button.get("href"))

    def test_utterresponse_local_list_filter_by_overig(self):
        utter1 = UtterResponseFactory.create(gemeente=self.gemeente, utter="utter_lo_3")

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                f"{self.antwoord_list_url}?category=overig", user=self.user
            )

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 2)

        # Check if there are delete and update buttons
        delete_url = reverse(
            "gemeentes:antwoord_delete",
            kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": utter1.pk},
        )
        update_url = reverse(
            "gemeentes:antwoord_update",
            kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": utter1.pk},
        )
        delete_button, update_button = rows[0].find_all("a")
        self.assertEqual(delete_url, delete_button.get("href"))
        self.assertEqual(update_url, update_button.get("href"))

        # Check if there is a link to the detail page
        detail_url = reverse(
            "gemeentes:antwoord_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        create_button = rows[1].find("a")
        self.assertIn(f"{detail_url}?utter_name=utter_lo_4", create_button.get("href"))

    def test_utterresponse_local_list_use_proper_default(self):
        UtterResponseFactory.create(
            gemeente=None, utter="utter_lo_1", response="default utter_lo_1"
        )
        UtterResponseFactory.create(gemeente=None, utter="utter_lo_2", response="text2")

        with requests_mock.Mocker() as m:
            m.get(
                DOMAIN_URL,
                json=dict(
                    responses=dict(list(LOCAL_UTTER_RESPONSES["responses"].items())[:2])
                ),
                headers=HEADERS,
            )

            response = self.app.get(self.antwoord_list_url, user=self.user)

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 2)

        # Use local default from database
        self.assertEqual(rows[0].find_all("td")[1].text, "default utter_lo_1")

        # Use local default from database
        self.assertEqual(rows[1].find_all("td")[1].text, "text2")

    def test_utterresponse_local_list_login_required(self):
        response = self.app.get(self.antwoord_list_url)
        self.assertEqual(response.status_code, 302)
        self.assertIn(self.login_url, response.location)


class UtterResponseListSortingTests(TwoFactorTestCaseMixin, WebTest):
    login_url = reverse("two_factor:login")

    def setUp(self):
        super().setUp()

        self.gemeente = GemeenteFactory(name="Amsterdam")
        self.user.gemeentes.add(self.gemeente)

        self.antwoord_list_url = reverse(
            "gemeentes:antwoord_list", kwargs={"gemeente_slug": self.gemeente.slug}
        )

        UtterResponseFactory.create(gemeente=None, utter="utter_lo_1", response="text1")
        UtterResponseFactory.create(gemeente=None, utter="utter_lo_2", response="text2")
        UtterResponseFactory.create(gemeente=None, utter="utter_lo_3", response="text3")
        UtterResponseFactory.create(gemeente=None, utter="utter_lo_4", response="text4")

        # Ensure cache is emptied between tests
        cache.delete("domain_tag")

    def test_utterresponse_list_default_sort_by_utter_name(self):
        UtterResponseFactory.create(
            gemeente=self.gemeente, utter="utter_lo_1", response="zzz"
        )

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(f"{self.antwoord_list_url}", user=self.user)

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 4)

        self.assertEqual(rows[0].find_all("td")[1].text, "zzz")
        self.assertEqual(rows[1].find_all("td")[1].text, "text2")
        self.assertEqual(rows[2].find_all("td")[1].text, "text3")
        self.assertEqual(rows[3].find_all("td")[1].text, "text4")

    def test_utterresponse_list_default_sort_by_response_desc(self):
        UtterResponseFactory.create(
            gemeente=self.gemeente, utter="utter_lo_1", response="zzz"
        )

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                f"{self.antwoord_list_url}?sort=response&order=desc", user=self.user
            )

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 4)

        self.assertEqual(rows[0].find_all("td")[1].text, "zzz")
        self.assertEqual(rows[1].find_all("td")[1].text, "text4")
        self.assertEqual(rows[2].find_all("td")[1].text, "text3")
        self.assertEqual(rows[3].find_all("td")[1].text, "text2")


class UtterResponseChangedOverviewTests(TwoFactorTestCaseMixin, WebTest):
    login_url = reverse("two_factor:login")

    def setUp(self):
        super().setUp()

        self.gemeente = GemeenteFactory(name="Amsterdam")
        self.user.gemeentes.add(self.gemeente)

        self.antwoord_changed_overview_url = reverse(
            "gemeentes:antwoord_changed_overview",
            kwargs={"gemeente_slug": self.gemeente.slug},
        )

        cache.delete("domain_tag")

    def test_utterresponse_changed_overview(self):
        UtterResponseFactory.create(
            gemeente=None, utter="utter_lo_1", response="default"
        )
        UtterResponseFactory.create(
            gemeente=None, utter="utter_lo_2", response="default"
        )

        utter1 = UtterResponseFactory.create(
            gemeente=self.gemeente, utter="utter_lo_1", response="override"
        )
        utter2 = UtterResponseFactory.create(
            gemeente=self.gemeente, utter="utter_lo_2", response="override"
        )

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(self.antwoord_changed_overview_url, user=self.user)

        rows = response.html("tr")[1:]

        # Only locally overriden should show up
        self.assertEqual(len(rows), 2)

        timestamps = ["01-01-2021", "01-01-2020"]

        for i, utter in enumerate([utter1, utter2]):
            with self.subTest(utter=utter):
                question1, question2 = rows[i].find_all("li")
                self.assertEqual(question1.text, "question1")
                self.assertEqual(question2.text, "question2")

                self.assertEqual(rows[i].find_all("td")[1].text, "override")
                self.assertEqual(rows[i].find_all("td")[2].text, "default")
                self.assertEqual(rows[i].find_all("td")[3].text.strip(), timestamps[i])

                # Check if there are delete and update buttons
                delete_url = reverse(
                    "gemeentes:antwoord_delete",
                    kwargs={
                        "gemeente_slug": self.gemeente.slug,
                        "antwoord_pk": utter.pk,
                    },
                )
                update_url = reverse(
                    "gemeentes:antwoord_update",
                    kwargs={
                        "gemeente_slug": self.gemeente.slug,
                        "antwoord_pk": utter.pk,
                    },
                )
                delete_button, update_button = rows[i].find_all("a")
                self.assertEqual(delete_url, delete_button.get("href"))
                self.assertEqual(update_url, update_button.get("href"))

                accept_button = rows[i].find("button")
                self.assertIsNone(accept_button)

    def test_utterresponse_changed_overview_modified_on_none_ascending(self):
        local_utter_responses = deepcopy(LOCAL_UTTER_RESPONSES)
        local_utter_responses["responses"]["utter_lo_2"][1]["custom"]["metadata"][
            "changed"
        ] = None

        UtterResponseFactory.create(
            gemeente=None, utter="utter_lo_1", response="default"
        )
        UtterResponseFactory.create(
            gemeente=None, utter="utter_lo_2", response="default"
        )
        UtterResponseFactory.create(
            gemeente=None, utter="utter_lo_3", response="default"
        )

        utter1 = UtterResponseFactory.create(
            gemeente=self.gemeente, utter="utter_lo_1", response="override"
        )
        utter2 = UtterResponseFactory.create(
            gemeente=self.gemeente, utter="utter_lo_2", response="override"
        )
        utter3 = UtterResponseFactory.create(
            gemeente=self.gemeente, utter="utter_lo_3", response="override"
        )

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=local_utter_responses, headers=HEADERS)

            response = self.app.get(
                f"{self.antwoord_changed_overview_url}?sort=modified_on&order=asc",
                user=self.user,
            )

        rows = response.html("tr")[1:]

        # Utters with modified_on=None should not show up
        self.assertEqual(len(rows), 3)

        timestamps = ["01-01-2019", "01-01-2021", ""]

        for i, utter in enumerate([utter3, utter1, utter2]):
            with self.subTest(utter=utter):
                self.assertEqual(rows[i].find_all("td")[1].text, "override")
                self.assertEqual(rows[i].find_all("td")[2].text, "default")
                self.assertEqual(rows[i].find_all("td")[3].text.strip(), timestamps[i])

                # Check if there are delete and update buttons
                delete_url = reverse(
                    "gemeentes:antwoord_delete",
                    kwargs={
                        "gemeente_slug": self.gemeente.slug,
                        "antwoord_pk": utter.pk,
                    },
                )
                update_url = reverse(
                    "gemeentes:antwoord_update",
                    kwargs={
                        "gemeente_slug": self.gemeente.slug,
                        "antwoord_pk": utter.pk,
                    },
                )
                delete_button, update_button = rows[i].find_all("a")
                self.assertEqual(delete_url, delete_button.get("href"))
                self.assertEqual(update_url, update_button.get("href"))

    def test_utterresponse_changed_overview_modified_on_none_descending(self):
        local_utter_responses = deepcopy(LOCAL_UTTER_RESPONSES)
        local_utter_responses["responses"]["utter_lo_2"][1]["custom"]["metadata"][
            "changed"
        ] = None

        UtterResponseFactory.create(
            gemeente=None, utter="utter_lo_1", response="default"
        )
        UtterResponseFactory.create(
            gemeente=None, utter="utter_lo_2", response="default"
        )
        UtterResponseFactory.create(
            gemeente=None, utter="utter_lo_3", response="default"
        )

        utter1 = UtterResponseFactory.create(
            gemeente=self.gemeente, utter="utter_lo_1", response="override"
        )
        utter2 = UtterResponseFactory.create(
            gemeente=self.gemeente, utter="utter_lo_2", response="override"
        )
        utter3 = UtterResponseFactory.create(
            gemeente=self.gemeente, utter="utter_lo_3", response="override"
        )

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=local_utter_responses, headers=HEADERS)

            response = self.app.get(
                f"{self.antwoord_changed_overview_url}?sort=modified_on&order=desc",
                user=self.user,
            )

        rows = response.html("tr")[1:]

        # Utters with modified_on=None should not show up
        self.assertEqual(len(rows), 3)

        timestamps = ["01-01-2021", "01-01-2019", ""]

        for i, utter in enumerate([utter1, utter3, utter2]):
            with self.subTest(utter=utter):
                self.assertEqual(rows[i].find_all("td")[1].text, "override")
                self.assertEqual(rows[i].find_all("td")[2].text, "default")
                self.assertEqual(rows[i].find_all("td")[3].text.strip(), timestamps[i])

                warning_icon = rows[i].find_all("td")[3].find("i")
                self.assertIsNone(warning_icon)

                # Check if there are delete and update buttons
                delete_url = reverse(
                    "gemeentes:antwoord_delete",
                    kwargs={
                        "gemeente_slug": self.gemeente.slug,
                        "antwoord_pk": utter.pk,
                    },
                )
                update_url = reverse(
                    "gemeentes:antwoord_update",
                    kwargs={
                        "gemeente_slug": self.gemeente.slug,
                        "antwoord_pk": utter.pk,
                    },
                )
                delete_button, update_button = rows[i].find_all("a")
                self.assertEqual(delete_url, delete_button.get("href"))
                self.assertEqual(update_url, update_button.get("href"))

    def test_utterresponse_changed_overview_search_query_on_questions(self):
        utter1 = UtterResponseFactory.create(gemeente=self.gemeente, utter="utter_lo_1")
        UtterResponseFactory.create(gemeente=None, utter="utter_lo_1")

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                f"{self.antwoord_changed_overview_url}?search=question1", user=self.user
            )

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 1)

        question1, question2 = rows[0].find_all("li")
        self.assertEqual(question1.text, "question1")
        self.assertEqual(question2.text, "question2")

        # Check if there are delete and update buttons
        delete_url = reverse(
            "gemeentes:antwoord_delete",
            kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": utter1.pk},
        )
        update_url = reverse(
            "gemeentes:antwoord_update",
            kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": utter1.pk},
        )
        delete_button, update_button = rows[0].find_all("a")
        self.assertEqual(delete_url, delete_button.get("href"))
        self.assertEqual(update_url, update_button.get("href"))

    def test_utterresponse_changed_overview_search_query_on_response(self):
        utter1 = UtterResponseFactory.create(gemeente=self.gemeente, utter="utter_lo_1")
        UtterResponseFactory.create(gemeente=None, utter="utter_lo_1")

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                f"{self.antwoord_changed_overview_url}?search=text1", user=self.user
            )

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 1)

        question1, question2 = rows[0].find_all("li")
        self.assertEqual(question1.text, "question1")
        self.assertEqual(question2.text, "question2")

        # Check if there are delete and update buttons
        delete_url = reverse(
            "gemeentes:antwoord_delete",
            kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": utter1.pk},
        )
        update_url = reverse(
            "gemeentes:antwoord_update",
            kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": utter1.pk},
        )
        delete_button, update_button = rows[0].find_all("a")
        self.assertEqual(delete_url, delete_button.get("href"))
        self.assertEqual(update_url, update_button.get("href"))

    def test_utterresponse_changed_overview_search_query_on_local_default_response(
        self,
    ):
        UtterResponseFactory.create(
            gemeente=None, utter="utter_lo_2", response="some local default"
        )
        utter2 = UtterResponseFactory.create(
            gemeente=self.gemeente, utter="utter_lo_2", response="some local override"
        )

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                f"{self.antwoord_changed_overview_url}?search=some+local+default",
                user=self.user,
            )

        rows = response.html("tr")[1:]

        # utter_lo_2 should show up
        self.assertEqual(len(rows), 1)

        question1, question2 = rows[0].find_all("li")
        self.assertEqual(question1.text, "question1")
        self.assertEqual(question2.text, "question2")

        # Check if there is a link to the detail page
        # Check if there are delete and update buttons
        delete_url = reverse(
            "gemeentes:antwoord_delete",
            kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": utter2.pk},
        )
        update_url = reverse(
            "gemeentes:antwoord_update",
            kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": utter2.pk},
        )
        delete_button, update_button = rows[0].find_all("a")
        self.assertEqual(delete_url, delete_button.get("href"))
        self.assertEqual(update_url, update_button.get("href"))

    def test_utterresponse_changed_overview_mark_outdated_responses(self):
        UtterResponseFactory.create(
            gemeente=None, utter="utter_lo_1", response="default"
        )
        UtterResponseFactory.create(
            gemeente=None, utter="utter_lo_2", response="default"
        )

        with freeze_time("2010-01-01T12:00:00"):
            utter1 = UtterResponseFactory.create(
                gemeente=self.gemeente, utter="utter_lo_1", response="override"
            )
            utter2 = UtterResponseFactory.create(
                gemeente=self.gemeente, utter="utter_lo_2", response="override"
            )

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(self.antwoord_changed_overview_url, user=self.user)

        rows = response.html("tr")[1:]

        # Only locally overriden should show up
        self.assertEqual(len(rows), 2)

        timestamps = ["01-01-2021", "01-01-2020"]

        for i, utter in enumerate([utter1, utter2]):
            with self.subTest(utter=utter):
                question1, question2 = rows[i].find_all("li")
                self.assertEqual(question1.text, "question1")
                self.assertEqual(question2.text, "question2")

                self.assertEqual(rows[i].find_all("td")[1].text, "override")
                self.assertEqual(rows[i].find_all("td")[2].text, "default")
                self.assertEqual(rows[i].find_all("td")[3].text.strip(), timestamps[i])

                warning_icon = rows[i].find_all("td")[3].find("i")
                self.assertIsNotNone(warning_icon)
                self.assertEqual(
                    warning_icon.attrs["class"],
                    ["utter-changed-warning", "cui-warning", "text-danger"],
                )

                # Check if there are delete and update buttons
                delete_url = reverse(
                    "gemeentes:antwoord_delete",
                    kwargs={
                        "gemeente_slug": self.gemeente.slug,
                        "antwoord_pk": utter.pk,
                    },
                )
                update_url = reverse(
                    "gemeentes:antwoord_update",
                    kwargs={
                        "gemeente_slug": self.gemeente.slug,
                        "antwoord_pk": utter.pk,
                    },
                )
                delete_button, update_button = rows[i].find_all("a")
                self.assertEqual(delete_url, delete_button.get("href"))
                self.assertEqual(update_url, update_button.get("href"))

                accept_button = rows[i].find("button")
                self.assertEqual(accept_button.attrs["data-response"], "override")
                self.assertEqual(accept_button.attrs["data-target-url"], update_url)


class UtterResponseChangedOverviewSortingTests(TwoFactorTestCaseMixin, WebTest):
    login_url = reverse("two_factor:login")

    def setUp(self):
        super().setUp()

        self.gemeente = GemeenteFactory(name="Amsterdam")
        self.user.gemeentes.add(self.gemeente)

        self.antwoord_changed_overview_url = reverse(
            "gemeentes:antwoord_changed_overview",
            kwargs={"gemeente_slug": self.gemeente.slug},
        )

        cache.delete("domain_tag")

    def test_utterresponse_list_default_sort_by_modified_on_desc(self):
        UtterResponseFactory.create(
            gemeente=self.gemeente, utter="utter_lo_1", response="bbb"
        )
        UtterResponseFactory.create(
            gemeente=self.gemeente, utter="utter_lo_2", response="ccc"
        )
        UtterResponseFactory.create(
            gemeente=self.gemeente, utter="utter_lo_3", response="aaa"
        )
        UtterResponseFactory.create(
            gemeente=self.gemeente, utter="utter_lo_4", response="ddd"
        )

        UtterResponseFactory.create(gemeente=None, utter="utter_lo_1")
        UtterResponseFactory.create(gemeente=None, utter="utter_lo_2")
        UtterResponseFactory.create(gemeente=None, utter="utter_lo_3")
        UtterResponseFactory.create(gemeente=None, utter="utter_lo_4")

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                f"{self.antwoord_changed_overview_url}", user=self.user
            )

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 4)

        self.assertEqual(rows[0].find_all("td")[1].text, "bbb")
        self.assertEqual(rows[1].find_all("td")[1].text, "ccc")
        self.assertEqual(rows[2].find_all("td")[1].text, "aaa")
        self.assertEqual(rows[3].find_all("td")[1].text, "ddd")

    def test_utterresponse_list_default_sort_by_modified_on_asc(self):
        UtterResponseFactory.create(
            gemeente=self.gemeente, utter="utter_lo_1", response="bbb"
        )
        UtterResponseFactory.create(
            gemeente=self.gemeente, utter="utter_lo_2", response="ccc"
        )
        UtterResponseFactory.create(
            gemeente=self.gemeente, utter="utter_lo_3", response="aaa"
        )
        UtterResponseFactory.create(
            gemeente=self.gemeente, utter="utter_lo_4", response="ddd"
        )

        UtterResponseFactory.create(gemeente=None, utter="utter_lo_1")
        UtterResponseFactory.create(gemeente=None, utter="utter_lo_2")
        UtterResponseFactory.create(gemeente=None, utter="utter_lo_3")
        UtterResponseFactory.create(gemeente=None, utter="utter_lo_4")

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                f"{self.antwoord_changed_overview_url}?sort=modified_on&order=asc",
                user=self.user,
            )

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 4)

        self.assertEqual(rows[0].find_all("td")[1].text, "ddd")
        self.assertEqual(rows[1].find_all("td")[1].text, "aaa")
        self.assertEqual(rows[2].find_all("td")[1].text, "ccc")
        self.assertEqual(rows[3].find_all("td")[1].text, "bbb")

    def test_utterresponse_list_default_sort_by_local_override(self):
        UtterResponseFactory.create(
            gemeente=self.gemeente, utter="utter_lo_1", response="bbb"
        )
        UtterResponseFactory.create(
            gemeente=self.gemeente, utter="utter_lo_2", response="ccc"
        )
        UtterResponseFactory.create(
            gemeente=self.gemeente, utter="utter_lo_3", response="aaa"
        )
        UtterResponseFactory.create(
            gemeente=self.gemeente, utter="utter_lo_4", response="ddd"
        )

        UtterResponseFactory.create(gemeente=None, utter="utter_lo_1")
        UtterResponseFactory.create(gemeente=None, utter="utter_lo_2")
        UtterResponseFactory.create(gemeente=None, utter="utter_lo_3")
        UtterResponseFactory.create(gemeente=None, utter="utter_lo_4")

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                f"{self.antwoord_changed_overview_url}?sort=response&order=asc",
                user=self.user,
            )

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 4)

        self.assertEqual(rows[0].find_all("td")[1].text, "aaa")
        self.assertEqual(rows[1].find_all("td")[1].text, "bbb")
        self.assertEqual(rows[2].find_all("td")[1].text, "ccc")
        self.assertEqual(rows[3].find_all("td")[1].text, "ddd")

    def test_utterresponse_list_default_sort_by_local_default(self):
        UtterResponseFactory.create(
            gemeente=None, utter="utter_lo_1", response="response4"
        )
        UtterResponseFactory.create(
            gemeente=None, utter="utter_lo_2", response="response3"
        )
        UtterResponseFactory.create(
            gemeente=None, utter="utter_lo_3", response="response2"
        )
        UtterResponseFactory.create(
            gemeente=None, utter="utter_lo_4", response="response1"
        )

        UtterResponseFactory.create(
            gemeente=self.gemeente, utter="utter_lo_1", response="bbb"
        )
        UtterResponseFactory.create(
            gemeente=self.gemeente, utter="utter_lo_2", response="ccc"
        )
        UtterResponseFactory.create(
            gemeente=self.gemeente, utter="utter_lo_3", response="aaa"
        )
        UtterResponseFactory.create(
            gemeente=self.gemeente, utter="utter_lo_4", response="ddd"
        )

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                f"{self.antwoord_changed_overview_url}?sort=default_response&order=asc",
                user=self.user,
            )

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 4)

        self.assertEqual(rows[0].find_all("td")[1].text, "ddd")
        self.assertEqual(rows[1].find_all("td")[1].text, "aaa")
        self.assertEqual(rows[2].find_all("td")[1].text, "ccc")
        self.assertEqual(rows[3].find_all("td")[1].text, "bbb")


class UtterResponseCreatedOverviewTests(TwoFactorTestCaseMixin, WebTest):
    login_url = reverse("two_factor:login")

    def setUp(self):
        super().setUp()

        self.gemeente = GemeenteFactory(name="Amsterdam")
        self.user.gemeentes.add(self.gemeente)

        self.antwoord_created_overview_url = reverse(
            "gemeentes:antwoord_created_overview",
            kwargs={"gemeente_slug": self.gemeente.slug},
        )

        cache.delete("domain_tag")

    def test_utterresponse_created_overview(self):
        UtterResponseFactory.create(
            gemeente=None, utter="utter_lo_1", response="default"
        )
        UtterResponseFactory.create(
            gemeente=None, utter="utter_lo_2", response="default"
        )

        UtterResponseFactory.create(
            gemeente=self.gemeente, utter="utter_lo_1", response="override"
        )

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(self.antwoord_created_overview_url, user=self.user)

        rows = response.html("tr")[1:]

        # Only non locally overriden should show up
        self.assertEqual(len(rows), 1)

        # for i, utter in enumerate([utter1, utter2]):
        #     with self.subTest(utter=utter):
        question1, question2 = rows[0].find_all("li")
        self.assertEqual(question1.text, "question1")
        self.assertEqual(question2.text, "question2")

        self.assertEqual(rows[0].find_all("td")[1].text, "default")
        self.assertEqual(rows[0].find_all("td")[2].text, "05-05-2019")

        # Check if there are delete and update buttons
        create_url = reverse(
            "gemeentes:antwoord_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        create_button = rows[0].find("a")
        self.assertEqual(
            f"{create_url}?utter_name=utter_lo_2", create_button.get("href")
        )

    def test_utterresponse_created_overview_search_query_on_questions_match(self):
        # utter1 = UtterResponseFactory.create(gemeente=self.gemeente, utter='utter_lo_1')
        UtterResponseFactory.create(gemeente=None, utter="utter_lo_1")
        UtterResponseFactory.create(gemeente=None, utter="utter_lo_2")

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                f"{self.antwoord_created_overview_url}?search=question1", user=self.user
            )

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 2)

    def test_utterresponse_created_overview_search_query_on_questions_no_match(self):
        # utter1 = UtterResponseFactory.create(gemeente=self.gemeente, utter='utter_lo_1')
        UtterResponseFactory.create(gemeente=None, utter="utter_lo_1")
        UtterResponseFactory.create(gemeente=None, utter="utter_lo_2")

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                f"{self.antwoord_created_overview_url}?search=foo", user=self.user
            )

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 0)

    def test_utterresponse_created_overview_search_query_on_local_default_response(
        self,
    ):
        UtterResponseFactory.create(
            gemeente=None, utter="utter_lo_2", response="some local default"
        )
        UtterResponseFactory.create(gemeente=None, utter="utter_lo_1", response="...")
        # utter2 = UtterResponseFactory.create(gemeente=self.gemeente, utter='utter_lo_2', response="some local override")

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                f"{self.antwoord_created_overview_url}?search=some+local+default",
                user=self.user,
            )

        rows = response.html("tr")[1:]

        # utter_lo_2 should show up
        self.assertEqual(len(rows), 1)

        # Check if there are delete and update buttons
        create_url = reverse(
            "gemeentes:antwoord_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        create_button = rows[0].find("a")
        self.assertEqual(
            f"{create_url}?utter_name=utter_lo_2", create_button.get("href")
        )


class UtterResponseCreatedOverviewSortingTests(TwoFactorTestCaseMixin, WebTest):
    login_url = reverse("two_factor:login")

    def setUp(self):
        super().setUp()

        self.gemeente = GemeenteFactory(name="Amsterdam")
        self.user.gemeentes.add(self.gemeente)

        self.antwoord_created_overview_url = reverse(
            "gemeentes:antwoord_created_overview",
            kwargs={"gemeente_slug": self.gemeente.slug},
        )

        cache.delete("domain_tag")

    def test_utterresponse_list_default_sort_by_created_desc(self):
        UtterResponseFactory.create(gemeente=None, utter="utter_lo_1", response="bbb")
        UtterResponseFactory.create(gemeente=None, utter="utter_lo_2", response="ccc")

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                f"{self.antwoord_created_overview_url}", user=self.user
            )

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 2)

        self.assertEqual(rows[0].find_all("td")[1].text, "bbb")
        self.assertEqual(rows[1].find_all("td")[1].text, "ccc")

    def test_utterresponse_list_default_sort_by_created_asc(self):
        UtterResponseFactory.create(gemeente=None, utter="utter_lo_1", response="bbb")
        UtterResponseFactory.create(gemeente=None, utter="utter_lo_2", response="ccc")

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                f"{self.antwoord_created_overview_url}?sort=created&order=asc",
                user=self.user,
            )

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 2)

        self.assertEqual(rows[0].find_all("td")[1].text, "ccc")
        self.assertEqual(rows[1].find_all("td")[1].text, "bbb")

    def test_utterresponse_list_default_sort_by_local_default(self):
        UtterResponseFactory.create(
            gemeente=None, utter="utter_lo_1", response="response4"
        )
        UtterResponseFactory.create(
            gemeente=None, utter="utter_lo_2", response="response3"
        )

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                f"{self.antwoord_created_overview_url}?sort=default_response&order=asc",
                user=self.user,
            )

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 2)

        self.assertEqual(rows[0].find_all("td")[1].text, "response3")
        self.assertEqual(rows[1].find_all("td")[1].text, "response4")


class UtterResponseReadTests(TwoFactorTestCaseMixin, WebTest):
    login_url = reverse("two_factor:login")

    def setUp(self):
        super().setUp()

        self.gemeente = GemeenteFactory(name="Amsterdam")
        self.user.gemeentes.add(self.gemeente)

        self.antwoord_list_url = reverse(
            "gemeentes:antwoord_list", kwargs={"gemeente_slug": self.gemeente.slug}
        )

    def test_utterresponse_read(self):
        utter = UtterResponseFactory.create(
            utter="utter_ask",
            gemeente=self.gemeente,
        )
        response = self.app.get(
            reverse(
                "gemeentes:antwoord_read",
                kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": utter.pk},
            ),
            user=self.user,
        )

        self.assertEqual(response.status_code, 200)

        self.assertIn(utter.utter, response.text)

        # Check if there are delete and update buttons
        delete_button, update_button = response.html.find(
            "div", {"id": "ui-view"}
        ).find_all("a", {"class": "btn"})
        delete_url = reverse(
            "gemeentes:antwoord_delete",
            kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": utter.pk},
        )
        update_url = reverse(
            "gemeentes:antwoord_update",
            kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": utter.pk},
        )
        self.assertEqual(delete_button.get("href"), delete_url)
        self.assertEqual(update_button.get("href"), update_url)

    def test_utterresponse_read_login_required(self):
        utter = UtterResponseFactory.create(
            gemeente=self.gemeente,
        )
        response = self.app.get(
            reverse(
                "gemeentes:antwoord_read",
                kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": utter.pk},
            ),
        )
        self.assertEqual(response.status_code, 302)
        self.assertIn(self.login_url, response.location)


class UtterResponseCreateTests(TwoFactorTestCaseMixin, WebTest):
    login_url = reverse("two_factor:login")

    def setUp(self):
        super().setUp()

        self.gemeente = GemeenteFactory(name="Amsterdam")
        self.user.gemeentes.add(self.gemeente)

        self.antwoord_list_url = reverse(
            "gemeentes:antwoord_list", kwargs={"gemeente_slug": self.gemeente.slug}
        )

        cache.clear()

    def test_utterresponse_create(self):
        ChannelFactory.create()
        create_url = reverse(
            "gemeentes:antwoord_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json={"responses": {"utter_lo_ask": {}}}, headers=HEADERS)

            response = self.app.get(
                f"{create_url}?utter_name=utter_lo_ask", user=self.user
            )

        form = response.forms[1]
        form["response"] = "<h1>no comprendo</h1>"
        # form['image'] = 'https://media1.tenor.com/images/d09906a9990dd5064e644e6acefedb6f/tenor.gif?itemid=13491038'
        # form['channel'] = channel.pk

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json={"responses": {"utter_ask": {}}}, headers=HEADERS)

            response = form.submit()

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.location, f"{self.antwoord_list_url}?category=overig")

        self.assertEqual(UtterResponse.objects.count(), 1)

        utter_response = UtterResponse.objects.last()

        # Verify that the utter itself cannot be changed
        self.assertEqual(utter_response.utter, "utter_lo_ask")

        self.assertEqual(utter_response.response, "<h1>no comprendo</h1>")
        # self.assertEqual(utter_response.image, 'https://media1.tenor.com/images/d09906a9990dd5064e644e6acefedb6f/tenor.gif?itemid=13491038')
        # self.assertEqual(utter_response.channel, channel)
        self.assertEqual(utter_response.gemeente, self.gemeente)

    def test_utterresponse_create_already_exists(self):
        ChannelFactory.create()
        UtterResponseFactory.create(
            utter="utter_lo_ask", gemeente=self.gemeente, response="original"
        )
        create_url = reverse(
            "gemeentes:antwoord_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json={"responses": {"utter_lo_ask": {}}}, headers=HEADERS)

            response = self.app.get(
                f"{create_url}?utter_name=utter_lo_ask", user=self.user
            )

        form = response.forms[1]
        form["response"] = "<h1>no comprendo</h1>"
        # form['image'] = 'https://media1.tenor.com/images/d09906a9990dd5064e644e6acefedb6f/tenor.gif?itemid=13491038'
        # form['channel'] = channel.pk

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json={"responses": {"utter_lo_ask": {}}}, headers=HEADERS)

            response = form.submit()

        self.assertEqual(response.status_code, 200)

        error = response.html.find("div", {"class": "alert"})
        self.assertEqual(
            error.text.strip(), _("Dit antwoord bestaat al voor deze gemeente")
        )

        self.assertEqual(UtterResponse.objects.count(), 1)

        utter_response = UtterResponse.objects.last()

        # Verify that the utter itself cannot be changed
        self.assertEqual(utter_response.utter, "utter_lo_ask")
        self.assertEqual(utter_response.response, "original")

    def test_utterresponse_create_display_metadata(self):
        ChannelFactory.create()
        create_url = reverse(
            "gemeentes:antwoord_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )

        with requests_mock.Mocker() as m:
            m.get(
                DOMAIN_URL,
                json={
                    "responses": {
                        "utter_lo_ask": [
                            {"text": "landelijk"},
                            {
                                "channel": "metadata",
                                "custom": {
                                    "default": [{"text": "something"}],
                                    "metadata": {
                                        "examples": ["example text"],
                                        "helptext": "help text",
                                        "questions": ["question1", "question2"],
                                        "related": ["utter1", "utter2"],
                                        "required": True,
                                    },
                                },
                            },
                        ]
                    }
                },
                headers=HEADERS,
            )

            response = self.app.get(
                f"{create_url}?utter_name=utter_lo_ask", user=self.user
            )

        self.assertEqual(response.status_code, 200)

        metadata = response.html.find("table")

        rows = metadata.find_all("tr")

        self.assertIn("question1", rows[0].find_all("td")[-1].text)
        self.assertIn("question2", rows[0].find_all("td")[-1].text)
        self.assertEqual(rows[1].find_all("li")[-1].text, "example text")
        self.assertIn("utter1", rows[2].find_all("td")[-1].text)
        self.assertIn("utter2", rows[2].find_all("td")[-1].text)

        help_text = response.html.find("p", {"id": "hint_id_response"})
        self.assertEqual(help_text.text, "help text")

    def test_utterresponse_create_display_metadata_api_500_error(self):
        ChannelFactory.create()
        create_url = reverse(
            "gemeentes:antwoord_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, status_code=500)

            response = self.app.get(
                f"{create_url}?utter_name=utter_ask", user=self.user
            )

        self.assertEqual(response.status_code, 200)

        metadata = response.html.find("table", {"id": "utter-metadata"})

        self.assertIsNone(metadata)

    def test_utterresponse_create_login_required(self):
        response = self.app.get(
            reverse(
                "gemeentes:antwoord_create",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                },
            ),
        )
        self.assertEqual(response.status_code, 302)
        self.assertIn(self.login_url, response.location)

    def test_utterresponse_create_redirects(self):
        created_overview_uri = reverse(
            "gemeentes:antwoord_created_overview",
            kwargs={"gemeente_slug": self.gemeente.slug},
        )
        created_overview_url = f"http://testserver{created_overview_uri}"

        default_utter_overview_uri = reverse(
            "gemeentes:antwoord_created_overview",
            kwargs={"gemeente_slug": self.gemeente.slug},
        )
        default_utter_overview_url = (
            f"http://testserver{default_utter_overview_uri}?category=verhuismelding"
        )

        ChannelFactory.create()
        create_url = reverse(
            "gemeentes:antwoord_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )

        for referrer in [created_overview_url, default_utter_overview_url]:
            with self.subTest(referrer=referrer):
                UtterResponse.objects.all().delete()

                with requests_mock.Mocker() as m:
                    m.get(
                        DOMAIN_URL,
                        json={"responses": {"utter_lo_ask": {}}},
                        headers=HEADERS,
                    )

                    response = self.app.get(
                        f"{create_url}?utter_name=utter_lo_ask",
                        user=self.user,
                        headers={"Referer": referrer},
                    )

                    form = response.forms[1]
                    form["response"] = "<h1>no comprendo</h1>"

                    response = form.submit()

                self.assertEqual(response.status_code, 302)
                self.assertEqual(response.location, referrer)

                self.assertEqual(UtterResponse.objects.count(), 1)


class UtterResponseUpdateTests(TwoFactorTestCaseMixin, WebTest):
    login_url = reverse("two_factor:login")

    def setUp(self):
        super().setUp()

        self.gemeente = GemeenteFactory(name="Amsterdam")
        self.user.gemeentes.add(self.gemeente)

        self.antwoord_list_url = reverse(
            "gemeentes:antwoord_list", kwargs={"gemeente_slug": self.gemeente.slug}
        )

        cache.clear()

    def test_utterresponse_update(self):
        channel = ChannelFactory.create()
        utter_response = UtterResponseFactory(
            utter="utter_lo_1",
            gemeente=self.gemeente,
            channel=channel,
        )

        with requests_mock.Mocker() as m:
            m.get(
                DOMAIN_URL,
                json={
                    "responses": {
                        "utter_lo_1": LOCAL_UTTER_RESPONSES["responses"]["utter_lo_1"]
                    }
                },
            )

            response = self.app.get(
                reverse(
                    "gemeentes:antwoord_update",
                    kwargs={
                        "gemeente_slug": self.gemeente.slug,
                        "antwoord_pk": utter_response.pk,
                    },
                ),
                user=self.user,
            )

            form = response.forms[1]
            form["response"] = "same"
            # form['image'] = 'https://google.com/someimg.jpg'
            # form['channel'] = channel.pk

            response = form.submit()

        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            response.location, f"{self.antwoord_list_url}?category=verhuismelding"
        )

        self.assertEqual(UtterResponse.objects.count(), 1)

        utter_response.refresh_from_db()

        # Verify that the utter itself cannot be changed
        self.assertEqual(utter_response.utter, "utter_lo_1")

        self.assertEqual(utter_response.response, "same")
        # self.assertEqual(utter_response.image, 'https://google.com/someimg.jpg')
        # self.assertEqual(utter_response.channel, channel)
        self.assertEqual(utter_response.gemeente, self.gemeente)

    def test_utterresponse_update_ajax_call(self):
        channel = ChannelFactory.create()
        utter_response = UtterResponseFactory(
            utter="utter_lo_1",
            gemeente=self.gemeente,
            channel=channel,
        )

        with requests_mock.Mocker() as m:
            m.get(
                DOMAIN_URL,
                json={
                    "responses": {
                        "utter_lo_1": LOCAL_UTTER_RESPONSES["responses"]["utter_lo_1"]
                    }
                },
            )

            response = self.app.get(
                reverse(
                    "gemeentes:antwoord_update",
                    kwargs={
                        "gemeente_slug": self.gemeente.slug,
                        "antwoord_pk": utter_response.pk,
                    },
                ),
                user=self.user,
            )

            form = response.forms[1]
            form["response"] = "same"

            response = form.submit(headers={"X-Requested-With": "XMLHttpRequest"})

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.location, "")

        self.assertEqual(UtterResponse.objects.count(), 1)

        utter_response.refresh_from_db()

        # Verify that the utter itself cannot be changed
        self.assertEqual(utter_response.utter, "utter_lo_1")

        self.assertEqual(utter_response.response, "same")
        self.assertEqual(utter_response.gemeente, self.gemeente)

    def test_utterresponse_update_display_metadata(self):
        channel = ChannelFactory.create()
        utter_response = UtterResponseFactory(
            utter="utter_lo_greet",
            gemeente=self.gemeente,
            channel=channel,
        )

        with requests_mock.Mocker() as m:
            m.get(
                DOMAIN_URL,
                json={
                    "responses": {
                        "utter_lo_greet": [
                            {"text": "landelijke default"},
                            {
                                "channel": "metadata",
                                "custom": {
                                    "response": "something",
                                    "metadata": {
                                        "examples": ["example text"],
                                        "helptext": "help text",
                                        "questions": ["question1", "question2"],
                                        "related": ["utter1", "utter2"],
                                        "required": True,
                                    },
                                },
                            },
                        ]
                    }
                },
            )

            response = self.app.get(
                reverse(
                    "gemeentes:antwoord_update",
                    kwargs={
                        "gemeente_slug": self.gemeente.slug,
                        "antwoord_pk": utter_response.pk,
                    },
                ),
                user=self.user,
            )

        self.assertEqual(response.status_code, 200)

        metadata = response.html.find("table")

        rows = metadata.find_all("tr")

        self.assertIn("question1", rows[0].find_all("td")[-1].text)
        self.assertIn("question2", rows[0].find_all("td")[-1].text)
        self.assertEqual(rows[1].find_all("li")[-1].text, "example text")
        self.assertIn("utter1", rows[2].find_all("td")[-1].text)
        self.assertIn("utter2", rows[2].find_all("td")[-1].text)

        help_text = response.html.find("p", {"id": "hint_id_response"})
        self.assertEqual(help_text.text, "help text")

    def test_utterresponse_update_display_metadata_api_500_error(self):
        channel = ChannelFactory.create()
        utter_response = UtterResponseFactory(
            utter="utter_greet",
            gemeente=self.gemeente,
            channel=channel,
        )

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, status_code=500)

            response = self.app.get(
                reverse(
                    "gemeentes:antwoord_update",
                    kwargs={
                        "gemeente_slug": self.gemeente.slug,
                        "antwoord_pk": utter_response.pk,
                    },
                ),
                user=self.user,
            )

        self.assertEqual(response.status_code, 200)

        metadata = response.html.find("table", {"id": "utter-metadata"})

        self.assertIsNone(metadata)

    def test_utterresponse_update_login_required(self):
        utter_response = UtterResponseFactory(gemeente=self.gemeente)
        response = self.app.get(
            reverse(
                "gemeentes:antwoord_update",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "antwoord_pk": utter_response.pk,
                },
            ),
        )
        self.assertEqual(response.status_code, 302)
        self.assertIn(self.login_url, response.location)

    def test_utterresponse_update_redirects(self):
        changed_overview_uri = reverse(
            "gemeentes:antwoord_changed_overview",
            kwargs={"gemeente_slug": self.gemeente.slug},
        )
        changed_overview_url = f"http://testserver{changed_overview_uri}"

        default_utter_overview_uri = reverse(
            "gemeentes:antwoord_created_overview",
            kwargs={"gemeente_slug": self.gemeente.slug},
        )
        default_utter_overview_url = (
            f"http://testserver{default_utter_overview_uri}?category=verhuismelding"
        )

        channel = ChannelFactory.create()
        utter_response = UtterResponseFactory(
            utter="utter_lo_1",
            gemeente=self.gemeente,
            channel=channel,
        )

        for referrer in [changed_overview_url, default_utter_overview_url]:
            with self.subTest(referrer=referrer):
                with requests_mock.Mocker() as m:
                    m.get(
                        DOMAIN_URL,
                        json={
                            "responses": {
                                "utter_lo_1": LOCAL_UTTER_RESPONSES["responses"][
                                    "utter_lo_1"
                                ]
                            }
                        },
                    )

                    response = self.app.get(
                        reverse(
                            "gemeentes:antwoord_update",
                            kwargs={
                                "gemeente_slug": self.gemeente.slug,
                                "antwoord_pk": utter_response.pk,
                            },
                        ),
                        user=self.user,
                        headers={"Referer": referrer},
                    )

                    form = response.forms[1]
                    form["response"] = "same"

                    response = form.submit()

                self.assertEqual(response.status_code, 302)
                self.assertEqual(response.location, referrer)

                self.assertEqual(UtterResponse.objects.count(), 1)


class UtterResponseDeleteTests(TwoFactorTestCaseMixin, WebTest):
    login_url = reverse("two_factor:login")

    def setUp(self):
        super().setUp()

        self.gemeente = GemeenteFactory(name="Amsterdam")
        self.user.gemeentes.add(self.gemeente)

        self.antwoord_list_url = reverse(
            "gemeentes:antwoord_list", kwargs={"gemeente_slug": self.gemeente.slug}
        )

    def test_utterresponse_delete(self):
        utter_response = UtterResponseFactory(
            gemeente=self.gemeente, utter="utter_greet"
        )
        response = self.app.get(
            reverse(
                "gemeentes:antwoord_delete",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "antwoord_pk": utter_response.pk,
                },
            ),
            user=self.user,
        )

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.location, f"{self.antwoord_list_url}?category=overig")

        self.assertEqual(UtterResponse.objects.count(), 0)

    def test_utterresponse_delete_login_required(self):
        utter_response = UtterResponseFactory(gemeente=self.gemeente)
        response = self.app.get(
            reverse(
                "gemeentes:antwoord_delete",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "antwoord_pk": utter_response.pk,
                },
            ),
        )
        self.assertEqual(response.status_code, 302)
        self.assertIn(self.login_url, response.location)

    def test_utterresponse_delete_redirects(self):
        changed_overview_uri = reverse(
            "gemeentes:antwoord_changed_overview",
            kwargs={"gemeente_slug": self.gemeente.slug},
        )
        changed_overview_url = f"http://testserver{changed_overview_uri}"

        default_utter_overview_uri = reverse(
            "gemeentes:antwoord_created_overview",
            kwargs={"gemeente_slug": self.gemeente.slug},
        )
        default_utter_overview_url = (
            f"http://testserver{default_utter_overview_uri}?category=verhuismelding"
        )

        channel = ChannelFactory.create()
        for referrer in [changed_overview_url, default_utter_overview_url]:
            utter_response = UtterResponseFactory(
                utter="utter_lo_1",
                gemeente=self.gemeente,
                channel=channel,
            )
            with self.subTest(referrer=referrer):
                with requests_mock.Mocker() as m:
                    m.get(
                        DOMAIN_URL,
                        json={
                            "responses": {
                                "utter_lo_1": LOCAL_UTTER_RESPONSES["responses"][
                                    "utter_lo_1"
                                ]
                            }
                        },
                    )

                    response = self.app.get(
                        reverse(
                            "gemeentes:antwoord_delete",
                            kwargs={
                                "gemeente_slug": self.gemeente.slug,
                                "antwoord_pk": utter_response.pk,
                            },
                        ),
                        user=self.user,
                        headers={"Referer": referrer},
                    )

                self.assertEqual(response.status_code, 302)
                self.assertEqual(response.location, referrer)

                self.assertEqual(UtterResponse.objects.count(), 0)


class UtterResponseGenericWebTests(TwoFactorTestCaseMixin, WebTest):
    login_url = reverse("two_factor:login")

    def setUp(self):
        super().setUp()

        self.gemeente = GemeenteFactory(name="Amsterdam")
        self.user.gemeentes.add(self.gemeente)

    def test_utterresponse_generic_update_404(self):
        response = self.app.get(
            reverse(
                "gemeentes:antwoord_update",
                kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": 2},
            ),
            user=self.user,
            status=404,
        )

        self.assertEqual(response.status_code, 404)

    def test_utterresponse_generic_delete_404(self):
        utter_response = UtterResponseFactory(gemeente=None)
        response = self.app.get(
            reverse(
                "gemeentes:antwoord_delete",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "antwoord_pk": utter_response.pk,
                },
            ),
            user=self.user,
            status=404,
        )

        self.assertEqual(response.status_code, 404)


@tag("permissions")
class UtterResponsePermissionTests(TwoFactorTestCaseMixin, WebTest):
    login_url = reverse("two_factor:login")
    user_is_admin = False
    user_is_superuser = False

    def setUp(self):
        super().setUp()

        gemeente = GemeenteFactory(name="Amsterdam")
        self.user.gemeentes.add(gemeente)
        self.user.save()

        self.gemeente = GemeenteFactory(name="rotterdam")
        self.antwoord_list_url = reverse(
            "gemeentes:antwoord_list", kwargs={"gemeente_slug": self.gemeente.slug}
        )

        # Ensure cache is emptied between tests
        cache.delete("domain_tag")

    def test_utterresponse_list_no_permission(self):
        # Should not have any influence
        grant_gemeente_perms(
            "admin", self.user, self.gemeente, ["hyperlink_read", "utter_write"]
        )

        self.utter1 = UtterResponseFactory(gemeente=self.gemeente, utter="utter_greet")
        self.utter2 = UtterResponseFactory(gemeente=self.gemeente, utter="utter_ask")

        response = self.app.get(self.antwoord_list_url, user=self.user, status=403)

        self.assertEqual(response.status_code, 403)

    def test_utterresponse_list_no_permission_for_current_gemeente(self):
        # Should not have any influence
        grant_gemeente_perms(
            "admin", self.user, Gemeente.objects.get(name="Amsterdam"), ["utter_read"]
        )

        self.utter1 = UtterResponseFactory(gemeente=self.gemeente, utter="utter_greet")
        self.utter2 = UtterResponseFactory(gemeente=self.gemeente, utter="utter_ask")

        response = self.app.get(self.antwoord_list_url, user=self.user, status=403)

        self.assertEqual(response.status_code, 403)

    def test_utterresponse_list_has_permission(self):
        # Should not have any influence
        grant_gemeente_perms("admin", self.user, self.gemeente, ["utter_read"])

        self.utter1 = UtterResponseFactory(gemeente=self.gemeente, utter="utter_greet")
        self.utter2 = UtterResponseFactory(gemeente=self.gemeente, utter="utter_ask")

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(self.antwoord_list_url, user=self.user)

        self.assertEqual(response.status_code, 200)

    def test_utterresponse_list_has_permission_read_only(self):
        # assign_perm("utters.utter_read", self.user, self.gemeente)
        grant_gemeente_perms("admin", self.user, self.gemeente, ["utter_read"])

        utter1 = UtterResponseFactory.create(gemeente=self.gemeente, utter="utter_lo_1")
        utter2 = UtterResponseFactory.create(gemeente=self.gemeente, utter="utter_lo_2")

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(self.antwoord_list_url, user=self.user)

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 4)

        for i, utter in enumerate([utter1, utter2]):
            with self.subTest(utter=utter):
                question1, question2 = rows[i].find_all("li")
                self.assertEqual(question1.text, "question1")
                self.assertEqual(question2.text, "question2")

                # Update/delete buttons should not appear
                buttons = rows[i].find_all("a")
                self.assertEqual(buttons, [])

        for i, utter in enumerate(list(LOCAL_UTTER_RESPONSES.keys())[2:4]):
            with self.subTest(utter=utter):
                # Create button should not appear
                buttons = rows[i].find_all("a")
                self.assertEqual(buttons, [])

    def test_utterresponse_create_no_permission(self):
        # Should not have any influence
        grant_gemeente_perms(
            "admin", self.user, self.gemeente, ["hyperlink_write", "utter_read"]
        )

        response = self.app.get(
            reverse(
                "gemeentes:antwoord_create",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                },
            ),
            user=self.user,
            status=403,
        )

        self.assertEqual(response.status_code, 403)

    def test_utterresponse_create_has_permission(self):
        # Should not have any influence
        grant_gemeente_perms("admin", self.user, self.gemeente, ["utter_write"])

        response = self.app.get(
            reverse(
                "gemeentes:antwoord_create",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                },
            ),
            user=self.user,
        )

        self.assertEqual(response.status_code, 200)

    def test_utterresponse_update_no_permission(self):
        # Should not have any influence
        grant_gemeente_perms(
            "admin", self.user, self.gemeente, ["hyperlink_write", "utter_read"]
        )

        channel = ChannelFactory.create()
        utter_response = UtterResponseFactory(
            gemeente=self.gemeente,
            channel=channel,
        )
        response = self.app.get(
            reverse(
                "gemeentes:antwoord_update",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "antwoord_pk": utter_response.pk,
                },
            ),
            user=self.user,
            status=403,
        )

        self.assertEqual(response.status_code, 403)

    def test_utterresponse_update_has_permission(self):
        # Should not have any influence
        grant_gemeente_perms("admin", self.user, self.gemeente, ["utter_write"])

        channel = ChannelFactory.create()
        utter_response = UtterResponseFactory(
            gemeente=self.gemeente,
            channel=channel,
        )
        response = self.app.get(
            reverse(
                "gemeentes:antwoord_update",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "antwoord_pk": utter_response.pk,
                },
            ),
            user=self.user,
        )

        self.assertEqual(response.status_code, 200)

    def test_utterresponse_delete_no_permission(self):
        # Should not have any influence
        grant_gemeente_perms(
            "admin", self.user, self.gemeente, ["hyperlink_write", "utter_read"]
        )

        utter_response = UtterResponseFactory(gemeente=self.gemeente)
        response = self.app.get(
            reverse(
                "gemeentes:antwoord_delete",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "antwoord_pk": utter_response.pk,
                },
            ),
            user=self.user,
            status=403,
        )

        self.assertEqual(response.status_code, 403)

    def test_utterresponse_delete_has_permission(self):
        # Should not have any influence
        grant_gemeente_perms("admin", self.user, self.gemeente, ["utter_write"])

        utter_response = UtterResponseFactory(
            gemeente=self.gemeente, utter="utter_greet"
        )
        response = self.app.get(
            reverse(
                "gemeentes:antwoord_delete",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "antwoord_pk": utter_response.pk,
                },
            ),
            user=self.user,
        )

        self.assertEqual(response.status_code, 302)

    def test_utterresponse_read_no_permission(self):
        grant_gemeente_perms(
            "admin", self.user, self.gemeente, ["hyperlink_read", "utter_write"]
        )

        utter = UtterResponseFactory.create(
            utter="utter_ask",
            gemeente=self.gemeente,
        )
        response = self.app.get(
            reverse(
                "gemeentes:antwoord_read",
                kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": utter.pk},
            ),
            user=self.user,
            status=403,
        )

        self.assertEqual(response.status_code, 403)

    def test_utterresponse_read_has_permission(self):
        grant_gemeente_perms("admin", self.user, self.gemeente, ["utter_read"])

        utter = UtterResponseFactory.create(
            utter="utter_ask",
            gemeente=self.gemeente,
        )
        response = self.app.get(
            reverse(
                "gemeentes:antwoord_read",
                kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": utter.pk},
            ),
            user=self.user,
        )

        self.assertEqual(response.status_code, 200)

    def test_utterresponse_read_has_permission_read_only(self):
        grant_gemeente_perms("admin", self.user, self.gemeente, ["utter_read"])

        utter = UtterResponseFactory.create(
            utter="utter_ask",
            gemeente=self.gemeente,
        )
        response = self.app.get(
            reverse(
                "gemeentes:antwoord_read",
                kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": utter.pk},
            ),
            user=self.user,
        )

        self.assertEqual(response.status_code, 200)

        self.assertIn(utter.utter, response.text)

        # Delete and update buttons should not appear
        buttons = response.html.find("div", {"id": "ui-view"}).find_all(
            "a", {"class": "btn"}
        )
        self.assertEqual(buttons, [])

    def test_utterresponse_changed_overview_no_permission(self):
        # Should not have any influence
        grant_gemeente_perms(
            "admin", self.user, self.gemeente, ["hyperlink_read", "utter_write"]
        )

        self.utter1 = UtterResponseFactory(gemeente=self.gemeente, utter="utter_greet")
        self.utter2 = UtterResponseFactory(gemeente=self.gemeente, utter="utter_ask")

        response = self.app.get(
            reverse(
                "gemeentes:antwoord_changed_overview",
                kwargs={"gemeente_slug": self.gemeente.slug},
            ),
            user=self.user,
            status=403,
        )

        self.assertEqual(response.status_code, 403)

    def test_utterresponse_changed_overview_no_permission_for_current_gemeente(self):
        # Should not have any influence
        grant_gemeente_perms(
            "admin", self.user, Gemeente.objects.get(name="Amsterdam"), ["utter_read"]
        )

        self.utter1 = UtterResponseFactory(gemeente=self.gemeente, utter="utter_greet")
        self.utter2 = UtterResponseFactory(gemeente=self.gemeente, utter="utter_ask")

        response = self.app.get(
            reverse(
                "gemeentes:antwoord_changed_overview",
                kwargs={"gemeente_slug": self.gemeente.slug},
            ),
            user=self.user,
            status=403,
        )

        self.assertEqual(response.status_code, 403)

    def test_utterresponse_changed_overview_has_permission(self):
        # Should not have any influence
        grant_gemeente_perms("admin", self.user, self.gemeente, ["utter_read"])

        self.utter1 = UtterResponseFactory(gemeente=self.gemeente, utter="utter_greet")
        self.utter2 = UtterResponseFactory(gemeente=self.gemeente, utter="utter_ask")

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                reverse(
                    "gemeentes:antwoord_changed_overview",
                    kwargs={"gemeente_slug": self.gemeente.slug},
                ),
                user=self.user,
            )

        self.assertEqual(response.status_code, 200)
