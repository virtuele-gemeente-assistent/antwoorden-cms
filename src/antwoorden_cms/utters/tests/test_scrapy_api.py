from unittest import skipIf

from django.conf import settings
from django.urls import reverse
from django.utils.translation import ugettext as _

from rest_framework import status
from rest_framework.test import APITestCase

from antwoorden_cms.config.models import PageMetadata
from antwoorden_cms.config.tests.factories import PageMetadataFactory

from .factories import GemeenteDomainFactory, GemeenteFactory


@skipIf(settings.ENABLE_SCRAPY_ENDPOINT is False, "Scrapy endpoint is enabled")
class ScrapyAPITests(APITestCase):
    def setUp(self):
        self.gemeente = GemeenteFactory.create(
            name="Meierijstad",
            slug="meierijstad",
        )
        self.domain = GemeenteDomainFactory(
            gemeente=self.gemeente,
            domain="meierijstad.nl",
            gem_default_enabled=True,
            direct_handover=False,
        )

        self.url = reverse("antwoorden_cms:scrapy_webhook")

    def test_gemeente_domain_does_not_exist(self):
        data = {
            "url": "https://www.not-meierijstad.nl/folder1/file",
            "title": "Page Title",
            "event": "add",
            "organisation": "not-meierijstad",
            "domain": "www.not-meierijstad.nl",
        }

        response = self.client.post(self.url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "non_field_errors": "No gemeente found for provided domain: {}".format(
                    "not-meierijstad.nl"
                )
            },
        )

    def test_url_not_provided_on_add_update_delete(self):
        data = {
            "title": "Page Title",
            "event": "add",
            "organisation": "not-meierijstad",
            "domain": "www.not-meierijstad.nl",
        }

        response = self.client.post(self.url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {"url": _("URL is required to add, update or delete page")},
        )

        data["event"] = "update"
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {"url": _("URL is required to add, update or delete page")},
        )

        data["event"] = "delete"
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {"url": _("URL is required to add, update or delete page")},
        )

    def test_start_event(self):

        data = {
            "organisation": "meierijstad",
            "domain": "www.meierijstad.nl",
            "event": "start",
        }

        PageMetadataFactory.create_batch(
            10, gemeente_domain=self.domain, page_on_site=True
        )

        with self.subTest("Test partial start event"):
            data["mode"] = "partial"

            response = self.client.post(self.url, data, format="json")
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(
                response.json(),
                {
                    "organisation": "meierijstad",
                    "domain": "www.meierijstad.nl",
                    "event": "start",
                    "mode": "partial",
                    "batch": True,
                },
            )
            self.assertTrue(
                PageMetadata.objects.filter(
                    gemeente_domain=self.domain, page_on_site=True
                ).exists()
            )

        with self.subTest("Test Full start event"):
            data["mode"] = "full"

            response = self.client.post(self.url, data, format="json")
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(
                response.json(),
                {
                    "organisation": "meierijstad",
                    "domain": "www.meierijstad.nl",
                    "event": "start",
                    "mode": "full",
                    "batch": True,
                },
            )
            # all should be set to false
            self.assertFalse(
                PageMetadata.objects.filter(
                    gemeente_domain=self.domain, page_on_site=True
                ).exists()
            )

    def test_end_event(self):

        page_on_site = PageMetadataFactory(
            gemeente_domain=self.domain,
            page_on_site=True,
        )
        page_not_on_site_no_defaults = PageMetadataFactory(
            gemeente_domain=self.domain,
            page_on_site=False,
            gem_enabled=None,
            direct_handover=None,
        )
        page_not_on_site_gem_enabled = PageMetadataFactory(
            gemeente_domain=self.domain,
            page_on_site=False,
            gem_enabled=True,
            direct_handover=None,
        )
        page_not_on_site_direct_handover = PageMetadataFactory(
            gemeente_domain=self.domain,
            page_on_site=False,
            gem_enabled=None,
            direct_handover=False,
        )

        other_domain = GemeenteDomainFactory(domain="not-meierijstad.nl")
        page_on_other_domain = PageMetadataFactory(
            gemeente_domain=other_domain,
            page_on_site=False,
            gem_enabled=None,
            direct_handover=None,
        )

        data = {
            "organisation": "meierijstad",
            "domain": "www.meierijstad.nl",
            "event": "end",
        }
        response = self.client.post(self.url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertTrue(PageMetadata.objects.filter(pk=page_on_site.pk).exists())
        self.assertFalse(
            PageMetadata.objects.filter(pk=page_not_on_site_no_defaults.pk).exists()
        )
        self.assertTrue(
            PageMetadata.objects.filter(pk=page_not_on_site_gem_enabled.pk).exists()
        )
        self.assertTrue(
            PageMetadata.objects.filter(pk=page_not_on_site_direct_handover.pk).exists()
        )
        self.assertTrue(
            PageMetadata.objects.filter(pk=page_on_other_domain.pk).exists()
        )

    def test_batch_without_items(self):
        data = {
            "organisation": "meierijstad",
            "domain": "www.meierijstad.nl",
            "event": "batch",
        }
        response = self.client.post(self.url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(), {"items": ["Items field is required for batch operation"]}
        )

        data["items"] = []
        response = self.client.post(self.url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(), {"items": ["Items field is required for batch operation"]}
        )

    def test_add_event(self):
        data = {
            "url": "https://www.meierijstad.nl/example/location",
            "title": "Page Title",
            "md5": "not in user",
            "event": "add",
            "organisation": "meierijstad",
            "domain": "www.meierijstad.nl",
        }
        self.assertEqual(PageMetadata.objects.count(), 0)

        response = self.client.post(self.url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(PageMetadata.objects.count(), 1)
        page = PageMetadata.objects.get()
        self.assertEqual(page.path, "/example/location")
        self.assertEqual(page.title, "Page Title")

        # try with existing event
        data["title"] = "Different title"
        response = self.client.post(self.url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(PageMetadata.objects.count(), 1)
        page.refresh_from_db()
        self.assertEqual(page.title, "Different title")

    def test_update_event(self):
        data = {
            "url": "https://www.meierijstad.nl/example/location",
            "title": "Page Title",
            "md5": "not in user",
            "event": "update",
            "organisation": "meierijstad",
            "domain": "www.meierijstad.nl",
        }
        self.assertEqual(PageMetadata.objects.count(), 0)

        response = self.client.post(self.url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(PageMetadata.objects.count(), 1)
        page = PageMetadata.objects.get()
        self.assertEqual(page.path, "/example/location")
        self.assertEqual(page.title, "Page Title")

        # try with existing event
        data["title"] = "Different title"
        response = self.client.post(self.url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(PageMetadata.objects.count(), 1)
        page.refresh_from_db()
        self.assertEqual(page.title, "Different title")

    def test_delete_event_on_existing_page(self):
        page_delete = PageMetadataFactory(
            gemeente_domain=self.domain,
            path="/another/location",
            title="Initial title",
            page_on_site=True,
        )
        data = {
            "url": "https://www.meierijstad.nl/another/location",
            "title": "Second Page",
            "md5": "not in user",
            "event": "delete",
            "organisation": "meierijstad",
            "domain": "www.meierijstad.nl",
        }
        self.assertEqual(PageMetadata.objects.count(), 1)

        response = self.client.post(self.url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # not removed until the end event
        self.assertEqual(PageMetadata.objects.count(), 1)

        page_delete.refresh_from_db()
        self.assertEqual(page_delete.title, "Second Page")
        self.assertFalse(page_delete.page_on_site)

    def test_delete_event_on_non_existent_page(self):
        data = {
            "url": "https://www.meierijstad.nl/another/location",
            "title": "Second Page",
            "md5": "not in user",
            "event": "delete",
            "organisation": "meierijstad",
            "domain": "www.meierijstad.nl",
        }
        self.assertEqual(PageMetadata.objects.count(), 0)

        response = self.client.post(self.url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(PageMetadata.objects.count(), 0)

    def test_batch_event_add(self):
        data = {
            "organisation": "meierijstad",
            "domain": "www.meierijstad.nl",
            "event": "batch",
            "items": [
                {
                    "url": "https://www.meierijstad.nl/example/location",
                    "title": "Page Title",
                    "md5": "not in user",
                    "event": "add",
                    "organisation": "meierijstad",
                    "domain": "www.meierijstad.nl",
                },
                {
                    "url": "https://www.meierijstad.nl/another/location",
                    "title": "Second Page",
                    "md5": "not in user",
                    "event": "update",
                    "organisation": "meierijstad",
                    "domain": "www.meierijstad.nl",
                },
            ],
        }

        self.assertEqual(PageMetadata.objects.count(), 0)
        response = self.client.post(self.url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(PageMetadata.objects.count(), 2)

        page_1 = PageMetadata.objects.get(path="/example/location")
        self.assertEqual(page_1.title, "Page Title")
        self.assertTrue(page_1.page_on_site)

        page_2 = PageMetadata.objects.get(path="/another/location")
        self.assertEqual(page_2.title, "Second Page")
        self.assertTrue(page_2.page_on_site)

    def test_batch_event_update(self):

        page_updated = PageMetadataFactory(
            gemeente_domain=self.domain,
            path="/example/location",
            title="Title at the start",
            page_on_site=True,
        )
        page_delete = PageMetadataFactory(
            gemeente_domain=self.domain,
            path="/another/location",
            title="Initial title",
            page_on_site=True,
        )

        data = {
            "organisation": "meierijstad",
            "domain": "www.meierijstad.nl",
            "event": "batch",
            "items": [
                {
                    "url": "https://www.meierijstad.nl/example/location",
                    "title": "Page Title",
                    "md5": "not in user",
                    "event": "add",
                    "organisation": "meierijstad",
                    "domain": "www.meierijstad.nl",
                },
                {
                    "url": "https://www.meierijstad.nl/another/location",
                    "title": "Second Page",
                    "md5": "not in user",
                    "event": "delete",
                    "organisation": "meierijstad",
                    "domain": "www.meierijstad.nl",
                },
            ],
        }

        self.assertEqual(PageMetadata.objects.count(), 2)
        response = self.client.post(self.url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(PageMetadata.objects.count(), 2)

        # page title should be changed
        page_updated.refresh_from_db()
        self.assertEqual(page_updated.title, "Page Title")
        self.assertTrue(page_updated.page_on_site)

        page_delete.refresh_from_db()
        self.assertEqual(page_delete.title, "Second Page")
        self.assertFalse(page_delete.page_on_site)
