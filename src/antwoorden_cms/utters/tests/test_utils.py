import json

from django.conf import settings
from django.core.cache import cache
from django.test import TestCase

import requests_mock

from ..utils import cache_utter_metadata, get_local_utters

HEADERS = {"Content-Type": "application/json"}

LOCAL_UTTER_RESPONSES = {
    "responses": {
        "utter_lo_1": [
            {"text": "landelijke default"},
            {
                "channel": "metadata",
                "custom": {
                    "default": [
                        {
                            "text": "text1",
                        }
                    ],
                    "metadata": {
                        "required": False,
                        "questions": ["question1", "question2"],
                        "category": "verhuismelding",
                        "examples": ["example1"],
                    },
                },
            },
        ],
        "utter_lo_2": [
            {"text": "landelijke default"},
            {
                "channel": "metadata",
                "custom": {
                    "default": [
                        {
                            "text": "text2",
                        }
                    ],
                    "metadata": {
                        "required": False,
                        "questions": ["question1", "question2"],
                        "category": "verhuismelding",
                        "examples": ["example1", "example2"],
                    },
                },
            },
        ],
        "utter_lo_3": [
            {"text": "landelijke default"},
            {
                "channel": "metadata",
                "custom": {
                    "default": [
                        {
                            "text": "staging default",
                        }
                    ],
                    "metadata": {
                        "required": False,
                        "questions": ["question1", "question2"],
                        "category": "verhuismelding",
                        "examples": ["example1", "example2"],
                    },
                },
            },
        ],
    }
}


TRANSFORMED_LOCAL_UTTER_RESPONSES = {
    "utter_lo_1": {
        "default": [
            {
                "text": "text1",
            }
        ],
        "metadata": {
            "required": False,
            "questions": ["question1", "question2"],
            "category": "verhuismelding",
            "examples": ["example1"],
        },
    },
    "utter_lo_2": {
        "default": [
            {
                "text": "text2",
            }
        ],
        "metadata": {
            "required": False,
            "questions": ["question1", "question2"],
            "category": "verhuismelding",
            "examples": ["example1", "example2"],
        },
    },
    "utter_lo_3": {
        "default": [
            {
                "text": "staging default",
            }
        ],
        "metadata": {
            "required": False,
            "questions": ["question1", "question2"],
            "category": "verhuismelding",
            "examples": ["example1", "example2"],
        },
    },
}

DOMAIN_URL = f"{settings.DOMAIN_URL}"


class LocalUtterRetrievalTestCase(TestCase):
    def test_no_cache_saved(self, *m):
        cache.delete("local_responses_metadata")
        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            data = get_local_utters()

        self.assertDictEqual(
            data,
            TRANSFORMED_LOCAL_UTTER_RESPONSES,
            cache.get("local_responses_metadata"),
        )

    def test_cache_saved_same_etag(self):
        cache.delete("responses_etag_no_refresh")
        cache.set("local_responses_metadata", TRANSFORMED_LOCAL_UTTER_RESPONSES)
        cache.set("domain_tag", "1234")
        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)
            data = get_local_utters()

        self.assertDictEqual(
            data,
            TRANSFORMED_LOCAL_UTTER_RESPONSES,
            cache.get("local_responses_metadata"),
        )

    def test_cache_saved_different_etag(self):
        cache.delete("responses_etag_no_refresh")
        cache.set("local_responses_metadata", {"some_other_utter": {"metadata": {}}})
        cache.set("domain_tag", "4321")
        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            data = get_local_utters()

        self.assertDictEqual(
            data,
            TRANSFORMED_LOCAL_UTTER_RESPONSES,
            cache.get("local_responses_metadata"),
        )

    def test_not_valid_json(self):
        cache.delete("local_responses_metadata")
        cache.delete("domain_tag")
        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, text="foo")

            data = get_local_utters()

        self.assertDictEqual(data, {})


class UtterMetadataRetrievalTests(TestCase):
    def test_cache_utter_metadata(self):
        cache.delete("local_responses_metadata")
        cache.delete("domain_tag")
        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            cache_utter_metadata()

        self.assertIsNotNone(cache.get("local_responses_metadata"))
        self.assertEqual(
            json.loads(cache.get("local_responses_metadata").decode("utf-8")),
            TRANSFORMED_LOCAL_UTTER_RESPONSES,
        )
