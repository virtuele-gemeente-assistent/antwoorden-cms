from django.contrib.auth.models import Group

import factory
from factory.django import DjangoModelFactory

from antwoorden_cms.accounts.models import User

from ..models import (
    Channel,
    Gemeente,
    GemeenteDomain,
    Hyperlink,
    LivechatConfiguration,
    Story,
    UtterResponse,
    VariableMetadata,
)


class UserFactory(DjangoModelFactory):
    class Meta:
        model = User

    username = factory.Sequence(lambda n: "test{}".format(n))
    password = factory.PostGenerationMethodCall("set_password", "password")


class GroupFactory(DjangoModelFactory):
    class Meta:
        model = Group


class ChannelFactory(DjangoModelFactory):
    class Meta:
        model = Channel


class GemeenteFactory(DjangoModelFactory):
    class Meta:
        model = Gemeente


class GemeenteDomainFactory(DjangoModelFactory):
    gemeente = factory.SubFactory(GemeenteFactory)

    class Meta:
        model = GemeenteDomain


class UtterResponseFactory(DjangoModelFactory):
    class Meta:
        model = UtterResponse


class HyperlinkFactory(DjangoModelFactory):
    class Meta:
        model = Hyperlink


class VariableMetadataFactory(DjangoModelFactory):
    class Meta:
        model = VariableMetadata


class LivechatConfigurationFactory(DjangoModelFactory):
    gemeente = factory.SubFactory(GemeenteFactory)

    class Meta:
        model = LivechatConfiguration


class StoryFactory(DjangoModelFactory):
    class Meta:
        model = Story
