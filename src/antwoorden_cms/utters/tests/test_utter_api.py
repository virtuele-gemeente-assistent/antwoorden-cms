from django.urls import reverse
from django.utils.http import urlencode

from rest_framework import status
from rest_framework.test import APITestCase

from .factories import (
    ChannelFactory,
    GemeenteFactory,
    HyperlinkFactory,
    UtterResponseFactory,
)


class UtterAPITests(APITestCase):
    def setUp(self):
        self.utter_default = UtterResponseFactory.create(
            utter="utter_ask", response="default que?", gemeente=None
        )

        self.gemeente1 = GemeenteFactory.create(name="Dongen")
        self.gemeente2 = GemeenteFactory.create(name="Utrecht")
        self.utter1 = UtterResponseFactory.create(
            utter="utter_ask", response="que?", gemeente=self.gemeente1
        )
        self.channel = ChannelFactory.create(name="voice")
        self.utter2 = UtterResponseFactory.create(
            utter="utter_ask", response="wut", gemeente=self.gemeente2
        )
        self.utter2 = UtterResponseFactory.create(
            utter="utter_ask",
            channel=self.channel,
            response="huh?",
            gemeente=self.gemeente2,
        )
        self.utter3 = UtterResponseFactory.create(
            utter="utter_greet", response="hoi", gemeente=self.gemeente2
        )

    def test_get_utter_default_channel(self):
        url = reverse(
            "antwoorden_cms:utters",
            kwargs={"gemeente": "dongen", "utter_name": "utter_ask"},
        )
        response = self.client.get(url)

        data = response.data

        self.assertEqual(data["text"], "que?")

    def test_get_utter_specific_channel(self):
        url = reverse(
            "antwoorden_cms:utters",
            kwargs={"gemeente": "utrecht", "utter_name": "utter_ask"},
        )
        response = self.client.get(f"{url}?{urlencode({'channel': 'voice'})}")

        data = response.data

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(data["text"], "huh?")

    def test_get_utter_404(self):
        url = reverse(
            "antwoorden_cms:utters",
            kwargs={"gemeente": "dongen", "utter_name": "utter_blegh"},
        )
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_utter_specific_channel_default_fallback(self):
        url = reverse(
            "antwoorden_cms:utters",
            kwargs={"gemeente": "utrecht", "utter_name": "utter_ask"},
        )
        response = self.client.get(f"{url}?{urlencode({'channel': 'different'})}")

        data = response.data

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(data["text"], "wut")

    def test_get_utter_format_hyperlink(self):
        UtterResponseFactory.create(
            utter="utter_paspoort_landingpage",
            response="[<paspoort_url.label>](<paspoort_url.url>)",
            gemeente=self.gemeente1,
        )
        HyperlinkFactory.create(
            name="paspoort_url",
            label="Ga naar de pagina paspoort",
            url="https://dongen.nl/paspoort",
            gemeente=self.gemeente1,
        )

        url = reverse(
            "antwoorden_cms:utters",
            kwargs={"gemeente": "dongen", "utter_name": "utter_paspoort_landingpage"},
        )
        response = self.client.get(url)

        data = response.data

        self.assertEqual(
            data["text"], "[Ga naar de pagina paspoort](https://dongen.nl/paspoort)"
        )

    def test_get_utter_format_hyperlink_duplicates_exist(self):
        UtterResponseFactory.create(
            utter="utter_paspoort_landingpage",
            response="[<paspoort_url.label>](<paspoort_url.url>)",
            gemeente=self.gemeente1,
        )
        HyperlinkFactory.create(
            name="paspoort_url",
            label="some duplicate",
            url="https://dongen.nl/paspoort",
            gemeente=self.gemeente1,
        )
        HyperlinkFactory.create(
            name="paspoort_url",
            label="Ga naar de pagina paspoort",
            url="https://dongen.nl/paspoort",
            gemeente=self.gemeente1,
        )

        url = reverse(
            "antwoorden_cms:utters",
            kwargs={"gemeente": "dongen", "utter_name": "utter_paspoort_landingpage"},
        )
        response = self.client.get(url)

        data = response.data

        self.assertEqual(
            data["text"], "[Ga naar de pagina paspoort](https://dongen.nl/paspoort)"
        )

    def test_get_utter_format_hyperlink_multiple_exist_for_different_gemeentes(self):
        UtterResponseFactory.create(
            utter="utter_paspoort_landingpage",
            response="[<paspoort_url.label>](<paspoort_url.url>)",
            gemeente=self.gemeente1,
        )
        HyperlinkFactory.create(
            name="paspoort_url",
            label="Ga naar de pagina paspoort",
            url="https://dongen.nl/paspoort",
            gemeente=self.gemeente1,
        )
        HyperlinkFactory.create(
            name="paspoort_url",
            label="Ga naar de pagina paspoort",
            url="https://utrecht.nl/paspoort",
            gemeente=self.gemeente2,
        )

        url = reverse(
            "antwoorden_cms:utters",
            kwargs={"gemeente": "dongen", "utter_name": "utter_paspoort_landingpage"},
        )
        response = self.client.get(url)

        data = response.data

        self.assertEqual(
            data["text"], "[Ga naar de pagina paspoort](https://dongen.nl/paspoort)"
        )

    def test_get_utter_format_hyperlink_does_not_exist(self):
        UtterResponseFactory.create(
            utter="utter_paspoort_landingpage",
            response="[<paspoort_url.label>](<paspoort_url.url>)",
            gemeente=self.gemeente1,
        )

        url = reverse(
            "antwoorden_cms:utters",
            kwargs={"gemeente": "dongen", "utter_name": "utter_paspoort_landingpage"},
        )
        response = self.client.get(url)

        data = response.data

        self.assertEqual(data["text"], "[paspoort_url.label](paspoort_url.url)")

    def test_get_utter_format_multiple_hyperlinks(self):
        UtterResponseFactory.create(
            utter="utter_paspoort_landingpage",
            response="Zie: [<paspoort_url.label>](<paspoort_url.url>) of [<id_kaart_url.label>](<id_kaart_url.url>)",
            gemeente=self.gemeente1,
        )
        HyperlinkFactory.create(
            name="paspoort_url",
            label="Ga naar de pagina paspoort",
            url="https://dongen.nl/paspoort",
            gemeente=self.gemeente1,
        )
        HyperlinkFactory.create(
            name="id_kaart_url",
            label="Ga naar de pagina id-kaart",
            url="https://dongen.nl/id-kaart",
            gemeente=self.gemeente1,
        )

        url = reverse(
            "antwoorden_cms:utters",
            kwargs={"gemeente": "dongen", "utter_name": "utter_paspoort_landingpage"},
        )
        response = self.client.get(url)

        data = response.data

        self.assertEqual(
            data["text"],
            "Zie: [Ga naar de pagina paspoort](https://dongen.nl/paspoort) of [Ga naar de pagina id-kaart](https://dongen.nl/id-kaart)",
        )

    def test_get_utter_default_fallback_hyperlink(self):
        UtterResponseFactory.create(
            utter="utter_hyperlink",
            response="here is a url [<id_kaart_url.label>](<id_kaart_url.url>)",
            gemeente=None,
        )
        HyperlinkFactory.create(
            name="id_kaart_url",
            label="Ga naar de pagina id-kaart",
            url="https://dongen.nl/id-kaart",
            gemeente=self.gemeente1,
        )
        url = reverse(
            "antwoorden_cms:utters",
            kwargs={"gemeente": "dongen", "utter_name": "utter_hyperlink"},
        )
        response = self.client.get(url)

        data = response.data

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            data["text"],
            "here is a url [Ga naar de pagina id-kaart](https://dongen.nl/id-kaart)",
        )

    def test_get_utter_format_multiple_dynamic_hyperlinks(self):
        UtterResponseFactory.create(
            utter="utter_paspoort_landingpage",
            response="Zie: [paspoort_url](https://demodam.nl/paspoort_url) of [id_kaart_url](https://demodam.nl/id_kaart_url)",
            gemeente=None,
        )
        HyperlinkFactory.create(
            name="paspoort_url",
            label="Ga naar de pagina paspoort",
            url="https://dongen.nl/paspoort",
            gemeente=self.gemeente1,
        )
        HyperlinkFactory.create(
            name="id_kaart_url",
            label="Ga naar de pagina id-kaart",
            url="https://dongen.nl/id-kaart",
            gemeente=self.gemeente1,
        )

        url = reverse(
            "antwoorden_cms:utters",
            kwargs={"gemeente": "dongen", "utter_name": "utter_paspoort_landingpage"},
        )
        response = self.client.get(url)

        data = response.data

        self.assertEqual(
            data["text"],
            "Zie: [Ga naar de pagina paspoort](https://dongen.nl/paspoort) of [Ga naar de pagina id-kaart](https://dongen.nl/id-kaart)",
        )

    def test_get_utter_format_dynamic_hyperlink_and_regular_variable(self):
        UtterResponseFactory.create(
            utter="utter_paspoort_landingpage",
            response="Zie: [paspoort_url](https://demodam.nl/paspoort_url) of [<id_kaart_url.label>](<id_kaart_url.url>)",
            gemeente=None,
        )
        HyperlinkFactory.create(
            name="paspoort_url",
            label="Ga naar de pagina paspoort",
            url="https://dongen.nl/paspoort",
            gemeente=self.gemeente1,
        )
        HyperlinkFactory.create(
            name="id_kaart_url",
            label="Ga naar de pagina id-kaart",
            url="https://dongen.nl/id-kaart",
            gemeente=self.gemeente1,
        )

        url = reverse(
            "antwoorden_cms:utters",
            kwargs={"gemeente": "dongen", "utter_name": "utter_paspoort_landingpage"},
        )
        response = self.client.get(url)

        data = response.data

        self.assertEqual(
            data["text"],
            "Zie: [Ga naar de pagina paspoort](https://dongen.nl/paspoort) of [Ga naar de pagina id-kaart](https://dongen.nl/id-kaart)",
        )

    def test_get_utter_format_multiple_dynamic_hyperlinks_not_found(self):
        UtterResponseFactory.create(
            utter="utter_paspoort_landingpage",
            response="Zie: [pagina paspoort](https://demodam.nl/paspoort_url) of [pagina idkaart](https://demodam.nl/id_kaart_url)",
            gemeente=None,
        )

        url = reverse(
            "antwoorden_cms:utters",
            kwargs={"gemeente": "dongen", "utter_name": "utter_paspoort_landingpage"},
        )
        response = self.client.get(url)

        data = response.data

        self.assertEqual(data["text"], "Zie: pagina paspoort of pagina idkaart")

    def test_get_utter_default_fallback(self):
        UtterResponseFactory.create(
            utter="utter_default",
            response="fallback",
            gemeente=None,
        )
        url = reverse(
            "antwoorden_cms:utters",
            kwargs={"gemeente": "utrecht", "utter_name": "utter_default"},
        )
        response = self.client.get(url)

        data = response.data

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(data["text"], "fallback")

    def test_get_utter_default_use_specific(self):
        UtterResponseFactory.create(
            utter="utter_default",
            response="fallback",
            gemeente=None,
        )
        UtterResponseFactory.create(
            utter="utter_default",
            response="utrecht default",
            gemeente=self.gemeente2,
        )
        url = reverse(
            "antwoorden_cms:utters",
            kwargs={"gemeente": "utrecht", "utter_name": "utter_default"},
        )
        response = self.client.get(url)

        data = response.data

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(data["text"], "utrecht default")

    def test_get_utter_municipality_not_connected(self):
        UtterResponseFactory.create(
            utter="utter_default",
            response="fallback",
            gemeente=None,
        )
        url = reverse(
            "antwoorden_cms:utters",
            kwargs={"gemeente": "tilburg", "utter_name": "utter_default"},
        )
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
