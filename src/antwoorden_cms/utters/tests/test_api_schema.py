from django.urls import reverse

from rest_framework import status
from rest_framework.test import APITestCase


class UtterAPISchemaTests(APITestCase):
    def test_get_schema_redoc(self):
        url = reverse("antwoorden_cms:schema-redoc")
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_schema_swagger_ui(self):
        url = reverse("antwoorden_cms:schema-swagger-ui")
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
