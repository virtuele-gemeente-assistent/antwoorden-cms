from django.test import TestCase, override_settings
from django.urls import reverse
from django.utils.translation import ugettext as _

import yaml
from django_webtest import WebTest

from antwoorden_cms.utters.tests.mixins import TwoFactorTestCaseMixin

from ..models import Gemeente, Hyperlink
from ..utils import export_hyperlinks, import_hyperlinks
from .factories import GemeenteFactory, HyperlinkFactory


class HyperlinkImportExportTestCase(TestCase):
    maxDiff = None

    def setUp(self):
        super().setUp()

        self.gemeente1 = GemeenteFactory.create(name="Amsterdam")
        self.gemeente2 = GemeenteFactory.create(name="Rotterdam")
        self.gemeente3 = GemeenteFactory.create(name="Utrecht")

        HyperlinkFactory.create(gemeente=self.gemeente1, name="hyperlink1", label="foo")
        HyperlinkFactory.create(
            gemeente=self.gemeente2, name="hyperlink1", url="bar.nl", title="bar"
        )
        HyperlinkFactory.create(gemeente=self.gemeente3, name="hyperlink1", title="baz")

        HyperlinkFactory.create(gemeente=self.gemeente1, name="hyperlink2", title="foo")
        HyperlinkFactory.create(gemeente=self.gemeente2, name="hyperlink2", label="bar")
        HyperlinkFactory.create(
            gemeente=self.gemeente3, name="hyperlink2", url="baz.nl", title="baz"
        )

    def test_export_hyperlinks(self):
        data = export_hyperlinks()

        self.assertDictEqual(
            data,
            {
                "hyperlinks": {
                    "hyperlink1": {
                        "amsterdam": {"label": "foo"},
                        "rotterdam": {"url": "bar.nl", "title": "bar"},
                        "utrecht": {"title": "baz"},
                    },
                    "hyperlink2": {
                        "amsterdam": {"title": "foo"},
                        "rotterdam": {"label": "bar"},
                        "utrecht": {"url": "baz.nl", "title": "baz"},
                    },
                }
            },
        )

    def test_import_hyperlinks(self):
        import_data = {
            "hyperlinks": {
                "hyperlink3": {
                    "amsterdam": {"label": "foo"},
                    "rotterdam": {"url": "bar.nl", "title": "bar"},
                },
                "hyperlink4": {
                    "amsterdam": {"title": "foo"},
                    "rotterdam": {"label": "bar"},
                },
            }
        }

        import_hyperlinks(import_data)

        hyperlink1 = Hyperlink.objects.get(
            gemeente__slug="amsterdam", name="hyperlink3"
        )
        self.assertEqual(hyperlink1.label, "foo")

        hyperlink2 = Hyperlink.objects.get(
            gemeente__slug="rotterdam", name="hyperlink3"
        )
        self.assertEqual(hyperlink2.url, "bar.nl")
        self.assertEqual(hyperlink2.title, "bar")

        hyperlink3 = Hyperlink.objects.get(
            gemeente__slug="amsterdam", name="hyperlink4"
        )
        self.assertEqual(hyperlink3.title, "foo")

        hyperlink4 = Hyperlink.objects.get(
            gemeente__slug="rotterdam", name="hyperlink4"
        )
        self.assertEqual(hyperlink4.label, "bar")

    def test_import_hyperlinks_gemeente_does_not_exist(self):
        import_data = {
            "hyperlinks": {
                "hyperlink": {
                    "dongen": {"label": "foo"},
                },
            }
        }

        import_hyperlinks(import_data)

        created_gemeente = Gemeente.objects.get(slug="dongen")
        self.assertEqual(created_gemeente.name, "Dongen")

        hyperlink = Hyperlink.objects.get(gemeente=created_gemeente)
        self.assertEqual(hyperlink.name, "hyperlink")
        self.assertEqual(hyperlink.label, "foo")

    def test_import_hyperlinks_delete_existing(self):
        self.assertEqual(Hyperlink.objects.count(), 6)

        import_data = {
            "hyperlinks": {
                "hyperlink": {
                    "dongen": {"label": "foo"},
                },
            }
        }

        import_hyperlinks(import_data, delete_existing=True)

        created_gemeente = Gemeente.objects.get(slug="dongen")
        self.assertEqual(created_gemeente.name, "Dongen")

        hyperlink = Hyperlink.objects.get(gemeente=created_gemeente)
        self.assertEqual(hyperlink.name, "hyperlink")
        self.assertEqual(hyperlink.label, "foo")

        self.assertEqual(Hyperlink.objects.count(), 1)


class HyperlinkImportExportWebTest(TwoFactorTestCaseMixin, WebTest):
    user_is_admin = True
    csrf_checks = False

    def setUp(self):
        super().setUp()

        self.gemeente = GemeenteFactory.create(name="Amsterdam")
        HyperlinkFactory.create(gemeente=self.gemeente, name="hyperlink1", label="foo")

    def test_import_hyperlinks_page_always_deletes_existing(self):
        import_data = {
            "hyperlinks": {
                "hyperlink": {
                    "amsterdam": {"label": "foo"},
                },
            }
        }

        import_yaml = yaml.dump(import_data).encode()
        response = self.app.get(reverse("admin:import_hyperlinks"), user=self.user)

        self.assertEqual(response.status_code, 200)

        form = response.form
        form["import_file"] = (
            "file",
            import_yaml,
        )
        response = form.submit("_import_hyperlinks")

        self.assertEqual(response.status_code, 302)

        response = response.follow()

        msgs = [str(m) for m in response.context["messages"]]

        self.assertEqual(len(msgs), 1)
        self.assertEqual(msgs[0], _("Hyperlinks were successfully imported"))

        hyperlink = Hyperlink.objects.get()
        self.assertEqual(hyperlink.name, "hyperlink")
        self.assertEqual(hyperlink.label, "foo")

        self.assertEqual(Hyperlink.objects.count(), 1)

    @override_settings(ENVIRONMENT="production")
    def test_import_hyperlinks_not_allowed_on_production(self):
        response = self.app.get(reverse("admin:import_hyperlinks"), user=self.user)

        self.assertEqual(response.status_code, 302)

        response = response.follow()
        msgs = [str(m) for m in response.context["messages"]]

        self.assertEqual(len(msgs), 1)
        self.assertEqual(
            msgs[0], _("Importing hyperlinks is not allowed on production")
        )

    def test_export_hyperlinks_page(self):
        response = self.app.get(reverse("admin:export_hyperlinks"), user=self.user)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content_type, "application/x-yaml")

        export_data = yaml.safe_load(response.content)
        self.assertDictEqual(
            export_data, {"hyperlinks": {"hyperlink1": {"amsterdam": {"label": "foo"}}}}
        )

    @override_settings(ENVIRONMENT="production")
    def test_export_hyperlinks_page_on_production(self):
        response = self.app.get(reverse("admin:export_hyperlinks"), user=self.user)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content_type, "application/x-yaml")

        export_data = yaml.safe_load(response.content)
        self.assertDictEqual(
            export_data, {"hyperlinks": {"hyperlink1": {"amsterdam": {"label": "foo"}}}}
        )

    def test_import_export_hyperlinks_button_visible(self):
        response = self.app.get(
            reverse("admin:utters_hyperlink_changelist"), user=self.user
        )

        self.assertEqual(response.status_code, 200)

        links = response.html.find("ul", {"class": "object-tools"}).find_all(
            "a", {"class": "historylink"}
        )

        self.assertEqual(len(links), 2)

        import_link, export_link = links

        self.assertEqual(import_link.attrs["href"], reverse("admin:import_hyperlinks"))
        self.assertEqual(export_link.attrs["href"], reverse("admin:export_hyperlinks"))

    @override_settings(ENVIRONMENT="production")
    def test_import_export_hyperlinks_button_not_visible_on_production(self):
        response = self.app.get(
            reverse("admin:utters_hyperlink_changelist"), user=self.user
        )

        self.assertEqual(response.status_code, 200)

        links = response.html.find("ul", {"class": "object-tools"}).find_all(
            "a", {"class": "historylink"}
        )

        self.assertEqual(len(links), 1)

        export_link = links[0]

        self.assertEqual(export_link.attrs["href"], reverse("admin:export_hyperlinks"))
