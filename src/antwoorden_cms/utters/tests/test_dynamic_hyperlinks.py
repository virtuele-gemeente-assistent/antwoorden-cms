from django.conf import settings
from django.test import TestCase
from django.urls import reverse

import requests_mock
from django_webtest import WebTest

from ..models import SiteConfiguration, UtterResponse
from .factories import (
    GemeenteFactory,
    HyperlinkFactory,
    UtterResponseFactory,
    VariableMetadataFactory,
)
from .mixins import TwoFactorTestCaseMixin

HEADERS = {"Content-Type": "application/json"}

LOCAL_UTTER_RESPONSES = {
    "responses": {
        "utter_lo_1": [
            {"text": "landelijke default"},
            {
                "channel": "metadata",
                "custom": {
                    "default": [
                        {
                            "text": "text1",
                        }
                    ],
                    "metadata": {
                        "required": False,
                        "questions": ["question1", "question2"],
                        "category": "verhuismelding",
                        "examples": ["example1"],
                    },
                },
            },
        ],
        "utter_lo_2": [
            {"text": "landelijke default"},
            {
                "channel": "metadata",
                "custom": {
                    "default": [
                        {
                            "text": "text2",
                        }
                    ],
                    "metadata": {
                        "required": False,
                        "questions": ["question1", "question2"],
                        "category": "verhuismelding",
                        "examples": ["example1", "example2"],
                    },
                },
            },
        ],
        "utter_lo_3": [
            {"text": "landelijke default"},
            {
                "channel": "metadata",
                "custom": {
                    "default": [
                        {
                            "text": "text3",
                        }
                    ],
                    "metadata": {
                        "required": True,
                        "category": "overig",
                        "examples": ["example2"],
                    },
                },
            },
        ],
        "utter_lo_4": [
            {"text": "landelijke default"},
            {
                "channel": "metadata",
                "custom": {
                    "default": [
                        {
                            "text": "text4",
                        }
                    ],
                    "metadata": {"required": True},
                },
            },
        ],
    }
}

DOMAIN_URL = settings.DOMAIN_URL


class DynamicHyperlinkTests(TestCase):
    def test_dynamic_hyperlink_fixed_label(self):
        gemeente = GemeenteFactory(name="Dongen")
        HyperlinkFactory.create(
            name="pagina_paspoort",
            label="custom label ignored",
            url="https://example.com",
            gemeente=gemeente,
        )
        response = "[pagina paspoort](https://demodam.nl/pagina_paspoort)"
        formatted = UtterResponse.format_hyperlinks(response, gemeente)
        self.assertEqual(formatted, "[pagina paspoort](https://example.com)")

    def test_dynamic_hyperlink_fixed_label_not_found(self):
        gemeente = GemeenteFactory(name="Dongen")
        response = "[pagina paspoort](https://demodam.nl/pagina_paspoort)"
        formatted = UtterResponse.format_hyperlinks(response, gemeente)
        self.assertEqual(formatted, "pagina paspoort")

    def test_dynamic_hyperlink_custom_label(self):
        gemeente = GemeenteFactory(name="Dongen")
        HyperlinkFactory.create(
            name="pagina_paspoort",
            label="custom label",
            url="https://example.com",
            gemeente=gemeente,
        )
        response = "[pagina_paspoort](https://demodam.nl/pagina_paspoort)"
        formatted = UtterResponse.format_hyperlinks(response, gemeente)
        self.assertEqual(formatted, "[custom label](https://example.com)")

    def test_dynamic_hyperlink_custom_label_not_found(self):
        gemeente = GemeenteFactory(name="Dongen")
        response = "[pagina_paspoort](https://demodam.nl/pagina_paspoort)"
        formatted = UtterResponse.format_hyperlinks(response, gemeente)
        self.assertEqual(formatted, "pagina_paspoort")

    def test_dynamic_hyperlink_name_allow_dash(self):
        gemeente = GemeenteFactory(name="Dongen")
        HyperlinkFactory.create(
            name="pagina-paspoort",
            label="paspoort",
            url="https://example.com",
            gemeente=gemeente,
        )
        response = "[pagina-paspoort](https://demodam.nl/pagina-paspoort)"
        formatted = UtterResponse.format_hyperlinks(response, gemeente)
        self.assertEqual(formatted, "[paspoort](https://example.com)")

    def test_dynamic_hyperlink_duplicates(self):
        gemeente = GemeenteFactory(name="Dongen")
        HyperlinkFactory.create(
            name="pagina_paspoort", url="duplicate", gemeente=gemeente
        )
        HyperlinkFactory.create(
            name="pagina_paspoort",
            label="paspoort",
            url="https://example.com",
            gemeente=gemeente,
        )
        response = "[pagina_paspoort](https://demodam.nl/pagina_paspoort)"
        formatted = UtterResponse.format_hyperlinks(response, gemeente)
        self.assertEqual(formatted, "[paspoort](https://example.com)")


class DynamicHyperlinkListTests(TwoFactorTestCaseMixin, WebTest):
    login_url = reverse("two_factor:login")

    def setUp(self):
        super().setUp()

        self.gemeente = GemeenteFactory(name="Amsterdam")
        self.user.gemeentes.add(self.gemeente)

        self.hyperlink_list_url = reverse(
            "gemeentes:dynamic_hyperlink_list",
            kwargs={"gemeente_slug": self.gemeente.slug},
        )

    def test_hyperlink_list(self):
        VariableMetadataFactory.create(
            name="some_link", verbose_name="some_link", category="hyperlinks"
        )
        VariableMetadataFactory.create(
            name="some_link2", verbose_name="some_link2", category="hyperlinks"
        )

        UtterResponseFactory.create(
            gemeente=None,
            utter="utter_lo_test",
            response="Link 1 [pagina paspoort](https://demodam.nl/some_link) and link 2 [pagina idkaart](https://demodam.nl/some_link2)",
        )
        hyperlink1 = HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )
        hyperlink2 = HyperlinkFactory(
            gemeente=self.gemeente, name="some_link2", url="url", label="label"
        )

        response = self.app.get(self.hyperlink_list_url, user=self.user)

        colnames = response.html("th")

        self.assertIn("Naam", colnames[0].text)
        self.assertIn("Zichtbare tekst (label)", colnames[1].text)
        self.assertIn("Internetadres (url)", colnames[2].text)

        rows = response.html("tr")[1:]

        self.assertEqual(len(rows), 2)
        for i, hyperlink in enumerate([hyperlink1, hyperlink2]):
            with self.subTest(hyperlink=hyperlink):
                cells = rows[i].find_all("td")
                if i == 0:
                    self.assertEqual(cells[0].text, "some_link")
                    self.assertEqual(cells[1].text, "label")
                    self.assertEqual(cells[2].text, "url")
                elif i == 1:
                    self.assertEqual(cells[0].text, "some_link2")
                    self.assertEqual(cells[1].text, "label")
                    self.assertEqual(cells[2].text, "url")

                delete_url = reverse(
                    "gemeentes:hyperlink_delete",
                    kwargs={
                        "gemeente_slug": self.gemeente.slug,
                        "hyperlink_pk": hyperlink.pk,
                    },
                )
                update_url = reverse(
                    "gemeentes:hyperlink_update",
                    kwargs={
                        "gemeente_slug": self.gemeente.slug,
                        "hyperlink_pk": hyperlink.pk,
                    },
                )
                delete_button, update_button = rows[i].find_all("a")
                self.assertEqual(delete_url, delete_button.get("href"))
                self.assertEqual(update_url, update_button.get("href"))

    def test_hyperlink_list_show_unused_hyperlink(self):
        VariableMetadataFactory.create(
            name="some_unused_link",
            verbose_name="some_unused_link",
            category="hyperlinks",
        )

        UtterResponseFactory.create(
            gemeente=None,
            utter="utter_lo_test",
            response="Link 1 [pagina paspoort](https://demodam.nl/some_unused_link)",
        )
        UtterResponseFactory.create(
            gemeente=self.gemeente, utter="utter_lo_test", response="bla"
        )

        response = self.app.get(self.hyperlink_list_url, user=self.user)

        colnames = response.html("th")
        self.assertIn("Naam", colnames[0].text)
        self.assertIn("Zichtbare tekst (label)", colnames[1].text)
        self.assertIn("Internetadres (url)", colnames[2].text)

        rows = response.html("tr")[1:]

        # Unused hyperlinks should also show up
        self.assertEqual(len(rows), 1)

        cells = rows[0].find_all("td")
        self.assertEqual(cells[0].text, "some_unused_link")
        self.assertEqual(cells[1].text, "")
        self.assertEqual(cells[2].text, "")

        create_url = reverse(
            "gemeentes:hyperlink_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        create_button = rows[0].find("a")
        self.assertEqual(
            f"{create_url}?name=some_unused_link", create_button.get("href")
        )

    def test_hyperlink_list_duplicate_hyperlink_for_municipality(self):
        """
        Regression test for: https://gitlab.com/virtuele-gemeente-assistent/gem/-/issues/1282
        """
        VariableMetadataFactory.create(name="some_link", category="hyperlinks")

        UtterResponseFactory.create(
            gemeente=None,
            utter="utter_lo_test",
            response="Link 1 [pagina paspoort](https://demodam.nl/some_link)",
        )
        HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )
        hyperlink2 = HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url2", label="label2"
        )

        response = self.app.get(self.hyperlink_list_url, user=self.user)

        rows = response.html("tr")[1:]

        self.assertEqual(len(rows), 1)

        delete_url = reverse(
            "gemeentes:hyperlink_delete",
            kwargs={"gemeente_slug": self.gemeente.slug, "hyperlink_pk": hyperlink2.pk},
        )
        update_url = reverse(
            "gemeentes:hyperlink_update",
            kwargs={"gemeente_slug": self.gemeente.slug, "hyperlink_pk": hyperlink2.pk},
        )
        delete_button, update_button = rows[0].find_all("a")
        self.assertEqual(delete_url, delete_button.get("href"))
        self.assertEqual(update_url, update_button.get("href"))

    def test_hyperlink_list_update_and_create(self):
        site_config = SiteConfiguration.get_solo()
        site_config.hyperlinks_show_only_used_attributes = False
        site_config.save()

        VariableMetadataFactory.create(name="some_link", category="hyperlinks")
        VariableMetadataFactory.create(name="some_link2", category="hyperlinks")

        UtterResponseFactory.create(
            gemeente=None,
            utter="utter_lo_test",
            response="Link 1 [pagina paspoort](https://demodam.nl/some_link) and link 2 [pagina idkaart](https://demodam.nl/some_link2)",
        )
        hyperlink1 = HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )

        response = self.app.get(self.hyperlink_list_url, user=self.user)

        rows = response.html("tr")[1:]

        self.assertEqual(len(rows), 2)

        delete_url = reverse(
            "gemeentes:hyperlink_delete",
            kwargs={"gemeente_slug": self.gemeente.slug, "hyperlink_pk": hyperlink1.pk},
        )
        update_url = reverse(
            "gemeentes:hyperlink_update",
            kwargs={"gemeente_slug": self.gemeente.slug, "hyperlink_pk": hyperlink1.pk},
        )
        delete_button, update_button = rows[0].find_all("a")
        self.assertEqual(delete_url, delete_button.get("href"))
        self.assertEqual(update_url, update_button.get("href"))

        # Check if there is a create button with the correct query params
        create_url = reverse(
            "gemeentes:hyperlink_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        query_string = "?name=some_link2"

        create_button = rows[1].find_all("a")[0]
        self.assertEqual(f"{create_url}{query_string}", create_button.get("href"))

    def test_hyperlink_list_login_required(self):
        response = self.app.get(self.hyperlink_list_url)
        self.assertEqual(response.status_code, 302)
        self.assertIn(self.login_url, response.location)

    def test_hyperlink_list_editable_required(self):
        site_config = SiteConfiguration.get_solo()
        site_config.hyperlinks_show_only_used_attributes = False
        site_config.save()

        VariableMetadataFactory.create(
            name="some_link", verbose_name="some_link", category="hyperlinks"
        )
        VariableMetadataFactory.create(
            name="some_other_link",
            verbose_name="some_other_link",
            category="hyperlinks",
        )

        UtterResponseFactory.create(
            gemeente=None,
            utter="utter_lo_test",
            response="Link 1 [pagina paspoort](https://demodam.nl/some_link) and link 2 [pagina idkaart](https://demodam.nl/some_other_link)",
        )

        gemeente2 = GemeenteFactory.create(name="Tilburg")
        HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )
        HyperlinkFactory(
            gemeente=gemeente2, name="some_other_link", url="url", label="label"
        )

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                reverse(
                    "gemeentes:dynamic_required_hyperlink_list",
                    kwargs={"gemeente_slug": self.gemeente.slug},
                ),
                user=self.user,
            )

        rows = response.html("tr")[1:]

        # some_link should not show up, because it already exists for current gemeente
        self.assertEqual(len(rows), 1)

        self.assertEqual(rows[0].find_all("td")[0].text, "some_other_link")

        # Check if there is a create button with the correct query params
        create_url = reverse(
            "gemeentes:hyperlink_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        query_string = "?name=some_other_link"

        create_button = rows[0].find_all("a")[0]
        self.assertEqual(f"{create_url}{query_string}", create_button.get("href"))

    def test_hyperlink_list_editable_required_show_unused_hyperlinks(self):
        site_config = SiteConfiguration.get_solo()
        site_config.hyperlinks_show_only_used_attributes = False
        site_config.save()

        VariableMetadataFactory.create(
            name="some_link", verbose_name="some_link", category="hyperlinks"
        )
        VariableMetadataFactory.create(
            name="some_unused_link",
            verbose_name="some_unused_link",
            category="hyperlinks",
        )

        UtterResponseFactory.create(
            gemeente=None,
            utter="utter_lo_test",
            response="Link 1 [pagina paspoort](https://demodam.nl/some_link) and link 2 [pagina idkaart](https://demodam.nl/some_unused_link)",
        )

        gemeente2 = GemeenteFactory.create(name="Tilburg")
        HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )
        HyperlinkFactory(
            gemeente=gemeente2, name="some_other_link", url="url", label="label"
        )

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                reverse(
                    "gemeentes:dynamic_required_hyperlink_list",
                    kwargs={"gemeente_slug": self.gemeente.slug},
                ),
                user=self.user,
            )

        rows = response.html("tr")[1:]

        # some_unused_link should show up, because metadata is defined for it
        self.assertEqual(len(rows), 1)

        self.assertEqual(rows[0].find_all("td")[0].text, "some_unused_link")

        # Check if there is a create button with the correct query params
        create_url = reverse(
            "gemeentes:hyperlink_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        query_string = "?name=some_unused_link"

        create_button = rows[0].find_all("a")[0]
        self.assertEqual(f"{create_url}{query_string}", create_button.get("href"))

    def test_hyperlink_list_editable_required_default(self):
        site_config = SiteConfiguration.get_solo()
        site_config.hyperlinks_show_only_used_attributes = False
        site_config.save()

        VariableMetadataFactory.create(
            name="some_link", verbose_name="some_link", category="hyperlinks"
        )
        VariableMetadataFactory.create(
            name="some_other_link",
            verbose_name="some_other_link",
            category="hyperlinks",
        )

        UtterResponseFactory.create(
            gemeente=None,
            utter="utter_lo_test",
            response="Link 1 [pagina paspoort](https://demodam.nl/some_link) and link 2 [pagina idkaart](https://demodam.nl/some_other_link)",
        )

        GemeenteFactory.create(name="Tilburg")

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                reverse(
                    "gemeentes:dynamic_required_hyperlink_list",
                    kwargs={"gemeente_slug": self.gemeente.slug},
                ),
                user=self.user,
            )

        rows = response.html("tr")[1:]

        # Both links should show up
        self.assertEqual(len(rows), 2)

        # Hyperlink 1
        self.assertEqual(rows[0].find_all("td")[0].text, "some_link")

        # Check if there is a create button with the correct query params
        create_url = reverse(
            "gemeentes:hyperlink_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        query_string = "?name=some_link"

        create_button = rows[0].find_all("a")[0]
        self.assertEqual(f"{create_url}{query_string}", create_button.get("href"))

        # Hyperlink 2
        self.assertEqual(rows[1].find_all("td")[0].text, "some_other_link")

        # Check if there is a create button with the correct query params
        create_url = reverse(
            "gemeentes:hyperlink_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        query_string = "?name=some_other_link"

        create_button = rows[1].find_all("a")[0]
        self.assertEqual(f"{create_url}{query_string}", create_button.get("href"))

    def test_hyperlink_list_editable_required_default_also_required_when_override_exists(
        self,
    ):
        site_config = SiteConfiguration.get_solo()
        site_config.hyperlinks_show_only_used_attributes = False
        site_config.save()

        VariableMetadataFactory.create(
            name="some_link", verbose_name="some_link", category="hyperlinks"
        )
        VariableMetadataFactory.create(
            name="some_other_link",
            verbose_name="some_other_link",
            category="hyperlinks",
        )
        VariableMetadataFactory.create(
            name="some_link2", verbose_name="some_link2", category="hyperlinks"
        )

        UtterResponseFactory.create(
            gemeente=None,
            utter="utter_lo_test",
            response="Link 1 [pagina paspoort](https://demodam.nl/some_link) and link 2 [pagina idkaart](https://demodam.nl/some_other_link) and [pagina paspoort](https://demodam.nl/some_link2)",
        )

        GemeenteFactory.create(name="Tilburg")
        UtterResponseFactory.create(
            utter="utter_lo_1",
            response="[<some_link.label>](<some_link.url>)",
            gemeente=None,
        )
        UtterResponseFactory.create(
            utter="utter_lo_2",
            response="[<some_other_link.label>](<some_other_link.url>)",
            gemeente=self.gemeente,
        )
        UtterResponseFactory.create(
            utter="utter_lo_1",
            response="[<some_link2.label>](<some_link2.url>)",
            gemeente=self.gemeente,
        )

        with requests_mock.Mocker() as m:
            m.get(DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS)

            response = self.app.get(
                reverse(
                    "gemeentes:dynamic_required_hyperlink_list",
                    kwargs={"gemeente_slug": self.gemeente.slug},
                ),
                user=self.user,
            )

        rows = response.html("tr")[1:]

        # Both links should show up
        self.assertEqual(len(rows), 3)

        # Hyperlink 1
        self.assertEqual(rows[0].find_all("td")[0].text, "some_link")

        # Check if there is a create button with the correct query params
        create_url = reverse(
            "gemeentes:hyperlink_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        query_string = "?name=some_link"

        create_button = rows[0].find_all("a")[0]
        self.assertEqual(f"{create_url}{query_string}", create_button.get("href"))

        # Hyperlink 2
        self.assertEqual(rows[1].find_all("td")[0].text, "some_link2")

        # Check if there is a create button with the correct query params
        create_url = reverse(
            "gemeentes:hyperlink_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        query_string = "?name=some_link2"

        create_button = rows[1].find_all("a")[0]
        self.assertEqual(f"{create_url}{query_string}", create_button.get("href"))

        # Hyperlink 3
        self.assertEqual(rows[2].find_all("td")[0].text, "some_other_link")

        # Check if there is a create button with the correct query params
        create_url = reverse(
            "gemeentes:hyperlink_create",
            kwargs={
                "gemeente_slug": self.gemeente.slug,
            },
        )
        query_string = "?name=some_other_link"

        create_button = rows[2].find_all("a")[0]
        self.assertEqual(f"{create_url}{query_string}", create_button.get("href"))


class DynamicHyperlinkListSortingTests(TwoFactorTestCaseMixin, WebTest):
    login_url = reverse("two_factor:login")

    def setUp(self):
        super().setUp()

        self.gemeente = GemeenteFactory(name="Amsterdam")
        self.user.gemeentes.add(self.gemeente)

        self.hyperlink_list_url = reverse(
            "gemeentes:dynamic_hyperlink_list",
            kwargs={"gemeente_slug": self.gemeente.slug},
        )

    def test_hyperlink_list_default_sort_by_name(self):
        UtterResponseFactory.create(
            gemeente=None,
            utter="utter_lo_test",
            response="Link 1 [pagina paspoort](https://demodam.nl/some_link) and [pagina paspoort](https://demodam.nl/some_link2)",
        )
        HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )
        UtterResponseFactory.create(utter="utter_lo_bla", response="<some_link2.url>")
        VariableMetadataFactory.create(
            name="some_link", verbose_name="some_link", category="hyperlinks"
        )
        VariableMetadataFactory.create(
            name="some_link2", verbose_name="some_link2", category="hyperlinks"
        )

        response = self.app.get(f"{self.hyperlink_list_url}", user=self.user)

        rows = response.html("tr")[1:]

        self.assertEqual(len(rows), 2)

        self.assertEqual(rows[0].find_all("td")[0].text, "some_link")
        self.assertEqual(rows[1].find_all("td")[0].text, "some_link2")

    def test_hyperlink_list_sort_by_name_desc(self):
        UtterResponseFactory.create(
            gemeente=None,
            utter="utter_lo_test",
            response="Link 1 [pagina paspoort](https://demodam.nl/some_link) and [pagina paspoort](https://demodam.nl/some_link2)",
        )
        HyperlinkFactory(
            gemeente=self.gemeente, name="some_link", url="url", label="label"
        )
        UtterResponseFactory.create(utter="utter_lo_bla", response="<some_link2.url>")
        VariableMetadataFactory.create(
            name="some_link", verbose_name="some_link", category="hyperlinks"
        )
        VariableMetadataFactory.create(
            name="some_link2", verbose_name="some_link2", category="hyperlinks"
        )

        response = self.app.get(
            f"{self.hyperlink_list_url}?sort=name&order=desc", user=self.user
        )

        rows = response.html("tr")[1:]

        self.assertEqual(len(rows), 2)

        self.assertEqual(rows[0].find_all("td")[0].text, "some_link2")
        self.assertEqual(rows[1].find_all("td")[0].text, "some_link")
