from django.urls import reverse

from rest_framework import status
from rest_framework.test import APITestCase

from .factories import GemeenteFactory


class OrganisationAPITestCase(APITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.gemeente1 = GemeenteFactory.create(
            name="Dongen",
            contact_url="https://dongen.nl/contact",
            phone_number="0612345678",
            supports_handover=False,
        )
        cls.gemeente2 = GemeenteFactory.create(
            name="Utrecht",
            contact_url="https://utrecht.nl/contact",
            phone_number="0687654321",
            supports_handover=True,
        )

    def test_organisation_list(self):
        expected = [
            {
                "url": "http://testserver/api/organisations/dongen/",
                "name": "Dongen",
                "slug": "dongen",
                "contact_url": "https://dongen.nl/contact",
                "phone_number": "0612345678",
                "municipality_logo": None,
                "supports_handover": False,
            },
            {
                "url": "http://testserver/api/organisations/utrecht/",
                "name": "Utrecht",
                "slug": "utrecht",
                "contact_url": "https://utrecht.nl/contact",
                "phone_number": "0687654321",
                "municipality_logo": None,
                "supports_handover": True,
            },
        ]

        response = self.client.get(reverse("antwoorden_cms:organisation_list"))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, expected)

    def test_organisation_detail(self):
        expected = {
            "url": "http://testserver/api/organisations/dongen/",
            "name": "Dongen",
            "slug": "dongen",
            "contact_url": "https://dongen.nl/contact",
            "phone_number": "0612345678",
            "municipality_logo": None,
            "supports_handover": False,
        }

        response = self.client.get(
            reverse(
                "antwoorden_cms:organisation_detail",
                kwargs={
                    "organisation_slug": self.gemeente1.slug,
                },
            )
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, expected)
