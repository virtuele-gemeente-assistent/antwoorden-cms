from unittest.mock import patch

from django.conf import settings
from django.core.cache import cache
from django.test import tag
from django.urls import reverse

import requests_mock
from django_webtest import WebTest
from requests.exceptions import ConnectionError

from antwoorden_cms.utters.constants import LOCAL_RESPONSES_METADATA_KEY

from ..models import SiteConfiguration
from .factories import (
    GemeenteFactory,
    HyperlinkFactory,
    StoryFactory,
    UtterResponseFactory,
    VariableMetadataFactory,
)
from .mixins import TwoFactorTestCaseMixin
from .utils import grant_gemeente_perms

HEADERS = {"Content-Type": "application/json"}

LOCAL_UTTER_RESPONSES = {
    "responses": {
        "utter_lo_1": [
            {"text": "landelijke default"},
            {
                "channel": "metadata",
                "custom": {
                    "default": [
                        {
                            "text": "text1",
                        }
                    ],
                    "metadata": {
                        "required": False,
                        "questions": ["question1", "question2"],
                        "category": "verhuismelding",
                        "examples": ["example1"],
                    },
                },
            },
        ],
        "utter_lo_2": [
            {"text": "landelijke default"},
            {
                "channel": "metadata",
                "custom": {
                    "default": [
                        {
                            "text": "text2",
                        }
                    ],
                    "metadata": {
                        "required": False,
                        "questions": ["question1", "question2"],
                        "category": "verhuismelding",
                        "examples": ["example1", "example2"],
                    },
                },
            },
        ],
        "utter_lo_3": [
            {"text": "landelijke default"},
            {
                "channel": "metadata",
                "custom": {
                    "default": [
                        {
                            "text": "text3",
                        }
                    ],
                    "metadata": {
                        "required": True,
                        "category": "overig",
                        "examples": ["example2"],
                    },
                },
            },
        ],
        "utter_lo_4": [
            {"text": "landelijke default"},
            {
                "channel": "metadata",
                "custom": {
                    "default": [
                        {
                            "text": "text4",
                        }
                    ],
                    "metadata": {"required": True},
                },
            },
        ],
    }
}

DOMAIN_URL = settings.DOMAIN_URL


class GemeenteDashboardTests(TwoFactorTestCaseMixin, WebTest):
    user_is_superuser = False

    def setUp(self):
        super().setUp()

        self.gemeente = GemeenteFactory(name="amsterdam")
        self.user.gemeentes.add(self.gemeente)

        StoryFactory.create(identifier="001", category="foo")
        StoryFactory.create(identifier="002", category="verhuizen")
        StoryFactory.create(identifier="003", category="paspoort")

        mocker = requests_mock.Mocker()
        mocker.start()
        self.addCleanup(mocker.stop)
        self.requests_mocker = mocker

        api_uri = reverse(
            "antwoorden_cms:story_settings_list",
            kwargs={"organisation": self.gemeente.slug},
        )
        host = self.app.extra_environ["HTTP_HOST"]
        api_url = f"http://{host}{api_uri}"

        self.requests_mocker.get(api_url, json=self.app.get(api_uri).json)

        cache.delete("domain_tag")

    @patch(
        "antwoorden_cms.utters.views.utterresponse.get_non_overridden_utters",
        return_value=[],
    )
    def test_gemeente_dashboard(self, *mocks):
        self.requests_mocker.get(
            DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS
        )

        response = self.app.get(
            reverse(
                "gemeentes:gemeente_detail",
                kwargs={"gemeente_slug": self.gemeente.slug},
            ),
            user=self.user,
        )

        hyperlink_row = response.html.find("div", {"class": "variable-tiles"})
        story_row = response.html.find("div", {"class": "story-tiles"})
        utter_row = response.html.find("div", {"class": "utter-tiles"})
        config_row = response.html.find("div", {"class": "config-tiles"})

        hyperlink_cards = hyperlink_row.find_all(recursive=False)
        # No metadata defined
        self.assertEqual(len(hyperlink_cards), 0)

        story_cards = story_row.find_all(recursive=False)
        story_cards = sorted(story_cards, key=lambda x: x.text)

        # Enabled by default
        self.assertEqual(len(story_cards), 3)

        _, paspoort_category, verhuizen_category = story_cards

        paspoort_category_link = paspoort_category.find("a").attrs["href"]
        self.assertEqual(
            paspoort_category_link,
            reverse(
                "gemeentes:stories_list",
                kwargs={"gemeente_slug": self.gemeente, "category": "paspoort"},
            ),
        )

        verhuizen_category_link = verhuizen_category.find("a").attrs["href"]
        self.assertEqual(
            verhuizen_category_link,
            reverse(
                "gemeentes:stories_list",
                kwargs={"gemeente_slug": self.gemeente, "category": "verhuizen"},
            ),
        )

        utter_cards = utter_row.find_all(recursive=False)

        # Disabled by default, only show changed/created overviews
        self.assertEqual(len(utter_cards), 2)

        config_cards = config_row.find_all(recursive=False)
        # Two for dialogs, 1 for statistics, one for widget code and one for config
        self.assertEqual(len(config_cards), 5)

    @patch(
        "antwoorden_cms.utters.views.utterresponse.get_non_overridden_utters",
        return_value=[],
    )
    @patch(
        "antwoorden_cms.utters.models.SiteConfiguration.get_solo",
        return_value=SiteConfiguration(
            disabled_story_categories=["paspoort", "foo"],
            enabled_utter_categories=["overig"],
            story_toggling_enabled=True,
            story_settings_default=False,
        ),
    )
    def test_gemeente_dashboard_enable_story_view_disable_utter_view(self, *mocks):
        self.requests_mocker.get(
            DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS
        )

        response = self.app.get(
            reverse(
                "gemeentes:gemeente_detail",
                kwargs={"gemeente_slug": self.gemeente.slug},
            ),
            user=self.user,
        )

        story_row = response.html.find("div", {"class": "story-tiles"})
        utter_row = response.html.find("div", {"class": "utter-tiles"})

        story_cards = story_row.find_all(recursive=False)
        self.assertEqual(len(story_cards), 1)
        verhuizen_category = story_cards[0]
        verhuizen_category_link = verhuizen_category.find("a").attrs["href"]
        warning_icon = verhuizen_category.find("i")
        self.assertEqual(warning_icon.attrs["class"], ["cui-warning"])
        self.assertEqual(
            verhuizen_category_link,
            reverse(
                "gemeentes:stories_list",
                kwargs={"gemeente_slug": self.gemeente, "category": "verhuizen"},
            ),
        )

        utter_cards = utter_row.find_all(recursive=False)
        # Two for utter categories, changed utter overview and created overview
        self.assertEqual(len(utter_cards), 3)

        overig_count = utter_cards[0].find_all("div")[-1].text
        self.assertEqual(int(overig_count), 2)
        overig_url = utter_cards[0].find("a").attrs["href"]
        self.assertEqual(
            f"{reverse('gemeentes:antwoord_list', kwargs={'gemeente_slug': self.gemeente})}?category=overig",
            overig_url,
        )

    @patch(
        "antwoorden_cms.utters.views.utterresponse.get_non_overridden_utters",
        return_value=[],
    )
    @patch(
        "antwoorden_cms.utters.models.SiteConfiguration.get_solo",
        return_value=SiteConfiguration(
            disabled_story_categories=["paspoort", "foo"],
            enabled_utter_categories=["overig"],
            story_toggling_enabled=True,
            story_settings_default=True,
        ),
    )
    def test_gemeente_dashboard_enable_story_view_disable_utter_view_story_settings_default_true(
        self, *mocks
    ):
        self.requests_mocker.get(
            DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS
        )

        response = self.app.get(
            reverse(
                "gemeentes:gemeente_detail",
                kwargs={"gemeente_slug": self.gemeente.slug},
            ),
            user=self.user,
        )

        story_row = response.html.find("div", {"class": "story-tiles"})
        utter_row = response.html.find("div", {"class": "utter-tiles"})

        story_cards = story_row.find_all(recursive=False)
        self.assertEqual(len(story_cards), 1)
        verhuizen_category = story_cards[0]
        verhuizen_category_link = verhuizen_category.find("a").attrs["href"]
        warning_icon = verhuizen_category.find("i")
        self.assertEqual(warning_icon.attrs["class"], ["cui-chat-bubble"])
        self.assertEqual(
            verhuizen_category_link,
            reverse(
                "gemeentes:stories_list",
                kwargs={"gemeente_slug": self.gemeente, "category": "verhuizen"},
            ),
        )

        utter_cards = utter_row.find_all(recursive=False)
        # Two for utter categories, changed utter overview and created overview
        self.assertEqual(len(utter_cards), 3)

        overig_count = utter_cards[0].find_all("div")[-1].text
        self.assertEqual(int(overig_count), 2)
        overig_url = utter_cards[0].find("a").attrs["href"]
        self.assertEqual(
            f"{reverse('gemeentes:antwoord_list', kwargs={'gemeente_slug': self.gemeente})}?category=overig",
            overig_url,
        )

    @patch(
        "antwoorden_cms.utters.views.utterresponse.get_non_overridden_utters",
        return_value=[],
    )
    def test_gemeente_dashboard_required_hyperlinks(self, *mocks):
        VariableMetadataFactory.create(name="pagina_contact", category="hyperlinks")
        self.requests_mocker.get(
            DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS
        )

        response = self.app.get(
            reverse(
                "gemeentes:gemeente_detail",
                kwargs={"gemeente_slug": self.gemeente.slug},
            ),
            user=self.user,
        )

        required_row = response.html.find("div", {"class": "required-tiles"})

        required_cards = required_row.find_all(recursive=False)
        self.assertEqual(len(required_cards), 1)

        required_count = required_cards[0].find_all("div")[-1].text
        self.assertEqual(int(required_count), 1)
        required_hyperlinks_url = required_cards[0].find("a").attrs["href"]
        self.assertEqual(
            reverse(
                "gemeentes:dynamic_required_hyperlink_list",
                kwargs={"gemeente_slug": self.gemeente},
            ),
            required_hyperlinks_url,
        )

    @patch(
        "antwoorden_cms.utters.views.utterresponse.get_non_overridden_utters",
        return_value=[],
    )
    def test_gemeente_dashboard_required_dynamic_hyperlinks(self, *mocks):
        VariableMetadataFactory.create(name="pagina_contact", category="hyperlinks")
        VariableMetadataFactory.create(name="pagina_contact2", category="hyperlinks")
        UtterResponseFactory.create(
            gemeente=None,
            utter="utter_lo_test",
            response="some url [test](https://demodam.nl/pagina_contact) [test2](https://demodam.nl/pagina_contact2)",
        )
        HyperlinkFactory.create(gemeente=self.gemeente, name="pagina_contact")
        self.requests_mocker.get(
            DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS
        )

        response = self.app.get(
            reverse(
                "gemeentes:gemeente_detail",
                kwargs={"gemeente_slug": self.gemeente.slug},
            ),
            user=self.user,
        )

        required_row = response.html.find("div", {"class": "required-tiles"})

        required_cards = required_row.find_all(recursive=False)
        self.assertEqual(len(required_cards), 1)

        required_count = required_cards[0].find_all("div")[-1].text
        self.assertEqual(int(required_count), 1)
        required_hyperlinks_url = required_cards[0].find("a").attrs["href"]
        self.assertEqual(
            reverse(
                "gemeentes:dynamic_required_hyperlink_list",
                kwargs={"gemeente_slug": self.gemeente},
            ),
            required_hyperlinks_url,
        )

    @patch(
        "antwoorden_cms.utters.views.utterresponse.get_non_overridden_utters",
        return_value=["tmp"],
    )
    def test_gemeente_dashboard_required_utters(self, *mocks):
        self.requests_mocker.get(
            DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS
        )

        response = self.app.get(
            reverse(
                "gemeentes:gemeente_detail",
                kwargs={"gemeente_slug": self.gemeente.slug},
            ),
            user=self.user,
        )

        required_row = response.html.find("div", {"class": "required-tiles"})

        required_cards = required_row.find_all(recursive=False)
        self.assertEqual(len(required_cards), 1)

        required_count = required_cards[0].find_all("div")[-1].text
        self.assertEqual(int(required_count), 1)
        required_utters_url = required_cards[0].find("a").attrs["href"]
        self.assertEqual(
            reverse(
                "gemeentes:antwoord_list_editable_required",
                kwargs={"gemeente_slug": self.gemeente},
            ),
            required_utters_url,
        )

    @patch(
        "antwoorden_cms.utters.views.utterresponse.get_non_overridden_utters",
        return_value=["tmp"],
    )
    def test_gemeente_dashboard_required_utters_and_hyperlinks(self, *mocks):
        VariableMetadataFactory.create(name="pagina_contact", category="hyperlinks")
        self.requests_mocker.get(
            DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS
        )

        response = self.app.get(
            reverse(
                "gemeentes:gemeente_detail",
                kwargs={"gemeente_slug": self.gemeente.slug},
            ),
            user=self.user,
        )

        # required_row, hyperlink_row, story_row, utter_row, config_row = response.html.find_all(
        #     "div", {"class": "row"}
        # )
        required_row = response.html.find("div", {"class": "required-tiles"})

        required_cards = required_row.find_all(recursive=False)
        self.assertEqual(len(required_cards), 2)

        required_count = required_cards[0].find_all("div")[-1].text
        self.assertEqual(int(required_count), 1)
        required_hyperlinks_url = required_cards[0].find("a").attrs["href"]
        self.assertEqual(
            reverse(
                "gemeentes:dynamic_required_hyperlink_list",
                kwargs={"gemeente_slug": self.gemeente},
            ),
            required_hyperlinks_url,
        )

        required_count = required_cards[1].find_all("div")[-1].text
        self.assertEqual(int(required_count), 1)
        required_utters_url = required_cards[1].find("a").attrs["href"]
        self.assertEqual(
            reverse(
                "gemeentes:antwoord_list_editable_required",
                kwargs={"gemeente_slug": self.gemeente},
            ),
            required_utters_url,
        )

    def test_gemeente_dashboard_local_utter_endpoint_404(self):
        cache.delete(LOCAL_RESPONSES_METADATA_KEY)
        self.requests_mocker.get(
            DOMAIN_URL, status_code=404, headers={"Content-Type": "text/html"}
        )

        response = self.app.get(
            reverse(
                "gemeentes:gemeente_detail",
                kwargs={"gemeente_slug": self.gemeente.slug},
            ),
            user=self.user,
        )

        row = response.html.find("div", {"class": "utter-tiles"})
        cards = row.find_all(recursive=False)

        # Only cards: one for changed utter overview and one for created utter overview
        self.assertEqual(len(cards), 2)

    def test_gemeente_dashboard_local_utter_endpoint_connection_error(self):
        self.requests_mocker.get(DOMAIN_URL, exc=ConnectionError)

        response = self.app.get(
            reverse(
                "gemeentes:gemeente_detail",
                kwargs={"gemeente_slug": self.gemeente.slug},
            ),
            user=self.user,
        )

        row = response.html.find("div", {"class": "utter-tiles"})
        cards = row.find_all(recursive=False)

        # Only cards: one for changed utter overview and one for created utter overview
        self.assertEqual(len(cards), 2)

    def test_gemeente_dashboard_variable_categories(self):
        VariableMetadataFactory.create(name="utter_test", category="verhuismelding")
        VariableMetadataFactory.create(name="utter_test_2", category="overig")
        VariableMetadataFactory.create(name="utter_test_3", category="overig")

        self.requests_mocker.get(
            DOMAIN_URL, status_code=404, headers={"Content-Type": "text/html"}
        )

        response = self.app.get(
            reverse(
                "gemeentes:gemeente_detail",
                kwargs={"gemeente_slug": self.gemeente.slug},
            ),
            user=self.user,
        )

        row = response.html.find_all("div", {"class": "row"})[1]
        cards = row.find_all(recursive=False)

        # Only 2 cards: one for required utters and one for required hyperlinks
        self.assertEqual(len(cards), 2)

        verhuismelding_count = cards[0].find_all("div")[-1].text
        self.assertEqual(int(verhuismelding_count), 1)
        verhuismelding_url = cards[0].find("a").attrs["href"]
        self.assertEqual(
            f"{reverse('gemeentes:hyperlink_list', kwargs={'gemeente_slug': self.gemeente})}?category=verhuismelding",
            verhuismelding_url,
        )

        overig_count = cards[1].find_all("div")[-1].text
        self.assertEqual(int(overig_count), 2)
        overig_url = cards[1].find("a").attrs["href"]
        self.assertEqual(
            f"{reverse('gemeentes:hyperlink_list', kwargs={'gemeente_slug': self.gemeente})}?category=overig",
            overig_url,
        )


@tag("permissions")
class GemeenteDashboardPermissionTests(TwoFactorTestCaseMixin, WebTest):
    user_is_admin = False
    user_is_superuser = False

    def setUp(self):
        super().setUp()

        self.gemeente = GemeenteFactory(name="amsterdam")
        self.user.gemeentes.add(self.gemeente)

        StoryFactory.create(identifier="001", category="foo")
        StoryFactory.create(identifier="002", category="verhuizen")
        StoryFactory.create(identifier="003", category="paspoort")

        mocker = requests_mock.Mocker()
        mocker.start()
        self.addCleanup(mocker.stop)
        self.requests_mocker = mocker

        api_uri = reverse(
            "antwoorden_cms:story_settings_list",
            kwargs={"organisation": self.gemeente.slug},
        )
        host = self.app.extra_environ["HTTP_HOST"]
        api_url = f"http://{host}{api_uri}"

        self.requests_mocker.get(api_url, json=self.app.get(api_uri).json)

        cache.delete("domain_tag")

    def test_gemeente_dashboard_no_permission_gem_widget_visible(self):
        # Should not have any influence
        grant_gemeente_perms(
            "admin",
            self.user,
            self.gemeente,
            ["utter_read", "hyperlink_read", "dialog_read", "statistics_read"],
        )

        self.requests_mocker.get(
            DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS
        )

        response = self.app.get(
            reverse(
                "gemeentes:gemeente_detail",
                kwargs={"gemeente_slug": self.gemeente.slug},
            ),
            user=self.user,
        )
        self.assertEqual(response.status_code, 200)

        # Gem should be visible
        widget_script = response.html.find("script", {"id": "chatwidget-script"})
        self.assertIsNotNone(widget_script)

        self.assertEqual(widget_script.attrs["crossorigin"], "anonymous")
        self.assertEqual(
            widget_script.attrs["src"],
            f"{settings.CHATBOT_URL}/static/js/widget.js",
        )
        self.assertEqual(widget_script.attrs["data-client-name"], "antwoorden-cms")

    def test_gemeente_dashboard_no_permission_widget_code_generator_visible(self):
        self.requests_mocker.get(
            DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS
        )

        response = self.app.get(
            reverse(
                "gemeentes:gemeente_detail",
                kwargs={"gemeente_slug": self.gemeente.slug},
            ),
            user=self.user,
        )
        self.assertEqual(response.status_code, 200)

        row = response.html.find("div", {"class": "config-tiles"})
        cards = row.find_all(recursive=False)

        # Widget code generator should be visible (even without permissions)
        self.assertEqual(len(cards), 1)

        widget_code_body_url = cards[0].find("a").attrs["data-widget-body-url"]
        self.assertEqual(
            f"{reverse('config:widget_code_body', kwargs={'gemeente_slug': self.gemeente})}",
            widget_code_body_url,
        )

    def test_gemeente_dashboard_has_permission(self):
        grant_gemeente_perms("admin", self.user, self.gemeente, ["gemeente_read"])

        self.requests_mocker.get(
            DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS
        )

        response = self.app.get(
            reverse(
                "gemeentes:gemeente_detail",
                kwargs={"gemeente_slug": self.gemeente.slug},
            ),
            user=self.user,
        )
        self.assertEqual(response.status_code, 200)

    @patch(
        "antwoorden_cms.utters.views.utterresponse.get_non_overridden_utters",
        return_value=[],
    )
    def test_gemeente_dashboard_has_permission_read_utters(self, *mocks):
        grant_gemeente_perms(
            "admin", self.user, self.gemeente, ["utter_read", "gemeente_read"]
        )

        self.requests_mocker.get(
            DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS
        )

        response = self.app.get(
            reverse(
                "gemeentes:gemeente_detail",
                kwargs={"gemeente_slug": self.gemeente.slug},
            ),
            user=self.user,
        )

        hyperlink_row, story_row, utter_row, config_row = response.html.find_all(
            "div", {"class": "row"}
        )

        hyperlink_cards = hyperlink_row.find_all(recursive=False)
        self.assertEqual(len(hyperlink_cards), 0)

        story_cards = story_row.find_all(recursive=False)
        self.assertEqual(len(story_cards), 3)

        utter_cards = utter_row.find_all(recursive=False)
        self.assertEqual(len(utter_cards), 2)

        config_cards = config_row.find_all(recursive=False)
        self.assertEqual(len(config_cards), 1)

    def test_gemeente_dashboard_has_permission_read_hyperlinks(self):
        VariableMetadataFactory.create(name="kosten_paspoort", category="leges")
        grant_gemeente_perms(
            "admin", self.user, self.gemeente, ["gemeente_read", "hyperlink_read"]
        )

        self.requests_mocker.get(
            DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS
        )

        response = self.app.get(
            reverse(
                "gemeentes:gemeente_detail",
                kwargs={"gemeente_slug": self.gemeente.slug},
            ),
            user=self.user,
        )

        (
            required_row,
            hyperlink_row,
            story_row,
            utter_row,
            config_row,
        ) = response.html.find_all("div", {"class": "row"})

        required_cards = required_row.find_all(recursive=False)
        self.assertEqual(len(required_cards), 1)

        hyperlink_cards = hyperlink_row.find_all(recursive=False)
        self.assertEqual(len(hyperlink_cards), 1)

        utter_cards = utter_row.find_all(recursive=False)
        self.assertEqual(len(utter_cards), 0)

        utter_cards = utter_row.find_all(recursive=False)
        self.assertEqual(len(utter_cards), 0)

        config_cards = config_row.find_all(recursive=False)
        self.assertEqual(len(config_cards), 1)

        hyperlinks_url = hyperlink_cards[0].find("a").attrs["href"]
        self.assertEqual(
            f"{reverse('gemeentes:hyperlink_list', kwargs={'gemeente_slug': self.gemeente})}?category=leges",
            hyperlinks_url,
        )

    def test_gemeente_dashboard_has_permission_read_page_metadata(self):
        grant_gemeente_perms(
            "admin", self.user, self.gemeente, ["gemeente_read", "page_metadata_read"]
        )

        self.requests_mocker.get(
            DOMAIN_URL, json=LOCAL_UTTER_RESPONSES, headers=HEADERS
        )

        response = self.app.get(
            reverse(
                "gemeentes:gemeente_detail",
                kwargs={"gemeente_slug": self.gemeente.slug},
            ),
            user=self.user,
        )

        (
            required_row,
            hyperlink_row,
            story_row,
            utter_row,
            config_row,
        ) = response.html.find_all("div", {"class": "row"})

        required_cards = required_row.find_all(recursive=False)
        self.assertEqual(len(required_cards), 0)

        hyperlink_cards = hyperlink_row.find_all(recursive=False)
        self.assertEqual(len(hyperlink_cards), 0)

        story_cards = story_row.find_all(recursive=False)
        self.assertEqual(len(story_cards), 0)

        utter_cards = utter_row.find_all(recursive=False)
        self.assertEqual(len(utter_cards), 0)

        # widget code generator and pagemetadata list
        config_cards = config_row.find_all(recursive=False)
        self.assertEqual(len(config_cards), 2)

        page_metadata_list_url = config_cards[1].find("a").attrs["href"]
        self.assertEqual(
            f"{reverse('config:page_metadata_list', kwargs={'gemeente_slug': self.gemeente})}",
            page_metadata_list_url,
        )
