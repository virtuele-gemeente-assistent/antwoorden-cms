import os
from typing import List

from django.conf import settings
from django.contrib.auth.models import Permission

import yaml

from .factories import GroupFactory


def grant_gemeente_perms(name, user, gemeente, perms):
    # Create a group with the specified permissions
    group = GroupFactory.create(name=name)
    gemeente_perms = Permission.objects.filter(codename__in=perms)
    group.permissions.set(gemeente_perms)

    # Add the specified Gemeente to the User
    user.gemeentes.add(gemeente)

    # Add the User to the created group
    user.groups.add(group)


MOCK_STORIES_DIRECTORY = os.path.join(
    settings.BASE_DIR, "src/antwoorden_cms/utters/tests/files"
)


def mock_retrieve_stories(category: str) -> List[str]:

    with open(f"{MOCK_STORIES_DIRECTORY}/{category}.yml", "r") as f:
        data = yaml.safe_load(f)
    return data


def mock_retrieve_story_categories() -> List[str]:

    files = os.listdir(MOCK_STORIES_DIRECTORY)
    return [f.split(".")[0] for f in files]
