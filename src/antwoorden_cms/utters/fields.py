from django import forms
from django.core import validators
from django.db.models import URLField

SCHEMES = validators.URLValidator.schemes + ["mailto"]


class MailtoAndURLValidator(validators.URLValidator):
    schemes = SCHEMES

    def __call__(self, value):
        # In case of mailto links, split and validate the mail address
        if value.startswith("mailto:"):
            mail_address = value.split("mailto:")[1]
            validators.EmailValidator()(mail_address)
            return

        super().__call__(value)


class MailtoAndURLFormField(forms.URLField):
    default_validators = [MailtoAndURLValidator()]

    def to_python(self, value):
        # Do not attempt to process mailto links further, because this results in
        # `mailto://<address>`, which causes `//` to be prepended to the email adress
        if value.startswith("mailto:"):
            return value

        return super().to_python(value)


class MailtoAndURLField(URLField):
    default_validators = [MailtoAndURLValidator()]

    def formfield(self, **kwargs):
        # As with CharField, this will cause URL validation to be performed
        # twice.
        return super().formfield(
            **{
                "form_class": MailtoAndURLFormField,
                **kwargs,
            }
        )
