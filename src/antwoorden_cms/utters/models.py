import json
import logging
import re
from datetime import datetime

from django.core.cache import cache
from django.db import models
from django.utils import timezone
from django.utils.functional import cached_property
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _

from auditlog.models import AuditlogHistoryField
from auditlog.registry import auditlog
from django_better_admin_arrayfield.models.fields import ArrayField
from ordered_model.models import OrderedModel
from solo.models import SingletonModel

from .choices import Environments, LivechatTypes
from .constants import (
    HYPERLINK_NAME_REGEX,
    LOCAL_RESPONSES_METADATA_KEY,
    MARKDOWN_LINK_REGEX,
)
from .fields import MailtoAndURLField

logger = logging.getLogger(__name__)


class Channel(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Gemeente(models.Model):
    name = models.CharField(max_length=255, unique=True)
    slug = models.SlugField(unique=True)
    possible_organisations_for_handover = ArrayField(
        models.CharField(max_length=255),
        verbose_name=_("Mogelijke organisaties voor handover"),
        help_text=_(
            "Namen of typen organisaties waarnaar doorgeschakeld kan worden via story instellingen"
        ),
        default=list,
        blank=True,
    )
    supports_handover = models.BooleanField(
        default=False,
        help_text=_(
            "Geeft aan of de gemeente een technische handover ondersteund op de website"
        ),
    )
    contact_url = models.URLField(
        max_length=1024,
        help_text=_("URL van de contactpagina van de organisatie"),
        blank=True,
    )
    phone_number = models.CharField(
        max_length=48, help_text=_("Telefoonnummer van de organisatie"), blank=True
    )
    municipality_logo = models.ImageField(
        verbose_name=_("Gemeente logo"),
        null=True,
        blank=True,
        help_text=_("Het logo dat in de menubalk getoond wordt voor deze gemeente."),
    )
    website_image = models.ImageField(
        verbose_name=_("Website afbeelding"),
        null=True,
        blank=True,
        help_text=_(
            "Een afbeelding van de website die getoond wordt voor niet ingelogde gebruikers van het CMS."
        ),
    )
    main_widget_color = models.CharField(
        verbose_name=_("Widget kleur"),
        blank=True,
        max_length=50,
        help_text=_(
            "De kleur van het chatbotwidget dat getoond wordt voor deze gemeente."
        ),
    )
    custom_widget_css = models.TextField(
        verbose_name=_("Custom widget styling"),
        blank=True,
        help_text=_(
            "Specifieke CSS om het widget te stijlen (wordt toegevoegd aan de "
            "gegenereerde styling op de `<gemeente>/_styling` pagina)"
        ),
    )
    # domains = ArrayField(
    #     base_field=models.CharField(max_length=1000),
    #     verbose_name=_("Gemeente domeinen"),
    #     help_text=_("Domeinen van de website van de gemeente"),
    #     default=list,
    # )

    # Global metadata configuration
    widget_title = models.CharField(
        default="Gem",
        verbose_name=_("Widget titel"),
        help_text=_("Titel van het widget zoals hij op de website komt te staan"),
        blank=True,
        max_length=255,
    )
    widget_subtitle = models.CharField(
        default="Je digitale hulp van de gemeente",
        verbose_name=_("Widget ondertitel"),
        help_text=_("Ondertitel van het widget zoals hij op de website komt te staan"),
        blank=True,
        max_length=255,
    )
    popup_message = models.CharField(
        default="Stel je vraag",
        verbose_name=_("Widget popupbericht"),
        help_text=_(
            "Bericht dat als popup verschijnt als het widget nog niet geopend is"
        ),
        blank=True,
        max_length=512,
    )
    widget_avatar = models.ImageField(
        verbose_name=_("Widget avatar"),
        null=True,
        blank=True,
        help_text=_("Avatar die in het widget getoond wordt voor deze gemeente"),
    )
    livechat_enabled = models.BooleanField(
        verbose_name=_("Livechat ingeschakeld"),
        default=True,
        help_text=_("Geeft aan of livechat ingeschakeld is voor deze organisatie"),
    )

    class Meta:
        permissions = (
            (
                "gemeente_read",
                _("Permissie om inhoud van gemeente dashboard in te zien"),
            ),
            (
                "utter_read",
                _("Permissie om inhoud van antwoorden in te zien"),
            ),
            (
                "utter_write",
                _(
                    "Permissie om inhoud van antwoorden aan te maken/wijzigen/verwijderen"
                ),
            ),
            (
                "hyperlink_read",
                _("Permissie om inhoud van hyperlinks/leges in te zien"),
            ),
            (
                "hyperlink_write",
                _(
                    "Permissie om inhoud van hyperlinks/leges aan te maken/wijzigen/verwijderen"
                ),
            ),
            (
                "statistics_read",
                _("Permissie om inhoud van statistieken in te zien"),
            ),
            (
                "dialog_read",
                _("Permissie om inhoud van gevoerde gesprekken met Gem in te zien"),
            ),
            (
                "dialog_evaluation_read",
                _("Permissie om evaluaties van gevoerde gesprekken met Gem in te zien"),
            ),
            (
                "dialog_evaluation_write",
                _(
                    "Permissie om evaluaties van gevoerde gesprekken met Gem aan te maken/passen"
                ),
            ),
            (
                "page_metadata_read",
                _(
                    "Permissie om metadata voor Gem op specifieke pagina's van de gemeente in te zien"
                ),
            ),
            (
                "page_metadata_write",
                _(
                    "Permissie om metadata voor Gem op specifieke pagina's van de gemeente aan te maken/wijzigen/verwijderen"
                ),
            ),
        )

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super().save(*args, **kwargs)

    @cached_property
    def default_livechat_config(self):
        return self.livechatconfiguration_set.order_by("order").first()


class GemeenteDomain(OrderedModel):
    gemeente = models.ForeignKey(
        Gemeente,
        on_delete=models.CASCADE,
        help_text=_("De gemeente waartoe dit domein behoort"),
    )
    domain = models.CharField(
        max_length=1000, help_text=_("Het domein van de gemeente")
    )
    gem_default_enabled = models.BooleanField(
        default=False,
        verbose_name=_("Gem standaard ingeschakeld"),
        help_text=_(
            "Geeft aan of Gem standaard ingeschakeld is (als dit niet expliciet aangegeven is "
            "voor een pagina)"
        ),
    )
    direct_handover = models.BooleanField(
        default=False,
        verbose_name=_("Directe doorschakeling naar medewerker"),
        help_text=_(
            "Geeft aan of er bij het openen van het widget meteen doorgeschakeld "
            "moet worden naar een KCC medewerker"
        ),
    )
    enable_indexing = models.BooleanField(
        _("Enabled Indexing"),
        default=False,
        help_text=_(
            "Enabled indexing of this domain. Setting is ignored for test domain."
        ),
    )
    environment = models.CharField(
        _("Environment"),
        max_length=64,
        choices=Environments.choices,
        default=Environments.TEST,
        help_text=_(
            "Production domains are indexed and return domain metadata per page."
            "Test domains are not indexed and will only return default metadata."
        ),
    )
    indexer = models.CharField(
        default=None,
        max_length=255,
        blank=True,
        null=True,
    )

    order_with_respect_to = "gemeente"

    class Meta(OrderedModel.Meta):
        unique_together = (
            "gemeente",
            "domain",
        )

    def __str__(self):
        return f"{self.gemeente.name} - {self.domain}"

    @property
    def enabled_pages(self):
        query = models.Q(gem_enabled=True)
        if self.gem_default_enabled:
            query |= models.Q(gem_enabled=None)

        return self.pagemetadata_set.filter(query).select_related("livechat_config")

    @property
    def disabled_pages(self):
        query = models.Q(gem_enabled=False)
        if not self.gem_default_enabled:
            query |= models.Q(gem_enabled=None)
        return self.pagemetadata_set.filter(query).values_list("path", flat=True)


class LivechatConfiguration(OrderedModel):
    gemeente = models.ForeignKey(
        Gemeente,
        on_delete=models.CASCADE,
        help_text=_("Gemeente waaraan deze livechat configuratie gekoppeld is."),
    )
    name = models.CharField(
        max_length=512, help_text=_("Naam van deze livechatkoppeling"), blank=True
    )
    identifier = models.CharField(
        help_text=_("ID van de livechat omgeving voor de gekoppelde gemeente."),
        max_length=512,
    )
    type = models.CharField(
        help_text=_("Type livechatkoppeling"),
        choices=LivechatTypes.choices,
        max_length=255,
        blank=True,
    )
    avatar_url = models.URLField(
        help_text=_("URL van de avatar die gebruikt wordt voor livechat."),
        blank=True,
    )
    avatar = models.ImageField(
        verbose_name=_("Avatar voor livechat"),
        null=True,
        blank=True,
        help_text=_("Avatar die gebruikt wordt voor livechat"),
    )

    history = AuditlogHistoryField()

    order_with_respect_to = "gemeente"

    class Meta:
        unique_together = (
            "gemeente",
            "identifier",
        )

    def __str__(self):
        return f"{self.name} ({self.type} - {self.identifier})"


class UtterResponse(models.Model):
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)
    utter = models.CharField(max_length=2550, help_text="Naam van de utter")
    response = models.TextField(help_text="Antwoord van de chatbot")
    gemeente = models.ForeignKey(
        Gemeente, on_delete=models.CASCADE, null=True, blank=True
    )
    image = models.URLField(
        blank=True,
        null=True,
        help_text="Verwijzing naar een afbeelding wat de chatbot doorstuurt, optioneel",
    )
    channel = models.ForeignKey(
        Channel,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        help_text="In te vullen indien het antwoord kanaal-specifiek is",
    )
    custom_html_answer = models.BooleanField(
        default=False,
        help_text="Of het antwoord in HTML is opgemaakt (custom utter template)",
    )
    type = models.CharField(
        max_length=50,
        blank=True,
        choices=[
            ("", "default"),
            ("ask_affirmation", "ask_affirmation (add top 2 intents as buttons)"),
        ],
    )
    is_staged = models.BooleanField(
        default=False,
        help_text=_(
            "Indicates whether the utter is a staged change, that will be effective "
            "after the next production update."
        ),
    )
    to_be_deleted = models.BooleanField(
        default=False,
        help_text=_(
            "Indicates whether this utter is staged to be deleted, will be effective "
            "after the next production update (only works if `is_staged` is True)"
        ),
    )

    history = AuditlogHistoryField()

    class Meta:
        verbose_name = "Antwoord"
        verbose_name_plural = "Antwoorden"

    def __str__(self):
        return "{}: {}".format(self.utter, self.response)

    @property
    def default_changed_after_local_changed(self):
        cached_local_responses_metadata = cache.get(LOCAL_RESPONSES_METADATA_KEY, {})

        # Check if the cached data exists and is in bytes format
        if isinstance(cached_local_responses_metadata, bytes):
            cached_local_responses_metadata = json.loads(
                cached_local_responses_metadata.decode("utf-8")
            )

        metadata = cached_local_responses_metadata.get(self.utter, {}).get(
            "metadata", {}
        )

        if not metadata or not metadata.get("changed"):
            return False

        return (
            timezone.make_aware(
                datetime.fromisoformat(metadata["changed"]),
                timezone=self.modified_on.tzinfo,
            )
            > self.modified_on
        )

    def get_button_data(self):
        buttons = list(self.buttons.values("title", "payload"))
        return buttons

    def save(self, *args, **kwargs):
        self.response = self.response.replace("\r", "")
        super().save(*args, **kwargs)

    # def get_intent_label(self, intent, gemeente, product):
    #     """
    #     Based on an intent from Rasa ("product_paspoort_aanvragen"), determine the correct
    #     Suggestion and return the textual representation of that label.

    #     If the intent is not available, create a new Suggestion and use the intent-name as
    #     the default label
    #     """
    #     existing_labels = Suggestion.objects.filter(intent=intent)

    #     # Gemeente+product-specific check
    #     gem_prod_label = existing_labels.filter(gemeente=gemeente, product=product)
    #     if gem_prod_label:
    #         return gem_prod_label[0].label

    #     # Gemeente-specific check
    #     gem_label = existing_labels.filter(gemeente=gemeente)
    #     if gem_label:
    #         return gem_label[0].label

    #     # Product-specific label check
    #     prod_label = existing_labels.filter(product=product)
    #     if prod_label:
    #         return prod_label[0].label

    #     # No specific label found
    #     intent_label, created = Suggestion.objects.get_or_create(
    #         intent=intent, defaults={"label": intent}
    #     )
    #     return intent_label.label

    # def get_affirmation_buttons(self, intent_ranking, gemeente, product):
    #     """
    #     Based on the intent_ranking from Rasa, return one or two possible intent-buttons with a sufficient confidence level.
    #     """
    #     if len(intent_ranking) > 1:
    #         diff_intent_confidence = intent_ranking[0].get(
    #             "confidence"
    #         ) - intent_ranking[1].get("confidence")
    #         if diff_intent_confidence < 0.2:
    #             intent_ranking = intent_ranking[:2]
    #         else:
    #             intent_ranking = intent_ranking[:1]

    #     first_intent_names = [
    #         intent.get("name", "")
    #         for intent in intent_ranking
    #         if intent.get("name", "") != "out_of_scope"
    #     ]

    #     # entities = tracker.latest_message.get("entities", [])
    #     # entities = {e["entity"]: e["value"] for e in entities}
    #     # logger.info(entities)
    #     entities_json = {}  # json.dumps(entities)

    #     buttons = []
    #     buttons_test = buttons
    #     for intent in first_intent_names:
    #         logger.info(intent)
    #         buttons.append(
    #             {
    #                 "title": "{}".format(
    #                     self.get_intent_label(intent, gemeente, product)
    #                 ),
    #                 "payload": "/{}".format(intent),  # entities_json?
    #             }
    #         )

    #     buttons.append({"title": "Iets anders", "payload": "/negative"})
    #     return buttons

    def fill_in_slots(self, text, slots):
        """
        Based on a textual response, fill in any slots (or arguments) we have received from Rasa.
        Ignore any empty slots.

        Eg.:

        Wil je je {product} aanvragen?

        with slots: {product: 'verhuizing'} will be converted to:

        Wil je je verhuizing aanvragen?
        """
        if not slots:
            return text
        for key, value in slots.items():
            if key and value:
                if isinstance(value, list):
                    value = value[0]
                if not isinstance(value, str):
                    continue
                text = text.replace("{%s}" % key, value)
        return text

    def data(
        self,
        gemeente=None,
        # product=None,
        slots=None,
        arguments=None,
        # intent_ranking=None,
    ):
        # if self.custom_html_answer:
        #     return {
        #         "custom": {
        #             "html": self.fill_in_slots(
        #                 self.fill_in_slots(self.response, slots), arguments
        #             )
        #         }
        #     }

        # buttons = self.get_button_data()
        # if self.type == "ask_affirmation":
        #     buttons = self.get_affirmation_buttons(intent_ranking, gemeente, product)

        text = self.format_hyperlinks(
            self.fill_in_slots(self.fill_in_slots(self.response, slots), arguments),
            gemeente,
        )

        response = {
            "text": text,
            # "buttons": buttons,
            "buttons": [],
            "image": self.image,
            "elements": [],
            "attachments": [],
        }
        return response

    @staticmethod
    def format_hyperlinks(
        response,
        gemeente,
    ):
        # By default, do not include staged hyperlinks
        kwargs = {"is_staged": False}

        def repl(matchobj):
            """
            Substitute CMS variables (e.g. <some_variable.label>) with the entered values
            """
            path = matchobj.group(0)[1:-1]
            try:
                name, attribute = path.split(".")
            except Exception:
                logger.info(f"Could not split path for {path}")
                return path

            hyperlink = Hyperlink.objects.filter(
                name=name, gemeente=gemeente, **kwargs
            ).last()
            if hyperlink and hasattr(hyperlink, attribute):
                return getattr(hyperlink, attribute)
            else:
                logger.info(f"No hyperlink {name} for {gemeente}")
                return path

        def repl_markdown_links(matchobj):
            """
            Substitute dynamic markdown hyperlinks with variables

            If the label and hyperlink name match, the label from the variable will be used.
            If not, the label is fixed
            """
            label = matchobj.group(1)
            name = matchobj.group(3)

            hyperlink = Hyperlink.objects.filter(
                name=name, gemeente=gemeente, **kwargs
            ).last()
            if hyperlink:
                if label == name:
                    return f"[{hyperlink.label}]({hyperlink.url})"
                else:
                    return f"[{label}]({hyperlink.url})"
            return label

        markdown_links_replaced = re.sub(
            MARKDOWN_LINK_REGEX, repl_markdown_links, response
        )

        return re.sub(HYPERLINK_NAME_REGEX, repl, markdown_links_replaced)


class Hyperlink(models.Model):
    name = models.CharField(
        max_length=255,
        help_text=_(
            "Unieke naam voor in het systeem. Niet aanpassen als deze al bestaat"
        ),
    )
    label = models.CharField(
        max_length=255,
        help_text=_(
            "Zichtbare tekst. Als 'URL' hieronder is ingevuld, wordt dit de klikbare tekst"
        ),
        blank=True,
    )
    url = MailtoAndURLField(
        max_length=2000,
        help_text=_(
            "Internetadres. Hier gaat de bezoeker naartoe als hij/zij op de klikbare tekst "
            "klikt. Alleen invullen indien van toepassing. Bij leges, bijvoorbeeld: kosten paspoort, "
            "of telefoonnummers etc. hoeft dit veld niet ingevuld te worden."
        ),
        blank=True,
    )
    title = models.CharField(
        max_length=1000,
        help_text=_(
            "Vul bij hyperlinks hier de tekst in voor gebruikers van hulptechnologie"
        ),
        blank=True,
    )
    gemeente = models.ForeignKey(
        Gemeente,
        blank=True,
        null=True,
        help_text=_("De gemeente waartoe deze hyperlink behoord."),
        on_delete=models.CASCADE,
    )
    is_staged = models.BooleanField(
        default=False,
        help_text=_(
            "Indicates whether the variable is only available on staging, that will be effective "
            "after the next production update."
        ),
    )

    history = AuditlogHistoryField()

    class Meta:
        verbose_name = "Hyperlink"
        verbose_name_plural = "Hyperlinks"

    def __str__(self):
        return f"{self.name} - {self.url} ({getattr(self.gemeente, 'name', '')})"


class VariableMetadata(models.Model):
    name = models.CharField(
        max_length=512, help_text=_("De machinenaam van de variabele")
    )
    category = models.CharField(
        max_length=255,
        help_text=_("De categorie waartoe deze variabele behoort"),
        blank=True,
    )
    verbose_name = models.CharField(
        max_length=512, help_text=_("De volledige naam van deze variabele"), blank=True
    )
    help_text = models.TextField(
        blank=True,
        help_text=_(
            "De help text die bij het aanmaken/bewerken van deze variabele verschijnt"
        ),
    )
    is_staged = models.BooleanField(
        default=False,
        help_text=_(
            "Indicates whether the variable is only available on staging, that will be effective "
            "after the next production update."
        ),
    )

    class Meta:
        verbose_name = _("Hyperlink en lege metadata")
        verbose_name_plural = _("Metadata voor hyperlinks en leges")


# TODO should probably be deprecated
class SiteConfiguration(SingletonModel):
    hyperlinks_show_only_used_attributes = models.BooleanField(
        default=False,
        help_text=_(
            "Indien aangevinkt, worden er op de aanmaak- en bewerkpagina's van "
            "hyperlinks/leges alleen de velden getoond die ook daadwerkelijk in "
            "utters gebruikt worden."
        ),
    )

    # Feature flags
    enable_staging_area = models.BooleanField(
        verbose_name=_("Staging area ingeschakeld"),
        default=False,
        help_text=_(
            "Indien aangevinkt, is het voor contentbeheerders mogelijk om de staging "
            "area in te zien en antwoorden/hyperlinks/leges alvast aan te maken."
        ),
    )
    disabled_story_categories = ArrayField(
        models.CharField(max_length=100),
        verbose_name=_("Uitgeschakelde story categorieën"),
        help_text=_(
            "Voor de aangegeven categorieën worden de story tegels NIET weergegeven"
        ),
        default=list,
        blank=True,
    )
    enabled_utter_categories = ArrayField(
        models.CharField(max_length=100),
        verbose_name=_("Ingeschakelde utter categorieën"),
        help_text=_(
            "Voor de aangegeven categorieën worden de utter tegels weergegeven"
        ),
        default=list,
        blank=True,
    )
    story_toggling_enabled = models.BooleanField(
        verbose_name=_("Aan/uitzetten van stories ingeschakeld"),
        default=False,
        help_text=_(
            "Indien aangevinkt, is het voor contentbeheerders mogelijk om de stories "
            "aan/uit te zetten per gemeente."
        ),
    )
    story_settings_default = models.BooleanField(
        verbose_name=_("Standaard waarde voor stories in- of uitgeschakeld"),
        default=True,
        help_text=_(
            "De standaardwaarde voor stories die niet expliciet in- of uitgeschakeld zijn"
        ),
    )

    class Meta:
        verbose_name = _("Site configuratie")


class Story(models.Model):
    identifier = models.CharField(
        max_length=1024,
        help_text=_("De unieke ID van de story"),
    )
    name = models.CharField(max_length=2048, help_text=_("Naam van de story"))
    category = models.CharField(max_length=256, help_text=_("De category van de story"))
    description = models.TextField(help_text=_("Naam van de story"), blank=True)
    date_created = models.DateTimeField(default=timezone.now)
    is_staging = models.BooleanField(
        default=False, help_text=_("Indicates whether this is a staging story")
    )

    class Meta:
        unique_together = (
            "identifier",
            "name",
        )


auditlog.register(UtterResponse)
auditlog.register(Hyperlink)
auditlog.register(LivechatConfiguration)
