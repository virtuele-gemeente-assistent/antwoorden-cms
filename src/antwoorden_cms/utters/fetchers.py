import json
import logging
from dataclasses import dataclass
from typing import Dict, List
from urllib.error import HTTPError

from django.conf import settings
from django.core.cache import cache

import requests
from requests.exceptions import ConnectionError, HTTPError as RequestsHTTPError, Timeout
from ruamel.yaml import YAML

from .constants import (
    ETAG_STORY_CATEGORIES_CACHE_KEY,
    STORY_CATEGORIES_CACHE_KEY,
    STORY_CATEGORIES_REFRESH_KEY,
)

logger = logging.getLogger(__name__)


@dataclass
class FunctionalStoriesFetcher:
    base_url: str = settings.MEDIA_CONTAINER_URL
    stories_path: str = settings.STORIES_PATH

    def _build_headers(self, etag: str = None) -> Dict[str, str]:
        """
        Helper method to construct HTTP headers, optionally including an ETag.
        """
        headers = {}
        if etag:
            headers["If-None-Match"] = etag
        return headers

    def _cache_data(self, key: str, data, etag_key: str = None, etag: str = None):
        """
        Helper method to cache data and optionally cache an ETag.
        """
        if etag_key and etag:
            logger.info("Setting ETag cache for key '%s': %s", etag_key, etag)
            cache.set(etag_key, etag, timeout=None)

        logger.info("Setting cache for key '%s'", key)
        cache.set(key, data, timeout=None)

    def retrieve_story_categories(self) -> List[str]:
        """
        Retrieve all story categories from the media container endpoint.
        """
        etag = cache.get(ETAG_STORY_CATEGORIES_CACHE_KEY)

        cache_exists = STORY_CATEGORIES_CACHE_KEY in cache

        if cache_exists and cache.get(STORY_CATEGORIES_REFRESH_KEY):
            logger.info("Refresh key exists for story categories. Using cache.")
            return cache.get(STORY_CATEGORIES_CACHE_KEY, [])

        headers = self._build_headers(etag)
        url = f"{self.base_url}{self.stories_path}"

        logger.info("Headers of retrieve_story_categories: %s", headers)
        logger.info("URL of retrieve_story_categories: %s", url)

        try:
            response = requests.get(
                url, headers=headers, timeout=settings.REQUESTS_TIMEOUT_SHORT
            )

            logger.info(
                "Response.status_code of retrieve_story_categories: %s",
                response.status_code,
            )

            if response.status_code == 304:
                logger.info("No new categories available. Using cached categories.")
                # Set the refresh key so subsequent calls use the cache
                cache.set(
                    STORY_CATEGORIES_REFRESH_KEY,
                    True,
                    timeout=settings.REQUESTS_TIMEOUT_LONG,
                )
                return cache.get(STORY_CATEGORIES_CACHE_KEY, [])

            if response.status_code != 200:
                logger.error(
                    "Failed to retrieve categories from %s. Status code: %d",
                    url,
                    response.status_code,
                )
                return cache.get(STORY_CATEGORIES_CACHE_KEY, [])

            categories = [
                category["name"].split(".")[0]
                for category in response.json()
                if not category["name"].startswith("_")
            ]

            logger.info("Retrieved %d categories.", len(categories))
            self._cache_data(
                STORY_CATEGORIES_CACHE_KEY,
                categories,
                ETAG_STORY_CATEGORIES_CACHE_KEY,
                etag,
            )

            # Flag a refresh so that repeated calls within a short interval do not trigger new requests.
            cache.set(
                STORY_CATEGORIES_REFRESH_KEY,
                True,
                timeout=settings.REQUESTS_TIMEOUT_LONG,
            )

            return categories

        except json.JSONDecodeError as e:
            logger.error("Failed to decode response from %s: %s", url, e)
            return cache.get(STORY_CATEGORIES_CACHE_KEY, [])

        except RequestsHTTPError as e:
            logger.error("HTTP error occurred while retrieving categories: %s", e)
            return cache.get(STORY_CATEGORIES_CACHE_KEY, [])

        except Timeout as e:
            logger.error("Request to %s timed out: %s", url, e)
            return cache.get(STORY_CATEGORIES_CACHE_KEY, [])

        except ConnectionError as e:
            logger.error(
                "Connection error while retrieving categories from %s: %s", url, e
            )
            return cache.get(STORY_CATEGORIES_CACHE_KEY, [])

        except Exception as e:
            logger.error(
                "Unexpected error while retrieving categories from %s: %s", url, e
            )
            return cache.get(STORY_CATEGORIES_CACHE_KEY, [])

    def retrieve_stories(self, category: str) -> List[Dict]:
        """
        Retrieve all stories for a category from the media container endpoint.
        """
        stories_refresh_key = f"{STORY_CATEGORIES_REFRESH_KEY}_{category}"
        stories_cache_key = f"retrieve_stories_{category}"
        etag_cache_key = f"retrieve_stories_etag_{category}"
        etag = cache.get(etag_cache_key)

        headers = self._build_headers(etag)
        url = f"{self.base_url}{self.stories_path}{category}.yml"

        logger.info("Headers of retrieve_stories: %s", headers)
        logger.info("URL of retrieve_stories: %s", url)

        cache_exists = stories_cache_key in cache

        if cache_exists and cache.get(stories_refresh_key):
            logger.info(
                "Refresh key exists for story category '%s'. Using cache.", category
            )
            return cache.get(stories_cache_key, [])

        try:
            response = requests.get(
                url,
                headers=headers,
                timeout=settings.REQUESTS_TIMEOUT_SHORT,
                allow_redirects=False,
            )

            logger.info(
                "Response.status_code of retrieve_stories: %s", response.status_code
            )
            etag = response.headers.get("ETag")
            location = response.headers.get("Location")

            if response.status_code == 302:
                logger.info("Redirecting to %s", location)
                response = requests.get(
                    location, headers=headers, timeout=settings.REQUESTS_TIMEOUT_SHORT
                )

            if response.status_code == 304:
                logger.info(
                    "No new stories for category '%s'. Using cached stories.", category
                )
                # Set the refresh key so subsequent calls use the cache
                cache.set(
                    stories_refresh_key,
                    True,
                    timeout=settings.REQUESTS_TIMEOUT_LONG,
                )
                return cache.get(stories_cache_key, [])

            response.raise_for_status()

            yaml = YAML(typ="safe")
            stories = yaml.load(response.content)

            self._cache_data(stories_cache_key, stories, etag_cache_key, etag)

            logger.info("Retrieved and cached stories for category '%s'.", category)
            # Flag a refresh so that repeated calls within a short interval do not trigger new requests.
            cache.set(stories_refresh_key, True, timeout=settings.REQUESTS_TIMEOUT_LONG)
            return stories

        except (HTTPError, RequestsHTTPError) as e:
            logger.error(
                "HTTP error while retrieving stories for category '%s' from %s: %s",
                category,
                url,
                e,
            )
            return cache.get(stories_cache_key, [])

        except Timeout as e:
            logger.error(
                "Request to %s timed out for category '%s': %s", url, category, e
            )
            return cache.get(stories_cache_key, [])

        except ConnectionError as e:
            logger.error(
                "Connection error while retrieving stories for category '%s' from %s: %s",
                category,
                url,
                e,
            )
            return cache.get(stories_cache_key, [])

        except Exception as e:
            logger.error(
                "Unexpected error while retrieving stories for category '%s' from %s: %s",
                category,
                url,
                e,
            )
            return cache.get(stories_cache_key, [])
