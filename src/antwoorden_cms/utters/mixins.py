from typing import List
from urllib.parse import urlparse

from django.contrib.auth.mixins import PermissionRequiredMixin, UserPassesTestMixin
from django.http import Http404

from rest_framework.permissions import BasePermission

from .dataclasses import UtterData
from .models import Gemeente


class OrganisationPermission(BasePermission):
    """
    Allows access only to users that have permission for municipalities.
    """

    def get_permission_required(self, view):
        permission_required = view.permission_required
        if isinstance(permission_required, str):
            return [permission_required]
        return permission_required

    def get_permission_object(self, request):
        try:
            return Gemeente.objects.get(
                slug=request.parser_context["kwargs"]["organisation"]
            )
        except Gemeente.DoesNotExist:
            return None

    def has_permission(self, request, view):
        gemeente = self.get_permission_object(request)
        if not gemeente:
            raise Http404

        return gemeente in request.user.gemeentes.all() and request.user.has_perms(
            self.get_permission_required(view)
        )


class ObjectPermissionMixin(PermissionRequiredMixin):
    def get_permission_object(self):
        try:
            return Gemeente.objects.get(slug=self.kwargs["gemeente_slug"])
        except Gemeente.DoesNotExist:
            return None

    def has_permission(self):
        """
        Only allow access if the user has the correct permissions and is
        assigned to the current gemeente
        """
        gemeente = self.get_permission_object()
        if not gemeente:
            raise Http404

        return (
            gemeente in self.request.user.gemeentes.all() and super().has_permission()
        )


class StaffRequiredMixin(UserPassesTestMixin):
    def test_func(self):
        return self.request.user.is_staff


class RedirectMixin:
    def get_success_url(self):
        url = self.request.session.get("redirect_url")
        if url:
            del self.request.session["redirect_url"]
            parsed = urlparse(url)

            # Only redirect if the url belongs to the current domain and is not
            # the same page as the current page
            if (
                self.request.get_host() == parsed.netloc
                and not parsed.path == self.request.path
            ):
                return url

    def get(self, request, *args, **kwargs):
        self.request.session["redirect_url"] = self.request.META.get("HTTP_REFERER")
        return super().get(request, *args, **kwargs)


class SortingMixin:
    default_sort_param = ""
    default_sort_order = ""

    def aggregate_utter_data(self):
        raise NotImplementedError

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["sort"] = self.request.GET.get("sort", self.default_sort_param)
        context["order"] = self.request.GET.get("order", self.default_sort_order)

        unsorted_result = context.get("utters", self.aggregate_utter_data())

        sorted_result = sorted(
            unsorted_result,
            key=UtterData.get_sort_func(context["sort"], context["order"]),
            reverse=context["order"] == "desc",
        )
        context["utters"] = sorted_result

        return context


class SearchMixin:
    def aggregate_utter_data(self):
        raise NotImplementedError

    def perform_search(
        self, utters: List[UtterData], search_query: str
    ) -> List[UtterData]:
        res = []
        for utter in utters:
            # Display this utter if there was no search query (or if there was a match)
            if not search_query or utter.perform_search_query(search_query):
                res.append(utter)
        return res

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        search_query = self.request.GET.get("search", "").lower()

        utters = context.get("utters", self.aggregate_utter_data())

        context["utters"] = self.perform_search(utters, search_query)
        context["search_query"] = search_query

        return context
