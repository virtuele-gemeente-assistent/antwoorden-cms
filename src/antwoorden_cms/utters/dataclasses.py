import logging
from dataclasses import dataclass
from datetime import MAXYEAR, MINYEAR, datetime
from typing import List, Optional

from django.utils.timezone import make_aware

from .models import Gemeente, UtterResponse

logger = logging.getLogger(__name__)


def perform_utter_search(utter_data, search_query):
    for response in utter_data["default"]:
        if search_query in response["text"].lower():
            return True

    for question in utter_data["metadata"].get("questions", []):
        if search_query in question.lower():
            return True

    for example in utter_data["metadata"].get("examples", []):
        if search_query in example.lower():
            return True

    return False


@dataclass
class UtterData:
    """
    Class to wrap local response data with defaults and metadata
    """

    utter_name: str
    metadata: dict
    gemeente: Gemeente
    local_default_prod: Optional[UtterResponse] = None
    local_override_prod: Optional[UtterResponse] = None

    @property
    def default_modified_on(self):
        if not self.metadata["metadata"].get("changed"):
            return None
        else:
            return datetime.fromisoformat(self.metadata["metadata"]["changed"])

    @property
    def default_created_on(self):
        if not self.metadata["metadata"].get("created"):
            return None
        else:
            return datetime.fromisoformat(self.metadata["metadata"]["created"])

    @property
    def staging_action_required(self):
        # If there is no changed date in metadata, no action is required
        if not self.default_modified_on:
            return False

        if self.local_override_prod and not self.local_override_staging:
            default_modified_on = make_aware(
                self.default_modified_on,
                timezone=self.local_override_prod.modified_on.tzinfo,
            )
            # Metadata changed date is newer than local override modified date
            if default_modified_on > self.local_override_prod.modified_on:
                return True
        elif self.local_override_staging:
            default_modified_on = make_aware(
                self.default_modified_on,
                timezone=self.local_override_staging.modified_on.tzinfo,
            )
            # Metadata changed date is newer than local staging override modified date
            if default_modified_on > self.local_override_staging.modified_on:
                return True
        # No action required if defaults are used
        return False

    def get_instance(self):
        return self.local_override_prod or self.local_default_prod

    @property
    def formatted_default_prod_response(self):
        if not self.local_default_prod:
            return ""

        return UtterResponse.format_hyperlinks(
            self.local_default_prod.response, self.gemeente
        )

    @property
    def formatted_prod_response(self):
        return UtterResponse.format_hyperlinks(
            getattr(self.get_instance(), "response", ""),
            self.gemeente,
        )

    def perform_search_query(self, search_query):
        if perform_utter_search(self.metadata, search_query):
            return True

        if (
            search_query in self.formatted_prod_response.lower()
            or search_query in self.formatted_default_prod_response.lower()
        ):
            return True
        return False

    @staticmethod
    def get_sort_func(sorting_param, order):
        def sort(obj):
            # Local variant exists
            if sorting_param == "utter":
                return obj.utter_name
            elif sorting_param == "questions":
                return obj.metadata["metadata"]["questions"]
            elif sorting_param == "response":
                return obj.formatted_prod_response
            elif sorting_param == "default_response":
                return obj.formatted_default_prod_response
            elif sorting_param == "created":
                return obj.default_created_on
            elif sorting_param == "modified_on":
                # Ensure null values always appear at the end
                if obj.default_modified_on is None:
                    if order == "desc":
                        return datetime(MINYEAR, 1, 1)
                    else:
                        return datetime(MAXYEAR, 12, 31)
                return obj.default_modified_on

        return sort

    def __repr__(self):
        return f"UtterData({self.utter_name})"

    def __str__(self):
        return f"UtterData({self.utter_name})"


class HashableDict(dict):
    def __hash__(self):
        values = [frozenset(x) if isinstance(x, dict) else x for x in self.values()]
        return hash((frozenset(self), frozenset(values)))


class Story:
    def __init__(
        self,
        story_name,
        story_id,
        conversation,
        local_utter_data,
        central_utter_data,
        metadata=None,
    ):
        self._response_cache = {}
        self.story_name = story_name
        self.story_id = story_id
        self.conversation = conversation
        self.metadata = metadata or {}

        self._local_utter_data = local_utter_data
        self._central_utter_data = central_utter_data

        translated = []
        for messages in self._bot_response:

            texts = []
            for text in messages[0]["text"]:
                texts.append(self.translate_utter(text))

            messages[0]["text"] = texts
            translated.append(messages[0])
        self.bot_messages = translated

        self.chains = self.get_chains()

    def _create_chain_story(self, messages):

        return {
            "story_id": self.story_id,
            "metadata": self.metadata,
            "messages": self.messages + messages,
        }

    def get_chains(self):

        stories = []
        chain = self.conversation.get("chain", [])

        for story in chain:

            sub_story = Story(
                story_name=story["name"],
                story_id=self.story_id,
                conversation=story,
                local_utter_data=self._local_utter_data,
                central_utter_data=self._central_utter_data,
                metadata=self.metadata,
            )

            if sub_story.chains:
                for sub_chain in sub_story.chains:
                    stories.append(self._create_chain_story(sub_chain["messages"]))
            else:
                stories.append(self._create_chain_story(sub_story.messages))

        if len(stories) < 1:
            stories = [self._create_chain_story([])]

        return stories

    def translate_utter(self, utter_name):

        if "utter_lo" in utter_name:
            if utter_name not in self._local_utter_data:
                logger.warning("Utter %s not found in local utter data", utter_name)
                result = {"string": ""}

            else:
                data = self._local_utter_data[utter_name]
                instance = data.get_instance()

                result = {
                    "string": UtterResponse.format_hyperlinks(
                        getattr(instance, "response", ""),
                        data.gemeente,
                    ),
                    "instance": instance,
                }
        else:
            central_data = self._central_utter_data.get(utter_name, [{}])[0]
            result = {"string": central_data.get("text", utter_name)}

        return result

    @property
    def user_messages(self):
        return self.conversation["input"]

    @property
    def _bot_response(self):
        return [self.conversation["output"]["responses"]]

    @property
    def messages(self):
        return [{"user": self.user_messages, "bot": self.bot_messages}]

    def search(self, search_query: str) -> bool:
        for message in self.bot_messages:
            for text in message["text"]:

                if search_query in text["string"]:
                    return True

        for message in self.user_messages:
            if search_query in message:
                return True

        return False

    def __repr__(self):
        return f"Story({self.story_name})"

    def __str__(self):
        return f"Story({self.story_name})"


def search_stories(search_query: str, stories: List[Story]) -> List[Story]:
    return [story for story in stories if story.search(search_query)]
