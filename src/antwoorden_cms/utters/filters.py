from django.contrib.admin import SimpleListFilter
from django.utils.translation import gettext_lazy as _


class LatestModifiedUtterFilter(SimpleListFilter):
    title = _("Datum aangepast")

    parameter_name = "modified_on"

    def lookups(self, request, model_admin):
        return [
            ("latest_modified", _("Laatst aangepast")),
            (None, _("No filter")),
        ]

    def default_value(self):
        return None

    def choices(self, cl):
        for lookup, title in self.lookup_choices:
            if self.value() is None and lookup == self.default_value():
                selected = True
            else:
                selected = self.value() == lookup
            yield {
                "selected": selected,
                "query_string": cl.get_query_string(
                    {
                        self.parameter_name: lookup,
                    },
                    [],
                ),
                "display": title,
            }

    def queryset(self, request, queryset):
        if self.value() == "latest_modified":
            return queryset.filter(gemeente__isnull=False).order_by("-modified_on")
        return queryset
