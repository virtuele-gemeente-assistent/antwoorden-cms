from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

import yaml

from ...models import VariableMetadata


class Command(BaseCommand):
    args = "local.yml"
    help = "Load local.yml file to extract and import default utters"

    def add_arguments(self, parser):
        parser.add_argument("--filename", type=str)
        parser.add_argument("--stream", type=bytes)
        parser.add_argument("--override", type=bool)
        parser.add_argument("--is-staged", type=bool)

    @transaction.atomic
    def parse_hyperlinks(self, hyperlinks, override=False, is_staged=False):
        if override:
            VariableMetadata.objects.filter(is_staged=is_staged).delete()

        # TODO only use metadata that does not exist on prod
        if is_staged:
            existing_prod_metadata = VariableMetadata.objects.filter(
                is_staged=False
            ).values_list("name", flat=True)
            hyperlinks = {
                name: data
                for name, data in hyperlinks.items()
                if name not in existing_prod_metadata
            }

        for name, value in hyperlinks.items():
            if not value:
                continue

            if "metadata" not in value:
                continue

            metadata = value["metadata"]
            VariableMetadata.objects.create(
                name=name,
                category=metadata.get("category", ""),
                verbose_name=metadata.get("name", ""),
                help_text=metadata.get("helptext", metadata.get("help_text", "")),
                is_staged=is_staged,
            )

    def handle(self, *args, **options):
        filename = options.get("filename")
        stream = options.get("stream")
        override = options.get("override")
        is_staged = options.get("is_staged") or False

        if filename and stream or (not filename and not stream):
            raise CommandError("Supply either --filename or --stream")

        if filename:
            print("Parsing {} file".format(filename))
            with open(filename, "r") as stream:
                data = yaml.safe_load(stream)
                self.parse_hyperlinks(data.get("hyperlinks"), is_staged=is_staged)
        elif stream:
            data = yaml.safe_load(stream)
            self.parse_hyperlinks(
                data.get("hyperlinks"), override=override, is_staged=is_staged
            )
