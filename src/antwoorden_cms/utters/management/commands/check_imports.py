import logging

from django.conf import settings
from django.core.management import call_command
from django.core.management.base import BaseCommand
from django.db import transaction

import requests

from ...utils import cache_utter_metadata

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Check if remote model was updated and run imports if this is the case"

    def import_prod_data(self, domain_content):
        if settings.ENVIRONMENT == "production":
            logger.info("Rasa model was updated, applying staged changes")
            # Issue: https://gitlab.com/virtuele-gemeente-assistent/gem/-/issues/3797
            # FIXME disabled for now
            # call_command("apply_staged_changes")

        logger.info("Rasa model was updated, running imports")
        logger.info("Importing default responses from rasa endpoint")
        call_command("import_default_responses", stream=domain_content, override=True)

        response = requests.get(
            settings.LOCAL_YML_URL, timeout=settings.REQUESTS_TIMEOUT_LONG
        )
        if response.status_code == 200:
            logger.info("Importing variable metadata from gitlab")
            call_command(
                "import_hyperlink_metadata", stream=response.content, override=True
            )

    @transaction.atomic
    def handle(self, *args, **options):
        logger.info(
            "Consulting domain fingerprint for default responses and metadata import"
        )
        (
            domain_changed,
            domain_content,
        ) = cache_utter_metadata()

        if domain_changed:
            self.import_prod_data(domain_content)
