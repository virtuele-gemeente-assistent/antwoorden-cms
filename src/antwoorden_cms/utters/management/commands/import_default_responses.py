import json
import logging

from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

import yaml

from ...models import Channel, UtterResponse

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    args = "domain.yml"
    help = "Load local defaults from a domain file"

    def add_arguments(self, parser):
        parser.add_argument("--filename", type=str)
        parser.add_argument("--stream", type=bytes)
        parser.add_argument("--override", type=bool)

    @transaction.atomic
    def parse_templates(self, responses, is_staged, override=False):
        if override:
            UtterResponse.objects.filter(gemeente=None, is_staged=is_staged).delete()

        for utter, value in responses.items():
            if not value:
                continue

            if "_lo_" not in utter:
                continue

            utter_metadata = {}
            for channel_data in value:
                if channel_data.get("channel", "") == "metadata":
                    utter_metadata = channel_data["custom"]
                    break

            if not utter_metadata:
                logger.info("No utter metadata for %s", utter)
                continue

            for utterance in utter_metadata["default"]:
                # Filter instead of get_or_create, because duplicates might exist
                results = UtterResponse.objects.filter(
                    utter=utter, gemeente=None, is_staged=is_staged
                )
            if not results.exists():
                response = utterance["text"]
                # response_type = "ask_affirmation" if utter == "utter_ask_affirmation" else ""
                obj = UtterResponse.objects.create(
                    utter=utter, response=response, gemeente=None, is_staged=is_staged
                )
                if "image" in utterance:
                    obj.image = utterance["image"]
                if "channel" in utterance:
                    channel, created = Channel.objects.get_or_create(
                        name=utterance["channel"]
                    )
                    obj.channel = channel
                obj.save()

    def handle(self, *args, **options):
        filename = options.get("filename")
        stream = options.get("stream")
        override = options.get("override")
        is_staged = False

        if filename and stream or (not filename and not stream):
            raise CommandError("Supply either --filename or --stream")

        if filename:
            print("Parsing {} file".format(filename))
            with open(filename, "r") as stream:
                data = yaml.safe_load(stream)
                responses = data.get("responses")
                self.parse_templates(responses, is_staged)
        elif stream:
            data = json.loads(stream)
            responses = data.get("responses")
            self.parse_templates(responses, is_staged, override=override)
