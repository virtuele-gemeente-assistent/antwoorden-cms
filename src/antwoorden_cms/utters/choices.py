from django.db.models import TextChoices
from django.utils.translation import gettext_lazy as _

from djchoices import ChoiceItem, DjangoChoices


class LivechatTypes(DjangoChoices):
    obi4wan = ChoiceItem("obi4wan", _("Obi4Wan"))
    livecom = ChoiceItem("livecom", _("Livecom"))
    salesforce = ChoiceItem("salesforce", _("Salesforce"))
    coosto = ChoiceItem("coosto", _("Coosto"))


class Environments(TextChoices):
    PRODUCTION = "production", _("Production")
    TEST = "test", _("Test")
