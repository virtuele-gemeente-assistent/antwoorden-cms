# Regex to match variables in utter responses
HYPERLINK_NAME_REGEX = r"<[A-z0-9\.\_\-]*>"

# Regex to match hyperlinks in markdown, used for dynamic hyperlinks
MARKDOWN_LINK_REGEX = r"\[([\w\s\d-]+)\]\(((?:\/|https?:\/\/)demodam.nl\/([\w\d-]*))\)"

DEFAULT_CACHE_TIMEOUT = 24 * 60 * 60

# Caching keys for utter metadata
RESPONSES_REFRESH_KEY = "responses_etag_no_refresh"
STORY_CATEGORIES_REFRESH_KEY = "stories_etag_no_refresh"
STORY_CATEGORIES_CACHE_KEY = "story_categories"
ETAG_STORY_CATEGORIES_CACHE_KEY = "retrieve_story_categories_etag"
ETAG_CACHE_KEY = "retrieve_local_utter_metadata_etag"
DOMAIN_TAG_KEY = "domain_tag"
CENTRAL_RESPONSES_KEY = "central_responses"
LOCAL_RESPONSES_METADATA_KEY = "local_responses_metadata"
