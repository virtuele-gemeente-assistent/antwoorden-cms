import logging

from django.db import transaction
from django.http import Http404
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_control

from djangorestframework_camel_case.parser import (
    CamelCaseJSONParser,
    CamelCaseMultiPartParser,
)
from djangorestframework_camel_case.render import CamelCaseJSONRenderer
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import exceptions, mixins
from rest_framework.authentication import SessionAuthentication
from rest_framework.decorators import action
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import BrowsableAPIRenderer
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet

from antwoorden_cms.config.models import OrganisationStorySettings, PageMetadata
from antwoorden_cms.utils.render import DomainMetadataCamelCaseJSONRenderer

from ..choices import Environments
from ..mixins import OrganisationPermission
from ..models import Gemeente, GemeenteDomain, SiteConfiguration, UtterResponse
from .serializers import (
    DomainMetadataDisabledSerializer,
    DomainMetadataSerializer,
    OrganisationDomainSerializer,
    OrganisationSerializer,
    OrganisationStorySettingsSerializer,
    PageMetadataSerializer,
    ScrapyWebhookSerializer,
    TestDomainMetadataSerializer,
    UtterResponseSerializer,
)

logger = logging.getLogger(__name__)

UTTER_NAME_PATH_PARAM = openapi.Parameter(
    "utter_name",
    openapi.IN_PATH,
    description="Naam van de utter",
    type=openapi.TYPE_STRING,
)

CHANNEL_QUERY_PARAM = openapi.Parameter(
    "channel",
    openapi.IN_QUERY,
    description="Naam van het kanaal",
    type=openapi.TYPE_STRING,
)

ORGANISATION_SLUG_PATH_PARAM = openapi.Parameter(
    "organisation_slug",
    openapi.IN_PATH,
    description="Slug van de organisatie",
    type=openapi.TYPE_STRING,
)


@method_decorator(
    name="retrieve",
    decorator=swagger_auto_schema(
        manual_parameters=[CHANNEL_QUERY_PARAM, UTTER_NAME_PATH_PARAM],
        operation_description="Haal een utter op",
    ),
)
class UtterResponseViewSet(ModelViewSet):
    """
    Retrieve an Utter
    """

    queryset = UtterResponse.objects.all()
    serializer_class = UtterResponseSerializer
    lookup_field = "utter_name"

    def get_object(self):
        channel = self.request.query_params.get("channel")
        use_staging_data = self.request.query_params.get("useStagingData") in [
            "True",
            "true",
        ]
        utter = self.kwargs[self.lookup_field]
        gemeente_slug = self.kwargs["gemeente"]

        # If gemeente is not connected, return 404
        gemeente = get_object_or_404(Gemeente, slug=gemeente_slug)

        qs = self.queryset.filter(utter=utter, gemeente=gemeente, is_staged=False)

        if use_staging_data:
            staging_qs = self.queryset.filter(
                utter=utter, gemeente=gemeente, is_staged=True
            )
            if staging_qs:
                if channel:
                    channel_filtered = staging_qs.filter(channel__name=channel)
                    if channel_filtered:
                        return channel_filtered.first()
                return staging_qs.first()
            # Only use staging default if there is no prod override
            elif not qs:
                staging_qs_default = self.queryset.filter(
                    utter=utter, gemeente=None, is_staged=True
                )
                if staging_qs_default:
                    if channel:
                        channel_filtered = staging_qs_default.filter(
                            channel__name=channel
                        )
                        if channel_filtered:
                            return channel_filtered.first()
                    return staging_qs_default.first()

        # FIXME should filter on channel as well
        # If utter does not exist for gemeente, use default as fallback
        if not qs.exists():
            return get_object_or_404(
                self.queryset, utter=utter, gemeente=None, is_staged=False
            )

        # If a specific response exists for the channel, return it
        if channel:
            channel_filtered = qs.filter(channel__name=channel)
            if channel_filtered:
                return channel_filtered.first()

        # Fallback: return response without specified channel
        obj = get_object_or_404(qs, utter=utter, is_staged=False, channel__name=None)
        return obj


@method_decorator(
    name="retrieve",
    decorator=swagger_auto_schema(
        manual_parameters=[ORGANISATION_SLUG_PATH_PARAM],
        operation_description="Haal een organisatie op",
    ),
)
class OrganisationDomainViewSet(ModelViewSet):
    """
    Retrieve an Organisation Domain
    """

    queryset = GemeenteDomain.objects.all()
    serializer_class = OrganisationDomainSerializer
    lookup_field = "gemeente__slug"
    lookup_url_kwarg = "organisation_slug"

    def retrieve(self, request, *args, **kwargs):
        gemeente_slug = self.kwargs.get(self.lookup_url_kwarg)
        try:
            gemeente = Gemeente.objects.get(slug=gemeente_slug)
            domains = self.queryset.filter(gemeente=gemeente)

            if not domains.exists():
                raise Http404(
                    f"No domains found for gemeente with slug '{gemeente_slug}'"
                )

            # Serialize all domains for this gemeente
            serializer = self.get_serializer(domains, many=True)
            return Response(serializer.data)
        except Gemeente.DoesNotExist:
            raise Http404(f"No gemeente found with slug '{gemeente_slug}'")


class PageMetadataView(mixins.CreateModelMixin, ReadOnlyModelViewSet):
    serializer_class = PageMetadataSerializer
    renderer_classes = (CamelCaseJSONRenderer,)
    parser_classes = (
        CamelCaseJSONParser,
        CamelCaseMultiPartParser,
    )


@method_decorator(cache_control(max_age=300), "dispatch")
class DomainMetadataView(ReadOnlyModelViewSet):
    renderer_classes = (
        DomainMetadataCamelCaseJSONRenderer,
        BrowsableAPIRenderer,
    )
    queryset = GemeenteDomain.objects.all().select_related("gemeente")

    def get_serializer_class(self):
        domain = self.get_object()

        if domain.environment == Environments.TEST:
            return TestDomainMetadataSerializer

        if domain.gem_default_enabled:
            return DomainMetadataSerializer
        return DomainMetadataDisabledSerializer

    def get_object(self):
        domain = self.request.query_params["domain"]
        domain = domain.replace("www.", "", 1)
        if domain.startswith("#"):
            gemeente = Gemeente.objects.get(name=domain[1:])

            domain = gemeente.gemeentedomain_set.first()
            if domain is None:
                raise Http404()
            return domain
        return get_object_or_404(self.queryset, domain=domain)

    def retrieve(self, request, *args, **kwargs):
        if "domain" not in self.request.query_params:
            raise exceptions.ValidationError("`domain` not present in request body")
        return super().retrieve(request, *args, **kwargs)


class OrganisationStorySettingsViewSet(ModelViewSet):
    authentication_classes = [SessionAuthentication]
    serializer_class = OrganisationStorySettingsSerializer
    queryset = OrganisationStorySettings.objects.all()
    permission_required = "utters.utter_write"

    def get_serializer(self, *args, **kwargs):
        return super().get_serializer(*args, **kwargs)

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context.update(self.kwargs)
        return context

    @property
    def permission_classes(self):
        if self.request.method == "GET":
            return []
        return [IsAuthenticated, OrganisationPermission]

    def get_queryset(self):
        organisation = get_object_or_404(Gemeente, slug=self.kwargs["organisation"])
        return super().get_queryset().filter(organisation=organisation)

    def get_object(self):
        organisation = get_object_or_404(Gemeente, slug=self.kwargs["organisation"])
        obj = OrganisationStorySettings.objects.filter(
            organisation=organisation,
            story_id=self.kwargs["story_id"],
        ).first()
        if obj:
            return obj

        if self.request.method == "GET":
            return OrganisationStorySettings(
                organisation=organisation,
                story_id=self.kwargs["story_id"],
                enabled=SiteConfiguration.get_solo().story_settings_default,
                handover_to_organisation="",
                crisis_answer=False,
            )
        else:
            raise Http404

    @transaction.atomic
    @action(detail=False, methods=["post"])
    def bulk_create_or_update(self, request, organisation=None):
        organisation = get_object_or_404(Gemeente, slug=organisation)

        serializer = self.get_serializer(data=request.data, many=True)
        serializer.is_valid(raise_exception=True)

        story_ids = [item["story_id"] for item in request.data]
        story_id_mapping = {item.pop("story_id"): item for item in request.data}

        existing_settings = OrganisationStorySettings.objects.filter(
            organisation=organisation, story_id__in=story_ids
        )
        settings_to_update = []
        for setting in existing_settings:
            changes = story_id_mapping.pop(setting.story_id)
            setting.enabled = changes["enabled"]
            setting.handover_to_organisation = changes.get(
                "handover_to_organisation", ""
            )
            setting.crisis_answer = changes.get("crisis_answer", False)
            setting.handover_livechat = changes.get("handover_livechat", False)
            setting.parking_answer = changes.get("parking_answer", False)
            settings_to_update.append(setting)

        OrganisationStorySettings.objects.bulk_update(
            settings_to_update,
            [
                "enabled",
                "handover_to_organisation",
                "crisis_answer",
                "handover_livechat",
                "parking_answer",
            ],
        )

        settings_to_create = []
        for story_id, data in story_id_mapping.items():
            settings_to_create.append(
                OrganisationStorySettings(
                    organisation=organisation,
                    story_id=story_id,
                    enabled=data["enabled"],
                    handover_to_organisation=data.get("handover_to_organisation", ""),
                    crisis_answer=data.get("crisis_answer", False),
                    handover_livechat=data.get("handover_livechat", False),
                    parking_answer=data.get("parking_answer", False),
                )
            )

        OrganisationStorySettings.objects.bulk_create(settings_to_create)

        return Response(data=None, status=204)


class OrganisationViewSet(ReadOnlyModelViewSet):
    queryset = Gemeente.objects.all()
    serializer_class = OrganisationSerializer
    lookup_url_kwarg = "organisation_slug"
    lookup_field = "slug"


class ScrapyWebhookView(GenericAPIView):
    queryset = PageMetadata.objects.all().select_related(
        "gemeente_domain", "gemeente_domain__gemeente"
    )
    serializer_class = ScrapyWebhookSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data

        domain = data["domain"].replace("www.", "")
        if data.get("mode") == "full":
            self.queryset.filter(gemeente_domain__domain=domain).update(
                page_on_site=False
            )

        event = data["event"]
        if event == "start":
            data["batch"] = True
        elif event in ["add", "update", "batch"]:
            serializer.save()
        elif event == "delete":
            serializer.save()
        elif event == "end":
            to_be_deleted = self.queryset.filter(
                gemeente_domain__domain=domain,
                page_on_site=False,
                gem_enabled=None,
                direct_handover=None,
            )
            to_be_deleted.delete()
        else:
            logger.warning("Unknown event: {}".format(event))

        # remove items
        data.pop("items", None)
        return Response(data=data)
