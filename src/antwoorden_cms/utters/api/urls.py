from django.conf import settings
from django.urls import path, re_path

from ..apps import AppConfig
from .api import schema_view
from .views import (
    DomainMetadataView,
    OrganisationDomainViewSet,
    OrganisationStorySettingsViewSet,
    OrganisationViewSet,
    PageMetadataView,
    ScrapyWebhookView,
    UtterResponseViewSet,
)

app_name = AppConfig.__name__


urlpatterns = [
    re_path(
        r"^swagger(?P<format>\.json|\.yaml)$",
        schema_view.without_ui(cache_timeout=0),
        name="schema-json",
    ),
    re_path(
        r"^swagger/$",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    re_path(
        r"^redoc/$", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc"
    ),
    path(
        "organisations/",
        OrganisationViewSet.as_view(
            {
                "get": "list",
            },
        ),
        name="organisation_list",
    ),
    path(
        "organisations/<slug:organisation_slug>/",
        OrganisationViewSet.as_view(
            {
                "get": "retrieve",
            },
        ),
        name="organisation_detail",
    ),
    path(
        "organisations/domains/<slug:organisation_slug>/",
        OrganisationDomainViewSet.as_view(
            {
                "get": "retrieve",
            },
        ),
        name="organisation_domains",
    ),
    path(
        "<slug:gemeente>/utters/<slug:utter_name>/",
        UtterResponseViewSet.as_view({"get": "retrieve"}),
        name="utters",
    ),
    path(
        "<slug:organisation>/stories/",
        OrganisationStorySettingsViewSet.as_view(
            {
                "get": "list",
            }
        ),
        name="story_settings_list",
    ),
    path(
        "<slug:organisation>/stories/_bulk_create_or_update/",
        OrganisationStorySettingsViewSet.as_view(
            {
                "post": "bulk_create_or_update",
            }
        ),
        name="story_settings_bulk_endpoint",
    ),
    path(
        "<slug:organisation>/stories/<slug:story_id>/",
        OrganisationStorySettingsViewSet.as_view(
            {
                "get": "retrieve",
                "post": "create",
                "put": "update",
                "patch": "partial_update",
                "delete": "destroy",
            }
        ),
        name="story_settings",
    ),
    path(
        "page-metadata",
        PageMetadataView.as_view({"post": "create"}),
        name="page_metadata",
    ),
    path(
        "domain-metadata",
        DomainMetadataView.as_view({"get": "retrieve"}),
        name="domain_metadata",
    ),
]

if settings.ENABLE_SCRAPY_ENDPOINT:
    urlpatterns.append(
        path(
            "scrapy-webhook",
            ScrapyWebhookView.as_view(),
            name="scrapy_webhook",
        ),
    )
