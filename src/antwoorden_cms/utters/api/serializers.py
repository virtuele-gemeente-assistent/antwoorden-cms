from urllib.parse import urlparse

from django.shortcuts import get_object_or_404
from django.utils.translation import gettext_lazy as _

from rest_framework import exceptions, serializers
from rest_framework.exceptions import ValidationError
from rest_framework.settings import api_settings

from antwoorden_cms.config.models import OrganisationStorySettings, PageMetadata
from antwoorden_cms.config.utils import get_domain_and_path_from_url

from ..models import Gemeente, GemeenteDomain


class UtterResponseSerializer(serializers.Serializer):
    text = serializers.CharField(help_text=_("The text to return in the response"))
    buttons = serializers.ListField(
        help_text=_("Buttons that will be shown in the response")
    )
    image = serializers.CharField(
        required=False, help_text=_("Optional image to be returned in the response")
    )
    elements = serializers.ListField(help_text=_("CURRENTLY NOT USED"))
    attachments = serializers.ListField(help_text=_("CURRENTLY NOT USED"))

    def to_representation(self, instance):
        arguments = {}
        gemeente = Gemeente.objects.get(slug=self.context["view"].kwargs["gemeente"])
        return instance.data(gemeente=gemeente, arguments=arguments)


class OrganisationDomainSerializer(serializers.ModelSerializer):
    class Meta:
        model = GemeenteDomain
        fields = ("domain", "enable_indexing", "environment", "indexer")


class PageMetadataSerializer(serializers.ModelSerializer):
    url = serializers.CharField(help_text=_("De URL van de pagina"), write_only=True)
    gem_enabled = serializers.SerializerMethodField(
        help_text=_("Geeft aan of Gem aanstaat op de pagina")
    )
    direct_handover = serializers.SerializerMethodField(
        help_text=_("Geeft aan of Gem aanstaat op de pagina")
    )
    widget_title = serializers.SerializerMethodField(
        help_text=_("Geeft aan of Gem aanstaat op de pagina")
    )
    widget_subtitle = serializers.SerializerMethodField(
        help_text=_("Geeft aan of Gem aanstaat op de pagina")
    )
    popup_message = serializers.SerializerMethodField(
        help_text=_("Geeft aan of Gem aanstaat op de pagina")
    )
    avatar_url = serializers.SerializerMethodField(
        help_text=_("Geeft aan of Gem aanstaat op de pagina")
    )
    organisation_name = serializers.SerializerMethodField(
        help_text=_("Naam van de organisatie")
    )
    organisation_slug = serializers.SerializerMethodField(
        help_text=_(
            "Slug van de organisatie zoals die binnen het Antwoorden CMS gebruikt wordt"
        )
    )
    livechat_type = serializers.SerializerMethodField(
        help_text=_("Type livechatkoppeling")
    )
    livechat_id = serializers.SerializerMethodField(
        help_text=_("ID van de livechatkoppeling")
    )
    livechat_avatar_url = serializers.SerializerMethodField(
        help_text=_("URL voor de avatar voor de livechatkoppeling")
    )

    def get_gem_enabled(self, obj):
        return obj.gem_enabled_with_default

    def get_direct_handover(self, obj):
        return obj.direct_handover_with_default

    def get_widget_title(self, obj):
        return obj.get_global_config_field("widget_title")

    def get_widget_subtitle(self, obj):
        return obj.get_global_config_field("widget_subtitle")

    def get_popup_message(self, obj):
        return obj.get_global_config_field("popup_message")

    def get_avatar_url(self, obj):
        request = self.context["request"]
        avatar_image = obj.get_global_config_field("widget_avatar")
        if avatar_image:
            return request.build_absolute_uri(avatar_image.url)
        return ""

    def get_organisation_name(self, obj):
        return obj.get_global_config_field("name")

    def get_organisation_slug(self, obj):
        return obj.get_global_config_field("slug")

    def get_livechat_type(self, obj):
        config = obj.get_livechat_config()
        return getattr(config, "type", "")

    def get_livechat_id(self, obj):
        config = obj.get_livechat_config()
        return getattr(config, "identifier", "")

    def get_livechat_avatar_url(self, obj):
        config = obj.get_livechat_config()
        avatar = getattr(config, "avatar", None)
        if avatar:
            request = self.context["request"]
            return request.build_absolute_uri(avatar.url)
        return getattr(config, "avatar_url", "")

    class Meta:
        model = PageMetadata
        fields = (
            "url",
            "title",
            "gem_enabled",
            "direct_handover",
            "widget_title",
            "widget_subtitle",
            "popup_message",
            "avatar_url",
            "organisation_name",
            "organisation_slug",
            "livechat_type",
            "livechat_id",
            "livechat_avatar_url",
        )
        read_only_fields = ("gem_enabled",)
        extra_kwargs = {"title": {"write_only": True}}

    def create(self, validated_data):
        url = validated_data["url"]

        # Workaround for calls from CMS, do not create a metadata entry, but
        # simply return the global values
        if url.startswith("#"):
            gemeente = Gemeente.objects.get(name=url[1:])
            return PageMetadata(
                gemeente_domain=gemeente.gemeentedomain_set.first(), gem_enabled=True
            )

        domain, path = get_domain_and_path_from_url(url)

        try:
            gemeente_domain = GemeenteDomain.objects.get(domain=domain)
        except GemeenteDomain.DoesNotExist:
            raise exceptions.ValidationError("No gemeente found for provided domain")

        page_metadata = PageMetadata.objects.filter(
            gemeente_domain=gemeente_domain, path__iexact=path
        ).first()

        if page_metadata is None:
            page_metadata = PageMetadata.objects.create(
                gemeente_domain=gemeente_domain,
                path=path,
                title=validated_data["title"],
            )

        return page_metadata


class DefaultSettingsSerializer(serializers.ModelSerializer):
    avatar_url = serializers.SerializerMethodField()
    organisation_name = serializers.SerializerMethodField()
    organisation_slug = serializers.SerializerMethodField()
    widget_title = serializers.SerializerMethodField()
    widget_subtitle = serializers.SerializerMethodField()
    popup_message = serializers.SerializerMethodField()

    def get_avatar_url(self, obj):
        request = self.context["request"]
        avatar_image = obj.gemeente.widget_avatar
        if avatar_image:
            return request.build_absolute_uri(avatar_image.url)
        return ""

    def get_organisation_name(self, obj):
        return obj.gemeente.name

    def get_organisation_slug(self, obj):
        return obj.gemeente.slug

    def get_widget_title(self, obj):
        return obj.gemeente.widget_title

    def get_widget_subtitle(self, obj):
        return obj.gemeente.widget_subtitle

    def get_popup_message(self, obj):
        return obj.gemeente.popup_message

    class Meta:
        model = GemeenteDomain
        fields = (
            "widget_enabled",
            "direct_handover",
            "organisation_name",
            "organisation_slug",
            "widget_title",
            "widget_subtitle",
            "popup_message",
            "avatar_url",
        )
        extra_kwargs = {
            "widget_enabled": {
                "source": "gem_default_enabled",
            },
        }


class ListToDictSerializer(serializers.ListSerializer):
    # Adapted from https://stackoverflow.com/a/55122427
    def to_representation(self, data):
        return {
            getattr(
                item, self.child.Meta.dict_serializer_key
            ): self.child.to_representation(item)
            for item in data
        }

    @property
    def data(self):
        ret = serializers.BaseSerializer.data.fget(self)
        return serializers.ReturnDict(ret, serializer=self)


class MultiPageMetadataSerializer(PageMetadataSerializer):
    class Meta:
        model = PageMetadata
        fields = (
            "title",
            "gem_enabled",
            "direct_handover",
            "livechat_type",
            "livechat_id",
            "livechat_avatar_url",
        )
        list_serializer_class = ListToDictSerializer
        dict_serializer_key = "path"


class DomainMetadataSerializer(serializers.ModelSerializer):
    defaults = DefaultSettingsSerializer(source="*")
    enabled_pages = MultiPageMetadataSerializer(many=True)
    disabled_pages = serializers.ListSerializer(child=serializers.CharField())

    class Meta:
        model = GemeenteDomain
        fields = (
            "defaults",
            "enabled_pages",
            "disabled_pages",
        )


class TestDomainMetadataSerializer(serializers.ModelSerializer):
    defaults = DefaultSettingsSerializer(source="*")

    class Meta:
        model = GemeenteDomain
        fields = ("defaults",)

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        ret["enabled_pages"] = {}
        ret["disabled_pages"] = []
        return ret


class DomainMetadataDisabledSerializer(DomainMetadataSerializer):
    def to_representation(self, instance):
        ret = super().to_representation(instance)
        ret["disabled_pages"] = []
        return ret


class OrganisationStorySettingsSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrganisationStorySettings
        fields = (
            "story_id",
            "enabled",
            "handover_to_organisation",
            "crisis_answer",
            "handover_livechat",
            "parking_answer",
        )
        read_only_fields = ("story_id",)

    def create(self, validated_data):
        validated_data["organisation"] = get_object_or_404(
            Gemeente, slug=self.context["organisation"]
        )
        validated_data["story_id"] = self.context["story_id"]
        return super().create(validated_data)

    def update(self, instance, validated_data):
        validated_data["organisation"] = instance.organisation
        validated_data["story_id"] = instance.story_id
        return super().update(instance, validated_data)

    def validate(self, attrs):
        if self.context["request"].method != "POST":
            return super().validate(attrs)

        if "story_id" in self.context:
            if OrganisationStorySettings.objects.filter(
                organisation__slug=self.context["organisation"],
                story_id=self.context["story_id"],
            ).exists():
                raise ValidationError(
                    "Settings for this story already exist for this organisation"
                )

        return super().validate(attrs)


class OrganisationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Gemeente
        fields = (
            "url",
            "name",
            "slug",
            "contact_url",
            "phone_number",
            "municipality_logo",
            "supports_handover",
        )
        extra_kwargs = {
            "url": {
                "view_name": "antwoorden_cms:organisation_detail",
                "lookup_field": "slug",
                "lookup_url_kwarg": "organisation_slug",
            }
        }


class BaseScrapySerializer(serializers.Serializer):
    organisation = serializers.CharField()
    domain = serializers.CharField()
    event = serializers.CharField()
    title = serializers.CharField(required=False)
    url = serializers.URLField(required=False)
    mode = serializers.CharField(required=False)
    md5 = serializers.CharField(required=False)

    @staticmethod
    def save_page(validated_data):
        url = validated_data.get("url")
        if url is None:
            raise serializers.ValidationError(
                {"url": _("URL is required to add, update or delete page")}
            )

        domain = validated_data["domain"].replace("www.", "")
        parsed = urlparse(url)
        path = parsed.path or "/"

        try:
            gemeente_domain = GemeenteDomain.objects.get(domain=domain)
        except GemeenteDomain.DoesNotExist:
            raise exceptions.ValidationError(
                {
                    api_settings.NON_FIELD_ERRORS_KEY: "No gemeente found for provided domain: {}".format(
                        domain
                    )
                }
            )

        page_on_site = True

        if validated_data["event"] == "delete":
            page_on_site = False

        try:
            page_metadata = PageMetadata.objects.get(
                gemeente_domain=gemeente_domain, path__iexact=path
            )
            page_metadata.page_on_site = page_on_site
            page_metadata.title = validated_data["title"]
            page_metadata.save()
        except PageMetadata.DoesNotExist:
            if not page_on_site:
                return False

            page_metadata = PageMetadata.objects.create(
                gemeente_domain=gemeente_domain,
                path=path,
                title=validated_data["title"],
            )
            page_metadata.page_on_site = page_on_site
            page_metadata.save()

        return page_metadata

    def create(self, validated_data):
        return self.save_page(validated_data)


class ScrapyWebhookSerializer(BaseScrapySerializer):
    items = BaseScrapySerializer(required=False, many=True, write_only=True)

    def validate(self, attrs):
        if attrs.get("event") == "batch" and not attrs.get("items"):
            raise ValidationError(
                {"items": "Items field is required for batch operation"}
            )

        return super().validate(attrs)

    def create(self, validated_data):
        if validated_data["event"] == "batch":
            created_pages = []
            for item in validated_data["items"]:
                created_pages.append(self.save_page(item))
            return created_pages
        return super().create(validated_data)
