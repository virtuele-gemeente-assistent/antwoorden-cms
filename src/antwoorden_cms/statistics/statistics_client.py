import datetime
import json
from typing import Dict, List, Literal, Tuple, Union

from django.conf import settings

import requests


class StatisticsClient:
    base_url = settings.STATISTICS_COMPONENT_URL

    @classmethod
    def fetch_statistics(
        cls,
        organisation_id: str,
        session_id: Union[str, None, List[str]],
        params: dict,
        type_: Literal["json", "csv"] = "csv",
        stream=False,
    ) -> Union[List[Dict], str, None]:
        headers = {}
        if type_ == "csv":
            headers["Accept"] = "text/csv"
        elif type_ == "json":
            headers["Accept"] = "application/json"

        url = f"{cls.base_url}/statistics/{organisation_id}"

        if isinstance(session_id, str):
            url += f"/{session_id}"
        elif isinstance(session_id, list):
            params["session_id"] = session_id

        params.pop("ordering", None)

        response = requests.get(
            url,
            params=params,
            headers=headers,
            stream=stream,
            timeout=settings.REQUESTS_TIMEOUT_SHORT,
        )
        if response.status_code != 200:
            return []
        elif stream:
            return response
        elif type_ == "csv":
            return response.text
        elif type_ == "json":
            return json.loads(response.text)

    @classmethod
    def fetch_aggregated_statistics(
        cls, organisation_id: Union[str, None], params: dict
    ) -> dict:
        url = f"{cls.base_url}/aggregated"

        if organisation_id:
            url += f"/{organisation_id}"
        params.pop("ordering", None)
        response = requests.get(
            url, params=params, timeout=settings.REQUESTS_TIMEOUT_LONG
        )
        return response.json()

    @classmethod
    def get_subject_choices(cls, organisation_id: str) -> List[Tuple[str, str]]:
        url = f"{cls.base_url}/subjects/{organisation_id}"
        response = requests.get(url, timeout=settings.REQUESTS_TIMEOUT_SHORT)
        if response.status_code == 404:
            raise ValueError(f"Subjects for organisation {organisation_id} not found")

        return [(subject["name"], subject["name"]) for subject in response.json()]

    @classmethod
    def save_evaluation(
        cls, organisation_id: str, session_id: str, evaluation: dict
    ) -> bool:
        url = f"{cls.base_url}/statistics/{organisation_id}/{session_id}"
        payload = {
            k: v if not isinstance(v, datetime.datetime) else v.isoformat()
            for k, v in evaluation.items()
        }

        classification = evaluation.get("classification")

        if classification:
            payload["is_evaluated"] = True
            response = requests.patch(
                url,
                json=payload,
                timeout=settings.REQUESTS_TIMEOUT_SHORT,
            )

            out = response.json()

            if out == {"result": "success"}:
                return True

        return False
