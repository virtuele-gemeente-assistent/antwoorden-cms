from django.urls import path

from .apps import AppConfig
from .views import (
    DynamicStatisticsView,
    GraphScriptView,
    StatisticsDashboardView,
    StatisticsExportView,
)

app_name = AppConfig.__name__

urlpatterns = [
    # New statistics
    path(
        "<slug:gemeente_slug>/dashboard/",
        StatisticsDashboardView.as_view(),
        name="statistics_dashboard",
    ),
    path(
        "<slug:gemeente_slug>/<slug:graph_slug>/",
        DynamicStatisticsView.as_view(),
        name="dynamic_statistics",
    ),
    path(
        "<slug:gemeente_slug>/_scripts/<slug:graph_slug>/",
        GraphScriptView.as_view(),
        name="graph_scripts",
    ),
    path(
        "<slug:gemeente_slug>/export",
        StatisticsExportView.as_view(),
        name="statistics_export",
    ),
]
