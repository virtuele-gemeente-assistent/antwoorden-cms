from django.utils.translation import gettext_lazy as _

from djchoices import ChoiceItem, DjangoChoices

HIGH_THRESHOLD = 0.8
LOW_THRESHOLD = 0.6

RASA_BUTTON_REGEX = r"(?:(?:^\/[\w\-]+)|(?:(?<=\s)\/[\w\-]+))(?:{\"?.*\"?})?"

PRODUCT_STATISTICS_QUERY = """
select
    trim(both '"' from (jsonb_path_query_first(events.data::jsonb, '$.parse_data.entities[*] ? (@.entity == "product").value')::varchar)) as entity,
    sum(case when events.data::json -> 'parse_data' -> 'intent' ->> 'name' = 'nlu_fallback' then 1 else 0 end) as s0,
    sum(case when (round((events.data::json -> 'parse_data' -> 'intent' ->> 'confidence')::numeric,2) between 0.7 and 0.799) and (events.data::json -> 'parse_data' -> 'intent' ->> 'name' != 'nlu_fallback') then 1 else 0 end) as s7,
    sum(case when round((events.data::json -> 'parse_data' -> 'intent' ->> 'confidence')::numeric,2) between 0.8 and 0.899 then 1 else 0 end) as s8,
    sum(case when round((events.data::json -> 'parse_data' -> 'intent' ->> 'confidence')::numeric,2) between 0.9 and 1.0 then 1 else 0 end) as s9,
    sum(case when round((events.data::json -> 'parse_data' -> 'intent' ->> 'confidence')::numeric,2) between 0.0 and 1.0 then 1 else 0 end) as total
from events
where events.data::json ->> 'event' = 'user'
  and events.data::json ->> 'text' not like '/%%'
  and (jsonb_path_query_first(events.data::jsonb, '$.parse_data.entities[*] ? (@.entity == "product").value')::varchar not like '%% %%' or jsonb_path_query_first(events.data::jsonb, '$.parse_data.entities[*] ? (@.entity == "product").value')::varchar is null)
  and sender_id in (
    select sender_id
    from events
    where events.data::json ->> 'event' = 'user'
          and events.data::json -> 'metadata' ->> 'username' is null
      and (timestamp '1970-1-1' + interval '1 second' * (events.data::json ->> 'timestamp')::float)::date between date_trunc('month', NOW() - interval '%s month') and date_trunc('month', NOW() + interval '1 month' - interval '%s month') - interval '1 day'
      and events.data::json -> 'metadata' ->> 'municipality' = %s
    group by sender_id
    having count(data) > 1)
group by
    jsonb_path_query_first(events.data::jsonb,
    '$.parse_data.entities[*] ? (@.entity == "product").value')
order by
    total,
    entity
"""

NUM_CHATS_QUERY = """
select to_char(date_trunc('month',(timestamp '1970-1-1' + interval '1 second' * (events.data::json ->> 'timestamp')::float)::date), 'YYYY-MM') as maand,
    count(distinct sender_id) as gesprekken,
    sum(case when events.data::json ->> 'event' = 'action' and events.data::json ->> 'name' = 'action_escalatie' then 1 else 0 end) as livechat,
    sum(case when events.data::json ->> 'event' = 'action' and events.data::json ->> 'name' = 'action_default_ask_affirmation' then 1 else 0 end) as reparaties

from events
where ((events.data::json ->> 'event' = 'user') or (events.data::json ->> 'event' = 'action' and events.data::json ->> 'name' in ('action_escalatie','action_default_ask_affirmation')))
  and sender_id in (
    select sender_id
    from events
    where events.data::json ->> 'event' = 'user'
          and events.data::json -> 'metadata' ->> 'username' is null
      and (timestamp '1970-1-1' + interval '1 second' * (events.data::json ->> 'timestamp')::float)::date between NOW() - interval '4 month' and NOW()
      and events.data::json -> 'metadata' ->> 'municipality' = %s
    group by sender_id
    having count(data) > 1)
group by maand
order by maand desc
"""

CHAT_DISTRIBUTION_QUERY = """
select
    to_char(date_trunc('month',(timestamp '1970-1-1' + interval '1 second' * (events.data::json ->> 'timestamp')::float)::date), 'YYYY-MM') as maand,
    sum(case when extract( hour from (timestamp '1970-1-1' + interval '1 second' * (events.data::json ->> 'timestamp')::float)::timestamp) = 0 then 1 else 0 end) as h00,
    sum(case when extract( hour from (timestamp '1970-1-1' + interval '1 second' * (events.data::json ->> 'timestamp')::float)::timestamp) = 1 then 1 else 0 end) as h01,
    sum(case when extract( hour from (timestamp '1970-1-1' + interval '1 second' * (events.data::json ->> 'timestamp')::float)::timestamp) = 2 then 1 else 0 end) as h02,
    sum(case when extract( hour from (timestamp '1970-1-1' + interval '1 second' * (events.data::json ->> 'timestamp')::float)::timestamp) = 3 then 1 else 0 end) as h03,
    sum(case when extract( hour from (timestamp '1970-1-1' + interval '1 second' * (events.data::json ->> 'timestamp')::float)::timestamp) = 4 then 1 else 0 end) as h04,
    sum(case when extract( hour from (timestamp '1970-1-1' + interval '1 second' * (events.data::json ->> 'timestamp')::float)::timestamp) = 5 then 1 else 0 end) as h05,
    sum(case when extract( hour from (timestamp '1970-1-1' + interval '1 second' * (events.data::json ->> 'timestamp')::float)::timestamp) = 6 then 1 else 0 end) as h06,
    sum(case when extract( hour from (timestamp '1970-1-1' + interval '1 second' * (events.data::json ->> 'timestamp')::float)::timestamp) = 7 then 1 else 0 end) as h07,
    sum(case when extract( hour from (timestamp '1970-1-1' + interval '1 second' * (events.data::json ->> 'timestamp')::float)::timestamp) = 8 then 1 else 0 end) as h08,
    sum(case when extract( hour from (timestamp '1970-1-1' + interval '1 second' * (events.data::json ->> 'timestamp')::float)::timestamp) = 9 then 1 else 0 end) as h09,
    sum(case when extract( hour from (timestamp '1970-1-1' + interval '1 second' * (events.data::json ->> 'timestamp')::float)::timestamp) = 10 then 1 else 0 end) as h10,
    sum(case when extract( hour from (timestamp '1970-1-1' + interval '1 second' * (events.data::json ->> 'timestamp')::float)::timestamp) = 11 then 1 else 0 end) as h11,
    sum(case when extract( hour from (timestamp '1970-1-1' + interval '1 second' * (events.data::json ->> 'timestamp')::float)::timestamp) = 12 then 1 else 0 end) as h12,
    sum(case when extract( hour from (timestamp '1970-1-1' + interval '1 second' * (events.data::json ->> 'timestamp')::float)::timestamp) = 13 then 1 else 0 end) as h13,
    sum(case when extract( hour from (timestamp '1970-1-1' + interval '1 second' * (events.data::json ->> 'timestamp')::float)::timestamp) = 14 then 1 else 0 end) as h14,
    sum(case when extract( hour from (timestamp '1970-1-1' + interval '1 second' * (events.data::json ->> 'timestamp')::float)::timestamp) = 15 then 1 else 0 end) as h15,
    sum(case when extract( hour from (timestamp '1970-1-1' + interval '1 second' * (events.data::json ->> 'timestamp')::float)::timestamp) = 16 then 1 else 0 end) as h16,
    sum(case when extract( hour from (timestamp '1970-1-1' + interval '1 second' * (events.data::json ->> 'timestamp')::float)::timestamp) = 17 then 1 else 0 end) as h17,
    sum(case when extract( hour from (timestamp '1970-1-1' + interval '1 second' * (events.data::json ->> 'timestamp')::float)::timestamp) = 18 then 1 else 0 end) as h18,
    sum(case when extract( hour from (timestamp '1970-1-1' + interval '1 second' * (events.data::json ->> 'timestamp')::float)::timestamp) = 19 then 1 else 0 end) as h19,
    sum(case when extract( hour from (timestamp '1970-1-1' + interval '1 second' * (events.data::json ->> 'timestamp')::float)::timestamp) = 20 then 1 else 0 end) as h20,
    sum(case when extract( hour from (timestamp '1970-1-1' + interval '1 second' * (events.data::json ->> 'timestamp')::float)::timestamp) = 21 then 1 else 0 end) as h21,
    sum(case when extract( hour from (timestamp '1970-1-1' + interval '1 second' * (events.data::json ->> 'timestamp')::float)::timestamp) = 22 then 1 else 0 end) as h22,
    sum(case when extract( hour from (timestamp '1970-1-1' + interval '1 second' * (events.data::json ->> 'timestamp')::float)::timestamp) = 23 then 1 else 0 end) as h23
from events
where events.data::json ->> 'event' = 'user' and sender_id in (
    select sender_id
    from events
    where events.data::json ->> 'event' = 'user'
      and events.data::json -> 'metadata' ->> 'username' is null
      and (timestamp '1970-1-1' + interval '1 second' * (events.data::json ->> 'timestamp')::float)::date between date_trunc('month',NOW()) - interval '2 month' and NOW()
      and events.data::json -> 'metadata' ->> 'municipality' = %s
    group by sender_id
    having count(data) > 1)
group by maand
order by maand desc
"""

POPULAR_INTENTS_QUERY = """
select  events.data::json -> 'parse_data' -> 'intent' ->> 'name' as intent_name, count(events.data)
from events
where events.data::json ->> 'event' = 'user'
and events.data::json -> 'parse_data' -> 'intent' ->> 'name' != 'greet'
and sender_id in (
    select sender_id
    from events
    where events.data::json ->> 'event' = 'user'
      and events.data::json -> 'metadata' ->> 'username' is null
      and (timestamp '1970-1-1' + interval '1 second' * (events.data::json ->> 'timestamp')::float)::date between NOW() - interval '1 month' and NOW()
      and events.data::json -> 'metadata' ->> 'municipality' = %s
    group by sender_id
    having count(data) > 1
)
group by events.data::json -> 'parse_data' -> 'intent' ->> 'name'
order by count desc, intent_name
"""

INTENT_DETAIL_QUERY = """
select
    concat(events.data::json -> 'parse_data' -> 'intent' ->> 'name','(',
    concat_ws(',',trim(both '"' from (jsonb_path_query_first(events.data::jsonb, '$.parse_data.entities[*] ? (@.entity == "municipality").value')::varchar)),
      trim(both '"' from (jsonb_path_query_first(events.data::jsonb, '$.parse_data.entities[*] ? (@.entity == "product").value')::varchar)),
      trim(both '"' from (jsonb_path_query_first(events.data::jsonb, '$.parse_data.entities[*] ? (@.entity == "namens").value')::varchar)),
      trim(both '"' from (jsonb_path_query_first(events.data::jsonb, '$.parse_data.entities[*] ? (@.entity == "spoed").value')::varchar)),
      case when jsonb_path_query_first(events.data::jsonb, '$.parse_data.entities[*] ? (@.entity == "land").value')::varchar is null then null else 'land' end),')') as intent,
    count(events.data)
from events
where events.data::json ->> 'event' = 'user'
  and events.data::json -> 'parse_data' -> 'intent' ->> 'name' = %s
  and sender_id in (
    select sender_id
    from events
    where events.data::json ->> 'event' = 'user'
      and events.data::json -> 'metadata' ->> 'username' is null
      and (timestamp '1970-1-1' + interval '1 second' * (events.data::json ->> 'timestamp')::float)::date between NOW() - interval '1 month' and NOW()
      and events.data::json -> 'metadata' ->> 'municipality' = %s
    group by sender_id
    having count(data) > 1
)
group by intent
order by count desc
"""

NUM_CHATS_PER_YEAR_QUERY = """
select to_char(date_trunc('year',(timestamp '1970-1-1' + interval '1 second' * (events.data::json ->> 'timestamp')::float)::date), 'YYYY') as jaar,
    count(distinct sender_id) as gesprekken
from events
where events.data::json ->> 'event' = 'user'
  and sender_id in (
    select sender_id
    from events
    where events.data::json ->> 'event' = 'user'
          and events.data::json -> 'metadata' ->> 'username' is null
      and events.data::json -> 'metadata' ->> 'municipality' = %s
    group by sender_id
    having count(data) > 1)
group by jaar
order by jaar desc
"""


class LivechatEscalationCauses(DjangoChoices):
    correctly_predicted = ChoiceItem("correctly_predicted", _("Correcte intentie"))
    uncertainty = ChoiceItem("uncertainty", _("Onzekerheid"))


class LivechatEscalationSuccessful(DjangoChoices):
    niet_van_toepassing = ChoiceItem("niet_van_toepassing", _("N.v.t."))
    ja = ChoiceItem("ja", _("Ja"))
    nee = ChoiceItem("nee", _("Nee"))


class LivechatEscalationFailedReasons(DjangoChoices):
    niet_van_toepassing = ChoiceItem("niet_van_toepassing", _("N.v.t."))
    buiten_kantoortijd = ChoiceItem("buiten_kantoortijd", _("Buiten kantoortijd"))
    bezet_of_niet_beschikbaar = ChoiceItem(
        "bezet_of_niet_beschikbaar", _("Bezet/niet beschikbaar binnen kantoortijd")
    )
    technische_fout = ChoiceItem("technische_fout", _("Technische fout"))
    user_declined = ChoiceItem("user_declined", _("Gebruiker weigerde"))


class PeriodUnits(DjangoChoices):
    days = ChoiceItem("days", _("Dag"))
    weeks = ChoiceItem("weeks", _("Week"))
    months = ChoiceItem("months", _("Maand"))
    years = ChoiceItem("years", _("Jaar"))
