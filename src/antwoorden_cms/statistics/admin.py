from django.contrib import admin

from ordered_model.admin import OrderedModelAdmin

from .models import Graph


@admin.register(Graph)
class GraphAdmin(OrderedModelAdmin):
    list_display = [
        "slug",
        "title",
        "move_up_down_links",
    ]
