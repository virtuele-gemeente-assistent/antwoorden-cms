import logging
import math
from datetime import datetime

from django.http import StreamingHttpResponse
from django.utils import timezone
from django.views.generic import DetailView, View

from dateutil.relativedelta import relativedelta
from two_factor.views import OTPRequiredMixin

from antwoorden_cms.statistics.statistics_client import StatisticsClient

from ..utters.mixins import ObjectPermissionMixin
from ..utters.models import Gemeente
from .models import Graph

logger = logging.getLogger(__name__)


class GraphScriptView(DetailView):
    model = Graph
    slug_url_kwarg = "graph_slug"
    slug_field = "slug"
    template_name = "graph_script.html"
    content_type = "text/javascript"


class DynamicStatisticsView(OTPRequiredMixin, ObjectPermissionMixin, DetailView):
    permission_required = "utters.statistics_read"
    template_name = "dynamic_statistics.html"
    model = Graph
    slug_url_kwarg = "graph_slug"
    slug_field = "slug"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)

        unit = self.request.GET.get("unit", self.object.graph_duration_default_unit)

        now = timezone.now().astimezone(timezone.pytz.timezone("Europe/Amsterdam"))
        # Ensure the date is properly rounded
        if unit == "days":
            now = now.replace(hour=0, minute=0, second=0, microsecond=0)
        if unit == "weeks":
            days_since_start_of_week = now.weekday()
            start_of_week = now - relativedelta(days=days_since_start_of_week)
            now = start_of_week.replace(hour=0, minute=0, second=0, microsecond=0)
        if unit == "months":
            now = now.replace(day=1, hour=0, minute=0, second=0, microsecond=0)
        if unit == "years":
            now = now.replace(month=1, day=1, hour=0, minute=0, second=0, microsecond=0)

        offset = self.request.GET.get("offset", None)
        try:
            offset = int(offset)
        except TypeError:
            offset = 0

        date = self.request.GET.get("date", None)
        if date:
            selected_date = datetime.strptime(date, "%Y-%m-%d").date()
            difference = (selected_date - now.date()).days

            offset = difference / self.object.graph_duration
            if offset.is_integer():
                offset += 1
            else:
                offset = math.ceil(offset)

        # Round the begin_date to the beginning of the smallest unit
        duration = relativedelta(**{unit: self.object.graph_duration})

        begin_date = now - (abs(offset) + 1) * duration
        # Ensure the proper timezone with/without DST is used
        begin_date = begin_date.astimezone(
            timezone.pytz.timezone("Europe/Amsterdam")
        ).replace(
            year=begin_date.year,
            month=begin_date.month,
            day=begin_date.day,
            hour=0,
            minute=0,
            second=0,
            microsecond=0,
        )
        end_date = now - (abs(offset) * duration)
        # Ensure the proper timezone with/without DST is used
        end_date = end_date.astimezone(
            timezone.pytz.timezone("Europe/Amsterdam")
        ).replace(
            year=end_date.year,
            month=end_date.month,
            day=end_date.day,
            hour=0,
            minute=0,
            second=0,
            microsecond=0,
        )

        query_params = {
            **self.object.extra_query_params,
            "start__gte": begin_date,
            "start__lt": end_date,
            "ordering": "start",
        }

        if (
            unit != self.object.graph_duration_default_unit
            and self.object.graph_duration != 1
        ):
            if unit == "days":
                query_params.update({"groupByX": "date"})
            if unit == "weeks":
                query_params.update({"groupByX": "iso_weekdate_year,week"})
            if unit == "months":
                query_params.update({"groupByX": "year,month"})
            if unit == "years":
                query_params.update({"groupByX": "year"})

        response = StatisticsClient.fetch_aggregated_statistics(
            organisation_id=self.get_permission_object().name, params=query_params
        )
        context["statistics"] = response
        context["statistics"]["params"] = query_params
        context["offset"] = offset
        context["duration"] = self.object.graph_duration
        context["unit"] = unit
        context["begin_date"] = begin_date
        context["end_date"] = end_date

        return context


class StatisticsDashboardView(DetailView):
    model = Gemeente
    slug_url_kwarg = "gemeente_slug"
    template_name = "statistics_dashboard.html"
    permission_required = "utters.gemeente_read"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["graph_definitions"] = Graph.objects.all()
        return context


def cast_nullboolean_to_int(boolean):
    if boolean is None:
        return boolean

    return int(boolean)


class StatisticsExportView(View):
    def get(self, request, *args, **kwargs):
        gemeente = Gemeente.objects.get(slug=self.kwargs["gemeente_slug"])

        extra_params = {}
        if "timestamp__gte" in self.request.GET:
            extra_params["start__gte"] = self.request.GET.get("timestamp__gte")
        if "timestamp__lt" in self.request.GET:
            extra_params["start__lt"] = self.request.GET.get("timestamp__lt")

        params = {
            "ordering": "start",
        }

        params.update(extra_params)

        response = StatisticsClient.fetch_statistics(
            organisation_id=gemeente.name,
            session_id=None,
            params=params,
            type_="csv",
            stream=True,
        )

        response = StreamingHttpResponse(
            streaming_content=response.iter_content(chunk_size=64 * 1024),
            content_type="text/csv",
        )
        # Needed for proper filename encoding when using Safari as browser
        response["Content-Disposition"] = "attachment; filename=export.csv"
        return response
