from django.test import tag
from django.urls import reverse

from django_webtest import WebTest

from antwoorden_cms.utters.tests.factories import GemeenteFactory
from antwoorden_cms.utters.tests.mixins import TwoFactorTestCaseMixin
from antwoorden_cms.utters.tests.utils import grant_gemeente_perms

from .factories import GraphFactory


@tag("permissions")
class StatisticsPermissionTests(TwoFactorTestCaseMixin, WebTest):
    login_url = reverse("two_factor:login")
    user_is_admin = False
    user_is_superuser = False

    def setUp(self):
        super().setUp()

        self.gemeente = GemeenteFactory.create(name="Dongen")

        self.gemeente2 = GemeenteFactory.create(name="Amsterdam")

        self.graph = GraphFactory(slug="language-distribution")

    def test_statistics_not_logged_in(self):
        response = self.app.get(
            reverse(
                "statistics:dynamic_statistics",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "graph_slug": self.graph.slug,
                },
            ),
        )

        self.assertEqual(response.status_code, 302)
        self.assertIn(self.login_url, response.location)

    def test_statistics_no_permission(self):
        # Should not influence the outcome
        grant_gemeente_perms(
            "admin",
            self.user,
            self.gemeente,
            ["utter_read", "hyperlink_read", "dialog_read"],
        )

        response = self.app.get(
            reverse(
                "statistics:dynamic_statistics",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "graph_slug": self.graph.slug,
                },
            ),
            user=self.user,
            status=403,
        )

        self.assertEqual(response.status_code, 403)

    def test_statistics_no_permission_for_current_gemeente(self):
        # Should not influence the outcome
        grant_gemeente_perms("admin", self.user, self.gemeente2, ["statistics_read"])

        response = self.app.get(
            reverse(
                "statistics:dynamic_statistics",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "graph_slug": self.graph.slug,
                },
            ),
            user=self.user,
            status=403,
        )

        self.assertEqual(response.status_code, 403)
