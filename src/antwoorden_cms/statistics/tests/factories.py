from factory.django import DjangoModelFactory

from ..models import Graph


class GraphFactory(DjangoModelFactory):
    class Meta:
        model = Graph
