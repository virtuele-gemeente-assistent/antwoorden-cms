from django.apps import AppConfig


class StatisticsConfig(AppConfig):
    name = "antwoorden_cms.statistics"
