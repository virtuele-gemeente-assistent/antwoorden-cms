import logging

from django.db import models
from django.utils.translation import gettext_lazy as _

from ordered_model.models import OrderedModel

from .constants import PeriodUnits

logger = logging.getLogger(__name__)


class Graph(OrderedModel):
    slug = models.SlugField(
        verbose_name=_("URL-deel"),
        help_text=_("Pad waarop de grafiek bereikbaar is"),
        unique=True,
    )
    graph_duration = models.IntegerField(
        verbose_name=_("Periode"),
        help_text=_("Geeft aan tot hoe ver terug de data in de grafiek gaat."),
        null=True,
        blank=True,
    )
    graph_duration_default_unit = models.CharField(
        verbose_name=_("Standaard periode eenheid"),
        help_text=_("Geeft aan tot hoe ver terug de data in de grafiek gaat."),
        null=True,
        blank=True,
        max_length=52,
        choices=PeriodUnits.choices,
    )
    round_duration = models.BooleanField(
        verbose_name=_("Rond periode af"),
        default=True,
        help_text=_(
            "Indien aangevinkt wordt de periode afgerond, zodat deze altijd begint bij het begin van de kleinste eenheid."
        ),
    )
    title = models.CharField(
        verbose_name=_("Korte titel"),
        max_length=512,
        help_text=_("Titel die getoond wordt boven de grafiek"),
    )
    verbose_title = models.CharField(
        verbose_name=_("Titel"),
        max_length=1024,
        help_text=_("Titel die op de tegel getoond wordt"),
    )
    code = models.TextField(
        verbose_name=_("Javascript code"),
        help_text=_("Javascript code die uitgevoerd wordt om de grafiek op te bouwen"),
    )
    description = models.TextField(
        verbose_name=_("Omschrijving"),
        blank=True,
        help_text=_("Omschrijving die getoond wordt bij de grafiek"),
    )
    extra_query_params = models.JSONField(
        verbose_name=_("Extra query parameters"),
        default=dict,
        help_text=_(
            "Extra query parameters die gebruikt worden bij de request naar de statistieken API"
        ),
    )

    def __str__(self):
        return self.title
