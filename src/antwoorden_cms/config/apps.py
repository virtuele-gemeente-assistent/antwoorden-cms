from django.apps import AppConfig


class ConfigConfig(AppConfig):
    name = "antwoorden_cms.config"
