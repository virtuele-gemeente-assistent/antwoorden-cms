from django.db import models
from django.utils.translation import gettext_lazy as _


class PageMetadata(models.Model):
    gemeente_domain = models.ForeignKey(
        "utters.GemeenteDomain",
        help_text=_("Het domein van de gemeente"),
        on_delete=models.CASCADE,
    )
    path = models.CharField(
        max_length=2000, help_text=_("Het pad van de URL van de pagina")
    )
    title = models.CharField(
        max_length=1000, help_text=_("De titel van de pagina"), blank=True
    )
    gem_enabled = models.BooleanField(
        default=None,
        help_text=_("Geeft aan of Gem aan staat op de pagina."),
        verbose_name=_("Gem ingeschakeld"),
        null=True,
    )
    direct_handover = models.BooleanField(
        null=True,
        default=None,
        verbose_name=_("Directe doorschakeling naar medewerker"),
        help_text=_(
            "Geeft aan of er bij het openen van het widget meteen doorgeschakeld "
            "moet worden naar een KCC medewerker"
        ),
    )
    livechat_config = models.ForeignKey(
        "utters.LivechatConfiguration",
        null=True,
        blank=True,
        help_text=_("Livechatconfiguratie die van toepassing is op deze pagina"),
        on_delete=models.SET_NULL,
    )

    page_on_site = models.BooleanField(
        default=False,
        verbose_name=_("Page on site"),
        help_text=_(
            "If the page is on the website. Will be removed if not and page uses default values"
        ),
    )

    class Meta:
        verbose_name = _("Pagina metadata")
        verbose_name_plural = _("Pagina metadata")

    @property
    def full_url(self):
        return f"{self.gemeente_domain.domain}{self.path}"

    @property
    def gem_enabled_with_default(self):

        if self.gem_enabled is None:
            return self.gemeente_domain.gem_default_enabled
        return self.gem_enabled

    @property
    def direct_handover_with_default(self):
        if self.direct_handover is None:
            return self.gemeente_domain.direct_handover
        return self.direct_handover

    def get_global_config_field(self, field_name, allow_none=False):
        if allow_none:
            return getattr(self.gemeente_domain.gemeente, field_name, None)
        return getattr(self.gemeente_domain.gemeente, field_name)

    def get_livechat_config(self):
        if not self.gemeente_domain.gemeente.livechat_enabled:
            return None

        if self.livechat_config:
            return self.livechat_config
        return self.get_global_config_field("default_livechat_config", allow_none=True)


class OrganisationStorySettings(models.Model):
    organisation = models.ForeignKey(
        "utters.Gemeente",
        help_text=_("De gemeente waarvoor deze instelling geldt"),
        on_delete=models.CASCADE,
    )
    story_id = models.CharField(
        max_length=1024, help_text=_("De unieke ID van de story")
    )
    enabled = models.BooleanField(
        default=True,
        help_text=_(
            "Geeft aan of deze story wel of niet ingeschakeld is voor de gemeente"
        ),
    )
    handover_to_organisation = models.CharField(
        max_length=2048,
        help_text=_(
            "Indicate that for this story, a handover must be done to the specified organisation"
        ),
        blank=True,
    )
    crisis_answer = models.BooleanField(
        default=False,
        help_text=_(
            "Geeft aan of er in plaats van deze story een crisisantwoord gegeven moet worden"
        ),
    )
    handover_livechat = models.BooleanField(
        default=False,
        help_text=_(
            "Geeft aan of er in plaats van deze story doorgeschakeld moet worden naar livechat"
        ),
    )
    parking_answer = models.BooleanField(
        default=False,
        help_text=_(
            "Geeft aan of er in plaats van deze story een parkerenantwoord gegeven moet worden"
        ),
    )

    class Meta:
        unique_together = ["organisation", "story_id"]


class Subject(models.Model):
    label = models.CharField(
        max_length=255,
        verbose_name=_("Leesbare tekst"),
        help_text=_("Leesbare tekst zoals die getoond wordt in het CMS"),
    )
    upl_name = models.CharField(
        max_length=255,
        verbose_name=_("UPL naam"),
        help_text=_("Naam zoals gehanteerd in UPL"),
    )
    config = models.ForeignKey(
        "utters.SiteConfiguration", on_delete=models.SET_NULL, null=True, blank=True
    )
