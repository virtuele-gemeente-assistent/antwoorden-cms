from django.urls import path

from .apps import AppConfig
from .views import (
    PageMetadataCreateView,
    PageMetadataDeleteView,
    PageMetadataDetailView,
    PageMetadataListView,
    PageMetadataUpdateView,
    WidgetCodeGeneratorBodyView,
    WidgetStylingView,
)

app_name = AppConfig.__name__

urlpatterns = [
    path(
        "<slug:gemeente_slug>/page-metadata/",
        PageMetadataListView.as_view(),
        name="page_metadata_list",
    ),
    path(
        "<slug:gemeente_slug>/page-metadata/create/",
        PageMetadataCreateView.as_view(),
        name="page_metadata_create",
    ),
    path(
        "<slug:gemeente_slug>/page-metadata/<int:page_metadata_pk>/",
        PageMetadataDetailView.as_view(),
        name="page_metadata_read",
    ),
    path(
        "<slug:gemeente_slug>/page-metadata/<int:page_metadata_pk>/update",
        PageMetadataUpdateView.as_view(),
        name="page_metadata_update",
    ),
    path(
        "<slug:gemeente_slug>/page-metadata/<int:page_metadata_pk>/delete",
        PageMetadataDeleteView.as_view(),
        name="page_metadata_delete",
    ),
    path(
        "<slug:gemeente_slug>/widget-code-body/",
        WidgetCodeGeneratorBodyView.as_view(),
        name="widget_code_body",
    ),
    path(
        "<slug:gemeente_slug>/_styling",
        WidgetStylingView.as_view(content_type="text/css"),
        name="widget_styling",
    ),
]
