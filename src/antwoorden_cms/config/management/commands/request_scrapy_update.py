import logging

from django.conf import settings
from django.core.management.base import BaseCommand

import requests

from antwoorden_cms.utters.choices import Environments
from antwoorden_cms.utters.models import GemeenteDomain

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    """
    Send request to Antwoorden
    """

    help = (
        "Sends requests to the Scrapy component to update all of the gemeente domains"
    )

    def add_arguments(self, parser):
        # TODO: handle full/partial update when implemented in scrapy
        parser.add_argument(
            "--partial",
            action="store_true",
            help="Request partial update instead of full update",
        )

    def send_request(self, domain, organisation):

        data = {
            "project": "default",
            "spider": "domain",
            "domain": domain,
            "organisation": organisation,
        }
        try:
            logger.info(
                "Sending request for {domain} from Gemeete {gemeente}".format(
                    domain=domain, gemeente=organisation
                )
            )
            url = settings.SCRAPY_URL + "/schedule.json"
            response = requests.post(
                url, data=data, timeout=settings.REQUESTS_TIMEOUT_LONG
            )
            logger.info("Received response {}".format(response.json()))

        except requests.Timeout:
            logger.error(
                "Failed to send request for {domain} from Gemeete {gemeente}".format(
                    domain=domain, gemeente=organisation
                )
            )

    def handle(self, *args, **options):

        for domain in GemeenteDomain.objects.filter(
            enable_indexing=True, environment=Environments.PRODUCTION
        ).select_related("gemeente"):
            self.send_request(domain.domain, domain.gemeente.name)
