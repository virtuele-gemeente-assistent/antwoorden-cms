from django.test import TestCase, override_settings
from django.urls import reverse

from antwoorden_cms.utters.tests.factories import GemeenteFactory

DEFAULT_WIDGET_STYLING = """<!-- Standaard widget styling -->
<link rel="stylesheet" href="{chatbot_url}/static/css/widget-v0.11.7.css" type="text/css">"""

CUSTOM_WIDGET_STYLING = """<!-- Verwijzing naar gemeente-specifieke styling (.css file) -->
<link rel="stylesheet" href="{self_url}/config/{gemeente}/_styling" type="text/css">"""

WEBCHAT_ELEMENT = '<div id="webchat"></div>'

DEFAULT_WIDGET_LIBRARY = """<!-- Basis script voor het widget -->
<script defer src="{chatbot_url}/static/js/webchat-v0.11.7.js" crossorigin="anonymous"></script>"""


WIDGET_SCRIPT = """
<script defer
    id="chatwidget-script"
    crossorigin="anonymous"
    src="{chatbot_url}/static/js/widget.js"
></script>"""


class WidgetCodeGeneratorTests(TestCase):
    def setUp(self):
        super().setUp()

        self.gemeente = GemeenteFactory(name="amsterdam")
        self.widget_code_body_url = reverse(
            "config:widget_code_body", kwargs={"gemeente_slug": self.gemeente.slug}
        )
        self.default_chatbot_url = "https://virtuele-gemeente-assistent.nl"

    @override_settings(CHATBOT_URL="https://virtuele-gemeente-assistent.nl")
    def test_generate_widget_code_body(self):
        response = self.client.get(self.widget_code_body_url)

        expected = (
            f"{WEBCHAT_ELEMENT.format(chatbot_url=self.default_chatbot_url)}\n"
            f"{WIDGET_SCRIPT.format(chatbot_url=self.default_chatbot_url)}"
        )

        self.assertEqual(response["content-type"], "text/plain")
        self.assertEqual(response.rendered_content, expected)

    @override_settings(CHATBOT_URL="https://test.virtuele-gemeente-assistent.nl")
    def test_generate_widget_code_body_different_domain(self):
        chatbot_url = "https://test.virtuele-gemeente-assistent.nl"
        response = self.client.get(self.widget_code_body_url)

        expected = (
            f"{WEBCHAT_ELEMENT.format(chatbot_url=chatbot_url)}\n"
            f"{WIDGET_SCRIPT.format(chatbot_url=chatbot_url)}"
        )

        self.assertEqual(response["content-type"], "text/plain")
        self.assertEqual(response.rendered_content, expected)


class WidgetCodeGeneratorStagingTests(TestCase):
    def setUp(self):
        super().setUp()

        self.gemeente = GemeenteFactory(name="amsterdam")
        self.widget_code_body_url = reverse(
            "config:widget_code_body", kwargs={"gemeente_slug": self.gemeente.slug}
        )
        self.default_chatbot_url = "https://virtuele-gemeente-assistent.nl"

    def test_generate_widget_code_body_different_domain(self):
        self.assertEqual.__self__.maxDiff = None
        chatbot_url = "https://test.virtuele-gemeente-assistent.nl"
        session = self.client.session
        session["environment"] = "staging"
        session["chatbot_url"] = chatbot_url
        session.save()

        response = self.client.get(self.widget_code_body_url)

        widget_script = (
            "\n"
            "<script defer\n"
            '    id="chatwidget-script"\n'
            '    crossorigin="anonymous"\n'
            '    src="{chatbot_url}/static/js/widget.js"\n'
            '    data-widget-subtitle="{subtitle}"\n'
            "></script>"
        )

        expected = (
            f"{WEBCHAT_ELEMENT.format(chatbot_url=chatbot_url)}\n"
            f"{widget_script.format(chatbot_url=chatbot_url, subtitle='Testomgeving')}"
        )

        self.assertEqual(response["content-type"], "text/plain")
        self.assertEqual(response.rendered_content, expected)
