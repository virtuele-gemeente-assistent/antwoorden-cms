import tempfile

from django.urls import reverse

from rest_framework.test import APITestCase

from antwoorden_cms.utters.choices import LivechatTypes
from antwoorden_cms.utters.tests.factories import (
    GemeenteDomainFactory,
    GemeenteFactory,
    LivechatConfigurationFactory,
)

from ..models import PageMetadata
from .factories import PageMetadataFactory


class PageMetadataAPITests(APITestCase):
    def test_post_create_new_page_metadata(self):
        gemeente = GemeenteFactory.create(name="Dongen")
        GemeenteDomainFactory(gemeente=gemeente, domain="dongen.nl")
        response = self.client.post(
            reverse("antwoorden_cms:page_metadata"),
            {"url": "https://dongen.nl", "title": "Some Title"},
        )

        self.assertEqual(response.status_code, 201)
        self.assertDictEqual(
            response.data,
            {
                "gem_enabled": False,
                "direct_handover": False,
                "widget_title": "Gem",
                "widget_subtitle": "Je digitale hulp van de gemeente",
                "popup_message": "Stel je vraag",
                "avatar_url": "",
                "organisation_name": "Dongen",
                "organisation_slug": "dongen",
                "livechat_type": "",
                "livechat_id": "",
                "livechat_avatar_url": "",
            },
        )

        metadata = PageMetadata.objects.get()
        self.assertEqual(metadata.title, "Some Title")
        self.assertEqual(metadata.path, "/")
        self.assertEqual(metadata.gem_enabled, None)

    def test_post_create_new_page_metadata_camelcase(self):
        gemeente = GemeenteFactory.create(name="Dongen")
        GemeenteDomainFactory(gemeente=gemeente, domain="dongen.nl")
        response = self.client.post(
            reverse("antwoorden_cms:page_metadata"),
            {"url": "https://dongen.nl", "title": "Some Title"},
        )

        self.assertEqual(response.status_code, 201)
        self.assertIn(b'"gemEnabled":false', response.content)

        metadata = PageMetadata.objects.get()
        self.assertEqual(metadata.title, "Some Title")
        self.assertEqual(metadata.path, "/")
        self.assertEqual(metadata.gem_enabled, None)

    def test_post_create_new_page_metadata_exists_for_other_gemeente(self):
        gemeente = GemeenteFactory.create(name="Dongen")
        GemeenteDomainFactory(gemeente=gemeente, domain="dongen.nl")

        PageMetadataFactory.create(
            path="/",
            title="Some Title",
            gem_enabled=True,
            gemeente_domain=GemeenteDomainFactory.create(
                gemeente__name="Tilburg", domain="tilburg.nl"
            ),
        )

        response = self.client.post(
            reverse("antwoorden_cms:page_metadata"),
            {"url": "https://dongen.nl", "title": "Some Title"},
        )

        self.assertEqual(response.status_code, 201)
        self.assertDictEqual(
            response.data,
            {
                "gem_enabled": False,
                "direct_handover": False,
                "widget_title": "Gem",
                "widget_subtitle": "Je digitale hulp van de gemeente",
                "popup_message": "Stel je vraag",
                "avatar_url": "",
                "organisation_name": "Dongen",
                "organisation_slug": "dongen",
                "livechat_type": "",
                "livechat_id": "",
                "livechat_avatar_url": "",
            },
        )

        self.assertEqual(PageMetadata.objects.count(), 2)

        metadata = PageMetadata.objects.get(gemeente_domain__gemeente__slug="dongen")
        self.assertEqual(metadata.title, "Some Title")
        self.assertEqual(metadata.path, "/")
        self.assertEqual(metadata.gem_enabled, None)

    def test_post_create_new_page_metadata_gemeente_does_not_exist(self):
        gemeente = GemeenteFactory.create(name="Dongen")
        GemeenteDomainFactory(gemeente=gemeente, domain="dongen.nl")
        response = self.client.post(
            reverse("antwoorden_cms:page_metadata"),
            {"url": "https://example.nl", "title": "Some Title"},
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data[0].code, "invalid")

        self.assertEqual(PageMetadata.objects.count(), 0)

    def test_post_page_metadata_already_exists(self):
        gemeente = GemeenteFactory.create(name="Dongen")
        gemeente_domain = GemeenteDomainFactory(gemeente=gemeente, domain="dongen.nl")
        PageMetadataFactory.create(
            path="/",
            title="Some Title",
            gem_enabled=True,
            gemeente_domain=gemeente_domain,
        )
        response = self.client.post(
            reverse("antwoorden_cms:page_metadata"),
            {"url": "https://dongen.nl", "title": "Some Title"},
        )

        self.assertEqual(response.status_code, 201)
        self.assertDictEqual(
            response.data,
            {
                "gem_enabled": True,
                "direct_handover": False,
                "widget_title": "Gem",
                "widget_subtitle": "Je digitale hulp van de gemeente",
                "popup_message": "Stel je vraag",
                "avatar_url": "",
                "organisation_name": "Dongen",
                "organisation_slug": "dongen",
                "livechat_type": "",
                "livechat_id": "",
                "livechat_avatar_url": "",
            },
        )

    def test_post_page_metadata_already_exists_case_insensitive(self):
        gemeente = GemeenteFactory.create(name="Dongen")
        gemeente_domain = GemeenteDomainFactory(gemeente=gemeente, domain="dongen.nl")
        PageMetadataFactory.create(
            path="/Some-Path-With-Uppercase",
            title="Some Title",
            gem_enabled=True,
            gemeente_domain=gemeente_domain,
        )
        response = self.client.post(
            reverse("antwoorden_cms:page_metadata"),
            {
                "url": "https://dongen.nl/some-path-With-uppercase",
                "title": "Some Title",
            },
        )

        self.assertEqual(response.status_code, 201)
        self.assertDictEqual(
            response.data,
            {
                "gem_enabled": True,
                "direct_handover": False,
                "widget_title": "Gem",
                "widget_subtitle": "Je digitale hulp van de gemeente",
                "popup_message": "Stel je vraag",
                "avatar_url": "",
                "organisation_name": "Dongen",
                "organisation_slug": "dongen",
                "livechat_type": "",
                "livechat_id": "",
                "livechat_avatar_url": "",
            },
        )
        # Should be saved with uppercase
        self.assertEqual(PageMetadata.objects.get().path, "/Some-Path-With-Uppercase")

    def test_post_page_metadata_antwoorden_cms_workaround(self):
        gemeente = GemeenteFactory.create(
            name="Dongen",
            widget_title="some title",
        )
        GemeenteDomainFactory(gemeente=gemeente, domain="dongen.nl")

        response = self.client.post(
            reverse("antwoorden_cms:page_metadata"),
            {"url": "#Dongen", "title": "Some Title"},
        )

        self.assertEqual(response.status_code, 201)
        self.assertDictEqual(
            response.data,
            {
                "gem_enabled": True,
                "direct_handover": False,
                "widget_title": "some title",
                "widget_subtitle": "Je digitale hulp van de gemeente",
                "popup_message": "Stel je vraag",
                "avatar_url": "",
                "organisation_name": "Dongen",
                "organisation_slug": "dongen",
                "livechat_type": "",
                "livechat_id": "",
                "livechat_avatar_url": "",
            },
        )

        # No Pagemetadata object should be created
        self.assertFalse(PageMetadata.objects.exists())

    def test_post_page_metadata_already_exists_ignore_www(self):
        gemeente = GemeenteFactory.create(name="West Betuwe")
        gemeente_domain = GemeenteDomainFactory(
            gemeente=gemeente, domain="westbetuwe.nl"
        )
        PageMetadataFactory.create(
            path="/",
            title="Some Title",
            gem_enabled=True,
            gemeente_domain=gemeente_domain,
        )
        response = self.client.post(
            reverse("antwoorden_cms:page_metadata"),
            {"url": "https://www.westbetuwe.nl", "title": "Some Title"},
        )

        self.assertEqual(response.status_code, 201)
        self.assertDictEqual(
            response.data,
            {
                "gem_enabled": True,
                "direct_handover": False,
                "widget_title": "Gem",
                "widget_subtitle": "Je digitale hulp van de gemeente",
                "popup_message": "Stel je vraag",
                "avatar_url": "",
                "organisation_name": "West Betuwe",
                "organisation_slug": "west-betuwe",
                "livechat_type": "",
                "livechat_id": "",
                "livechat_avatar_url": "",
            },
        )

    def test_post_page_metadata_already_exists_ignore_query_params(self):
        gemeente = GemeenteFactory.create(name="Dongen")
        gemeente_domain = GemeenteDomainFactory(gemeente=gemeente, domain="dongen.nl")
        PageMetadataFactory.create(
            path="/",
            title="Some Title",
            gem_enabled=True,
            gemeente_domain=gemeente_domain,
        )
        response = self.client.post(
            reverse("antwoorden_cms:page_metadata"),
            {"url": "https://dongen.nl?foo=bar", "title": "Some Title"},
        )

        self.assertEqual(response.status_code, 201)
        self.assertDictEqual(
            response.data,
            {
                "gem_enabled": True,
                "direct_handover": False,
                "widget_title": "Gem",
                "widget_subtitle": "Je digitale hulp van de gemeente",
                "popup_message": "Stel je vraag",
                "avatar_url": "",
                "organisation_name": "Dongen",
                "organisation_slug": "dongen",
                "livechat_type": "",
                "livechat_id": "",
                "livechat_avatar_url": "",
            },
        )

    def test_post_page_metadata_already_exists_ignore_trailing_slash(self):
        gemeente = GemeenteFactory.create(name="Dongen")
        gemeente_domain = GemeenteDomainFactory(gemeente=gemeente, domain="dongen.nl")
        PageMetadataFactory.create(
            path="/foo/bar",
            title="Some Title",
            gem_enabled=True,
            gemeente_domain=gemeente_domain,
        )
        response = self.client.post(
            reverse("antwoorden_cms:page_metadata"),
            {"url": "https://dongen.nl/foo/bar/", "title": "Some Title"},
        )

        self.assertEqual(response.status_code, 201)
        self.assertDictEqual(
            response.data,
            {
                "gem_enabled": True,
                "direct_handover": False,
                "widget_title": "Gem",
                "widget_subtitle": "Je digitale hulp van de gemeente",
                "popup_message": "Stel je vraag",
                "avatar_url": "",
                "organisation_name": "Dongen",
                "organisation_slug": "dongen",
                "livechat_type": "",
                "livechat_id": "",
                "livechat_avatar_url": "",
            },
        )

    def test_post_page_metadata_already_exists_multiple_domains(self):
        gemeente = GemeenteFactory.create(name="Dongen")
        gemeente_domain = GemeenteDomainFactory(gemeente=gemeente, domain="dongen.nl")
        gemeente_domain2 = GemeenteDomainFactory(
            gemeente=gemeente, domain="test.dongen.nl"
        )
        PageMetadataFactory.create(
            path="/",
            title="Some Title",
            gem_enabled=True,
            gemeente_domain=gemeente_domain,
        )
        PageMetadataFactory.create(
            path="/",
            title="Some Title",
            gem_enabled=False,
            gemeente_domain=gemeente_domain2,
        )

        response = self.client.post(
            reverse("antwoorden_cms:page_metadata"),
            {"url": "https://dongen.nl", "title": "Some Title"},
        )

        self.assertEqual(response.status_code, 201)
        self.assertDictEqual(
            response.data,
            {
                "gem_enabled": True,
                "direct_handover": False,
                "widget_title": "Gem",
                "widget_subtitle": "Je digitale hulp van de gemeente",
                "popup_message": "Stel je vraag",
                "avatar_url": "",
                "organisation_name": "Dongen",
                "organisation_slug": "dongen",
                "livechat_type": "",
                "livechat_id": "",
                "livechat_avatar_url": "",
            },
        )

    def test_post_page_metadata_does_not_exist_multiple_domains(self):
        gemeente = GemeenteFactory.create(name="Dongen")
        gemeente_domain = GemeenteDomainFactory(gemeente=gemeente, domain="dongen.nl")
        GemeenteDomainFactory(gemeente=gemeente, domain="test.dongen.nl")
        PageMetadataFactory.create(
            path="/",
            title="Some Title",
            gem_enabled=True,
            gemeente_domain=gemeente_domain,
        )

        response = self.client.post(
            reverse("antwoorden_cms:page_metadata"),
            {"url": "https://test.dongen.nl", "title": "Some Title"},
        )

        self.assertEqual(response.status_code, 201)
        self.assertDictEqual(
            response.data,
            {
                "gem_enabled": False,
                "direct_handover": False,
                "widget_title": "Gem",
                "widget_subtitle": "Je digitale hulp van de gemeente",
                "popup_message": "Stel je vraag",
                "avatar_url": "",
                "organisation_name": "Dongen",
                "organisation_slug": "dongen",
                "livechat_type": "",
                "livechat_id": "",
                "livechat_avatar_url": "",
            },
        )

    def test_post_page_metadata_other_attributes(self):
        gemeente = GemeenteFactory.create(
            name="Dongen",
            widget_title="Gemeente Dongen",
            widget_subtitle="",
            popup_message="Chat met ons",
        )
        LivechatConfigurationFactory.create(
            gemeente=gemeente,
            type=LivechatTypes.obi4wan,
            identifier="foo",
            avatar_url="http://example.com/image",
        )
        gemeente_domain = GemeenteDomainFactory(gemeente=gemeente, domain="dongen.nl")
        PageMetadataFactory.create(
            path="/",
            title="Some Title",
            gem_enabled=True,
            gemeente_domain=gemeente_domain,
            direct_handover=True,
        )
        response = self.client.post(
            reverse("antwoorden_cms:page_metadata"),
            {"url": "https://dongen.nl", "title": "Some Title"},
        )

        self.assertEqual(response.status_code, 201)
        self.assertDictEqual(
            response.data,
            {
                "gem_enabled": True,
                "direct_handover": True,
                "widget_title": "Gemeente Dongen",
                "widget_subtitle": "",
                "popup_message": "Chat met ons",
                "avatar_url": "",
                "organisation_name": "Dongen",
                "organisation_slug": "dongen",
                "livechat_type": LivechatTypes.obi4wan,
                "livechat_id": "foo",
                "livechat_avatar_url": "http://example.com/image",
            },
        )

    def test_post_page_metadata_avatar_url(self):
        image = tempfile.NamedTemporaryFile(suffix=".png")
        gemeente = GemeenteFactory.create(name="Dongen", widget_avatar=image.name)
        gemeente_domain = GemeenteDomainFactory(gemeente=gemeente, domain="dongen.nl")
        PageMetadataFactory.create(
            path="/",
            title="Some Title",
            gem_enabled=True,
            gemeente_domain=gemeente_domain,
        )
        response = self.client.post(
            reverse("antwoorden_cms:page_metadata"),
            {"url": "https://dongen.nl", "title": "Some Title"},
        )

        self.assertEqual(response.status_code, 201)
        self.assertDictEqual(
            response.data,
            {
                "gem_enabled": True,
                "direct_handover": False,
                "widget_title": "Gem",
                "widget_subtitle": "Je digitale hulp van de gemeente",
                "popup_message": "Stel je vraag",
                "avatar_url": f"http://testserver{gemeente.widget_avatar.url}",
                "organisation_name": "Dongen",
                "organisation_slug": "dongen",
                "livechat_type": "",
                "livechat_id": "",
                "livechat_avatar_url": "",
            },
        )


class PageMetadataAPIGemEnabledTests(APITestCase):
    def test_post_page_metadata_gem_default_enabled_true_local_enabled_true(self):
        gemeente = GemeenteFactory.create(name="Dongen")
        gemeente_domain = GemeenteDomainFactory(
            gemeente=gemeente, domain="dongen.nl", gem_default_enabled=True
        )
        PageMetadataFactory.create(
            path="/",
            title="Some Title",
            gem_enabled=True,
            gemeente_domain=gemeente_domain,
        )
        response = self.client.post(
            reverse("antwoorden_cms:page_metadata"),
            {"url": "https://www.dongen.nl", "title": "Some Title"},
        )

        self.assertEqual(response.status_code, 201)
        self.assertTrue(response.data["gem_enabled"])

    def test_post_page_metadata_gem_default_enabled_true_local_enabled_false(self):
        gemeente = GemeenteFactory.create(name="Dongen")
        gemeente_domain = GemeenteDomainFactory(
            gemeente=gemeente, domain="dongen.nl", gem_default_enabled=True
        )
        PageMetadataFactory.create(
            path="/",
            title="Some Title",
            gem_enabled=False,
            gemeente_domain=gemeente_domain,
        )
        response = self.client.post(
            reverse("antwoorden_cms:page_metadata"),
            {"url": "https://www.dongen.nl", "title": "Some Title"},
        )

        self.assertEqual(response.status_code, 201)
        self.assertFalse(response.data["gem_enabled"])

    def test_post_page_metadata_gem_default_enabled_true_local_enabled_none(self):
        gemeente = GemeenteFactory.create(name="Dongen")
        gemeente_domain = GemeenteDomainFactory(
            gemeente=gemeente, domain="dongen.nl", gem_default_enabled=True
        )
        PageMetadataFactory.create(
            path="/",
            title="Some Title",
            gem_enabled=None,
            gemeente_domain=gemeente_domain,
        )
        response = self.client.post(
            reverse("antwoorden_cms:page_metadata"),
            {"url": "https://www.dongen.nl", "title": "Some Title"},
        )

        self.assertEqual(response.status_code, 201)
        self.assertTrue(response.data["gem_enabled"])

    def test_post_page_metadata_gem_default_enabled_false_local_enabled_true(self):
        gemeente = GemeenteFactory.create(name="Dongen")
        gemeente_domain = GemeenteDomainFactory(
            gemeente=gemeente, domain="dongen.nl", gem_default_enabled=False
        )
        PageMetadataFactory.create(
            path="/",
            title="Some Title",
            gem_enabled=True,
            gemeente_domain=gemeente_domain,
        )
        response = self.client.post(
            reverse("antwoorden_cms:page_metadata"),
            {"url": "https://www.dongen.nl", "title": "Some Title"},
        )

        self.assertEqual(response.status_code, 201)
        self.assertTrue(response.data["gem_enabled"])

    def test_post_page_metadata_gem_default_enabled_false_local_enabled_false(self):
        gemeente = GemeenteFactory.create(name="Dongen")
        gemeente_domain = GemeenteDomainFactory(
            gemeente=gemeente, domain="dongen.nl", gem_default_enabled=False
        )
        PageMetadataFactory.create(
            path="/",
            title="Some Title",
            gem_enabled=False,
            gemeente_domain=gemeente_domain,
        )
        response = self.client.post(
            reverse("antwoorden_cms:page_metadata"),
            {"url": "https://www.dongen.nl", "title": "Some Title"},
        )

        self.assertEqual(response.status_code, 201)
        self.assertFalse(response.data["gem_enabled"])

    def test_post_page_metadata_gem_default_enabled_false_local_enabled_none(self):
        gemeente = GemeenteFactory.create(name="Dongen")
        gemeente_domain = GemeenteDomainFactory(
            gemeente=gemeente, domain="dongen.nl", gem_default_enabled=False
        )
        PageMetadataFactory.create(
            path="/",
            title="Some Title",
            gem_enabled=None,
            gemeente_domain=gemeente_domain,
        )
        response = self.client.post(
            reverse("antwoorden_cms:page_metadata"),
            {"url": "https://www.dongen.nl", "title": "Some Title"},
        )

        self.assertEqual(response.status_code, 201)
        self.assertFalse(response.data["gem_enabled"])


class PageMetadataAPIDirectHandoverTests(APITestCase):
    def test_post_page_metadata_direct_handover_true_local_enabled_true(self):
        gemeente = GemeenteFactory.create(name="Dongen")
        gemeente_domain = GemeenteDomainFactory(
            gemeente=gemeente, domain="dongen.nl", direct_handover=True
        )
        PageMetadataFactory.create(
            path="/",
            title="Some Title",
            direct_handover=True,
            gemeente_domain=gemeente_domain,
        )
        response = self.client.post(
            reverse("antwoorden_cms:page_metadata"),
            {"url": "https://www.dongen.nl", "title": "Some Title"},
        )

        self.assertEqual(response.status_code, 201)
        self.assertTrue(response.data["direct_handover"])

    def test_post_page_metadata_direct_handover_true_local_enabled_false(self):
        gemeente = GemeenteFactory.create(name="Dongen")
        gemeente_domain = GemeenteDomainFactory(
            gemeente=gemeente, domain="dongen.nl", direct_handover=True
        )
        PageMetadataFactory.create(
            path="/",
            title="Some Title",
            direct_handover=False,
            gemeente_domain=gemeente_domain,
        )
        response = self.client.post(
            reverse("antwoorden_cms:page_metadata"),
            {"url": "https://www.dongen.nl", "title": "Some Title"},
        )

        self.assertEqual(response.status_code, 201)
        self.assertFalse(response.data["direct_handover"])

    def test_post_page_metadata_direct_handover_true_local_enabled_none(self):
        gemeente = GemeenteFactory.create(name="Dongen")
        gemeente_domain = GemeenteDomainFactory(
            gemeente=gemeente, domain="dongen.nl", direct_handover=True
        )
        PageMetadataFactory.create(
            path="/",
            title="Some Title",
            direct_handover=None,
            gemeente_domain=gemeente_domain,
        )
        response = self.client.post(
            reverse("antwoorden_cms:page_metadata"),
            {"url": "https://www.dongen.nl", "title": "Some Title"},
        )

        self.assertEqual(response.status_code, 201)
        self.assertTrue(response.data["direct_handover"])

    def test_post_page_metadata_direct_handover_false_local_enabled_true(self):
        gemeente = GemeenteFactory.create(name="Dongen")
        gemeente_domain = GemeenteDomainFactory(
            gemeente=gemeente, domain="dongen.nl", direct_handover=False
        )
        PageMetadataFactory.create(
            path="/",
            title="Some Title",
            direct_handover=True,
            gemeente_domain=gemeente_domain,
        )
        response = self.client.post(
            reverse("antwoorden_cms:page_metadata"),
            {"url": "https://www.dongen.nl", "title": "Some Title"},
        )

        self.assertEqual(response.status_code, 201)
        self.assertTrue(response.data["direct_handover"])

    def test_post_page_metadata_direct_handover_false_local_enabled_false(self):
        gemeente = GemeenteFactory.create(name="Dongen")
        gemeente_domain = GemeenteDomainFactory(
            gemeente=gemeente, domain="dongen.nl", direct_handover=False
        )
        PageMetadataFactory.create(
            path="/",
            title="Some Title",
            direct_handover=False,
            gemeente_domain=gemeente_domain,
        )
        response = self.client.post(
            reverse("antwoorden_cms:page_metadata"),
            {"url": "https://www.dongen.nl", "title": "Some Title"},
        )

        self.assertEqual(response.status_code, 201)
        self.assertFalse(response.data["direct_handover"])

    def test_post_page_metadata_direct_handover_false_local_enabled_none(self):
        gemeente = GemeenteFactory.create(name="Dongen")
        gemeente_domain = GemeenteDomainFactory(
            gemeente=gemeente, domain="dongen.nl", direct_handover=False
        )
        PageMetadataFactory.create(
            path="/",
            title="Some Title",
            direct_handover=None,
            gemeente_domain=gemeente_domain,
        )
        response = self.client.post(
            reverse("antwoorden_cms:page_metadata"),
            {"url": "https://www.dongen.nl", "title": "Some Title"},
        )

        self.assertEqual(response.status_code, 201)
        self.assertFalse(response.data["direct_handover"])


class PageMetadataAPILivechatConfigTests(APITestCase):
    def test_post_page_metadata_multiple_configs_default_livechat_config(self):
        gemeente = GemeenteFactory.create(
            name="Dongen",
            livechat_enabled=True,
            widget_title="Gemeente Dongen",
            widget_subtitle="",
            popup_message="Chat met ons",
        )
        LivechatConfigurationFactory.create(
            gemeente=gemeente,
            type=LivechatTypes.obi4wan,
            identifier="foo",
            avatar_url="http://example.com/image1",
            order=2,
        )
        LivechatConfigurationFactory.create(
            gemeente=gemeente,
            type=LivechatTypes.obi4wan,
            identifier="bar",
            avatar_url="http://example.com/image2",
            order=0,
        )
        LivechatConfigurationFactory.create(
            gemeente=gemeente,
            type=LivechatTypes.obi4wan,
            identifier="baz",
            avatar_url="http://example.com/image3",
            order=1,
        )
        gemeente_domain = GemeenteDomainFactory(gemeente=gemeente, domain="dongen.nl")
        PageMetadataFactory.create(
            path="/",
            title="Some Title",
            gem_enabled=True,
            gemeente_domain=gemeente_domain,
            direct_handover=True,
        )
        response = self.client.post(
            reverse("antwoorden_cms:page_metadata"),
            {"url": "https://dongen.nl", "title": "Some Title"},
        )

        self.assertEqual(response.status_code, 201)
        self.assertDictEqual(
            response.data,
            {
                "gem_enabled": True,
                "direct_handover": True,
                "widget_title": "Gemeente Dongen",
                "widget_subtitle": "",
                "popup_message": "Chat met ons",
                "avatar_url": "",
                "organisation_name": "Dongen",
                "organisation_slug": "dongen",
                "livechat_type": LivechatTypes.obi4wan,
                "livechat_id": "bar",
                "livechat_avatar_url": "http://example.com/image2",
            },
        )

    def test_post_page_metadata_multiple_configs_default_livechat_config_livechat_disabled(
        self,
    ):
        gemeente = GemeenteFactory.create(
            name="Dongen",
            livechat_enabled=False,
            widget_title="Gemeente Dongen",
            widget_subtitle="",
            popup_message="Chat met ons",
        )
        LivechatConfigurationFactory.create(
            gemeente=gemeente,
            type=LivechatTypes.obi4wan,
            identifier="foo",
            avatar_url="http://example.com/image1",
            order=2,
        )
        LivechatConfigurationFactory.create(
            gemeente=gemeente,
            type=LivechatTypes.obi4wan,
            identifier="bar",
            avatar_url="http://example.com/image2",
            order=0,
        )
        LivechatConfigurationFactory.create(
            gemeente=gemeente,
            type=LivechatTypes.obi4wan,
            identifier="baz",
            avatar_url="http://example.com/image3",
            order=1,
        )
        gemeente_domain = GemeenteDomainFactory(gemeente=gemeente, domain="dongen.nl")
        PageMetadataFactory.create(
            path="/",
            title="Some Title",
            gem_enabled=True,
            gemeente_domain=gemeente_domain,
            direct_handover=True,
        )
        response = self.client.post(
            reverse("antwoorden_cms:page_metadata"),
            {"url": "https://dongen.nl", "title": "Some Title"},
        )

        self.assertEqual(response.status_code, 201)
        self.assertDictEqual(
            response.data,
            {
                "gem_enabled": True,
                "direct_handover": True,
                "widget_title": "Gemeente Dongen",
                "widget_subtitle": "",
                "popup_message": "Chat met ons",
                "avatar_url": "",
                "organisation_name": "Dongen",
                "organisation_slug": "dongen",
                "livechat_type": "",
                "livechat_id": "",
                "livechat_avatar_url": "",
            },
        )

    def test_post_page_metadata_multiple_configs_specific_livechat_config(self):
        gemeente = GemeenteFactory.create(
            name="Dongen",
            widget_title="Gemeente Dongen",
            widget_subtitle="",
            popup_message="Chat met ons",
        )
        LivechatConfigurationFactory.create(
            gemeente=gemeente,
            type=LivechatTypes.obi4wan,
            identifier="foo",
            avatar_url="http://example.com/image1",
            order=2,
        )
        LivechatConfigurationFactory.create(
            gemeente=gemeente,
            type=LivechatTypes.obi4wan,
            identifier="bar",
            avatar_url="http://example.com/image2",
            order=0,
        )
        config = LivechatConfigurationFactory.create(
            gemeente=gemeente,
            type=LivechatTypes.obi4wan,
            identifier="baz",
            avatar_url="http://example.com/image3",
            order=1,
        )
        gemeente_domain = GemeenteDomainFactory(gemeente=gemeente, domain="dongen.nl")
        PageMetadataFactory.create(
            path="/",
            title="Some Title",
            gem_enabled=True,
            gemeente_domain=gemeente_domain,
            direct_handover=True,
            livechat_config=config,
        )
        response = self.client.post(
            reverse("antwoorden_cms:page_metadata"),
            {"url": "https://dongen.nl", "title": "Some Title"},
        )

        self.assertEqual(response.status_code, 201)
        self.assertDictEqual(
            response.data,
            {
                "gem_enabled": True,
                "direct_handover": True,
                "widget_title": "Gemeente Dongen",
                "widget_subtitle": "",
                "popup_message": "Chat met ons",
                "avatar_url": "",
                "organisation_name": "Dongen",
                "organisation_slug": "dongen",
                "livechat_type": LivechatTypes.obi4wan,
                "livechat_id": "baz",
                "livechat_avatar_url": "http://example.com/image3",
            },
        )

    def test_post_page_metadata_multiple_configs_specific_livechat_config_livechat_disabled(
        self,
    ):
        gemeente = GemeenteFactory.create(
            name="Dongen",
            livechat_enabled=False,
            widget_title="Gemeente Dongen",
            widget_subtitle="",
            popup_message="Chat met ons",
        )
        LivechatConfigurationFactory.create(
            gemeente=gemeente,
            type=LivechatTypes.obi4wan,
            identifier="foo",
            avatar_url="http://example.com/image1",
            order=2,
        )
        LivechatConfigurationFactory.create(
            gemeente=gemeente,
            type=LivechatTypes.obi4wan,
            identifier="bar",
            avatar_url="http://example.com/image2",
            order=0,
        )
        config = LivechatConfigurationFactory.create(
            gemeente=gemeente,
            type=LivechatTypes.obi4wan,
            identifier="baz",
            avatar_url="http://example.com/image3",
            order=1,
        )
        gemeente_domain = GemeenteDomainFactory(gemeente=gemeente, domain="dongen.nl")
        PageMetadataFactory.create(
            path="/",
            title="Some Title",
            gem_enabled=True,
            gemeente_domain=gemeente_domain,
            direct_handover=True,
            livechat_config=config,
        )
        response = self.client.post(
            reverse("antwoorden_cms:page_metadata"),
            {"url": "https://dongen.nl", "title": "Some Title"},
        )

        self.assertEqual(response.status_code, 201)
        self.assertDictEqual(
            response.data,
            {
                "gem_enabled": True,
                "direct_handover": True,
                "widget_title": "Gemeente Dongen",
                "widget_subtitle": "",
                "popup_message": "Chat met ons",
                "avatar_url": "",
                "organisation_name": "Dongen",
                "organisation_slug": "dongen",
                "livechat_type": "",
                "livechat_id": "",
                "livechat_avatar_url": "",
            },
        )

    def test_post_page_metadata_no_livechat_config(self):
        gemeente = GemeenteFactory.create(
            name="Dongen",
            widget_title="Gemeente Dongen",
            widget_subtitle="",
            popup_message="Chat met ons",
        )
        gemeente_domain = GemeenteDomainFactory(gemeente=gemeente, domain="dongen.nl")
        PageMetadataFactory.create(
            path="/",
            title="Some Title",
            gem_enabled=True,
            gemeente_domain=gemeente_domain,
            direct_handover=True,
        )
        response = self.client.post(
            reverse("antwoorden_cms:page_metadata"),
            {"url": "https://dongen.nl", "title": "Some Title"},
        )

        self.assertEqual(response.status_code, 201)
        self.assertDictEqual(
            response.data,
            {
                "gem_enabled": True,
                "direct_handover": True,
                "widget_title": "Gemeente Dongen",
                "widget_subtitle": "",
                "popup_message": "Chat met ons",
                "avatar_url": "",
                "organisation_name": "Dongen",
                "organisation_slug": "dongen",
                "livechat_type": "",
                "livechat_id": "",
                "livechat_avatar_url": "",
            },
        )

    def test_post_page_metadata_multiple_configs_default_livechat_config_with_avatar(
        self,
    ):
        image1 = tempfile.NamedTemporaryFile(suffix=".png")
        image2 = tempfile.NamedTemporaryFile(suffix=".png")
        gemeente = GemeenteFactory.create(
            name="Dongen",
            widget_title="Gemeente Dongen",
            widget_subtitle="",
            popup_message="Chat met ons",
        )
        LivechatConfigurationFactory.create(
            gemeente=gemeente,
            type=LivechatTypes.obi4wan,
            identifier="foo",
            avatar_url="http://example.com/image1",
            avatar=image1.name,
            order=2,
        )
        LivechatConfigurationFactory.create(
            gemeente=gemeente,
            type=LivechatTypes.obi4wan,
            identifier="bar",
            avatar_url="http://example.com/image2",
            avatar=image2.name,
            order=0,
        )
        LivechatConfigurationFactory.create(
            gemeente=gemeente,
            type=LivechatTypes.obi4wan,
            identifier="baz",
            avatar_url="http://example.com/image3",
            order=1,
        )
        gemeente_domain = GemeenteDomainFactory(gemeente=gemeente, domain="dongen.nl")
        PageMetadataFactory.create(
            path="/",
            title="Some Title",
            gem_enabled=True,
            gemeente_domain=gemeente_domain,
            direct_handover=True,
        )
        response = self.client.post(
            reverse("antwoorden_cms:page_metadata"),
            {"url": "https://dongen.nl", "title": "Some Title"},
        )

        self.assertEqual(response.status_code, 201)
        self.assertDictEqual(
            response.data,
            {
                "gem_enabled": True,
                "direct_handover": True,
                "widget_title": "Gemeente Dongen",
                "widget_subtitle": "",
                "popup_message": "Chat met ons",
                "avatar_url": "",
                "organisation_name": "Dongen",
                "organisation_slug": "dongen",
                "livechat_type": LivechatTypes.obi4wan,
                "livechat_id": "bar",
                "livechat_avatar_url": f"http://testserver/media{image2.name}",
            },
        )

    def test_post_page_metadata_multiple_configs_default_livechat_config_without_avatar_and_avatar_url(
        self,
    ):
        image1 = tempfile.NamedTemporaryFile(suffix=".png")
        gemeente = GemeenteFactory.create(
            name="Dongen",
            widget_title="Gemeente Dongen",
            widget_subtitle="",
            popup_message="Chat met ons",
        )
        LivechatConfigurationFactory.create(
            gemeente=gemeente,
            type=LivechatTypes.obi4wan,
            identifier="foo",
            avatar_url="http://example.com/image1",
            avatar=image1.name,
            order=2,
        )
        LivechatConfigurationFactory.create(
            gemeente=gemeente,
            type=LivechatTypes.obi4wan,
            identifier="bar",
            order=0,
        )
        LivechatConfigurationFactory.create(
            gemeente=gemeente,
            type=LivechatTypes.obi4wan,
            identifier="baz",
            avatar_url="http://example.com/image3",
            order=1,
        )
        gemeente_domain = GemeenteDomainFactory(gemeente=gemeente, domain="dongen.nl")
        PageMetadataFactory.create(
            path="/",
            title="Some Title",
            gem_enabled=True,
            gemeente_domain=gemeente_domain,
            direct_handover=True,
        )
        response = self.client.post(
            reverse("antwoorden_cms:page_metadata"),
            {"url": "https://dongen.nl", "title": "Some Title"},
        )

        self.assertEqual(response.status_code, 201)
        self.assertDictEqual(
            response.data,
            {
                "gem_enabled": True,
                "direct_handover": True,
                "widget_title": "Gemeente Dongen",
                "widget_subtitle": "",
                "popup_message": "Chat met ons",
                "avatar_url": "",
                "organisation_name": "Dongen",
                "organisation_slug": "dongen",
                "livechat_type": LivechatTypes.obi4wan,
                "livechat_id": "bar",
                "livechat_avatar_url": "",
            },
        )
