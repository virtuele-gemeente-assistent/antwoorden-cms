import tempfile

from django.test import tag
from django.urls import reverse
from django.utils.translation import ugettext as _

import requests_mock
from django_webtest import WebTest

from antwoorden_cms.utters.choices import LivechatTypes
from antwoorden_cms.utters.models import Gemeente, GemeenteDomain
from antwoorden_cms.utters.tests.factories import (
    GemeenteDomainFactory,
    GemeenteFactory,
    LivechatConfigurationFactory,
)
from antwoorden_cms.utters.tests.mixins import TwoFactorTestCaseMixin
from antwoorden_cms.utters.tests.utils import grant_gemeente_perms

from ..models import PageMetadata
from .factories import PageMetadataFactory


class PageMetadataCRUDTests(TwoFactorTestCaseMixin, WebTest):
    login_url = reverse("two_factor:login")
    user_is_admin = False

    def setUp(self):
        super().setUp()

        gemeente = GemeenteFactory(name="amsterdam")
        GemeenteDomainFactory(gemeente=gemeente, domain="amsterdam.nl")
        self.user.gemeentes.add(gemeente)
        self.user.save()

        self.gemeente = GemeenteFactory(name="rotterdam")
        self.gemeente_domain = GemeenteDomainFactory(
            gemeente=self.gemeente, domain="rotterdam.nl"
        )
        self.page_metadata_list_url = reverse(
            "config:page_metadata_list", kwargs={"gemeente_slug": self.gemeente.slug}
        )

    def test_page_metadata_list(self):
        grant_gemeente_perms(
            "admin",
            self.user,
            self.gemeente,
            ["page_metadata_read", "page_metadata_write"],
        )

        config = LivechatConfigurationFactory.create(
            gemeente=self.gemeente,
            type=LivechatTypes.obi4wan,
            identifier="foo",
            avatar_url="http://example.com/image",
            name="Specific livechat",
        )

        self.metadata1 = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain,
            title="Paspoort",
            path="/paspoort",
            gem_enabled=True,
            direct_handover=True,
        )
        self.metadata2 = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain,
            title="ID kaart",
            path="/id-kaart",
            direct_handover=True,
            livechat_config=config,
        )
        PageMetadataFactory(
            gemeente_domain=GemeenteDomain.objects.get(gemeente__name="amsterdam"),
            title="ID kaart",
            path="/id-kaart",
        )

        response = self.app.get(self.page_metadata_list_url, user=self.user)

        self.assertEqual(response.status_code, 200)

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 2)

        for i, metadata in enumerate([self.metadata2, self.metadata1]):
            with self.subTest(metadata=metadata):
                detail_link = rows[i].find_all("td")[0].find("a")
                self.assertEqual(
                    detail_link.attrs["href"],
                    reverse(
                        "config:page_metadata_read",
                        kwargs={
                            "gemeente_slug": self.gemeente.slug,
                            "page_metadata_pk": metadata.pk,
                        },
                    ),
                )

                page_title = rows[i].find_all("td")[1]
                self.assertEqual(page_title.text, metadata.title)

                page_link = rows[i].find_all("td")[2].find("a")
                self.assertEqual(
                    page_link.attrs["href"],
                    f"https://{self.gemeente_domain.domain}{metadata.path}",
                )
                self.assertEqual(
                    page_link.text,
                    f"https://{self.gemeente_domain.domain}{metadata.path}",
                )

                if i == 0:
                    livechat_config = rows[i].find_all("td")[3].text
                    self.assertEqual(livechat_config.strip(), "Specific livechat")
                elif i == 1:
                    livechat_config = rows[i].find_all("td")[3].text
                    self.assertEqual(livechat_config.strip(), "Specific livechat")

                gem_enabled = rows[i].find_all("td")[4].find("img").attrs["alt"]
                self.assertEqual(gem_enabled, str(metadata.gem_enabled_with_default))

                direct_handover = rows[i].find_all("td")[5].find("img").attrs["alt"]
                self.assertEqual(direct_handover, str(metadata.direct_handover))

                # Only the two links shown above should appear
                # Update/delete buttons should not appear
                buttons = rows[i].find_all("a")
                self.assertEqual(len(buttons), 4)

                delete_button, update_button = buttons[2:4]

                self.assertEqual(
                    delete_button.attrs["href"],
                    reverse(
                        "config:page_metadata_delete",
                        kwargs={
                            "gemeente_slug": self.gemeente.slug,
                            "page_metadata_pk": metadata.pk,
                        },
                    ),
                )
                self.assertEqual(
                    update_button.attrs["href"],
                    reverse(
                        "config:page_metadata_update",
                        kwargs={
                            "gemeente_slug": self.gemeente.slug,
                            "page_metadata_pk": metadata.pk,
                        },
                    ),
                )

    def test_page_metadata_list_sort(self):
        grant_gemeente_perms(
            "admin",
            self.user,
            self.gemeente,
            ["page_metadata_read", "page_metadata_write"],
        )

        config1 = LivechatConfigurationFactory.create(
            gemeente=self.gemeente,
            type=LivechatTypes.obi4wan,
            identifier="foo",
            avatar_url="http://example.com/image",
            name="Specific livechat",
        )
        config2 = LivechatConfigurationFactory.create(
            gemeente=self.gemeente,
            type=LivechatTypes.obi4wan,
            identifier="bar",
            avatar_url="http://example.com/image2",
            name="Livechat 2",
        )

        self.metadata1 = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain,
            title="Paspoort",
            path="/paspoort",
            gem_enabled=True,
            direct_handover=True,
            livechat_config=config2,
        )
        self.metadata2 = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain,
            title="ID kaart",
            path="/id-kaart",
            gem_enabled=None,
            direct_handover=True,
        )
        self.metadata3 = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain,
            title="verhuizen",
            path="/verhuizen",
            gem_enabled=False,
            direct_handover=False,
            livechat_config=config1,
        )
        PageMetadataFactory(
            gemeente_domain=GemeenteDomain.objects.get(gemeente__name="amsterdam"),
            title="ID kaart",
            path="/id-kaart",
            gem_enabled=False,
            direct_handover=False,
        )

        subtests = [
            ("title", "asc", [self.metadata2, self.metadata1, self.metadata3]),
            ("title", "desc", [self.metadata3, self.metadata1, self.metadata2]),
            ("path", "asc", [self.metadata2, self.metadata1, self.metadata3]),
            ("path", "desc", [self.metadata3, self.metadata1, self.metadata2]),
            ("gem_enabled", "asc", [self.metadata2, self.metadata3, self.metadata1]),
            ("gem_enabled", "desc", [self.metadata1, self.metadata3, self.metadata2]),
            (
                "direct_handover",
                "asc",
                [self.metadata3, self.metadata1, self.metadata2],
            ),
            (
                "direct_handover",
                "desc",
                [self.metadata1, self.metadata2, self.metadata3],
            ),
            (
                "livechat_config__name",
                "asc",
                [self.metadata1, self.metadata3, self.metadata2],
            ),
            (
                "livechat_config__name",
                "desc",
                [self.metadata2, self.metadata3, self.metadata1],
            ),
        ]
        subtests = [
            ("gem_enabled", "asc", [self.metadata3, self.metadata1, self.metadata2]),
            ("gem_enabled", "desc", [self.metadata2, self.metadata1, self.metadata3]),
        ]
        for sort, order, ordering in subtests:
            with self.subTest(sort=sort, order=order):
                response = self.app.get(
                    self.page_metadata_list_url,
                    {"sort": sort, "order": order},
                    user=self.user,
                )

                self.assertEqual(response.status_code, 200)

                rows = response.html("tr")[1:]

                # Generic utterresponses should not show up
                self.assertEqual(len(rows), 3)

                for i, metadata in enumerate(ordering):
                    with self.subTest(metadata=metadata):
                        detail_link = rows[i].find_all("td")[0].find("a")

                        self.assertEqual(
                            detail_link.attrs["href"],
                            reverse(
                                "config:page_metadata_read",
                                kwargs={
                                    "gemeente_slug": self.gemeente.slug,
                                    "page_metadata_pk": metadata.pk,
                                },
                            ),
                        )

    def test_page_metadata_list_no_gemeente_domain(self):
        gemeente = GemeenteFactory.create(name="utrecht")
        grant_gemeente_perms(
            "admin", self.user, gemeente, ["page_metadata_read", "page_metadata_write"]
        )

        response = self.app.get(
            reverse(
                "config:page_metadata_list", kwargs={"gemeente_slug": gemeente.slug}
            ),
            user=self.user,
        )

        self.assertEqual(response.status_code, 200)

        rows = response.html("tr")[1:]

        # No pagemetadata exists
        self.assertEqual(len(rows), 0)

        metadata_list_body = response.html("div", {"class": "card-body"})[2]

        self.assertEqual(
            metadata_list_body.text.strip(), _("Er zijn nog geen domeinen ingesteld")
        )

    def test_page_metadata_list_pagination(self):
        grant_gemeente_perms(
            "admin",
            self.user,
            self.gemeente,
            ["page_metadata_read", "page_metadata_write"],
        )

        PageMetadataFactory.create(
            gemeente_domain=self.gemeente_domain, title="some title"
        )
        PageMetadataFactory.create_batch(101, gemeente_domain=self.gemeente_domain)

        response = self.app.get(self.page_metadata_list_url, user=self.user)

        self.assertEqual(response.status_code, 200)

        rows = response.html("tr")[1:]

        # Only 10 should show up
        self.assertEqual(len(rows), 10)

        pagination = response.html.find("ul", {"class": "pagination"})

        self.assertIsNotNone(pagination)

        pagination_links = response.html.findAll("a", {"class": "page-link"})

        self.assertEqual(pagination_links[0].attrs["href"], "#")
        self.assertEqual(pagination_links[0].text, _("Previous"))

        self.assertEqual(
            pagination_links[1].attrs["href"],
            "?page=2&sort=title&order=asc&domain=rotterdam.nl",
        )
        self.assertEqual(pagination_links[1].text, "2")

        self.assertEqual(
            pagination_links[2].attrs["href"],
            "?page=3&sort=title&order=asc&domain=rotterdam.nl",
        )
        self.assertEqual(pagination_links[2].text, "3")

        self.assertEqual(
            pagination_links[3].attrs["href"],
            "?page=4&sort=title&order=asc&domain=rotterdam.nl",
        )
        self.assertEqual(pagination_links[3].text, "4")

        self.assertEqual(
            pagination_links[4].attrs["href"],
            "?page=5&sort=title&order=asc&domain=rotterdam.nl",
        )
        self.assertEqual(pagination_links[4].text, "5")

        # Show the last page
        self.assertEqual(
            pagination_links[5].attrs["href"],
            "?page=11&sort=title&order=asc&domain=rotterdam.nl",
        )
        self.assertEqual(pagination_links[5].text, "11")

        self.assertEqual(
            pagination_links[6].attrs["href"],
            "?page=2&sort=title&order=asc&domain=rotterdam.nl",
        )
        self.assertEqual(pagination_links[6].text, _("Next"))

        # First row should be the first PageMetadata object
        self.assertEqual(rows[0].findAll("td")[1].text, "")

    def test_page_metadata_list_pagination_show_first_page(self):
        grant_gemeente_perms(
            "admin",
            self.user,
            self.gemeente,
            ["page_metadata_read", "page_metadata_write"],
        )

        PageMetadataFactory.create(
            gemeente_domain=self.gemeente_domain, title="some title"
        )
        PageMetadataFactory.create_batch(101, gemeente_domain=self.gemeente_domain)

        response = self.app.get(
            self.page_metadata_list_url, {"page": 11}, user=self.user
        )

        self.assertEqual(response.status_code, 200)

        rows = response.html("tr")[1:]

        # Only 10 should show up
        self.assertEqual(len(rows), 2)

        pagination = response.html.find("ul", {"class": "pagination"})

        self.assertIsNotNone(pagination)

        pagination_links = response.html.findAll("a", {"class": "page-link"})

        self.assertEqual(
            pagination_links[0].attrs["href"],
            "?page=10&sort=title&order=asc&domain=rotterdam.nl",
        )
        self.assertEqual(pagination_links[0].text, _("Previous"))

        self.assertEqual(
            pagination_links[1].attrs["href"],
            "?page=1&sort=title&order=asc&domain=rotterdam.nl",
        )
        self.assertEqual(pagination_links[1].text, "1")

        self.assertEqual(
            pagination_links[2].attrs["href"],
            "?page=7&sort=title&order=asc&domain=rotterdam.nl",
        )
        self.assertEqual(pagination_links[2].text, "7")

        self.assertEqual(
            pagination_links[3].attrs["href"],
            "?page=8&sort=title&order=asc&domain=rotterdam.nl",
        )
        self.assertEqual(pagination_links[3].text, "8")

        self.assertEqual(
            pagination_links[4].attrs["href"],
            "?page=9&sort=title&order=asc&domain=rotterdam.nl",
        )
        self.assertEqual(pagination_links[4].text, "9")

        # Show the last page
        self.assertEqual(
            pagination_links[5].attrs["href"],
            "?page=10&sort=title&order=asc&domain=rotterdam.nl",
        )
        self.assertEqual(pagination_links[5].text, "10")

        self.assertEqual(pagination_links[6].attrs["href"], "#")
        self.assertEqual(pagination_links[6].text, _("Next"))

        # First row should be the first PageMetadata object
        self.assertEqual(rows[0].findAll("td")[1].text, "")

    def test_page_metadata_list_pagination_with_search_and_sorting(self):
        grant_gemeente_perms(
            "admin",
            self.user,
            self.gemeente,
            ["page_metadata_read", "page_metadata_write"],
        )

        PageMetadataFactory.create_batch(
            41, gemeente_domain=self.gemeente_domain, title="some title"
        )
        PageMetadataFactory.create_batch(101, gemeente_domain=self.gemeente_domain)

        response = self.app.get(
            self.page_metadata_list_url,
            {"sort": "title", "order": "desc", "search": "title"},
            user=self.user,
        )

        self.assertEqual(response.status_code, 200)

        rows = response.html("tr")[1:]

        # Only 10 should show up
        self.assertEqual(len(rows), 10)

        pagination = response.html.find("ul", {"class": "pagination"})

        self.assertIsNotNone(pagination)

        pagination_links = response.html.findAll("a", {"class": "page-link"})

        self.assertEqual(pagination_links[0].attrs["href"], "#")
        self.assertEqual(pagination_links[0].text, _("Previous"))

        self.assertEqual(
            pagination_links[1].attrs["href"],
            "?page=2&sort=title&order=desc&search=title&domain=rotterdam.nl",
        )
        self.assertEqual(pagination_links[1].text, "2")

        self.assertEqual(
            pagination_links[2].attrs["href"],
            "?page=3&sort=title&order=desc&search=title&domain=rotterdam.nl",
        )
        self.assertEqual(pagination_links[2].text, "3")

        self.assertEqual(
            pagination_links[3].attrs["href"],
            "?page=4&sort=title&order=desc&search=title&domain=rotterdam.nl",
        )
        self.assertEqual(pagination_links[3].text, "4")

        self.assertEqual(
            pagination_links[4].attrs["href"],
            "?page=5&sort=title&order=desc&search=title&domain=rotterdam.nl",
        )
        self.assertEqual(pagination_links[4].text, "5")

        self.assertEqual(
            pagination_links[5].attrs["href"],
            "?page=2&sort=title&order=desc&search=title&domain=rotterdam.nl",
        )
        self.assertEqual(pagination_links[5].text, _("Next"))

        # First row should be the first PageMetadata object
        self.assertEqual(rows[0].findAll("td")[1].text, "some title")

    def test_page_metadata_list_pagination_go_to_page(self):
        grant_gemeente_perms(
            "admin",
            self.user,
            self.gemeente,
            ["page_metadata_read", "page_metadata_write"],
        )

        PageMetadataFactory.create_batch(21, gemeente_domain=self.gemeente_domain)
        PageMetadataFactory.create(
            gemeente_domain=self.gemeente_domain, title="some title"
        )

        response = self.app.get(f"{self.page_metadata_list_url}?page=3", user=self.user)

        self.assertEqual(response.status_code, 200)

        rows = response.html("tr")[1:]

        # Only the last 2 should show up
        self.assertEqual(len(rows), 2)

        pagination = response.html.find("ul", {"class": "pagination"})

        self.assertIsNotNone(pagination)

        # First row should be the first PageMetadata object
        self.assertEqual(rows[1].findAll("td")[1].text, "some title")

    def test_page_metadata_list_gem_default_enabled_true_page_none(self):
        grant_gemeente_perms(
            "admin",
            self.user,
            self.gemeente,
            ["page_metadata_read", "page_metadata_write"],
        )
        self.gemeente_domain.gem_default_enabled = True
        self.gemeente_domain.save()

        self.metadata1 = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain,
            title="Paspoort",
            path="/paspoort",
            gem_enabled=None,
        )
        PageMetadataFactory(
            gemeente_domain=GemeenteDomain.objects.get(gemeente__name="amsterdam"),
            title="ID kaart",
            path="/id-kaart",
        )

        response = self.app.get(self.page_metadata_list_url, user=self.user)

        self.assertEqual(response.status_code, 200)

        rows = response.html("tr")[1:]

        self.assertEqual(len(rows), 1)

        livechat_config = rows[0].find_all("td")[3].text
        self.assertEqual(livechat_config.strip(), "---")

        gem_enabled = rows[0].find_all("td")[4].find("img").attrs["alt"]
        self.assertEqual(gem_enabled, "True")

    def test_page_metadata_list_gem_default_enabled_false_page_none(self):
        grant_gemeente_perms(
            "admin",
            self.user,
            self.gemeente,
            ["page_metadata_read", "page_metadata_write"],
        )
        self.gemeente.gem_default_enabled = False
        self.gemeente.save()

        self.metadata1 = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain,
            title="Paspoort",
            path="/paspoort",
            gem_enabled=None,
        )
        PageMetadataFactory(
            gemeente_domain=GemeenteDomain.objects.get(gemeente__name="amsterdam"),
            title="ID kaart",
            path="/id-kaart",
        )

        response = self.app.get(self.page_metadata_list_url, user=self.user)

        self.assertEqual(response.status_code, 200)

        rows = response.html("tr")[1:]

        self.assertEqual(len(rows), 1)

        livechat_config = rows[0].find_all("td")[3].text
        self.assertEqual(livechat_config.strip(), "---")

        gem_enabled = rows[0].find_all("td")[4].find("img").attrs["alt"]
        self.assertEqual(gem_enabled, "False")

    def test_page_metadata_list_gem_default_enabled_true_page_false(self):
        grant_gemeente_perms(
            "admin",
            self.user,
            self.gemeente,
            ["page_metadata_read", "page_metadata_write"],
        )
        self.gemeente_domain.gem_default_enabled = True
        self.gemeente_domain.save()

        self.metadata1 = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain,
            title="Paspoort",
            path="/paspoort",
            gem_enabled=False,
        )
        PageMetadataFactory(
            gemeente_domain=GemeenteDomain.objects.get(gemeente__name="amsterdam"),
            title="ID kaart",
            path="/id-kaart",
        )

        response = self.app.get(self.page_metadata_list_url, user=self.user)

        self.assertEqual(response.status_code, 200)

        rows = response.html("tr")[1:]

        self.assertEqual(len(rows), 1)

        livechat_config = rows[0].find_all("td")[3].text
        self.assertEqual(livechat_config.strip(), "---")

        gem_enabled = rows[0].find_all("td")[4].find("img").attrs["alt"]
        self.assertEqual(gem_enabled, "False")

    def test_page_metadata_list_gem_default_enabled_false_page_true(self):
        grant_gemeente_perms(
            "admin",
            self.user,
            self.gemeente,
            ["page_metadata_read", "page_metadata_write"],
        )
        self.gemeente.gem_default_enabled = False
        self.gemeente.save()

        self.metadata1 = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain,
            title="Paspoort",
            path="/paspoort",
            gem_enabled=True,
        )
        PageMetadataFactory(
            gemeente_domain=GemeenteDomain.objects.get(gemeente__name="amsterdam"),
            title="ID kaart",
            path="/id-kaart",
        )

        response = self.app.get(self.page_metadata_list_url, user=self.user)

        self.assertEqual(response.status_code, 200)

        rows = response.html("tr")[1:]

        self.assertEqual(len(rows), 1)

        livechat_config = rows[0].find_all("td")[3].text
        self.assertEqual(livechat_config.strip(), "---")

        gem_enabled = rows[0].find_all("td")[4].find("img").attrs["alt"]
        self.assertEqual(gem_enabled, "True")

    def test_page_metadata_list_search(self):
        grant_gemeente_perms(
            "admin",
            self.user,
            self.gemeente,
            ["page_metadata_read", "page_metadata_write"],
        )

        PageMetadataFactory.create_batch(21, gemeente_domain=self.gemeente_domain)
        PageMetadataFactory.create(
            gemeente_domain=self.gemeente_domain, title="some title"
        )
        PageMetadataFactory.create(
            gemeente_domain=self.gemeente_domain, title="some title different"
        )

        response = self.app.get(
            f"{self.page_metadata_list_url}?search=some+title", user=self.user
        )

        search_button = response.html.find("button", {"id": "search-submit"})
        search_field = response.html.find("input", {"id": "search-field"})
        search_clear = response.html.find("button", {"id": "search-clear"})
        self.assertIsNotNone(search_button)
        self.assertIsNotNone(search_field)
        self.assertIsNotNone(search_clear)

        self.assertEqual(response.status_code, 200)

        rows = response.html("tr")[1:]

        # Only the search results should show up
        self.assertEqual(len(rows), 2)

        pagination = response.html.find("ul", {"class": "pagination"})

        self.assertIsNone(pagination)

        # First row should be the first PageMetadata object
        self.assertEqual(rows[0].findAll("td")[1].text, "some title")
        self.assertEqual(rows[1].findAll("td")[1].text, "some title different")

    def test_page_metadata_list_search_livechat_config_name(self):
        grant_gemeente_perms(
            "admin",
            self.user,
            self.gemeente,
            ["page_metadata_read", "page_metadata_write"],
        )

        config1 = LivechatConfigurationFactory.create(
            gemeente=self.gemeente,
            type=LivechatTypes.obi4wan,
            identifier="foo",
            avatar_url="http://example.com/image2",
            name="Livechat1",
        )
        config2 = LivechatConfigurationFactory.create(
            gemeente=self.gemeente,
            type=LivechatTypes.obi4wan,
            identifier="bar",
            avatar_url="http://example.com/image2",
            name="Livechat2",
        )

        PageMetadataFactory.create_batch(
            21, gemeente_domain=self.gemeente_domain, livechat_config=config1
        )
        PageMetadataFactory.create(
            gemeente_domain=self.gemeente_domain,
            title="some title",
            livechat_config=config1,
        )
        PageMetadataFactory.create(
            gemeente_domain=self.gemeente_domain,
            title="some title",
            livechat_config=None,
        )
        PageMetadataFactory.create(
            gemeente_domain=self.gemeente_domain,
            title="some title",
            livechat_config=config2,
        )
        PageMetadataFactory.create(
            gemeente_domain=self.gemeente_domain,
            title="some title different",
            livechat_config=config2,
        )

        response = self.app.get(
            f"{self.page_metadata_list_url}?search=Livechat2", user=self.user
        )

        search_button = response.html.find("button", {"id": "search-submit"})
        search_field = response.html.find("input", {"id": "search-field"})
        search_clear = response.html.find("button", {"id": "search-clear"})
        self.assertIsNotNone(search_button)
        self.assertIsNotNone(search_field)
        self.assertIsNotNone(search_clear)

        self.assertEqual(response.status_code, 200)

        rows = response.html("tr")[1:]

        # Only the search results should show up
        self.assertEqual(len(rows), 2)

        pagination = response.html.find("ul", {"class": "pagination"})

        self.assertIsNone(pagination)

        # First row should be the first PageMetadata object
        self.assertEqual(rows[0].findAll("td")[1].text, "some title")
        self.assertEqual(rows[1].findAll("td")[1].text, "some title different")

    def test_page_metadata_create(self):
        grant_gemeente_perms(
            "admin",
            self.user,
            self.gemeente,
            ["page_metadata_write", "page_metadata_read"],
        )

        response = self.app.get(
            reverse(
                "config:page_metadata_create",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                },
            ),
            user=self.user,
        )

        form = response.forms[1]

        form["title"] = "some title"
        form["full_url"] = "https://rotterdam.nl/some/path"
        form["gem_enabled"] = True
        form["direct_handover"] = True
        form["gemeente_domain"] = self.gemeente_domain.pk

        response = form.submit()
        self.assertEqual(response.status_code, 302)

        response = response.follow()
        self.assertEqual(PageMetadata.objects.count(), 1)

        metadata = PageMetadata.objects.get()
        self.assertEqual(metadata.title, "some title")
        self.assertEqual(metadata.path, "/some/path")
        self.assertEqual(metadata.full_url, "rotterdam.nl/some/path")
        self.assertEqual(metadata.gem_enabled, True)
        self.assertEqual(metadata.direct_handover, True)

    def test_page_metadata_read(self):
        grant_gemeente_perms(
            "admin",
            self.user,
            self.gemeente,
            ["page_metadata_read", "page_metadata_write"],
        )

        metadata = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain,
            title="Paspoort",
            path="/paspoort",
            gem_enabled=True,
        )
        response = self.app.get(
            reverse(
                "config:page_metadata_read",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "page_metadata_pk": metadata.pk,
                },
            ),
            user=self.user,
        )

        self.assertEqual(response.status_code, 200)

        header = response.html.find("div", {"class": "card-header"})
        header_buttons = header.find_all("a")

        self.assertEqual(len(header_buttons), 2)

        delete_button, update_button = header_buttons

        self.assertEqual(
            delete_button.attrs["href"],
            reverse(
                "config:page_metadata_delete",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "page_metadata_pk": metadata.pk,
                },
            ),
        )
        self.assertEqual(
            update_button.attrs["href"],
            reverse(
                "config:page_metadata_update",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "page_metadata_pk": metadata.pk,
                },
            ),
        )

    def test_page_metadata_read_gem_default_enabled(self):
        grant_gemeente_perms(
            "admin",
            self.user,
            self.gemeente,
            ["page_metadata_read", "page_metadata_write"],
        )

        self.gemeente_domain.gem_default_enabled = True
        self.gemeente_domain.save()

        metadata = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain,
            title="Paspoort",
            path="/paspoort",
            gem_enabled=None,
        )
        response = self.app.get(
            reverse(
                "config:page_metadata_read",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "page_metadata_pk": metadata.pk,
                },
            ),
            user=self.user,
        )

        self.assertEqual(response.status_code, 200)

        gem_enabled = response.html.find("table").find("img")
        self.assertEqual(gem_enabled.attrs["src"], "/static/admin/img/icon-yes.svg")
        self.assertEqual(gem_enabled.attrs["alt"], "True")

    def test_page_metadata_update(self):
        grant_gemeente_perms(
            "admin",
            self.user,
            self.gemeente,
            ["page_metadata_write", "page_metadata_read"],
        )

        config1 = LivechatConfigurationFactory.create(
            gemeente=self.gemeente,
            type=LivechatTypes.obi4wan,
            identifier="foo",
            avatar_url="http://example.com/image2",
            name="Livechat1",
            order=0,
        )
        config2 = LivechatConfigurationFactory.create(
            gemeente=self.gemeente,
            type=LivechatTypes.obi4wan,
            identifier="bar",
            avatar_url="http://example.com/image2",
            name="Livechat2",
            order=1,
        )

        metadata = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain,
            title="Paspoort",
            path="/paspoort",
            gem_enabled=True,
            direct_handover=True,
        )
        response = self.app.get(
            reverse(
                "config:page_metadata_update",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "page_metadata_pk": metadata.pk,
                },
            ),
            user=self.user,
        )

        self.assertEqual(response.status_code, 200)

        form = response.forms[1]

        livechat_options = [
            ("", True, "Standaard koppeling: Livechat1 (obi4wan - foo)"),
            (str(config1.pk), False, "Livechat1 (obi4wan - foo)"),
            (str(config2.pk), False, "Livechat2 (obi4wan - bar)"),
        ]
        self.assertEqual(form["livechat_config"].options, livechat_options)

        form["gem_enabled"] = False
        form["direct_handover"] = False
        form["livechat_config"] = str(config2.pk)

        response = form.submit()
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            response.location,
            f"{self.page_metadata_list_url}?domain={self.gemeente_domain.domain}",
        )

        response = response.follow()
        self.assertEqual(PageMetadata.objects.count(), 1)

        metadata = PageMetadata.objects.get()
        self.assertEqual(metadata.title, "Paspoort")
        self.assertEqual(metadata.path, "/paspoort")
        self.assertEqual(metadata.full_url, "rotterdam.nl/paspoort")
        self.assertEqual(metadata.gem_enabled, False)
        self.assertEqual(metadata.direct_handover, False)
        self.assertEqual(metadata.livechat_config, config2)

    def test_page_metadata_update_gem_enabled_default_label_false(self):
        grant_gemeente_perms(
            "admin",
            self.user,
            self.gemeente,
            ["page_metadata_write", "page_metadata_read"],
        )

        metadata = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain,
            title="Paspoort",
            path="/paspoort",
            gem_enabled=None,
        )
        response = self.app.get(
            reverse(
                "config:page_metadata_update",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "page_metadata_pk": metadata.pk,
                },
            ),
            user=self.user,
        )

        self.assertEqual(response.status_code, 200)

        form = response.forms[1]

        self.assertEqual(
            form["gem_enabled"].options,
            [
                ("", True, "Standaardwaarde: nee"),
                ("True", False, "Ja"),
                ("False", False, "Nee"),
            ],
        )

        form["gem_enabled"] = ""

        response = form.submit()
        self.assertEqual(response.status_code, 302)

        response = response.follow()
        self.assertEqual(PageMetadata.objects.count(), 1)

        metadata = PageMetadata.objects.get()
        self.assertEqual(metadata.title, "Paspoort")
        self.assertEqual(metadata.path, "/paspoort")
        self.assertEqual(metadata.full_url, "rotterdam.nl/paspoort")
        self.assertEqual(metadata.gem_enabled, None)

    def test_page_metadata_update_page_url_from_multiple_domains(self):
        grant_gemeente_perms(
            "admin",
            self.user,
            self.gemeente,
            ["page_metadata_write", "page_metadata_read"],
        )

        self.gemeente.domains = ["test.rotterdam.nl", "rotterdam.nl"]

        metadata = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain,
            title="Paspoort",
            path="/paspoort",
            gem_enabled=None,
        )
        with requests_mock.Mocker() as m:
            m.head("https://rotterdam.nl/paspoort", status_code=301)
            response = self.app.get(
                reverse(
                    "config:page_metadata_update",
                    kwargs={
                        "gemeente_slug": self.gemeente.slug,
                        "page_metadata_pk": metadata.pk,
                    },
                ),
                user=self.user,
            )

        self.assertEqual(response.status_code, 200)

        form = response.forms[1]
        self.assertEqual(form["full_url"].value, "https://rotterdam.nl/paspoort")

    def test_page_metadata_update_gem_enabled_default_label_true(self):
        grant_gemeente_perms(
            "admin",
            self.user,
            self.gemeente,
            ["page_metadata_write", "page_metadata_read"],
        )

        self.gemeente_domain.gem_default_enabled = True
        self.gemeente_domain.save()

        metadata = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain,
            title="Paspoort",
            path="/paspoort",
            gem_enabled=None,
        )
        response = self.app.get(
            reverse(
                "config:page_metadata_update",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "page_metadata_pk": metadata.pk,
                },
            ),
            user=self.user,
        )

        self.assertEqual(response.status_code, 200)

        form = response.forms[1]

        self.assertEqual(
            form["gem_enabled"].options,
            [
                ("", True, "Standaardwaarde: ja"),
                ("True", False, "Ja"),
                ("False", False, "Nee"),
            ],
        )

        form["gem_enabled"] = ""

        response = form.submit()
        self.assertEqual(response.status_code, 302)

        response = response.follow()
        self.assertEqual(PageMetadata.objects.count(), 1)

        metadata = PageMetadata.objects.get()
        self.assertEqual(metadata.title, "Paspoort")
        self.assertEqual(metadata.path, "/paspoort")
        self.assertEqual(metadata.full_url, "rotterdam.nl/paspoort")
        self.assertEqual(metadata.gem_enabled, None)

    def test_page_metadata_delete(self):
        grant_gemeente_perms("admin", self.user, self.gemeente, ["page_metadata_write"])

        metadata = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain,
            title="Paspoort",
            path="/paspoort",
            gem_enabled=True,
        )
        response = self.app.get(
            reverse(
                "config:page_metadata_delete",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "page_metadata_pk": metadata.pk,
                },
            ),
            user=self.user,
        )

        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            response.location,
            f"{self.page_metadata_list_url}?domain={self.gemeente_domain.domain}",
        )

        self.assertEqual(PageMetadata.objects.count(), 0)


@tag("permissions")
class PageMetadataPermissionTests(TwoFactorTestCaseMixin, WebTest):
    login_url = reverse("two_factor:login")
    user_is_admin = False
    user_is_superuser = False

    def setUp(self):
        super().setUp()

        gemeente = GemeenteFactory(name="amsterdam")
        GemeenteDomainFactory(gemeente=gemeente, domain="amsterdam.nl")
        self.user.gemeentes.add(gemeente)
        self.user.save()

        self.gemeente = GemeenteFactory(name="rotterdam")
        self.gemeente_domain = GemeenteDomainFactory(
            gemeente=self.gemeente, domain="rotterdam.nl"
        )
        self.page_metadata_list_url = reverse(
            "config:page_metadata_list", kwargs={"gemeente_slug": self.gemeente.slug}
        )

    def test_page_metadata_list_no_permission(self):
        # Should not have any influence
        grant_gemeente_perms(
            "admin", self.user, self.gemeente, ["utter_read", "page_metadata_write"]
        )

        self.metadata1 = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain,
            title="Paspoort",
            path="/paspoort",
            gem_enabled=True,
        )
        self.metadata2 = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain,
            title="ID kaart",
            path="/id-kaart",
        )

        response = self.app.get(self.page_metadata_list_url, user=self.user, status=403)

        self.assertEqual(response.status_code, 403)

    def test_page_metadata_list_no_permission_for_current_gemeente(self):
        # Should not have any influence
        grant_gemeente_perms(
            "admin",
            self.user,
            Gemeente.objects.get(name="amsterdam"),
            ["page_metadata_read"],
        )

        self.metadata1 = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain,
            title="Paspoort",
            path="/paspoort",
            gem_enabled=True,
        )
        self.metadata2 = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain,
            title="ID kaart",
            path="/id-kaart",
        )

        response = self.app.get(self.page_metadata_list_url, user=self.user, status=403)

        self.assertEqual(response.status_code, 403)

    def test_page_metadata_list_has_permission_read_and_write(self):
        # Should not have any influence
        grant_gemeente_perms(
            "admin",
            self.user,
            self.gemeente,
            ["page_metadata_read", "page_metadata_write"],
        )

        self.metadata1 = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain,
            title="Paspoort",
            path="/paspoort",
            gem_enabled=True,
        )
        self.metadata2 = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain,
            title="ID kaart",
            path="/id-kaart",
        )

        response = self.app.get(self.page_metadata_list_url, user=self.user)

        self.assertEqual(response.status_code, 200)

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 2)

        for i, metadata in enumerate([self.metadata2, self.metadata1]):
            with self.subTest(metadata=metadata):
                detail_link = rows[i].find_all("td")[0].find("a")
                self.assertEqual(
                    detail_link.attrs["href"],
                    reverse(
                        "config:page_metadata_read",
                        kwargs={
                            "gemeente_slug": self.gemeente.slug,
                            "page_metadata_pk": metadata.pk,
                        },
                    ),
                )

                page_title = rows[i].find_all("td")[1]
                self.assertEqual(page_title.text, metadata.title)

                page_link = rows[i].find_all("td")[2].find("a")
                self.assertEqual(
                    page_link.attrs["href"],
                    f"https://{self.gemeente_domain.domain}{metadata.path}",
                )
                self.assertEqual(
                    page_link.text,
                    f"https://{self.gemeente_domain.domain}{metadata.path}",
                )

                livechat_config = rows[i].find_all("td")[3].text
                self.assertEqual(livechat_config.strip(), "---")

                gem_enabled = rows[i].find_all("td")[4].find("img").attrs["alt"]
                self.assertEqual(gem_enabled, str(metadata.gem_enabled_with_default))

                # Only the two links shown above should appear
                # Update/delete buttons should not appear
                buttons = rows[i].find_all("a")
                self.assertEqual(len(buttons), 4)

                delete_button, update_button = buttons[2:4]

                self.assertEqual(
                    delete_button.attrs["href"],
                    reverse(
                        "config:page_metadata_delete",
                        kwargs={
                            "gemeente_slug": self.gemeente.slug,
                            "page_metadata_pk": metadata.pk,
                        },
                    ),
                )
                self.assertEqual(
                    update_button.attrs["href"],
                    reverse(
                        "config:page_metadata_update",
                        kwargs={
                            "gemeente_slug": self.gemeente.slug,
                            "page_metadata_pk": metadata.pk,
                        },
                    ),
                )

    def test_page_metadata_list_has_permission_read_only(self):
        grant_gemeente_perms("admin", self.user, self.gemeente, ["page_metadata_read"])

        self.metadata1 = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain,
            title="Paspoort",
            path="/paspoort",
            gem_enabled=True,
        )
        self.metadata2 = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain,
            title="ID kaart",
            path="/id-kaart",
        )
        PageMetadataFactory(
            gemeente_domain=GemeenteDomain.objects.get(gemeente__name="amsterdam"),
            title="ID kaart",
            path="/id-kaart",
        )

        response = self.app.get(self.page_metadata_list_url, user=self.user)

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 2)

        for i, metadata in enumerate([self.metadata2, self.metadata1]):
            with self.subTest(metadata=metadata):
                detail_link = rows[i].find_all("td")[0].find("a")
                self.assertEqual(
                    detail_link.attrs["href"],
                    reverse(
                        "config:page_metadata_read",
                        kwargs={
                            "gemeente_slug": self.gemeente.slug,
                            "page_metadata_pk": metadata.pk,
                        },
                    ),
                )
                page_title = rows[i].find_all("td")[1]
                self.assertEqual(page_title.text, metadata.title)

                page_link = rows[i].find_all("td")[2].find("a")
                self.assertEqual(
                    page_link.attrs["href"],
                    f"https://{self.gemeente_domain.domain}{metadata.path}",
                )
                self.assertEqual(
                    page_link.text,
                    f"https://{self.gemeente_domain.domain}{metadata.path}",
                )

                livechat_config = rows[i].find_all("td")[3].text
                self.assertEqual(livechat_config.strip(), "---")

                gem_enabled = rows[i].find_all("td")[4].find("img").attrs["alt"]
                self.assertEqual(gem_enabled, str(metadata.gem_enabled_with_default))

                # Only the two links shown above should appear
                # Update/delete buttons should not appear
                buttons = rows[i].find_all("a")
                self.assertEqual(len(buttons), 2)

    def test_page_metadata_create_no_permission(self):
        # Should not have any influence
        grant_gemeente_perms(
            "admin", self.user, self.gemeente, ["utter_write", "page_metadata_read"]
        )

        response = self.app.get(
            reverse(
                "config:page_metadata_create",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                },
            ),
            user=self.user,
            status=403,
        )

        self.assertEqual(response.status_code, 403)

    def test_page_metadata_create_has_permission(self):
        # Should not have any influence
        grant_gemeente_perms("admin", self.user, self.gemeente, ["page_metadata_write"])

        response = self.app.get(
            reverse(
                "config:page_metadata_create",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                },
            ),
            user=self.user,
        )

        self.assertEqual(response.status_code, 200)

    def test_page_metadata_update_no_permission(self):
        # Should not have any influence
        grant_gemeente_perms(
            "admin", self.user, self.gemeente, ["utter_write", "page_metadata_read"]
        )

        metadata = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain,
            title="Paspoort",
            path="/paspoort",
            gem_enabled=True,
        )
        response = self.app.get(
            reverse(
                "config:page_metadata_update",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "page_metadata_pk": metadata.pk,
                },
            ),
            user=self.user,
            status=403,
        )

        self.assertEqual(response.status_code, 403)

    def test_page_metadata_update_has_permission(self):
        # Should not have any influence
        grant_gemeente_perms("admin", self.user, self.gemeente, ["page_metadata_write"])

        metadata = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain,
            title="Paspoort",
            path="/paspoort",
            gem_enabled=True,
        )
        response = self.app.get(
            reverse(
                "config:page_metadata_update",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "page_metadata_pk": metadata.pk,
                },
            ),
            user=self.user,
        )

        self.assertEqual(response.status_code, 200)

    def test_page_metadata_delete_no_permission(self):
        # Should not have any influence
        grant_gemeente_perms(
            "admin", self.user, self.gemeente, ["utter_write", "page_metadata_read"]
        )

        metadata = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain,
            title="Paspoort",
            path="/paspoort",
            gem_enabled=True,
        )
        response = self.app.get(
            reverse(
                "config:page_metadata_delete",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "page_metadata_pk": metadata.pk,
                },
            ),
            user=self.user,
            status=403,
        )

        self.assertEqual(response.status_code, 403)

    def test_page_metadata_delete_has_permission(self):
        # Should not have any influence
        grant_gemeente_perms("admin", self.user, self.gemeente, ["page_metadata_write"])

        metadata = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain,
            title="Paspoort",
            path="/paspoort",
            gem_enabled=True,
        )
        response = self.app.get(
            reverse(
                "config:page_metadata_delete",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "page_metadata_pk": metadata.pk,
                },
            ),
            user=self.user,
        )

        self.assertEqual(response.status_code, 302)

        self.assertEqual(PageMetadata.objects.count(), 0)

    def test_page_metadata_read_no_permission(self):
        grant_gemeente_perms(
            "admin", self.user, self.gemeente, ["utter_read", "page_metadata_write"]
        )

        metadata = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain,
            title="Paspoort",
            path="/paspoort",
            gem_enabled=True,
        )
        response = self.app.get(
            reverse(
                "config:page_metadata_read",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "page_metadata_pk": metadata.pk,
                },
            ),
            user=self.user,
            status=403,
        )

        self.assertEqual(response.status_code, 403)

    def test_page_metadata_read_has_permission_read_and_write(self):
        grant_gemeente_perms(
            "admin",
            self.user,
            self.gemeente,
            ["page_metadata_read", "page_metadata_write"],
        )

        metadata = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain,
            title="Paspoort",
            path="/paspoort",
            gem_enabled=True,
        )
        response = self.app.get(
            reverse(
                "config:page_metadata_read",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "page_metadata_pk": metadata.pk,
                },
            ),
            user=self.user,
        )

        self.assertEqual(response.status_code, 200)

        header = response.html.find("div", {"class": "card-header"})
        header_buttons = header.find_all("a")

        self.assertEqual(len(header_buttons), 2)

        delete_button, update_button = header_buttons

        self.assertEqual(
            delete_button.attrs["href"],
            reverse(
                "config:page_metadata_delete",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "page_metadata_pk": metadata.pk,
                },
            ),
        )
        self.assertEqual(
            update_button.attrs["href"],
            reverse(
                "config:page_metadata_update",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "page_metadata_pk": metadata.pk,
                },
            ),
        )

    def test_page_metadata_read_has_permission_read_only(self):
        grant_gemeente_perms("admin", self.user, self.gemeente, ["page_metadata_read"])

        metadata = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain,
            title="Paspoort",
            path="/paspoort",
            gem_enabled=True,
        )
        response = self.app.get(
            reverse(
                "config:page_metadata_read",
                kwargs={
                    "gemeente_slug": self.gemeente.slug,
                    "page_metadata_pk": metadata.pk,
                },
            ),
            user=self.user,
        )

        self.assertEqual(response.status_code, 200)

        # Delete and update buttons should not appear
        header = response.html.find("div", {"class": "card-header"})

        header_buttons = header.find_all("a")

        self.assertEqual(len(header_buttons), 0)


class PageMetadataGlobalSettingsTests(TwoFactorTestCaseMixin, WebTest):
    login_url = reverse("two_factor:login")
    user_is_admin = False

    def setUp(self):
        super().setUp()

        gemeente = GemeenteFactory(name="amsterdam")
        GemeenteDomainFactory(gemeente=gemeente, domain="amsterdam.nl")
        self.user.gemeentes.add(gemeente)
        self.user.save()

        self.gemeente = GemeenteFactory(name="rotterdam")
        self.gemeente_domain = GemeenteDomainFactory(
            gemeente=self.gemeente, domain="rotterdam.nl"
        )
        self.page_metadata_list_url = reverse(
            "config:page_metadata_list", kwargs={"gemeente_slug": self.gemeente.slug}
        )

    def test_page_metadata_list_global_gem_settings_update(self):
        grant_gemeente_perms(
            "admin",
            self.user,
            self.gemeente,
            ["page_metadata_read", "page_metadata_write"],
        )

        response = self.app.get(self.page_metadata_list_url, user=self.user)

        self.assertEqual(response.status_code, 200)

        form = response.forms[1]

        form["main_widget_color"] = "#FF0000"
        form["custom_widget_css"] = "#webchat .rw-title { color: #FF0000; }"
        # form["gem_default_enabled"] = True

        form["widget_title"] = "title"
        form["widget_subtitle"] = "subtitle"
        form["popup_message"] = "message"

        response = form.submit(
            upload_files=[
                ("widget_avatar", "src/antwoorden_cms/config/tests/files/avatar.png")
            ]
        )

        self.assertEqual(response.status_code, 302)

        self.gemeente.refresh_from_db()

        self.assertEqual(self.gemeente.main_widget_color, "#FF0000")
        self.assertEqual(
            self.gemeente.custom_widget_css, "#webchat .rw-title { color: #FF0000; }"
        )
        # self.assertEqual(self.gemeente.gem_default_enabled, True)

        # self.assertEqual(self.gemeente.direct_handover, False)
        self.assertEqual(self.gemeente.widget_title, "title")
        self.assertEqual(self.gemeente.widget_subtitle, "subtitle")
        self.assertEqual(self.gemeente.popup_message, "message")
        self.assertIsNotNone(self.gemeente.widget_avatar)

    def test_page_metadata_list_global_gem_settings_update_failure(self):
        grant_gemeente_perms(
            "admin",
            self.user,
            self.gemeente,
            ["page_metadata_read", "page_metadata_write"],
        )

        response = self.app.get(self.page_metadata_list_url, user=self.user)

        self.assertEqual(response.status_code, 200)

        form = response.forms[1]

        form["main_widget_color"] = "#FF0000"
        form["custom_widget_css"] = "#webchat .rw-title { color: #FF0000; }"
        # form["gem_default_enabled"] = True

        form["widget_title"] = "title"
        form["widget_subtitle"] = "subtitle"
        form["popup_message"] = "message"
        form["widget_avatar"] = (
            "bla",
            b"bla",
        )

        response = form.submit()

        self.assertEqual(response.status_code, 200)

        self.gemeente.refresh_from_db()

        self.assertEqual(self.gemeente.main_widget_color, "")
        self.assertEqual(self.gemeente.custom_widget_css, "")
        # self.assertEqual(self.gemeente.gem_default_enabled, False)

        # self.assertEqual(self.gemeente.direct_handover, False)
        self.assertEqual(self.gemeente.widget_title, "Gem")
        self.assertEqual(
            self.gemeente.widget_subtitle, "Je digitale hulp van de gemeente"
        )
        self.assertEqual(self.gemeente.popup_message, "Stel je vraag")
        self.assertEqual(self.gemeente.widget_avatar.name, "")

    def test_page_metadata_list_global_gem_settings_read(self):
        grant_gemeente_perms(
            "admin",
            self.user,
            self.gemeente,
            ["page_metadata_read", "page_metadata_write"],
        )

        self.gemeente.main_widget_color = "#00FF00"
        self.gemeente.custom_widget_css = "#webchat .rw-title { color: #00FF00; }"
        # self.gemeente.gem_default_enabled = True

        # self.gemeente.direct_handover = True
        self.gemeente.widget_title = "title"
        self.gemeente.widget_subtitle = "subtitle"
        self.gemeente.popup_message = "message"

        image = tempfile.NamedTemporaryFile(suffix=".png")
        self.gemeente.widget_avatar = image.name

        self.gemeente.save()

        response = self.app.get(self.page_metadata_list_url, user=self.user)

        self.assertEqual(response.status_code, 200)

        form = response.forms[1]

        self.assertEqual(form["main_widget_color"].value, "#00FF00")
        self.assertEqual(
            form["custom_widget_css"].value, "#webchat .rw-title { color: #00FF00; }"
        )
        # self.assertEqual(form["gem_default_enabled"].checked, True)

        # self.assertEqual(self.gemeente.direct_handover, True)
        self.assertEqual(self.gemeente.widget_title, "title")
        self.assertEqual(self.gemeente.widget_subtitle, "subtitle")
        self.assertEqual(self.gemeente.popup_message, "message")
        self.assertEqual(self.gemeente.widget_avatar.name, image.name)

    def test_page_metadata_list_global_gem_settings_update_other_fields_not_possible(
        self,
    ):
        grant_gemeente_perms(
            "admin",
            self.user,
            self.gemeente,
            ["page_metadata_read", "page_metadata_write"],
        )

        response = self.app.get(self.page_metadata_list_url, user=self.user)

        self.assertEqual(response.status_code, 200)

        form = response.forms[1]

        form["main_widget_color"] = "#FF0000"
        form["custom_widget_css"] = "#webchat .rw-title { color: #FF0000; }"
        # form["gem_default_enabled"] = True

        for field in ["name", "slug", "demo", "active", "municipality_logo"]:
            with self.subTest(field=field):
                self.assertNotIn(field, form.fields)


class PageMetadataDomainSettingsTests(TwoFactorTestCaseMixin, WebTest):
    login_url = reverse("two_factor:login")
    user_is_admin = False

    def setUp(self):
        super().setUp()

        self.gemeente = GemeenteFactory(name="rotterdam")
        self.gemeente_domain = GemeenteDomainFactory(
            gemeente=self.gemeente, domain="rotterdam.nl"
        )
        self.page_metadata_list_url = reverse(
            "config:page_metadata_list", kwargs={"gemeente_slug": self.gemeente.slug}
        )

    def test_page_metadata_list_global_gem_settings_update(self):
        grant_gemeente_perms(
            "admin",
            self.user,
            self.gemeente,
            ["page_metadata_read", "page_metadata_write"],
        )

        self.gemeente_domain.gem_default_enabled = True
        self.gemeente_domain.direct_handover = False
        self.gemeente_domain.save()

        response = self.app.get(self.page_metadata_list_url, user=self.user)
        self.assertEqual(response.status_code, 200)

        form = response.forms["gemeente_domain_formset"]
        form["gemeentedomain_set-0-direct_handover"] = True
        form["gemeentedomain_set-0-gem_default_enabled"] = False

        response = form.submit("_save_domains")
        self.assertEqual(response.status_code, 302)

        self.gemeente_domain.refresh_from_db()
        self.assertFalse(self.gemeente_domain.gem_default_enabled)
        self.assertTrue(self.gemeente_domain.direct_handover)

    def test_page_metadata_list_global_gem_settings_update_multiple(self):
        grant_gemeente_perms(
            "admin",
            self.user,
            self.gemeente,
            ["page_metadata_read", "page_metadata_write"],
        )

        gemeente_domain2 = GemeenteDomainFactory(
            gemeente=self.gemeente,
            domain="test.rotterdam.nl",
            gem_default_enabled=False,
            direct_handover=True,
        )

        self.gemeente_domain.gem_default_enabled = True
        self.gemeente_domain.direct_handover = False
        self.gemeente_domain.save()

        response = self.app.get(self.page_metadata_list_url, user=self.user)
        self.assertEqual(response.status_code, 200)

        form = response.forms["gemeente_domain_formset"]
        form["gemeentedomain_set-0-direct_handover"] = True
        form["gemeentedomain_set-0-gem_default_enabled"] = False
        form["gemeentedomain_set-1-direct_handover"] = False
        form["gemeentedomain_set-1-gem_default_enabled"] = True
        response = form.submit("_save_domains")
        self.assertEqual(response.status_code, 302)

        self.gemeente_domain.refresh_from_db()
        self.assertFalse(self.gemeente_domain.gem_default_enabled)
        self.assertTrue(self.gemeente_domain.direct_handover)

        gemeente_domain2.refresh_from_db()
        self.assertTrue(gemeente_domain2.gem_default_enabled)
        self.assertFalse(gemeente_domain2.direct_handover)


class PageMetadataListDomainSwitchTests(TwoFactorTestCaseMixin, WebTest):
    login_url = reverse("two_factor:login")
    user_is_admin = False

    def setUp(self):
        super().setUp()

        gemeente = GemeenteFactory(name="amsterdam")
        GemeenteDomainFactory(gemeente=gemeente, domain="amsterdam.nl")
        self.user.gemeentes.add(gemeente)
        self.user.save()

        self.gemeente = GemeenteFactory(name="rotterdam")
        self.gemeente_domain1 = GemeenteDomainFactory(
            gemeente=self.gemeente, domain="rotterdam.nl"
        )
        self.gemeente_domain2 = GemeenteDomainFactory(
            gemeente=self.gemeente, domain="test.rotterdam.nl"
        )
        self.page_metadata_list_url = reverse(
            "config:page_metadata_list", kwargs={"gemeente_slug": self.gemeente.slug}
        )

        self.metadata1 = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain1,
            title="Paspoort",
            path="/paspoort",
            gem_enabled=True,
        )
        self.metadata2 = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain1,
            title="ID kaart",
            path="/id-kaart",
            gem_enabled=True,
        )
        self.metadata3 = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain2,
            title="Paspoorttest",
            path="/paspoort",
            gem_enabled=False,
        )
        self.metadata4 = PageMetadataFactory(
            gemeente_domain=self.gemeente_domain2,
            title="ID kaarttest",
            path="/id-kaarttest",
            gem_enabled=False,
        )
        PageMetadataFactory(
            gemeente_domain=GemeenteDomain.objects.get(gemeente__name="amsterdam"),
            title="ID kaart",
            path="/id-kaart",
        )

        grant_gemeente_perms(
            "admin",
            self.user,
            self.gemeente,
            ["page_metadata_read", "page_metadata_write"],
        )

    def test_page_metadata_list_default_domain_filter(self):
        response = self.app.get(self.page_metadata_list_url, user=self.user)

        self.assertEqual(response.status_code, 200)

        domain_switcher = response.html.find("select", {"id": "domain-switch"})
        selected_option = domain_switcher.find("option", {"selected": True})
        self.assertEqual(selected_option.attrs["value"], "rotterdam.nl")
        self.assertEqual(selected_option.text, "rotterdam.nl")

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 2)

        for i, metadata in enumerate([self.metadata2, self.metadata1]):
            with self.subTest(metadata=metadata):
                detail_link = rows[i].find_all("td")[0].find("a")
                self.assertEqual(
                    detail_link.attrs["href"],
                    reverse(
                        "config:page_metadata_read",
                        kwargs={
                            "gemeente_slug": self.gemeente.slug,
                            "page_metadata_pk": metadata.pk,
                        },
                    ),
                )

                page_title = rows[i].find_all("td")[1]
                self.assertEqual(page_title.text, metadata.title)

                page_link = rows[i].find_all("td")[2].find("a")
                self.assertEqual(
                    page_link.attrs["href"],
                    f"https://{self.gemeente_domain1.domain}{metadata.path}",
                )
                self.assertEqual(
                    page_link.text,
                    f"https://{self.gemeente_domain1.domain}{metadata.path}",
                )

                livechat_config = rows[i].find_all("td")[3].text
                self.assertEqual(livechat_config.strip(), "---")

                gem_enabled = rows[i].find_all("td")[4].find("img").attrs["alt"]
                self.assertEqual(gem_enabled, "True")

    def test_page_metadata_list_default_domain_filter_alternate_ordering(self):
        # Switch ordering of domains
        self.gemeente_domain1.order = 2
        self.gemeente_domain2.order = 1
        self.gemeente_domain1.save()
        self.gemeente_domain2.save()
        response = self.app.get(self.page_metadata_list_url, user=self.user)

        self.assertEqual(response.status_code, 200)

        domain_switcher = response.html.find("select", {"id": "domain-switch"})
        selected_option = domain_switcher.find("option", {"selected": True})
        self.assertEqual(selected_option.attrs["value"], "test.rotterdam.nl")
        self.assertEqual(selected_option.text, "test.rotterdam.nl")

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 2)

        for i, metadata in enumerate([self.metadata4, self.metadata3]):
            with self.subTest(metadata=metadata):
                detail_link = rows[i].find_all("td")[0].find("a")
                self.assertEqual(
                    detail_link.attrs["href"],
                    reverse(
                        "config:page_metadata_read",
                        kwargs={
                            "gemeente_slug": self.gemeente.slug,
                            "page_metadata_pk": metadata.pk,
                        },
                    ),
                )

                page_title = rows[i].find_all("td")[1]
                self.assertEqual(page_title.text, metadata.title)

                page_link = rows[i].find_all("td")[2].find("a")
                self.assertEqual(
                    page_link.attrs["href"],
                    f"https://{self.gemeente_domain2.domain}{metadata.path}",
                )
                self.assertEqual(
                    page_link.text,
                    f"https://{self.gemeente_domain2.domain}{metadata.path}",
                )

                livechat_config = rows[i].find_all("td")[3].text
                self.assertEqual(livechat_config.strip(), "---")

                gem_enabled = rows[i].find_all("td")[4].find("img").attrs["alt"]
                self.assertEqual(gem_enabled, "False")

    def test_page_metadata_list_default_domain_filter_query_param(self):
        response = self.app.get(
            f"{self.page_metadata_list_url}?domain=test.rotterdam.nl", user=self.user
        )

        self.assertEqual(response.status_code, 200)

        domain_switcher = response.html.find("select", {"id": "domain-switch"})
        selected_option = domain_switcher.find("option", {"selected": True})
        self.assertEqual(selected_option.attrs["value"], "test.rotterdam.nl")
        self.assertEqual(selected_option.text, "test.rotterdam.nl")

        rows = response.html("tr")[1:]

        # Generic utterresponses should not show up
        self.assertEqual(len(rows), 2)

        for i, metadata in enumerate([self.metadata4, self.metadata3]):
            with self.subTest(metadata=metadata):
                detail_link = rows[i].find_all("td")[0].find("a")
                self.assertEqual(
                    detail_link.attrs["href"],
                    reverse(
                        "config:page_metadata_read",
                        kwargs={
                            "gemeente_slug": self.gemeente.slug,
                            "page_metadata_pk": metadata.pk,
                        },
                    ),
                )

                page_title = rows[i].find_all("td")[1]
                self.assertEqual(page_title.text, metadata.title)

                page_link = rows[i].find_all("td")[2].find("a")
                self.assertEqual(
                    page_link.attrs["href"],
                    f"https://{self.gemeente_domain2.domain}{metadata.path}",
                )
                self.assertEqual(
                    page_link.text,
                    f"https://{self.gemeente_domain2.domain}{metadata.path}",
                )

                livechat_config = rows[i].find_all("td")[3].text
                self.assertEqual(livechat_config.strip(), "---")

                gem_enabled = rows[i].find_all("td")[4].find("img").attrs["alt"]
                self.assertEqual(gem_enabled, "False")
