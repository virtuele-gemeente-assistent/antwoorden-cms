from django.urls import reverse

from django_webtest import WebTest

from antwoorden_cms.utters.tests.factories import GemeenteFactory
from antwoorden_cms.utters.tests.mixins import TwoFactorTestCaseMixin


class WidgetStylingTests(TwoFactorTestCaseMixin, WebTest):
    user_is_admin = False

    def setUp(self):
        super().setUp()

        self.gemeente = GemeenteFactory(name="amsterdam")
        self.user.gemeentes.add(self.gemeente)

    def test_widget_styling_endpoint(self):
        self.gemeente.main_widget_color = "#FF0000"
        self.gemeente.save()

        response = self.app.get(
            reverse(
                "config:widget_styling", kwargs={"gemeente_slug": self.gemeente.slug}
            )
        )
        self.assertEqual(response.status_code, 200)

        self.assertIn("#FF0000", response.text)

    def test_widget_styling_endpoint_default(self):
        response = self.app.get(
            reverse(
                "config:widget_styling", kwargs={"gemeente_slug": self.gemeente.slug}
            )
        )
        self.assertEqual(response.status_code, 200)

        self.assertIn("#525252", response.text)

    def test_widget_styling_endpoint_custom_css(self):
        self.gemeente.main_widget_color = "#FF0000"
        self.gemeente.custom_widget_css = (
            """#webchat .rw-conversation-container {border-radius: 10px;}"""
        )
        self.gemeente.save()

        response = self.app.get(
            reverse(
                "config:widget_styling", kwargs={"gemeente_slug": self.gemeente.slug}
            )
        )
        self.assertEqual(response.status_code, 200)

        expected = "#webchat .rw-launcher, #webchat .rw-header, #webchat .rw-close-button, #webchat .rw-client, #webchat .rw-avatar {\n    background-color: #FF0000 !important;\n}\n\n#webchat .rw-conversation-container {border-radius: 10px;}\n"
        self.assertEqual(response.text, expected)

    def test_widget_styling_endpoint_custom_css_safe(self):
        self.gemeente.main_widget_color = "#FF0000"
        self.gemeente.custom_widget_css = """#webchat .rw-conversation-container {background-image: url('some.url');}"""
        self.gemeente.save()

        response = self.app.get(
            reverse(
                "config:widget_styling", kwargs={"gemeente_slug": self.gemeente.slug}
            )
        )
        self.assertEqual(response.status_code, 200)

        expected = "#webchat .rw-launcher, #webchat .rw-header, #webchat .rw-close-button, #webchat .rw-client, #webchat .rw-avatar {\n    background-color: #FF0000 !important;\n}\n\n#webchat .rw-conversation-container {background-image: url('some.url');}\n"
        self.assertEqual(response.text, expected)
