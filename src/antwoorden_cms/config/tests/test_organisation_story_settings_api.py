import json
from unittest.mock import patch

from django.contrib.auth.models import Permission
from django.urls import reverse

from rest_framework.test import APITestCase

from antwoorden_cms.utters.models import Gemeente, SiteConfiguration
from antwoorden_cms.utters.tests.factories import (
    GemeenteFactory,
    GroupFactory,
    UserFactory,
)

from ..models import OrganisationStorySettings
from .factories import OrganisationStorySettingsFactory


class StorySettingsAPITests(APITestCase):
    @classmethod
    def setUpTestData(cls) -> None:
        super().setUpTestData()

        cls.gemeente = GemeenteFactory.create(name="Dongen")
        cls.gemeente2 = GemeenteFactory.create(name="Utrecht")

        cls.user = UserFactory.create(username="foo", password="bar")
        group = GroupFactory.create(name="admin")
        gemeente_perms = Permission.objects.filter(
            codename__in=[p[0] for p in Gemeente._meta.permissions]
        )
        group.permissions.set(gemeente_perms)
        cls.user.groups.add(group)
        cls.user.gemeentes.add(cls.gemeente)

    def setUp(self):
        super().setUp()

        self.client.force_authenticate(self.user)

    def test_create_settings(self):
        response = self.client.post(
            reverse(
                "antwoorden_cms:story_settings",
                kwargs={"organisation": "dongen", "story_id": "story1"},
            ),
            {"enabled": False, "handover_to_organisation": "Tilburg"},
        )

        self.assertEqual(response.status_code, 201)
        self.assertDictEqual(
            response.data,
            {
                "story_id": "story1",
                "enabled": False,
                "handover_to_organisation": "Tilburg",
                "crisis_answer": False,
                "handover_livechat": False,
                "parking_answer": False,
            },
        )

        settings = OrganisationStorySettings.objects.get()
        self.assertEqual(settings.organisation, self.gemeente)
        self.assertEqual(settings.story_id, "story1")
        self.assertEqual(settings.enabled, False)

    def test_create_settings_no_handover(self):
        response = self.client.post(
            reverse(
                "antwoorden_cms:story_settings",
                kwargs={"organisation": "dongen", "story_id": "story1"},
            ),
            {"enabled": False, "handover_to_organisation": ""},
        )

        self.assertEqual(response.status_code, 201)
        self.assertDictEqual(
            response.data,
            {
                "story_id": "story1",
                "enabled": False,
                "handover_to_organisation": "",
                "crisis_answer": False,
                "handover_livechat": False,
                "parking_answer": False,
            },
        )

        settings = OrganisationStorySettings.objects.get()
        self.assertEqual(settings.organisation, self.gemeente)
        self.assertEqual(settings.story_id, "story1")
        self.assertEqual(settings.enabled, False)

    def test_create_settings_duplicate(self):
        OrganisationStorySettingsFactory.create(
            organisation=self.gemeente, story_id="story1", enabled=False
        )

        response = self.client.post(
            reverse(
                "antwoorden_cms:story_settings",
                kwargs={"organisation": "dongen", "story_id": "story1"},
            ),
            {"enabled": False},
        )

        self.assertEqual(response.status_code, 400)

        settings = OrganisationStorySettings.objects.get()
        self.assertEqual(settings.organisation, self.gemeente)
        self.assertEqual(settings.story_id, "story1")
        self.assertEqual(settings.enabled, False)

    def test_create_settings_non_existing_organisation(self):
        response = self.client.post(
            reverse(
                "antwoorden_cms:story_settings",
                kwargs={"organisation": "foo", "story_id": "story1"},
            ),
            {"enabled": False},
        )

        self.assertEqual(response.status_code, 404)
        self.assertFalse(OrganisationStorySettings.objects.exists())

    def test_bulk_update_and_create_settings(self):
        OrganisationStorySettingsFactory.create(
            organisation=self.gemeente, story_id="story1", enabled=False
        )
        OrganisationStorySettingsFactory.create(
            organisation=self.gemeente2, story_id="story2", enabled=True
        )

        response = self.client.post(
            reverse(
                "antwoorden_cms:story_settings_bulk_endpoint",
                kwargs={"organisation": "dongen"},
            ),
            data=json.dumps(
                [
                    {"story_id": "story1", "enabled": True},
                    {"story_id": "story2", "enabled": False},
                ]
            ),
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 204)
        self.assertEqual(OrganisationStorySettings.objects.count(), 3)

        setting1, setting2 = OrganisationStorySettings.objects.filter(
            organisation=self.gemeente
        ).order_by("story_id")

        self.assertEqual(setting1.story_id, "story1")
        self.assertEqual(setting1.enabled, True)
        self.assertEqual(setting2.story_id, "story2")
        self.assertEqual(setting2.enabled, False)

        # Should be unaffected
        setting3 = OrganisationStorySettings.objects.get(organisation=self.gemeente2)

        self.assertEqual(setting3.story_id, "story2")
        self.assertEqual(setting3.enabled, True)

    def test_bulk_update_and_create_settings_non_existing_organisation(self):
        response = self.client.post(
            reverse(
                "antwoorden_cms:story_settings_bulk_endpoint",
                kwargs={"organisation": "foo"},
            ),
            data=json.dumps(
                [
                    {"story_id": "story1", "enabled": False},
                    {"story_id": "story2", "enabled": True},
                ]
            ),
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 404)

    def test_retrieve_settings(self):
        OrganisationStorySettingsFactory.create(
            organisation=self.gemeente, story_id="story1", enabled=False
        )

        response = self.client.get(
            reverse(
                "antwoorden_cms:story_settings",
                kwargs={"organisation": "dongen", "story_id": "story1"},
            ),
        )

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            response.data,
            {
                "story_id": "story1",
                "enabled": False,
                "handover_to_organisation": "",
                "crisis_answer": False,
                "handover_livechat": False,
                "parking_answer": False,
            },
        )

        settings = OrganisationStorySettings.objects.get()
        self.assertEqual(settings.organisation, self.gemeente)
        self.assertEqual(settings.story_id, "story1")
        self.assertEqual(settings.enabled, False)

    @patch(
        "antwoorden_cms.utters.models.SiteConfiguration.get_solo",
        return_value=SiteConfiguration(
            story_toggling_enabled=True,
            story_settings_default=True,
        ),
    )
    def test_retrieve_settings_default_true(self, mock_config):
        response = self.client.get(
            reverse(
                "antwoorden_cms:story_settings",
                kwargs={"organisation": "dongen", "story_id": "story1"},
            ),
        )

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            response.data,
            {
                "story_id": "story1",
                "enabled": True,
                "handover_to_organisation": "",
                "crisis_answer": False,
                "handover_livechat": False,
                "parking_answer": False,
            },
        )
        self.assertFalse(OrganisationStorySettings.objects.exists())

    @patch(
        "antwoorden_cms.utters.models.SiteConfiguration.get_solo",
        return_value=SiteConfiguration(
            story_toggling_enabled=True,
            story_settings_default=False,
        ),
    )
    def test_retrieve_settings_default_false(self, mock_config):
        response = self.client.get(
            reverse(
                "antwoorden_cms:story_settings",
                kwargs={"organisation": "dongen", "story_id": "story1"},
            ),
        )

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            response.data,
            {
                "story_id": "story1",
                "enabled": False,
                "handover_to_organisation": "",
                "crisis_answer": False,
                "handover_livechat": False,
                "parking_answer": False,
            },
        )
        self.assertFalse(OrganisationStorySettings.objects.exists())

    def test_update_settings(self):
        OrganisationStorySettingsFactory.create(
            organisation=self.gemeente, story_id="story1", enabled=False
        )

        response = self.client.put(
            reverse(
                "antwoorden_cms:story_settings",
                kwargs={"organisation": "dongen", "story_id": "story1"},
            ),
            {"enabled": True},
        )

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            response.data,
            {
                "story_id": "story1",
                "enabled": True,
                "handover_to_organisation": "",
                "crisis_answer": False,
                "handover_livechat": False,
                "parking_answer": False,
            },
        )

        settings = OrganisationStorySettings.objects.get()
        self.assertEqual(settings.organisation, self.gemeente)
        self.assertEqual(settings.story_id, "story1")
        self.assertEqual(settings.enabled, True)

    def test_update_settings_not_found(self):
        response = self.client.put(
            reverse(
                "antwoorden_cms:story_settings",
                kwargs={"organisation": "dongen", "story_id": "story1"},
            ),
            {"enabled": True},
        )

        self.assertEqual(response.status_code, 404)

    def test_partial_update_settings(self):
        OrganisationStorySettingsFactory.create(
            organisation=self.gemeente, story_id="story1", enabled=False
        )

        response = self.client.patch(
            reverse(
                "antwoorden_cms:story_settings",
                kwargs={"organisation": "dongen", "story_id": "story1"},
            ),
            {"enabled": True, "handover_to_organisation": ""},
        )

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            response.data,
            {
                "story_id": "story1",
                "enabled": True,
                "handover_to_organisation": "",
                "crisis_answer": False,
                "handover_livechat": False,
                "parking_answer": False,
            },
        )

        settings = OrganisationStorySettings.objects.get()
        self.assertEqual(settings.organisation, self.gemeente)
        self.assertEqual(settings.story_id, "story1")
        self.assertEqual(settings.enabled, True)

    def test_partial_update_settings_not_found(self):
        response = self.client.patch(
            reverse(
                "antwoorden_cms:story_settings",
                kwargs={"organisation": "dongen", "story_id": "story1"},
            ),
            {"enabled": True},
        )

        self.assertEqual(response.status_code, 404)

    def test_delete_settings(self):
        OrganisationStorySettingsFactory.create(
            organisation=self.gemeente, story_id="story1", enabled=False
        )

        response = self.client.delete(
            reverse(
                "antwoorden_cms:story_settings",
                kwargs={"organisation": "dongen", "story_id": "story1"},
            ),
        )

        self.assertEqual(response.status_code, 204)
        self.assertFalse(OrganisationStorySettings.objects.exists())

    def test_delete_settings_not_found(self):
        response = self.client.delete(
            reverse(
                "antwoorden_cms:story_settings",
                kwargs={"organisation": "dongen", "story_id": "story1"},
            ),
        )

        self.assertEqual(response.status_code, 404)
        self.assertFalse(OrganisationStorySettings.objects.exists())


class StorySettingsAPIPermissionTests(APITestCase):
    @classmethod
    def setUpTestData(cls) -> None:
        super().setUpTestData()

        cls.gemeente = GemeenteFactory.create(name="Dongen")
        cls.gemeente2 = GemeenteFactory.create(name="Utrecht")

        cls.user = UserFactory.create(username="foo", password="bar")
        group = GroupFactory.create(name="admin")
        gemeente_perms = Permission.objects.filter(
            codename__in=[p[0] for p in Gemeente._meta.permissions]
        )
        group.permissions.set(gemeente_perms)
        cls.user.groups.add(group)
        cls.user.gemeentes.add(cls.gemeente2)

        # User with correct municipality but limited permissions
        cls.user2 = UserFactory.create(username="foo2", password="bar2")
        group2 = GroupFactory.create(name="admin dongen")
        gemeente_perms2 = Permission.objects.filter(
            codename__in=[
                p[0] for p in Gemeente._meta.permissions if p[0] != "utter_write"
            ]
        )
        group2.permissions.set(gemeente_perms2)
        cls.user2.groups.add(group2)
        cls.user2.gemeentes.add(cls.gemeente)

    def test_create_settings_auth_required(self):
        response = self.client.post(
            reverse(
                "antwoorden_cms:story_settings",
                kwargs={"organisation": "dongen", "story_id": "story1"},
            ),
            {"enabled": False},
        )

        self.assertEqual(response.status_code, 403)
        self.assertFalse(OrganisationStorySettings.objects.exists())

    def test_create_settings_auth_required_for_organisation(self):
        self.client.force_authenticate(self.user)

        response = self.client.post(
            reverse(
                "antwoorden_cms:story_settings",
                kwargs={"organisation": "dongen", "story_id": "story1"},
            ),
            {"enabled": False},
        )

        self.assertEqual(response.status_code, 403)
        self.assertFalse(OrganisationStorySettings.objects.exists())

    def test_create_settings_auth_required_and_permissions(self):
        self.client.force_authenticate(self.user2)

        response = self.client.post(
            reverse(
                "antwoorden_cms:story_settings",
                kwargs={"organisation": "dongen", "story_id": "story1"},
            ),
            {"enabled": False},
        )

        self.assertEqual(response.status_code, 403)
        self.assertFalse(OrganisationStorySettings.objects.exists())

    def test_bulk_update_and_create_settings_auth_required(self):
        response = self.client.post(
            reverse(
                "antwoorden_cms:story_settings_bulk_endpoint",
                kwargs={"organisation": "dongen"},
            ),
            data=json.dumps(
                [
                    {"story_id": "story1", "enabled": False},
                    {"story_id": "story2", "enabled": True},
                ]
            ),
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 403)
        self.assertFalse(OrganisationStorySettings.objects.exists())

    def test_bulk_update_and_create_settings_auth_required_for_organisation(self):
        self.client.force_authenticate(self.user)

        response = self.client.post(
            reverse(
                "antwoorden_cms:story_settings_bulk_endpoint",
                kwargs={"organisation": "dongen"},
            ),
            data=json.dumps(
                [
                    {"story_id": "story1", "enabled": False},
                    {"story_id": "story2", "enabled": True},
                ]
            ),
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 403)
        self.assertFalse(OrganisationStorySettings.objects.exists())

    def test_bulk_update_and_create_settings_auth_required_and_permissions(self):
        self.client.force_authenticate(self.user2)

        response = self.client.post(
            reverse(
                "antwoorden_cms:story_settings_bulk_endpoint",
                kwargs={"organisation": "dongen"},
            ),
            data=json.dumps(
                [
                    {"story_id": "story1", "enabled": False},
                    {"story_id": "story2", "enabled": True},
                ]
            ),
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 403)
        self.assertFalse(OrganisationStorySettings.objects.exists())

    def test_retrieve_settings_no_auth_required(self):
        OrganisationStorySettingsFactory.create(
            organisation=self.gemeente, story_id="story1", enabled=False
        )

        response = self.client.get(
            reverse(
                "antwoorden_cms:story_settings",
                kwargs={"organisation": "dongen", "story_id": "story1"},
            ),
        )

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            response.data,
            {
                "story_id": "story1",
                "enabled": False,
                "handover_to_organisation": "",
                "crisis_answer": False,
                "handover_livechat": False,
                "parking_answer": False,
            },
        )

        settings = OrganisationStorySettings.objects.get()
        self.assertEqual(settings.organisation, self.gemeente)
        self.assertEqual(settings.story_id, "story1")
        self.assertEqual(settings.enabled, False)

    def test_list_settings_no_auth_required(self):
        OrganisationStorySettingsFactory.create(
            organisation=self.gemeente,
            story_id="story1",
            enabled=True,
            crisis_answer=True,
        )
        OrganisationStorySettingsFactory.create(
            organisation=self.gemeente,
            story_id="story2",
            enabled=True,
            handover_to_organisation="Tilburg",
        )
        OrganisationStorySettingsFactory.create(
            organisation=self.gemeente,
            story_id="story3",
            enabled=True,
            handover_to_organisation="",
            handover_livechat=True,
        )
        OrganisationStorySettingsFactory.create(
            organisation=self.gemeente,
            story_id="story4",
            enabled=True,
            handover_to_organisation="",
            parking_answer=True,
        )
        OrganisationStorySettingsFactory.create(
            organisation=self.gemeente2, story_id="story1", enabled=False
        )

        response = self.client.get(
            reverse(
                "antwoorden_cms:story_settings_list",
                kwargs={"organisation": "dongen"},
            ),
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data,
            [
                {
                    "story_id": "story1",
                    "enabled": True,
                    "handover_to_organisation": "",
                    "crisis_answer": True,
                    "handover_livechat": False,
                    "parking_answer": False,
                },
                {
                    "story_id": "story2",
                    "enabled": True,
                    "handover_to_organisation": "Tilburg",
                    "crisis_answer": False,
                    "handover_livechat": False,
                    "parking_answer": False,
                },
                {
                    "story_id": "story3",
                    "enabled": True,
                    "handover_to_organisation": "",
                    "crisis_answer": False,
                    "handover_livechat": True,
                    "parking_answer": False,
                },
                {
                    "story_id": "story4",
                    "enabled": True,
                    "handover_to_organisation": "",
                    "crisis_answer": False,
                    "handover_livechat": False,
                    "parking_answer": True,
                },
            ],
        )

    def test_update_settings_auth_required(self):
        response = self.client.put(
            reverse(
                "antwoorden_cms:story_settings",
                kwargs={"organisation": "dongen", "story_id": "story1"},
            ),
            {"enabled": False},
        )

        self.assertEqual(response.status_code, 403)
        self.assertFalse(OrganisationStorySettings.objects.exists())

    def test_update_settings_auth_required_for_organisation(self):
        self.client.force_authenticate(self.user)

        response = self.client.put(
            reverse(
                "antwoorden_cms:story_settings",
                kwargs={"organisation": "dongen", "story_id": "story1"},
            ),
            {"enabled": False},
        )

        self.assertEqual(response.status_code, 403)
        self.assertFalse(OrganisationStorySettings.objects.exists())

    def test_update_settings_auth_required_and_permissions(self):
        self.client.force_authenticate(self.user2)

        response = self.client.put(
            reverse(
                "antwoorden_cms:story_settings",
                kwargs={"organisation": "dongen", "story_id": "story1"},
            ),
            {"enabled": False},
        )

        self.assertEqual(response.status_code, 403)
        self.assertFalse(OrganisationStorySettings.objects.exists())

    def test_partial_update_settings_auth_required(self):
        response = self.client.patch(
            reverse(
                "antwoorden_cms:story_settings",
                kwargs={"organisation": "dongen", "story_id": "story1"},
            ),
            {"enabled": False},
        )

        self.assertEqual(response.status_code, 403)
        self.assertFalse(OrganisationStorySettings.objects.exists())

    def test_partial_update_settings_auth_required_for_organisation(self):
        self.client.force_authenticate(self.user)

        response = self.client.patch(
            reverse(
                "antwoorden_cms:story_settings",
                kwargs={"organisation": "dongen", "story_id": "story1"},
            ),
            {"enabled": False},
        )

        self.assertEqual(response.status_code, 403)
        self.assertFalse(OrganisationStorySettings.objects.exists())

    def test_partial_update_settings_auth_required_and_permissions(self):
        self.client.force_authenticate(self.user2)

        response = self.client.patch(
            reverse(
                "antwoorden_cms:story_settings",
                kwargs={"organisation": "dongen", "story_id": "story1"},
            ),
            {"enabled": False},
        )

        self.assertEqual(response.status_code, 403)
        self.assertFalse(OrganisationStorySettings.objects.exists())

    def test_delete_settings_auth_required(self):
        response = self.client.delete(
            reverse(
                "antwoorden_cms:story_settings",
                kwargs={"organisation": "dongen", "story_id": "story1"},
            ),
        )

        self.assertEqual(response.status_code, 403)
        self.assertFalse(OrganisationStorySettings.objects.exists())

    def test_delete_settings_auth_required_for_organisation(self):
        self.client.force_authenticate(self.user)

        response = self.client.delete(
            reverse(
                "antwoorden_cms:story_settings",
                kwargs={"organisation": "dongen", "story_id": "story1"},
            ),
        )

        self.assertEqual(response.status_code, 403)
        self.assertFalse(OrganisationStorySettings.objects.exists())

    def test_delete_settings_auth_required__and_permissions(self):
        self.client.force_authenticate(self.user2)

        response = self.client.delete(
            reverse(
                "antwoorden_cms:story_settings",
                kwargs={"organisation": "dongen", "story_id": "story1"},
            ),
        )

        self.assertEqual(response.status_code, 403)
        self.assertFalse(OrganisationStorySettings.objects.exists())
