from factory.django import DjangoModelFactory

from ..models import OrganisationStorySettings, PageMetadata, Subject


class PageMetadataFactory(DjangoModelFactory):
    class Meta:
        model = PageMetadata


class OrganisationStorySettingsFactory(DjangoModelFactory):
    class Meta:
        model = OrganisationStorySettings


class SubjectFactory(DjangoModelFactory):
    class Meta:
        model = Subject
