from django.test import TestCase

from antwoorden_cms.utters.tests.factories import GemeenteDomainFactory, GemeenteFactory

from .factories import PageMetadataFactory


class PageMetadataModelTests(TestCase):
    def setUp(self):
        self.gemeente = GemeenteFactory.create(
            name="Meierijstad",
            slug="meierijstad",
        )
        self.domain = GemeenteDomainFactory(
            gemeente=self.gemeente,
            domain="meierijstad.nl",
            gem_default_enabled=True,
            direct_handover=False,
        )

    def test_page_metadata_gem_enabled_with_default(self):

        page_use_default = PageMetadataFactory(
            gemeente_domain=self.domain, gem_enabled=None
        )
        page_gem_disabled = PageMetadataFactory(
            gemeente_domain=self.domain, gem_enabled=False
        )

        self.assertTrue(page_use_default.gem_enabled_with_default)
        self.assertFalse(page_gem_disabled.gem_enabled_with_default)

        self.domain.gem_default_enabled = False
        self.domain.save()

        self.assertFalse(page_use_default.gem_enabled_with_default)
        self.assertFalse(page_gem_disabled.gem_enabled_with_default)

    def test_page_metadata_direct_handover_with_default(self):

        page_use_default = PageMetadataFactory(
            gemeente_domain=self.domain, direct_handover=None
        )
        page_gem_disabled = PageMetadataFactory(
            gemeente_domain=self.domain, direct_handover=True
        )

        self.assertFalse(page_use_default.direct_handover_with_default)
        self.assertTrue(page_gem_disabled.direct_handover_with_default)

        self.domain.direct_handover = True
        self.domain.save()

        self.assertTrue(page_use_default.direct_handover_with_default)
        self.assertTrue(page_gem_disabled.direct_handover_with_default)
