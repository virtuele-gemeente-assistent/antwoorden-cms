from django.conf import settings
from django.core.management import call_command
from django.test import TestCase

import requests
from requests_mock import Mocker

from antwoorden_cms.utters.choices import Environments
from antwoorden_cms.utters.models import GemeenteDomain
from antwoorden_cms.utters.tests.factories import GemeenteDomainFactory, GemeenteFactory


@Mocker()
class RequestScrapyUpdateTests(TestCase):
    def setUp(self):
        self.gemeente = GemeenteFactory(name="Utrecht")
        self.domain = GemeenteDomainFactory(
            gemeente=self.gemeente,
            domain="utrecht.tk",
            enable_indexing=True,
            environment=Environments.PRODUCTION,
        )
        self.url = settings.SCRAPY_URL + "/schedule.json"

    def test_success(self, mock):

        mock_data = {
            "node_name": "894959ad704e",
            "status": "ok",
            "jobid": "c96cdea0f28011eeb2420242ac1c0002",
        }

        mock.post(
            self.url,
            json=mock_data,
            status_code=200,
        )

        with self.assertLogs(
            "antwoorden_cms.config.management.commands.request_scrapy_update",
            level="INFO",
        ) as info:

            call_command("request_scrapy_update")
            self.assertEqual(
                info.output,
                [
                    "INFO:antwoorden_cms.config.management.commands.request_scrapy_update:"
                    "Sending request for {domain} from Gemeete {gemeente}".format(
                        domain="utrecht.tk", gemeente="Utrecht"
                    ),
                    "INFO:antwoorden_cms.config.management.commands.request_scrapy_update:"
                    "Received response {}".format(mock_data),
                ],
            )

    def test_timeout(self, mock):
        mock.post(self.url, exc=requests.exceptions.ConnectTimeout)

        with self.assertLogs(
            "antwoorden_cms.config.management.commands.request_scrapy_update",
            level="INFO",
        ) as info:
            call_command("request_scrapy_update")
            self.assertEqual(
                info.output,
                [
                    "INFO:antwoorden_cms.config.management.commands.request_scrapy_update:"
                    "Sending request for {domain} from Gemeete {gemeente}".format(
                        domain="utrecht.tk", gemeente="Utrecht"
                    ),
                    "ERROR:antwoorden_cms.config.management.commands.request_scrapy_update:"
                    "Failed to send request for {domain} from Gemeete {gemeente}".format(
                        domain="utrecht.tk", gemeente="Utrecht"
                    ),
                ],
            )

    def test_enable_indexing(self, mock):
        """
        Should only send index request if enable_indexing is true
        """

        mock_data = {
            "node_name": "894959ad704e",
            "status": "ok",
            "jobid": "c96cdea0f28011eeb2420242ac1c0002",
        }
        mock.post(
            self.url,
            json=mock_data,
            status_code=200,
        )

        GemeenteDomainFactory(
            gemeente=self.gemeente,
            domain="utrecht.cs",
            enable_indexing=False,
            environment=Environments.PRODUCTION,
        )

        call_command("request_scrapy_update")

        self.assertEqual(GemeenteDomain.objects.count(), 2)
        self.assertEqual(mock.call_count, 1)

    def test_environment(self, mock):
        """
        Should only send index request if environment is Production, regardless of whether enabled_indexing is true
        """

        mock_data = {
            "node_name": "894959ad704e",
            "status": "ok",
            "jobid": "c96cdea0f28011eeb2420242ac1c0002",
        }
        mock.post(
            self.url,
            json=mock_data,
            status_code=200,
        )

        GemeenteDomainFactory(
            gemeente=self.gemeente,
            domain="utrecht.cs",
            enable_indexing=True,
            environment=Environments.TEST,
        )

        call_command("request_scrapy_update")

        self.assertEqual(GemeenteDomain.objects.count(), 2)
        self.assertEqual(mock.call_count, 1)
