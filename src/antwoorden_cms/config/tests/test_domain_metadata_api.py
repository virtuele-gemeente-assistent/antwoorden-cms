from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

from rest_framework.test import APITestCase

from antwoorden_cms.utters.choices import Environments, LivechatTypes
from antwoorden_cms.utters.tests.factories import (
    GemeenteDomainFactory,
    GemeenteFactory,
    LivechatConfigurationFactory,
)

from .factories import PageMetadataFactory


class DomainMetadataAPITests(APITestCase):
    def test_post_get_domain_metadata_domain_does_not_exist(self):

        response = self.client.get(
            reverse("antwoorden_cms:domain_metadata"),
            {"domain": "dongen_12345.nl"},
        )

        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.data, {"detail": _("Not found.")})

    def test_post_get_domain_metadata_gem_default_enabled(self):
        gemeente = GemeenteFactory.create(name="Dongen")
        LivechatConfigurationFactory.create(
            gemeente=gemeente,
            type=LivechatTypes.obi4wan,
            identifier="default",
            avatar_url="http://example.com/image",
        )
        config2 = LivechatConfigurationFactory.create(
            gemeente=gemeente,
            type=LivechatTypes.livecom,
            identifier="foo",
            avatar_url="http://example.com/image2",
        )

        domain1 = GemeenteDomainFactory(
            gemeente=gemeente,
            domain="dongen.nl",
            gem_default_enabled=True,
            environment=Environments.PRODUCTION,
        )
        domain2 = GemeenteDomainFactory(
            gemeente=gemeente,
            domain="test.dongen.nl",
            gem_default_enabled=True,
            environment=Environments.PRODUCTION,
        )

        PageMetadataFactory.create(
            gemeente_domain=domain1,
            path="/foo",
            title="foo",
            gem_enabled=True,
        )
        PageMetadataFactory.create(
            gemeente_domain=domain1,
            path="/bar",
            title="bar",
            gem_enabled=True,
            livechat_config=config2,
        )
        PageMetadataFactory.create(
            gemeente_domain=domain1,
            path="/new",
            title="new",
            gem_enabled=None,
        )
        PageMetadataFactory.create(
            gemeente_domain=domain1,
            path="/baz",
            gem_enabled=False,
        )
        PageMetadataFactory.create(
            gemeente_domain=domain1,
            path="/demo",
            gem_enabled=False,
        )

        # Should not show up
        PageMetadataFactory.create(
            gemeente_domain=domain2,
            path="/demo2",
            gem_enabled=True,
        )

        response = self.client.get(
            reverse("antwoorden_cms:domain_metadata"),
            {"domain": "dongen.nl"},
        )

        expected = {
            "defaults": {
                "widget_enabled": True,
                "direct_handover": False,
                "organisation_name": "Dongen",
                "organisation_slug": "dongen",
                "widget_title": "Gem",
                "widget_subtitle": "Je digitale hulp van de gemeente",
                "popup_message": "Stel je vraag",
                "avatar_url": "",
            },
            "enabled_pages": {
                "/foo": {
                    "title": "foo",
                    "gem_enabled": True,
                    "direct_handover": False,
                    "livechat_type": "obi4wan",
                    "livechat_id": "default",
                    "livechat_avatar_url": "http://example.com/image",
                },
                "/bar": {
                    "title": "bar",
                    "gem_enabled": True,
                    "direct_handover": False,
                    "livechat_type": "livecom",
                    "livechat_id": "foo",
                    "livechat_avatar_url": "http://example.com/image2",
                },
                "/new": {
                    "title": "new",
                    "gem_enabled": True,
                    "direct_handover": False,
                    "livechat_type": "obi4wan",
                    "livechat_id": "default",
                    "livechat_avatar_url": "http://example.com/image",
                },
            },
            "disabled_pages": ["/baz", "/demo"],
        }

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(response.data, expected)

    def test_post_get_domain_metadata_gem_default_enabled_via_cms(self):
        gemeente = GemeenteFactory.create(name="Dongen")
        LivechatConfigurationFactory.create(
            gemeente=gemeente,
            type=LivechatTypes.obi4wan,
            identifier="default",
            avatar_url="http://example.com/image",
        )
        config2 = LivechatConfigurationFactory.create(
            gemeente=gemeente,
            type=LivechatTypes.livecom,
            identifier="foo",
            avatar_url="http://example.com/image2",
        )

        domain1 = GemeenteDomainFactory(
            gemeente=gemeente,
            domain="dongen.nl",
            gem_default_enabled=True,
            environment=Environments.PRODUCTION,
        )
        domain2 = GemeenteDomainFactory(
            gemeente=gemeente,
            domain="test.dongen.nl",
            gem_default_enabled=True,
            environment=Environments.PRODUCTION,
        )

        PageMetadataFactory.create(
            gemeente_domain=domain1,
            path="/foo",
            title="foo",
            gem_enabled=True,
        )
        PageMetadataFactory.create(
            gemeente_domain=domain1,
            path="/bar",
            title="bar",
            gem_enabled=True,
            livechat_config=config2,
        )
        PageMetadataFactory.create(
            gemeente_domain=domain1,
            path="/new",
            title="new",
            gem_enabled=None,
        )
        PageMetadataFactory.create(
            gemeente_domain=domain1,
            path="/baz",
            gem_enabled=False,
        )
        PageMetadataFactory.create(
            gemeente_domain=domain1,
            path="/demo",
            gem_enabled=False,
        )

        # Should not show up
        PageMetadataFactory.create(
            gemeente_domain=domain2,
            path="/demo2",
            gem_enabled=True,
        )

        # Fetch by name
        response = self.client.get(
            reverse("antwoorden_cms:domain_metadata"),
            {"domain": "#Dongen"},
        )

        expected = {
            "defaults": {
                "widget_enabled": True,
                "direct_handover": False,
                "organisation_name": "Dongen",
                "organisation_slug": "dongen",
                "widget_title": "Gem",
                "widget_subtitle": "Je digitale hulp van de gemeente",
                "popup_message": "Stel je vraag",
                "avatar_url": "",
            },
            "enabled_pages": {
                "/foo": {
                    "title": "foo",
                    "gem_enabled": True,
                    "direct_handover": False,
                    "livechat_type": "obi4wan",
                    "livechat_id": "default",
                    "livechat_avatar_url": "http://example.com/image",
                },
                "/bar": {
                    "title": "bar",
                    "gem_enabled": True,
                    "direct_handover": False,
                    "livechat_type": "livecom",
                    "livechat_id": "foo",
                    "livechat_avatar_url": "http://example.com/image2",
                },
                "/new": {
                    "title": "new",
                    "gem_enabled": True,
                    "direct_handover": False,
                    "livechat_type": "obi4wan",
                    "livechat_id": "default",
                    "livechat_avatar_url": "http://example.com/image",
                },
            },
            "disabled_pages": ["/baz", "/demo"],
        }

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(response.data, expected)

    def test_post_get_domain_metadata_gem_default_disabled(self):
        gemeente = GemeenteFactory.create(name="Dongen")
        LivechatConfigurationFactory.create(
            gemeente=gemeente,
            type=LivechatTypes.obi4wan,
            identifier="default",
            avatar_url="http://example.com/image",
        )
        config2 = LivechatConfigurationFactory.create(
            gemeente=gemeente,
            type=LivechatTypes.livecom,
            identifier="foo",
            avatar_url="http://example.com/image2",
        )

        domain1 = GemeenteDomainFactory(
            gemeente=gemeente,
            domain="dongen.nl",
            gem_default_enabled=False,
            environment=Environments.PRODUCTION,
        )
        domain2 = GemeenteDomainFactory(
            gemeente=gemeente,
            domain="test.dongen.nl",
            gem_default_enabled=False,
            environment=Environments.PRODUCTION,
        )

        PageMetadataFactory.create(
            gemeente_domain=domain1,
            path="/foo",
            title="foo",
            gem_enabled=True,
        )
        PageMetadataFactory.create(
            gemeente_domain=domain1,
            path="/bar",
            title="bar",
            gem_enabled=True,
            livechat_config=config2,
        )
        PageMetadataFactory.create(
            gemeente_domain=domain1,
            path="/new",
            title="new",
            gem_enabled=None,
        )
        PageMetadataFactory.create(
            gemeente_domain=domain1,
            path="/baz",
            gem_enabled=False,
        )
        PageMetadataFactory.create(
            gemeente_domain=domain1,
            path="/demo",
            gem_enabled=False,
        )

        # Should not show up
        PageMetadataFactory.create(
            gemeente_domain=domain2,
            path="/demo2",
            gem_enabled=True,
        )

        response = self.client.get(
            reverse("antwoorden_cms:domain_metadata"),
            {"domain": "dongen.nl"},
        )

        expected = {
            "defaults": {
                "widget_enabled": False,
                "direct_handover": False,
                "organisation_name": "Dongen",
                "organisation_slug": "dongen",
                "widget_title": "Gem",
                "widget_subtitle": "Je digitale hulp van de gemeente",
                "popup_message": "Stel je vraag",
                "avatar_url": "",
            },
            "enabled_pages": {
                "/foo": {
                    "title": "foo",
                    "gem_enabled": True,
                    "direct_handover": False,
                    "livechat_type": "obi4wan",
                    "livechat_id": "default",
                    "livechat_avatar_url": "http://example.com/image",
                },
                "/bar": {
                    "title": "bar",
                    "gem_enabled": True,
                    "direct_handover": False,
                    "livechat_type": "livecom",
                    "livechat_id": "foo",
                    "livechat_avatar_url": "http://example.com/image2",
                },
            },
            "disabled_pages": [],
        }

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(response.data, expected)

    def test_post_get_domain_metadata_livechat_config_disabled(self):
        gemeente = GemeenteFactory.create(
            name="Dongen",
            livechat_enabled=False,
        )
        LivechatConfigurationFactory.create(
            gemeente=gemeente,
            type=LivechatTypes.obi4wan,
            identifier="default",
            avatar_url="http://example.com/image",
        )
        config2 = LivechatConfigurationFactory.create(
            gemeente=gemeente,
            type=LivechatTypes.livecom,
            identifier="foo",
            avatar_url="http://example.com/image2",
        )

        domain1 = GemeenteDomainFactory(
            gemeente=gemeente,
            domain="dongen.nl",
            gem_default_enabled=True,
            environment=Environments.PRODUCTION,
        )
        domain2 = GemeenteDomainFactory(
            gemeente=gemeente,
            domain="test.dongen.nl",
            gem_default_enabled=True,
            environment=Environments.PRODUCTION,
        )

        PageMetadataFactory.create(
            gemeente_domain=domain1,
            path="/foo",
            title="foo",
            gem_enabled=True,
        )
        PageMetadataFactory.create(
            gemeente_domain=domain1,
            path="/bar",
            title="bar",
            gem_enabled=True,
            livechat_config=config2,
        )
        PageMetadataFactory.create(
            gemeente_domain=domain1,
            path="/new",
            title="new",
            gem_enabled=None,
        )
        PageMetadataFactory.create(
            gemeente_domain=domain1,
            path="/baz",
            gem_enabled=False,
        )
        PageMetadataFactory.create(
            gemeente_domain=domain1,
            path="/demo",
            gem_enabled=False,
        )

        # Should not show up
        PageMetadataFactory.create(
            gemeente_domain=domain2,
            path="/demo2",
            gem_enabled=True,
        )

        response = self.client.get(
            reverse("antwoorden_cms:domain_metadata"),
            {"domain": "dongen.nl"},
        )

        expected = {
            "defaults": {
                "widget_enabled": True,
                "direct_handover": False,
                "organisation_name": "Dongen",
                "organisation_slug": "dongen",
                "widget_title": "Gem",
                "widget_subtitle": "Je digitale hulp van de gemeente",
                "popup_message": "Stel je vraag",
                "avatar_url": "",
            },
            "enabled_pages": {
                "/foo": {
                    "title": "foo",
                    "gem_enabled": True,
                    "direct_handover": False,
                    "livechat_type": "",
                    "livechat_id": "",
                    "livechat_avatar_url": "",
                },
                "/bar": {
                    "title": "bar",
                    "gem_enabled": True,
                    "direct_handover": False,
                    "livechat_type": "",
                    "livechat_id": "",
                    "livechat_avatar_url": "",
                },
                "/new": {
                    "title": "new",
                    "gem_enabled": True,
                    "direct_handover": False,
                    "livechat_type": "",
                    "livechat_id": "",
                    "livechat_avatar_url": "",
                },
            },
            "disabled_pages": ["/baz", "/demo"],
        }

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(response.data, expected)

    def test_post_get_domain_metadata_render(self):
        gemeente = GemeenteFactory.create(name="Dongen")
        LivechatConfigurationFactory.create(
            gemeente=gemeente,
            type=LivechatTypes.obi4wan,
            identifier="default",
            avatar_url="http://example.com/image",
        )

        domain1 = GemeenteDomainFactory(
            gemeente=gemeente,
            domain="dongen.nl",
            gem_default_enabled=True,
            environment=Environments.PRODUCTION,
        )

        PageMetadataFactory.create(
            gemeente_domain=domain1,
            path="/Inwoners/Alle_onderwerpen/Paspoort_en_ID_kaart/Paspoort_en_identiteitskaart",
            title="foo",
            gem_enabled=True,
        )

        response = self.client.get(
            reverse("antwoorden_cms:domain_metadata"),
            {"domain": "dongen.nl"},
        )

        expected = {
            "defaults": {
                "widget_enabled": True,
                "direct_handover": False,
                "organisation_name": "Dongen",
                "organisation_slug": "dongen",
                "widget_title": "Gem",
                "widget_subtitle": "Je digitale hulp van de gemeente",
                "popup_message": "Stel je vraag",
                "avatar_url": "",
            },
            "enabled_pages": {
                "/Inwoners/Alle_onderwerpen/Paspoort_en_ID_kaart/Paspoort_en_identiteitskaart": {
                    "title": "foo",
                    "gem_enabled": True,
                    "direct_handover": False,
                    "livechat_type": "obi4wan",
                    "livechat_id": "default",
                    "livechat_avatar_url": "http://example.com/image",
                },
            },
            "disabled_pages": [],
        }

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(response.data, expected)

        rendered_response = response.render()

        rendered_expected = {
            "defaults": {
                "widgetEnabled": True,
                "directHandover": False,
                "organisationName": "Dongen",
                "organisationSlug": "dongen",
                "widgetTitle": "Gem",
                "widgetSubtitle": "Je digitale hulp van de gemeente",
                "popupMessage": "Stel je vraag",
                "avatarUrl": "",
            },
            "enabledPages": {
                "/Inwoners/Alle_onderwerpen/Paspoort_en_ID_kaart/Paspoort_en_identiteitskaart": {
                    "title": "foo",
                    "gemEnabled": True,
                    "directHandover": False,
                    "livechatType": "obi4wan",
                    "livechatId": "default",
                    "livechatAvatarUrl": "http://example.com/image",
                },
            },
            "disabledPages": [],
        }

        self.assertDictEqual(rendered_response.json(), rendered_expected)

    def test_post_get_domain_metadata_no_domain(self):
        gemeente = GemeenteFactory.create(
            name="Bergen-op-zoom",
            slug="bergen-op-zoom",
        )

        LivechatConfigurationFactory.create(
            gemeente=gemeente,
            type=LivechatTypes.obi4wan,
            identifier="default",
            avatar_url="http://example.com/image",
        )

        response = self.client.get(
            reverse("antwoorden_cms:domain_metadata"),
            {"domain": "#Bergen-op-zoom"},
        )
        expected = {"detail": _("Not found.")}

        self.assertEqual(response.status_code, 404)
        self.assertDictEqual(response.data, expected)

    def test_post_get_domain_metadata_test_environment(self):
        gemeente = GemeenteFactory.create(name="Dongen")
        LivechatConfigurationFactory.create(
            gemeente=gemeente,
            type=LivechatTypes.obi4wan,
            identifier="default",
            avatar_url="http://example.com/image",
        )

        domain1 = GemeenteDomainFactory(
            gemeente=gemeente,
            domain="dongen.nl",
            gem_default_enabled=True,
            environment=Environments.TEST,
        )

        PageMetadataFactory.create(
            gemeente_domain=domain1,
            path="/Inwoners/Alle_onderwerpen/Paspoort_en_ID_kaart/Paspoort_en_identiteitskaart",
            title="foo",
            gem_enabled=True,
        )

        response = self.client.get(
            reverse("antwoorden_cms:domain_metadata"),
            {"domain": "dongen.nl"},
        )

        expected = {
            "defaults": {
                "widget_enabled": True,
                "direct_handover": False,
                "organisation_name": "Dongen",
                "organisation_slug": "dongen",
                "widget_title": "Gem",
                "widget_subtitle": "Je digitale hulp van de gemeente",
                "popup_message": "Stel je vraag",
                "avatar_url": "",
            },
            "enabled_pages": {},
            "disabled_pages": [],
        }

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(response.data, expected)

        rendered_response = response.render()

        rendered_expected = {
            "defaults": {
                "widgetEnabled": True,
                "directHandover": False,
                "organisationName": "Dongen",
                "organisationSlug": "dongen",
                "widgetTitle": "Gem",
                "widgetSubtitle": "Je digitale hulp van de gemeente",
                "popupMessage": "Stel je vraag",
                "avatarUrl": "",
            },
            "enabledPages": {},
            "disabledPages": [],
        }

        self.assertDictEqual(rendered_response.json(), rendered_expected)
