import logging

from django.db.models import F, Q
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_control
from django.views.generic import (
    CreateView,
    DeleteView,
    DetailView,
    ListView,
    TemplateView,
    UpdateView,
)
from django.views.generic.edit import ModelFormMixin

from two_factor.views import OTPRequiredMixin

from antwoorden_cms.utters.mixins import ObjectPermissionMixin
from antwoorden_cms.utters.models import Gemeente

from .forms import GemeenteDomainFormset, GlobalGemSettingsForm, PageMetadataForm
from .models import PageMetadata
from .utils import get_domain_and_path_from_url

logger = logging.getLogger(__name__)


class PageMetadataListView(
    OTPRequiredMixin, ObjectPermissionMixin, ModelFormMixin, ListView
):
    template_name = "config/page_metadata_list.html"
    model = PageMetadata
    paginate_by = 10

    form_class = GlobalGemSettingsForm

    permission_required = "utters.page_metadata_read"

    default_sort_param = "title"
    default_sort_order = "asc"

    def get_object(self):
        try:
            return Gemeente.objects.get(slug=self.kwargs["gemeente_slug"])
        except Gemeente.DoesNotExist:
            return None

    def get_selected_domain(self):
        gemeente_domain = self.get_object().gemeentedomain_set.first()
        if not gemeente_domain:
            return None

        return self.request.GET.get(
            "domain", self.get_object().gemeentedomain_set.first().domain
        )

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):

        self.object = self.get_object()
        self.object_list = self.get_queryset()

        if "_save_domains" in request.POST:
            formset = GemeenteDomainFormset(
                request.POST, request.FILES, instance=self.object
            )
            if formset.is_valid():
                self.formset_valid(formset)
            return self.form_valid(formset)
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        return self.form_invalid(form)

    def formset_invalid(self, formset):
        return self.render_to_response(self.get_context_data(formset=formset))

    def formset_valid(self, formset):
        formset.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse(
            "config:page_metadata_list",
            kwargs={"gemeente_slug": self.kwargs["gemeente_slug"]},
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["search_query"] = self.request.GET.get("search", "").lower()
        context["selected_domain"] = self.get_selected_domain()

        if "formset" not in context or context["formset"] is None:
            context["formset"] = GemeenteDomainFormset(instance=self.object)

        context["sort"] = self.request.GET.get("sort", self.default_sort_param)
        context["order"] = self.request.GET.get("order", self.default_sort_order)

        return context

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.filter(
            gemeente_domain__gemeente__slug=self.kwargs["gemeente_slug"],
            gemeente_domain__domain=self.get_selected_domain(),
        ).order_by("pk")

        search_query = self.request.GET.get("search", "").lower()
        if search_query:
            qs = qs.filter(
                Q(path__icontains=search_query)
                | Q(title__icontains=search_query)
                | Q(livechat_config__name__icontains=search_query)
            )

        sort = self.request.GET.get("sort", self.default_sort_param)
        order = self.request.GET.get("order", self.default_sort_order)

        kwargs = {}
        if sort == "gem_enabled":
            kwargs = {"nulls_last": True} if order == "asc" else {"nulls_first": True}
        elif sort == "direct_handover":
            # Fix ordering for `direct_handover`, which is a NullBooleanField
            kwargs = {"nulls_last": True} if order == "asc" else {"nulls_first": True}
        elif sort == "livechat_config__name":
            kwargs = {"nulls_last": True} if order == "asc" else {"nulls_first": True}
        order_query = (
            F(sort).asc(**kwargs) if order == "asc" else F(sort).desc(**kwargs)
        )

        return qs.order_by(order_query)


class PageMetadataCreateView(OTPRequiredMixin, ObjectPermissionMixin, CreateView):
    template_name = "config/page_metadata_create.html"
    model = PageMetadata
    form_class = PageMetadataForm

    permission_required = "utters.page_metadata_write"

    def get_success_url(self):
        return reverse(
            "config:page_metadata_list",
            kwargs={"gemeente_slug": self.kwargs["gemeente_slug"]},
        )

    def form_valid(self, form):
        self.object = form.save()
        _, path = get_domain_and_path_from_url(form.data["full_url"])
        self.object.path = path
        return super().form_valid(form)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["gemeente_slug"] = self.kwargs["gemeente_slug"]
        return kwargs


class PageMetadataDetailView(OTPRequiredMixin, ObjectPermissionMixin, DetailView):
    template_name = "config/page_metadata_detail.html"
    model = PageMetadata
    pk_url_kwarg = "page_metadata_pk"

    permission_required = "utters.page_metadata_read"

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(gemeente_domain__gemeente__slug=self.kwargs["gemeente_slug"])

    def get_success_url(self):
        return reverse(
            "config:page_metadata_list",
            kwargs={"gemeente_slug": self.kwargs["gemeente_slug"]},
        )


class PageMetadataUpdateView(OTPRequiredMixin, ObjectPermissionMixin, UpdateView):
    template_name = "config/page_metadata_update.html"
    model = PageMetadata
    form_class = PageMetadataForm
    pk_url_kwarg = "page_metadata_pk"

    permission_required = "utters.page_metadata_write"

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(gemeente_domain__gemeente__slug=self.kwargs["gemeente_slug"])

    def get_success_url(self):
        query_string = ""
        try:
            domain = self.get_object().gemeente_domain.domain
            query_string = f"?domain={domain}"
        except Exception:
            logger.exception("Something went wrong while parsing referer header")

        return (
            reverse(
                "config:page_metadata_list",
                kwargs={"gemeente_slug": self.kwargs["gemeente_slug"]},
            )
            + query_string
        )


class PageMetadataDeleteView(OTPRequiredMixin, ObjectPermissionMixin, DeleteView):
    model = PageMetadata
    pk_url_kwarg = "page_metadata_pk"

    permission_required = "utters.page_metadata_write"

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(gemeente_domain__gemeente__slug=self.kwargs["gemeente_slug"])

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)

    def get_success_url(self):
        query_string = ""
        try:
            domain = self.get_object().gemeente_domain.domain
            query_string = f"?domain={domain}"
        except Exception:
            logger.exception("Something went wrong while parsing referer header")

        return (
            reverse(
                "config:page_metadata_list",
                kwargs={"gemeente_slug": self.kwargs["gemeente_slug"]},
            )
            + query_string
        )


class WidgetCodeGeneratorBodyView(TemplateView):
    template_name = "config/widget_code_body.html"
    content_type = "text/plain"


@method_decorator(cache_control(max_age=600), "dispatch")
class WidgetStylingView(TemplateView):
    template_name = "config/widget_styling.css"
    content_type = "text/css"
