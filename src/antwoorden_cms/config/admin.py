from django.contrib import admin, messages
from django.contrib.admin.views.decorators import staff_member_required
from django.http import HttpResponseRedirect
from django.template.response import TemplateResponse
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from django_otp.decorators import otp_required
from openpyxl import load_workbook

from .forms import SubjectsImportForm
from .models import OrganisationStorySettings, PageMetadata
from .utils import import_subjects


@admin.register(PageMetadata)
class PageMetadataAdmin(admin.ModelAdmin):
    list_display = (
        "path",
        "title",
        "gemeente_domain",
        "page_on_site",
        "gem_enabled",
        "direct_handover",
    )
    search_fields = (
        "path",
        "title",
    )
    list_filter = (
        "gemeente_domain",
        "gem_enabled",
    )


@admin.register(OrganisationStorySettings)
class OrganisationStorySettingsAdmin(admin.ModelAdmin):
    list_display = (
        "organisation",
        "story_id",
        "enabled",
        "handover_to_organisation",
        "crisis_answer",
        "handover_livechat",
        "parking_answer",
    )
    search_fields = ("story_id",)
    list_filter = (
        "organisation",
        "enabled",
    )


@otp_required
@staff_member_required
def import_subjects_view(request):
    form = SubjectsImportForm(request.POST, request.FILES)
    if "_import_subjects" in request.POST:
        form = SubjectsImportForm(request.POST, request.FILES)
        if form.is_valid():
            try:
                import_file = form.cleaned_data["import_file"]
                wb = load_workbook(import_file)
                ws = wb[wb.sheetnames[0]]
                rows_iter = ws.iter_rows(min_row=2, max_row=ws.max_row)
                import_data = [
                    {"upl_name": row[0].value, "label": row[1].value}
                    for row in rows_iter
                ]

                import_subjects(import_data)
                messages.add_message(
                    request,
                    messages.SUCCESS,
                    _("Subjects were successfully imported"),
                )
                return HttpResponseRedirect(
                    reverse("admin:utters_siteconfiguration_change")
                )
            except Exception as exc:
                messages.add_message(request, messages.ERROR, exc)
    else:
        form = SubjectsImportForm()

    return TemplateResponse(request, "admin/subjects_import.html", {"form": form})
