from django import forms
from django.utils.translation import gettext_lazy as _

from antwoorden_cms.utters.models import Gemeente, GemeenteDomain, LivechatConfiguration

from .models import PageMetadata


class PageMetadataForm(forms.ModelForm):
    full_url = forms.URLField(
        label=_("URL"),
        help_text=_("De URL van de pagina"),
    )

    class Meta:
        model = PageMetadata
        fields = (
            "title",
            "full_url",
            # "page_on_site",
            "gem_enabled",
            "livechat_config",
            "direct_handover",
            "gemeente_domain",
        )
        labels = {"title": _("Paginatitel"), "gem_enabled": _("Gem ingeschakeld?")}
        widgets = {
            # "page_on_site": forms.Select(
            #     choices=(
            #         (
            #             True,
            #             "Found on site",
            #         ),
            #         (
            #             False,
            #             "Not found on site",
            #         ),
            #     )
            # ),
            "gem_enabled": forms.Select(
                choices=(
                    (
                        None,
                        "Standaardwaarde",
                    ),
                    (
                        True,
                        "Ja",
                    ),
                    (
                        False,
                        "Nee",
                    ),
                )
            ),
            "direct_handover": forms.Select(
                choices=(
                    (
                        None,
                        "Standaardwaarde",
                    ),
                    (
                        True,
                        "Ja",
                    ),
                    (
                        False,
                        "Nee",
                    ),
                )
            ),
        }

    def __init__(self, *args, **kwargs):
        gemeente_slug = kwargs.pop("gemeente_slug", None)
        super().__init__(*args, **kwargs)

        self.fields["gemeente_domain"] = forms.ModelChoiceField(
            queryset=GemeenteDomain.objects.filter(gemeente__slug=gemeente_slug)
        )
        self.fields["livechat_config"] = forms.ModelChoiceField(
            queryset=LivechatConfiguration.objects.filter(gemeente__slug=gemeente_slug),
            label=_("Livechatconfiguratie"),
            required=False,
        )

        if self.instance.pk:
            self.fields.pop("gemeente_domain")

            self.fields["full_url"].initial = f"https://{self.instance.full_url}"
            self.fields["full_url"].disabled = True
            self.fields["title"].disabled = True
            # self.fields["page_on_site"].disabled = True

            gem_default_enabled = self.instance.gemeente_domain.gem_default_enabled
            default_label = "ja" if gem_default_enabled else "nee"
            self.fields["gem_enabled"].widget = forms.Select(
                choices=(
                    (
                        None,
                        f"Standaardwaarde: {default_label}",
                    ),
                    (
                        True,
                        "Ja",
                    ),
                    (
                        False,
                        "Nee",
                    ),
                )
            )

            direct_handover_default = self.instance.gemeente_domain.direct_handover
            default_label = "ja" if direct_handover_default else "nee"
            self.fields["direct_handover"].widget = forms.Select(
                choices=(
                    (
                        None,
                        f"Standaardwaarde: {default_label}",
                    ),
                    (
                        True,
                        "Ja",
                    ),
                    (
                        False,
                        "Nee",
                    ),
                )
            )

            livechat_configs = self.instance.gemeente_domain.gemeente.livechatconfiguration_set.order_by(
                "order"
            )
            default_config = livechat_configs.first()
            if default_config:
                default_option = _(
                    "Standaard koppeling: {livechat_config_name}"
                ).format(livechat_config_name=str(default_config))
            else:
                default_option = "---"
            self.fields["livechat_config"] = forms.ModelChoiceField(
                queryset=self.instance.gemeente_domain.gemeente.livechatconfiguration_set.order_by(
                    "order"
                ),
                empty_label=default_option,
                label=_("Livechatconfiguratie"),
                required=False,
            )


class GemeenteDomainForm(forms.ModelForm):
    class Meta:
        model = GemeenteDomain
        fields = ("gem_default_enabled", "direct_handover")


class GlobalGemSettingsForm(forms.ModelForm):
    class Meta:
        model = Gemeente
        fields = (
            "main_widget_color",
            "custom_widget_css",
            "widget_title",
            "widget_subtitle",
            "popup_message",
            "widget_avatar",
            "livechat_enabled",
        )
        widgets = {
            "main_widget_color": forms.widgets.TextInput(
                attrs={"type": "color", "style": "max-width: 6em"}
            ),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if self.instance and not self.instance.livechatconfiguration_set.exists():
            self.fields.pop("livechat_enabled")


GemeenteDomainFormset = forms.inlineformset_factory(
    Gemeente, GemeenteDomain, GemeenteDomainForm, extra=0, can_delete=False
)


class SubjectsImportForm(forms.Form):
    import_file = forms.FileField(
        label=_("bestand"),
        required=True,
        help_text=_("Het Excel bestand met de onderwerpen."),
    )
