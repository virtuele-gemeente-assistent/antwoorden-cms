import logging
from urllib.parse import urlparse

from django.db import transaction

from .models import Subject

logger = logging.getLogger(__name__)


def get_domain_and_path_from_url(url):
    parsed = urlparse(url)
    domain = parsed.netloc.replace("www.", "", 1)
    path = parsed.path or "/"

    if path != "/" and path.endswith("/"):
        path = path[:-1]

    return domain, path


@transaction.atomic
def import_subjects(data):
    logger.info("Deleting all current subjects")
    Subject.objects.all().delete()

    entries = [Subject(**entry) for entry in data]
    Subject.objects.bulk_create(entries)

    logger.info("New subjects successfully imported")
