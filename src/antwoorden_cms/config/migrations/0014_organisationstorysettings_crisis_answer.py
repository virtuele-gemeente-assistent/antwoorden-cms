# Generated by Django 3.2.15 on 2023-01-26 09:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("config", "0013_subject"),
    ]

    operations = [
        migrations.AddField(
            model_name="organisationstorysettings",
            name="crisis_answer",
            field=models.BooleanField(
                default=False,
                help_text="Geeft aan of er in plaats van deze story een crisisantwoord gegeven moet worden",
            ),
        ),
    ]
