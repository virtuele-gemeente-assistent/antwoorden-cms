from django.contrib.admin.views.decorators import staff_member_required
from django.utils.decorators import method_decorator

from revproxy.views import ProxyView


@method_decorator(staff_member_required, name="dispatch")
class RevProxyView(ProxyView):
    def dispatch(self, request, host_and_port, rest=None):
        host, port = host_and_port.split(":")

        self.upstream = f"http://{host}:{port}"
        return super().dispatch(request, rest or "")
