from django.contrib.sessions.middleware import SessionMiddleware
from django.test import RequestFactory, TestCase

from ..middleware import ChatbotEnvironmentMiddleware


class ChatbotEnvironmentMiddlewareTestCase(TestCase):
    def test_middleware_set_default_production(self):
        request = RequestFactory().get("/")

        # Set the session
        SessionMiddleware().process_request(request)
        ChatbotEnvironmentMiddleware(lambda x: x).process_request(request)

        self.assertEqual(request.session["environment"], "production")

    def test_middleware_keep_staging(self):
        request = RequestFactory().get("/")

        # Set the session
        SessionMiddleware().process_request(request)
        request.session["environment"] = "staging"
        request.session.save()
        ChatbotEnvironmentMiddleware(lambda x: x).process_request(request)

        self.assertEqual(request.session["environment"], "staging")
