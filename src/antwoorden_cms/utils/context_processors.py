from django.conf import settings as django_settings

from antwoorden_cms.utters.models import Gemeente, SiteConfiguration


def settings(request):
    public_settings = (
        "GOOGLE_ANALYTICS_ID",
        "ENVIRONMENT",
        "ENVIRONMENT_NAME",
        "SHOW_ALERT",
        "PROJECT_NAME",
        "CHATBOT_URL",
        "MEDIA_URL",
    )

    context = {
        "settings": dict(
            [(k, getattr(django_settings, k, None)) for k in public_settings]
        ),
    }

    if hasattr(django_settings, "SENTRY_CONFIG"):
        context.update(dsn=django_settings.SENTRY_CONFIG.get("public_dsn", ""))

    return context


def current_gemeentes(request):
    return {
        "gemeentes": getattr(request.user, "gemeentes", Gemeente.objects.none())
        .all()
        .order_by("name")
    }


def current_gemeente(request):
    resolver_match = request.resolver_match

    if resolver_match:
        gemeente = request.resolver_match.kwargs.get("gemeente_slug", None)
        if gemeente:
            try:
                gemeente = Gemeente.objects.get(slug=gemeente)
            except Gemeente.DoesNotExist:
                gemeente = None
            return {"current_gemeente": gemeente}
    return {"current_gemeente": None}


def site_config(request):
    return {"site_config": SiteConfiguration.get_solo()}
