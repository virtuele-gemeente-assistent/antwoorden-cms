from datetime import datetime

from django.conf import settings
from django.contrib.auth import logout
from django.http import HttpResponseRedirect


class SessionExpiredMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        self.process_request(request)
        response = self.get_response(request)
        return response

    def process_request(self, request):
        now = datetime.now()

        last_activity = datetime.fromtimestamp(
            request.session.get("last_activity") or now.timestamp()
        )
        if (now - last_activity).seconds > settings.SESSION_COOKIE_AGE:
            logout(request)
            return HttpResponseRedirect("LOGIN_PAGE_URL")

        if not request.is_ajax():
            request.session["last_activity"] = now.timestamp()


class ChatbotEnvironmentMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        self.process_request(request)
        response = self.get_response(request)
        return response

    def process_request(self, request):
        if not request.session.get("environment"):
            request.session["environment"] = "production"
            request.session["chatbot_url"] = settings.CHATBOT_URL
