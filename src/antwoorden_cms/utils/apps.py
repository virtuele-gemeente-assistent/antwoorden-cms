from django.apps import AppConfig


class UtilsConfig(AppConfig):
    name = "antwoorden_cms.utils"

    def ready(self):
        from . import checks  # noqa
