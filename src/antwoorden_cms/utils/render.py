import re
from collections import OrderedDict

from django.utils.encoding import force_str
from django.utils.functional import Promise

from djangorestframework_camel_case.settings import api_settings
from djangorestframework_camel_case.util import is_iterable, underscore_to_camel
from rest_framework.utils.serializer_helpers import ReturnDict

camelize_re = re.compile(r"[a-z0-9]?_[a-z0-9]")


class DomainMetadataCamelCaseJSONRenderer(api_settings.RENDERER_CLASS):

    json_underscoreize = api_settings.JSON_UNDERSCOREIZE

    def render(self, data, *args, **kwargs):
        return super().render(
            camelize(data, **self.json_underscoreize), *args, **kwargs
        )


def camelize(data, skip=False, **options):
    # Handle lazy translated strings.
    ignore_fields = options.get("ignore_fields") or ()
    ignore_keys = options.get("ignore_keys") or ()
    skip_next_fields = options.get("skip_next_fields") or ()

    if isinstance(data, Promise):
        data = force_str(data)
    if isinstance(data, dict):
        if isinstance(data, ReturnDict):
            new_dict = ReturnDict(serializer=data.serializer)
        else:
            new_dict = OrderedDict()
        for key, value in data.items():
            if isinstance(key, Promise):
                key = force_str(key)
            if not skip and isinstance(key, str) and "_" in key:
                new_key = re.sub(camelize_re, underscore_to_camel, key)
            else:
                new_key = key

            skip_next = key in skip_next_fields or new_key in skip_next_fields

            if key not in ignore_fields and new_key not in ignore_fields:
                result = camelize(value, skip_next, **options)
            else:
                result = value
            if key in ignore_keys or new_key in ignore_keys:
                new_dict[key] = result
            else:
                new_dict[new_key] = result
        return new_dict
    if is_iterable(data) and not isinstance(data, str):
        return [camelize(item, skip, **options) for item in data]
    return data
