from django.contrib.auth import views as auth_views
from django.urls import re_path, reverse_lazy
from django.views.generic.base import TemplateView

from registration.backends.default.views import (
    ActivationView,
    RegistrationView,
    ResendActivationView,
)

urlpatterns = [
    re_path(
        r"^activate/complete/$",
        TemplateView.as_view(template_name="registration/activation_complete.html"),
        name="registration_activation_complete",
    ),
    re_path(
        r"^activate/resend/$",
        ResendActivationView.as_view(),
        name="registration_resend_activation",
    ),
    # Activation keys get matched by \w+ instead of the more specific
    # [a-fA-F0-9]{40} because a bad activation key should still get to the view;
    # that way it can return a sensible "invalid key" message instead of a
    # confusing 404.
    re_path(
        r"^activate/(?P<activation_key>\w+)/$",
        ActivationView.as_view(),
        name="registration_activate",
    ),
    re_path(
        r"^register/complete/$",
        TemplateView.as_view(template_name="registration/registration_complete.html"),
        name="registration_complete",
    ),
    re_path(
        r"^register/closed/$",
        TemplateView.as_view(template_name="registration/registration_closed.html"),
        name="registration_disallowed",
    ),
    re_path(r"^register/$", RegistrationView.as_view(), name="registration_register"),
    # Exclude login url because of 2FA
    re_path(
        r"^logout/$",
        auth_views.LogoutView.as_view(template_name="registration/logout.html"),
        name="auth_logout",
    ),
    re_path(
        r"^password/change/$",
        auth_views.PasswordChangeView.as_view(
            success_url=reverse_lazy("auth_password_change_done")
        ),
        name="auth_password_change",
    ),
    re_path(
        r"^password/change/done/$",
        auth_views.PasswordChangeDoneView.as_view(),
        name="auth_password_change_done",
    ),
    re_path(
        r"^password/reset/$",
        auth_views.PasswordResetView.as_view(
            success_url=reverse_lazy("auth_password_reset_done")
        ),
        name="auth_password_reset",
    ),
    re_path(
        r"^password/reset/complete/$",
        auth_views.PasswordResetCompleteView.as_view(),
        name="auth_password_reset_complete",
    ),
    re_path(
        r"^password/reset/done/$",
        auth_views.PasswordResetDoneView.as_view(),
        name="auth_password_reset_done",
    ),
    re_path(
        r"^password/reset/confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$",
        auth_views.PasswordResetConfirmView.as_view(
            success_url=reverse_lazy("auth_password_reset_complete")
        ),
        name="auth_password_reset_confirm",
    ),
]
