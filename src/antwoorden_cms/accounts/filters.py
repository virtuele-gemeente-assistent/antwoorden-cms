from django.contrib import admin
from django.db.models import Q
from django.utils.translation import gettext_lazy as _


class HasGitLabTokenFilter(admin.SimpleListFilter):
    title = _("Heeft gitlab token")
    parameter_name = "has_gitlab_token"

    def lookups(self, request, model_admin):
        return (
            ("1", "Yes"),
            ("0", "No"),
        )

    def queryset(self, request, queryset):
        value = self.value()
        if value == "1":
            return queryset.filter(~Q(gitlab_access_token=""))
        elif value == "0":
            return queryset.filter(gitlab_access_token="")
        return queryset
