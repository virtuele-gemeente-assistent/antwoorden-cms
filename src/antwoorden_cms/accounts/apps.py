from django.apps import AppConfig


class AccountsConfig(AppConfig):
    name = "antwoorden_cms.accounts"
