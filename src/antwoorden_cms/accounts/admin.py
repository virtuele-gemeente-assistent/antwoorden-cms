from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm as _UserChangeForm
from django.template.response import TemplateResponse
from django.utils.translation import gettext_lazy as _

from hijack_admin.admin import HijackUserAdminMixin
from two_factor.admin import AdminSiteOTPRequired

from .filters import HasGitLabTokenFilter
from .models import User


class UserChangeForm(_UserChangeForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["gemeentes"].queryset = self.fields["gemeentes"].queryset.order_by(
            "name"
        )


@admin.register(User)
class _UserAdmin(UserAdmin, HijackUserAdminMixin):
    list_display = UserAdmin.list_display + (
        "has_gitlab_token",
        "date_joined",
        "last_login",
        "hijack_field",
    )
    list_filter = UserAdmin.list_filter + (HasGitLabTokenFilter,)
    form = UserChangeForm

    fieldsets = UserAdmin.fieldsets + (
        (
            _("Gemeentes"),
            {
                "fields": ("gemeentes",),
            },
        ),
        (
            _("GitLab toegang"),
            {
                "fields": ("gitlab_access_token",),
            },
        ),
        (
            _("Speciale permissies"),
            {
                "fields": ("can_export_dialogs",),
            },
        ),
    )
    add_fieldsets = UserAdmin.add_fieldsets + (
        (
            _("Gemeentes"),
            {
                "fields": ("gemeentes",),
            },
        ),
        (
            _("GitLab toegang"),
            {
                "fields": ("gitlab_access_token",),
            },
        ),
        (
            _("Speciale permissies"),
            {
                "fields": ("can_export_dialogs",),
            },
        ),
    )

    def has_gitlab_token(self, obj):
        return bool(obj.gitlab_access_token)

    has_gitlab_token.boolean = True
    has_gitlab_token.short_description = _("Heeft GitLab token")


class CustomAdminSiteOTPRequired(AdminSiteOTPRequired):
    def login(self, request, extra_context=None):
        """
        Redirects to the site login page for the given HttpRequest.

        In case the user is_staff and does not yet have 2FA enabled, the user
        is directed to the 2FA setup wizard
        """
        if request.user.is_staff and not self.has_permission(request):
            return TemplateResponse(
                request=request,
                template="two_factor/core/otp_required.html",
                status=403,
            )
        return super().login(request, extra_context=extra_context)
