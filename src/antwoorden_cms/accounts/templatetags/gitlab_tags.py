from django import template

from ..utils import check_gitlab_token_is_valid

register = template.Library()


@register.filter
def check_gitlab_token(user):

    return check_gitlab_token_is_valid(user)
