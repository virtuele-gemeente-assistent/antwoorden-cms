# Generated by Django 2.2.11 on 2020-11-04 13:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("accounts", "0002_user_gemeentes"),
    ]

    operations = [
        migrations.AddField(
            model_name="user",
            name="gitlab_access_token",
            field=models.CharField(
                blank=True,
                help_text="Access token to connect with GitLab API",
                max_length=255,
                null=True,
            ),
        ),
    ]
