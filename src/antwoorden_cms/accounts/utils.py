import logging

from django.conf import settings
from django.core.cache import cache

import requests
from requests.exceptions import Timeout

logger = logging.getLogger(__name__)


def check_gitlab_token_is_valid(user) -> bool:

    valid = cache.get(f"{user.username}_gitlab_token_valid")

    if valid is None:
        valid = False
        if user.gitlab_access_token:
            try:
                response = requests.get(
                    "https://gitlab.com/api/v4/personal_access_tokens",
                    headers={"PRIVATE-TOKEN": user.gitlab_access_token},
                    timeout=settings.REQUESTS_TIMEOUT_SHORT,
                )

                if response.status_code == 200:
                    valid = True
            except Timeout as e:
                logger.warning(
                    "Failed to check token validate due to timeout error: " + str(e)
                )

            cache.set(f"{user.username}_gitlab_token_valid", valid, 300)

    return valid
