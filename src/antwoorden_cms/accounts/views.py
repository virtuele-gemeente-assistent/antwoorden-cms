from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.utils.translation import gettext_lazy as _
from django.views.generic import FormView, View

from two_factor.views import OTPRequiredMixin

from .forms import GitLabTokenForm
from .utils import check_gitlab_token_is_valid


class GitLabTokenView(OTPRequiredMixin, FormView):
    template_name = "accounts/gitlab_token.html"
    form_class = GitLabTokenForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["user"] = self.request.user
        return kwargs

    def get_initial(self):
        initial = super().get_initial()
        initial["gitlab_access_token"] = self.request.user.gitlab_access_token
        return initial

    def get_success_url(self):
        return self.request.path

    def form_valid(self, form):
        form.save()
        messages.success(self.request, _("GitLab token succesvol gewijzigd!"))
        return super().form_valid(form)


class GitLabTokenValidView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        valid = check_gitlab_token_is_valid(self.request.user)
        return JsonResponse({"valid": valid}, status=200)
