from django import forms
from django.core.cache import cache

from .models import User


class GitLabTokenForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ("gitlab_access_token",)
        widgets = {
            "gitlab_access_token": forms.TextInput(attrs={"style": "max-width: 18em"})
        }

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop("user")
        super().__init__(*args, **kwargs)

    def save(self, *args, **kwargs):
        self.user.gitlab_access_token = self.cleaned_data["gitlab_access_token"]
        self.user.save()

        cache.delete(f"{self.user.username}_gitlab_token_valid")
