from django.core.cache import cache
from django.urls import reverse

from django_webtest import WebTest
from requests_mock import Mocker

from antwoorden_cms.utters.tests.mixins import TwoFactorTestCaseMixin


class GitLabTokenFormTests(TwoFactorTestCaseMixin, WebTest):
    user_is_admin = False

    def test_submit_token_form(self):
        response = self.app.get(reverse("gitlab_token"), user=self.user)

        form = response.forms[1]

        form["gitlab_access_token"] = "test"
        form.submit()

        self.user.refresh_from_db()

        self.assertEqual(self.user.gitlab_access_token, "test")

    def test_token_form_login_required(self):
        response = self.app.get(reverse("gitlab_token"))

        self.assertEqual(response.status_code, 302)

        login_url = reverse("two_factor:login")
        self.assertEqual(response.location, f"{login_url}?next=/gitlab-token/")


@Mocker()
class GitLabTokenValidViewTests(TwoFactorTestCaseMixin, WebTest):
    def setUp(self):
        super().setUp()
        self.url = reverse("gitlab_token_check_valid")
        self.gitlab_url = "https://gitlab.com/api/v4/personal_access_tokens"

        self.user.gitlab_access_token = "token"
        self.user.save()

        cache.clear()

    def test_require_login(self, mock):

        response = self.app.get(self.url)

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response, reverse("two_factor:login") + f"?next={self.url}"
        )

    def test_valid_token(self, request_mock):
        request_mock.get(
            self.gitlab_url,
            status_code=200,
        )
        response = self.app.get(self.url, user=self.user)
        self.assertEqual(response.status_code, 200)

        self.assertEqual(response.json, {"valid": True})

    def test_invalid_token(self, request_mock):

        request_mock.get(
            self.gitlab_url,
            status_code=401,
        )
        response = self.app.get(self.url, user=self.user)
        self.assertEqual(response.status_code, 200)

        self.assertEqual(response.json, {"valid": False})
