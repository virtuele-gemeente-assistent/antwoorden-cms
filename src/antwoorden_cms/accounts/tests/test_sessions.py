from django.test import override_settings
from django.urls import reverse

from django_webtest import WebTest
from freezegun import freeze_time

from antwoorden_cms.utters.tests.mixins import TwoFactorTestCaseMixin


@override_settings(SESSION_COOKIE_AGE=5)
class SessionTimeoutTests(TwoFactorTestCaseMixin, WebTest):
    user_is_admin = False

    def test_session_timeout(self):
        with freeze_time("2021-01-01T12:00:00"):
            response = self.app.get(reverse("two_factor:login"))
            form = response.forms[1]

            form["auth-username"] = self.user.username
            form["auth-password"] = "test"
            response = form.submit()

        with freeze_time("2021-01-01T12:00:06"):
            response = self.app.get(reverse("password_change"))

            self.assertEqual(response.status_code, 302)
            self.assertIn(reverse("two_factor:login"), response.location)

    def test_session_timeout_after_inactivity(self):
        with freeze_time("2021-01-01T12:00:00"):
            response = self.app.get(reverse("two_factor:login"))
            form = response.forms[1]

            form["auth-username"] = self.user.username
            form["auth-password"] = "test"
            response = form.submit()
            self.assertEqual(response.status_code, 302)
            self.assertEqual(response.location, "/")

        with freeze_time("2021-01-01T12:00:04"):
            response = self.app.get(reverse("password_change"))

            self.assertEqual(response.status_code, 200)

        with freeze_time("2021-01-01T12:00:06"):
            response = self.app.get(reverse("password_change"))

            self.assertEqual(response.status_code, 200)

        with freeze_time("2021-01-01T12:00:12"):
            response = self.app.get(reverse("password_change"))

            self.assertEqual(response.status_code, 302)
            self.assertIn(reverse("two_factor:login"), response.location)
