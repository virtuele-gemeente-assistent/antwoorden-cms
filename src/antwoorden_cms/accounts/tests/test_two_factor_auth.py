from django.urls import reverse

from django_webtest import WebTest

from antwoorden_cms.utters.tests.factories import GemeenteFactory, UserFactory


class TwoFactorAuthTests(WebTest):
    def setUp(self):
        self.user = UserFactory.create()
        self.gemeente = GemeenteFactory(name="amsterdam")
        self.user.gemeente = self.gemeente
        self.user.save()

    def test_base_site_views_require_2fa(self):
        two_factor_setup_url = reverse("two_factor:setup")
        two_fa_setup_xpath = f"//a[contains(@href,'{two_factor_setup_url}')]"

        views = [
            reverse("gitlab_token"),
            reverse(
                "dialogs:dialog_export", kwargs={"gemeente_slug": self.gemeente.slug}
            ),
            reverse(
                "dialogs:dialog_list_logging_component",
                kwargs={"gemeente_slug": self.gemeente.slug},
            ),
            reverse(
                "dialogs:dialog_detail_logging_component",
                kwargs={"gemeente_slug": self.gemeente.slug, "session_id": "1234"},
            ),
            reverse(
                "dialogs:test_dialog_list_logging_component",
                kwargs={"gemeente_slug": self.gemeente.slug},
            ),
            reverse(
                "dialogs:test_dialog_detail_logging_component",
                kwargs={"gemeente_slug": self.gemeente.slug, "session_id": "1234"},
            ),
            reverse(
                "config:page_metadata_list",
                kwargs={"gemeente_slug": self.gemeente.slug},
            ),
            reverse(
                "config:page_metadata_create",
                kwargs={"gemeente_slug": self.gemeente.slug},
            ),
            reverse(
                "config:page_metadata_read",
                kwargs={"gemeente_slug": self.gemeente.slug, "page_metadata_pk": 1},
            ),
            reverse(
                "config:page_metadata_update",
                kwargs={"gemeente_slug": self.gemeente.slug, "page_metadata_pk": 1},
            ),
            reverse(
                "config:page_metadata_delete",
                kwargs={"gemeente_slug": self.gemeente.slug, "page_metadata_pk": 1},
            ),
            # reverse("gemeentes:gemeente_detail", kwargs={"gemeente_slug": self.gemeente.slug}),
            reverse(
                "gemeentes:antwoord_list_editable_required",
                kwargs={"gemeente_slug": self.gemeente.slug},
            ),
            reverse(
                "gemeentes:antwoord_list", kwargs={"gemeente_slug": self.gemeente.slug}
            ),
            reverse(
                "gemeentes:antwoord_create",
                kwargs={"gemeente_slug": self.gemeente.slug},
            ),
            reverse(
                "gemeentes:antwoord_read",
                kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": 1},
            ),
            reverse(
                "gemeentes:antwoord_update",
                kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": 1},
            ),
            reverse(
                "gemeentes:antwoord_delete",
                kwargs={"gemeente_slug": self.gemeente.slug, "antwoord_pk": 1},
            ),
            reverse(
                "gemeentes:hyperlink_list", kwargs={"gemeente_slug": self.gemeente.slug}
            ),
            reverse(
                "gemeentes:hyperlink_list_editable",
                kwargs={"gemeente_slug": self.gemeente.slug},
            ),
            reverse(
                "gemeentes:hyperlink_create",
                kwargs={"gemeente_slug": self.gemeente.slug},
            ),
            reverse(
                "gemeentes:hyperlink_read",
                kwargs={"gemeente_slug": self.gemeente.slug, "hyperlink_pk": 1},
            ),
            reverse(
                "gemeentes:hyperlink_update",
                kwargs={"gemeente_slug": self.gemeente.slug, "hyperlink_pk": 1},
            ),
            reverse(
                "gemeentes:hyperlink_delete",
                kwargs={"gemeente_slug": self.gemeente.slug, "hyperlink_pk": 1},
            ),
        ]

        for url in views:
            with self.subTest(url=url):
                response = self.app.get(url, user=self.user, status=403)

                self.assertEquals(response.status_code, 403)
                self.assertEquals(len(response.lxml.xpath(two_fa_setup_xpath)), 1)

    def test_admin_site_login_2fa_prompt(self):
        user = UserFactory.create(is_staff=True, is_superuser=True)
        two_factor_setup_url = reverse("two_factor:setup")
        two_fa_setup_xpath = f"//a[contains(@href,'{two_factor_setup_url}')]"

        response = self.app.get(reverse("admin:login"), user=user, status=403)

        self.assertEquals(response.status_code, 403)
        self.assertEquals(len(response.lxml.xpath(two_fa_setup_xpath)), 1)

    def test_admin_site_views_without_2fa(self):
        user = UserFactory.create(is_staff=True, is_superuser=True)

        views = [
            reverse("admin:index"),
            reverse("admin:import_default_responses"),
            reverse("admin:import_hyperlinks"),
            reverse("admin:export_hyperlinks"),
            reverse("admin:import_subjects"),
        ]

        for url in views:
            with self.subTest(url=url):
                response = self.app.get(url, user=user)

                self.assertEquals(response.status_code, 302)
                self.assertIn("login", response.location)

    def test_admin_site_no_staff(self):
        views = [
            reverse("admin:index"),
            reverse("admin:import_default_responses"),
            reverse("admin:import_hyperlinks"),
            reverse("admin:export_hyperlinks"),
            reverse("admin:import_subjects"),
        ]

        for url in views:
            with self.subTest(url=url):
                response = self.app.get(url, user=self.user, status=302)

                self.assertEquals(response.status_code, 302)
                self.assertIn("login", response.location)

    def test_home_accessible_without_2fa(self):
        response = self.app.get(reverse("gemeentes:home"), user=self.user)
        self.assertEqual(response.status_code, 200)
