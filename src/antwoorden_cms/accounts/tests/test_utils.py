from django.core.cache import cache
from django.test import TestCase

from requests_mock import Mocker

from antwoorden_cms.utters.tests.factories import UserFactory

from ..utils import check_gitlab_token_is_valid


@Mocker()
class CheckGitlabTokenIsValidTest(TestCase):
    def setUp(self):
        self.url = "https://gitlab.com/api/v4/personal_access_tokens"

        cache.clear()

    def test_no_token(self, request_mock):

        user = UserFactory()
        self.assertFalse(check_gitlab_token_is_valid(user))
        self.assertIsNone(cache.get(f"{user.username}_gitlab_token_valid"))

    def test_valid_token(self, request_mock):
        user = UserFactory(gitlab_access_token="valid-token")
        request_mock.get(
            self.url,
            status_code=200,
        )
        self.assertTrue(check_gitlab_token_is_valid(user))
        self.assertTrue(cache.get(f"{user.username}_gitlab_token_valid"))

    def test_invalid_token(self, request_mock):
        user = UserFactory(gitlab_access_token="invalid-token")

        request_mock.get(
            self.url,
            status_code=401,
        )
        self.assertFalse(check_gitlab_token_is_valid(user))
        self.assertFalse(cache.get(f"{user.username}_gitlab_token_valid"))

    def test_cache(self, request_mock):
        user = UserFactory(gitlab_access_token="invalid-token")

        cache.set(f"{user.username}_gitlab_token_valid", True)
        request_mock.get(
            self.url,
            status_code=401,
        )
        self.assertTrue(check_gitlab_token_is_valid(user))
        self.assertTrue(cache.get(f"{user.username}_gitlab_token_valid"))
        self.assertFalse(request_mock.called)
