const statsData = document.getElementById("popularIntentStats");

if (statsData) {
    const data = JSON.parse(statsData.textContent);
    const transposedData = data[0].map((_, colIndex) => data.map(row => row[colIndex]));

    const labels = transposedData[0];

    const canvas = document.getElementById('myChart')

    var ctx = canvas.getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'horizontalBar',

        // The data for our dataset
        data: {
            labels: labels,

            datasets: [{
                data: transposedData[1],
                backgroundColor: "#358500"
            }]
        },


        // Configuration options go here
        options: {
            responsive: true,
            maintainAspectRatio: false,
            legend: {
                display: false
            },
            tooltip: {
                position: 'average',
            },
            scales: {
                xAxes: [{
                    ticks: {
                        min: 0,
                    }
                }],
                yAxes: [{
                    barThickness: 20,
                }]
            }
        }
    });

    if(transposedData[1].length < 10) {
        canvas.parentNode.style.height = "600px";
    } else {
        canvas.parentNode.style.height = `${transposedData[1].length*40}px`;
    }

    canvas.addEventListener("click", (e) => {
        let elements = chart.getElementsAtEvent(e);
        if(elements.length) {
            window.location = `${window.location}${elements[0]._model.label}`
        }
    });
}
