const exportDialogsButton = document.getElementById("export-dialogs");
const exportFrom = document.querySelector("#export-from");
const exportTo = document.querySelector("#export-to");

if(exportDialogsButton && exportFrom && exportTo) {
    exportDialogsButton.addEventListener("click", (e) => {
        let params = new URLSearchParams();
        if(exportFrom.value) params.set("timestamp__gte", exportFrom.value)
        if(exportTo.value) params.set("timestamp__lt", exportTo.value)
        window.location = `${exportDialogsButton.dataset.url}?${params.toString()}`
    })
}