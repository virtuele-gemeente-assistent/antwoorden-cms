const statsData = document.getElementById("numChatsPerYearStats");

if (statsData) {
    const data = JSON.parse(statsData.textContent);
    const transposedData = data[0].map((_, colIndex) => data.map(row => row[colIndex]));

    const labels = transposedData[0];

    const categories = ['Conversaties'];
    const colors = ['#3474eb'];

    datasets = []
    for(var i=0; i<categories.length; i++) {
        datasets.push({
            label: categories[i],
            data: transposedData[i+1],
            backgroundColor: colors[i]
        })
    }

    var ctx = document.getElementById('myChart').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels: labels,
            datasets: datasets
        },

        // Configuration options go here
        options: {
            scales: {
                x: { stacked: true },
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                    }
                }]
            },
            legend: {
                position: 'right',
                reverse: true,
            },
            tooltip: {
                position: 'average',
            }
        }
    });
}
