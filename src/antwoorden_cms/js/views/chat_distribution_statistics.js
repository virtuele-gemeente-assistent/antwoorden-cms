const statsData = document.getElementById("chatDistributionStats");

if (statsData) {
    const data = JSON.parse(statsData.textContent)[0];

    var ctx = document.getElementById('myChart').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels: Array(data.length).fill().map((_, i) => i),
            datasets: [{
                label: "Gemiddeld aantal gebruikersberichten",
                data: data,
                fill: false,
                borderColor: "#32a852",
                backgroundColor: "#1e6b33",
                cubicInterpolationMode: "monotone",
            }]
        },

        // Configuration options go here
        options: {
            scales: {
                xAxes: [{
                    ticks: {
                        callback: function(value, index, values) {
                            return `${("0"+index).slice(-2)}:00`
                        }
                    }
                }]
            },
        }
    });
}
