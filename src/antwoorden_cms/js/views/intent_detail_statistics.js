const statsData = document.getElementById("intentDetailStats");

if (statsData) {
    const data = JSON.parse(statsData.textContent);
    const transposedData = data[0].map((_, colIndex) => data.map(row => row[colIndex]));

    const labels = transposedData[0];

    const canvas = document.getElementById('myChart')

    var ctx = canvas.getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'horizontalBar',

        // The data for our dataset
        data: {
            labels: labels,

            datasets: [{
                data: transposedData[1],
                backgroundColor: "#3366AA",
            }]
        },


        // Configuration options go here
        options: {
            responsive: true,
            maintainAspectRatio: false,
            legend: {
                display: false
            },
            tooltip: {
                position: 'average',
            },
            scales: {
                xAxes: [{
                    ticks: {
                        min: 0,
                    }
                }],
                yAxes: [{
                    maxBarThickness: 40,
                }]
            },
        }
    });

    if(transposedData[1].length < 10) {
        canvas.parentNode.style.height = "600px";
    } else {
        canvas.parentNode.style.height = `${transposedData[1].length*50}px`;
    }
}
