const graphDatepicker = document.getElementById("graph-datepicker");

if(graphDatepicker) {
    // Only allow dates that are not in the future
    graphDatepicker.max = new Date().toISOString().split("T")[0];

    let params = new URLSearchParams(window.location.search);
    if(params.get("date")) graphDatepicker.value = params.get("date");

    graphDatepicker.addEventListener("change", (e) => {
        window.location = `${graphDatepicker.dataset.url}&date=${e.target.value}`
    })
}