import {FIXED_VALUES} from '../components/stories.js';
import {getCookie} from '../components/dialogs.js';

let testResultToggle = document.querySelector("#toggle-test-results");
let currentMunicipality = JSON.parse((document.querySelector("#municipality") || {}).textContent || '""')

if(testResultToggle) {
    testResultToggle.addEventListener("click", (e) => {
        let results = document.querySelectorAll(".story-test-results");
        for(let result of results) {
            result.classList.toggle("d-none");
        }

        let buttonRows = document.querySelectorAll(".story-utter-buttons");
        for(let buttonRow of buttonRows) {
            buttonRow.classList.toggle("d-none");
        }
    });
}

let allStoriesToggle = document.querySelector("#toggle-all-stories");
if(allStoriesToggle) {
    allStoriesToggle.addEventListener("change", (e) => {
        if(allStoriesToggle.value === "empty") return

        let enabled = allStoriesToggle.value !== "disabled";
        let handoverToOrganisation = ""
        let crisis_answer = allStoriesToggle.value === "crisis_answer";
        let parking_answer = allStoriesToggle.value === "parking_answer";
        let handover_livechat = allStoriesToggle.value === "handover_livechat";

        if(!FIXED_VALUES.includes(allStoriesToggle.value)) {
            handoverToOrganisation = allStoriesToggle.value;
        }

        let toggleStorySelects = document.querySelectorAll(".toggle-story");
        let postData = [];
        for(let select of toggleStorySelects) {
            postData.push({
                story_id: select.dataset.storyId,
                enabled: enabled,
                handover_to_organisation: handoverToOrganisation,
                crisis_answer: crisis_answer,
                handover_livechat: handover_livechat,
                parking_answer: parking_answer
            });
        }

        fetch(`/api/${currentMunicipality}/stories/_bulk_create_or_update/`, {
            method: "POST",
            body: JSON.stringify(postData),
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': getCookie("csrftoken"),
            },
        })
            .then((response) => {
                if(response.status === 204) {
                    for(let select of toggleStorySelects) {
                        select.value = allStoriesToggle.value;

                        select.parentElement.querySelector(".story-toggle-title").classList.remove("d-none");
                        select.parentElement.querySelector(".story-toggle-title-undefined").classList.add("d-none");

                        let emptyOption = select.querySelector("[value='empty']")
                        if(emptyOption) {
                            emptyOption.remove()
                        }

                        select.dataset.hasObj = "true";
                    }
                }
            })
    });
}
