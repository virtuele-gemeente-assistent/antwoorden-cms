const statsData = document.getElementById("numChatsStats");

if (statsData) {
    const data = JSON.parse(statsData.textContent);
    const transposedData = data[0].map((_, colIndex) => data.map(row => row[colIndex]));

    const labels = transposedData[0];
    labels.forEach(function(part, index, labels) {
        labels[index] = new Date(labels[index]).toLocaleString('nl-NL', { month: 'long' });
    });

    const categories = ['Conversaties', 'Livechat', 'Reparaties'];
    const colors = ['#3474eb', '#34eb5c', '#eb5234'];

    datasets = []
    for(var i=0; i<categories.length; i++) {
        datasets.push({
            label: categories[i],
            data: transposedData[i+1],
            backgroundColor: colors[i]
        })
    }

    var ctx = document.getElementById('myChart').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels: labels,
            datasets: datasets
        },

        // Configuration options go here
        options: {
            scales: {
                x: { stacked: true },
            },
            legend: {
                position: 'right',
                reverse: true,
            },
            tooltip: {
                position: 'average',
            }
        }
    });
}
