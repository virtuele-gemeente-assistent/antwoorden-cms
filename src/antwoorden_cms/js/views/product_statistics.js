const statsData = document.getElementById("productStats");

if (statsData) {
    const data = JSON.parse(statsData.textContent)
    for(let j = 0; j < data.length; j++) {
        const currentMonthData = data[j][1];

        if(!currentMonthData.length) continue;

        const transposedData = currentMonthData[0].map((_, colIndex) => currentMonthData.map(row => row[colIndex]));

        const labels = transposedData[0];
        labels.forEach(function(part, index, labels) {
            if(labels[index] === null) {
                labels[index] = "Generiek";
            }
        });

        const categories = ['0-7', '7-8', '8-9', '9-10']
        const colors = ['#ff0000', '#76b500', '#49a300', '#358500']

        datasets = []
        for(var i=0; i<categories.length; i++) {
            datasets.push({
                label: categories[i],
                data: transposedData[i+1],
                backgroundColor: colors[i]
            })
        }

        var ctx = document.getElementById(`myChart-${j+1}`).getContext('2d');
        var chart = new Chart(ctx, {
            // The type of chart we want to create
            type: 'bar',

            // The data for our dataset
            data: {
                labels: labels,
                datasets: datasets
            },

            // Configuration options go here
            options: {
                scales: {
                    xAxes: [{ stacked: true }],
                    yAxes: [{ stacked: true }]
                },
                legend: {
                    position: 'right',
                    reverse: true,
                },
                tooltip: {
                    position: 'average',
                }
            }
        });
    }
}
