// THIS IS A GULP GENERATED FILE!!!

import './chat_distribution_statistics';
import './intent_detail_statistics';
import './num_chats_statistics';
import './num_chats_per_year_statistics';
import './popular_intent_statistics';
import './product_statistics';
import './stories_list';
import './dynamic_statistics';
import './dialog_export';

// Ensure that the widget resets
window.sessionStorage.removeItem("chat_session");
window.sessionStorage.removeItem("municipality");
window.sessionStorage.removeItem("obi-chat-customer-guid");
window.sessionStorage.removeItem("obi-chat-guid");
