var navLogo = document.querySelector(".navbar-brand-full");
var navLogoSmall = document.querySelector(".navbar-brand-minimized");

function resetSession() {
    if (window.sessionStorage) {
        window.sessionStorage.clear();
    }
    window.location = window.location;
}
navLogo.addEventListener('click', resetSession, false);
navLogoSmall.addEventListener('click', resetSession, false);
