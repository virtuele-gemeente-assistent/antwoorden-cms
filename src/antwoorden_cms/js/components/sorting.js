let sortingLinks = document.querySelectorAll(".sorting-link");

for(link of sortingLinks) {
    link.addEventListener("click", (e) => {
        let sortOrder;
        if(!e.target.classList.contains("asc")) {
            sortOrder = "asc";
        } else {
            sortOrder = "desc";
        }

        const urlParams = new URLSearchParams(window.location.search);
        urlParams.set("sort", e.target.dataset.paramName);
        urlParams.set("order", sortOrder);
        window.location.search = urlParams;
    });
}
