let toggleLinks = document.querySelectorAll('.toggle-link');

for(var link of toggleLinks) {
    link.addEventListener('click', (e) => {
        const urlParams = new URLSearchParams(window.location.search);

        let initial = urlParams.get(e.target.dataset.paramName);
        let toggle ;
        toggle = initial === 'false';

        urlParams.set(e.target.dataset.paramName, toggle);
        window.location.search = urlParams;
    });
}
