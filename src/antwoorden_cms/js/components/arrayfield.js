// Adapted from https://github.com/gradam/django-better-admin-arrayfield
import $ from 'jquery';
import 'select2';

window.addEventListener('load', function () {
    let item_count = 1;

    function addRemoveEventListener(widgetElement) {
        widgetElement.querySelectorAll('.remove').forEach(element => {
            element.addEventListener('click', () => {
                element.parentNode.remove();
            });
        });
    }

    function addSelect2(widgetElement) {
        widgetElement.querySelectorAll('select').forEach(element => {
            $(element).select2();
        });
    }

    function initializeWidget(widgetElement) {
        console.log("bongos");

        const initialElement = widgetElement.querySelector('.array-item');
        const elementTemplate = initialElement.cloneNode(true);
        const parentElement = initialElement.parentElement;

        if (initialElement.getAttribute('data-isNone')) {
            initialElement.remove();
            elementTemplate.removeAttribute('data-isNone');
            elementTemplate.removeAttribute('style');
        }
        addRemoveEventListener(widgetElement);

        widgetElement.querySelector('.add-array-item').addEventListener('click', () => {
            const currentItemCount = widgetElement.querySelectorAll(".array-item").length
            if(widgetElement.dataset.maxElements && currentItemCount >= parseInt(widgetElement.dataset.maxElements)) return

            item_count++;
            const newElement = elementTemplate.cloneNode(true);
            const id_parts = newElement.querySelector('select').getAttribute('id').split('_');
            const id = id_parts.slice(0, -1).join('_') + '_' + String(item_count - 1);
            newElement.querySelector('select').setAttribute('id', id);
            newElement.querySelector('select').value = '';
            addRemoveEventListener(newElement);
            parentElement.appendChild(newElement);

            $(newElement.querySelector('select')).select2();
        });
    }

    document.querySelectorAll(".array-field-widget").forEach((widgetElement) => {
        initializeWidget(widgetElement);
        addSelect2(widgetElement);
    })

    // django.jQuery(".dynamic-array-widget").not(".empty-form .dynamic-array-widget").each(
    //     (index, widgetElement) => initializeWidget(widgetElement)
    // );

    // django.jQuery(document).on('formset:added', function(event, $row, formsetName) {
    //     $row[0].querySelectorAll(".dynamic-array-widget").forEach(
    //         widgetElement => initializeWidget(widgetElement)
    //     );
    // });
});
