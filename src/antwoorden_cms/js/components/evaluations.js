function defineConditionalFields(conditionFieldIdMapping, dependentFieldIds) {
    let conditionFieldMapping = [];
    for(let [id, value] of conditionFieldIdMapping) {
        conditionFieldMapping.push([document.querySelector(id), value]);
    }

    let dependentFields = [];
    for (let dependentField of dependentFieldIds) {
        dependentFields.push(document.querySelector(dependentField));
    }

    function toggleField(field, hidden) {
        if(hidden) field.classList.add("d-none");
        else field.classList.remove("d-none");
    }

    let [firstField, firstValue] = conditionFieldMapping[0];

    if(firstField) {
        if(firstField.checked) {
            for(let field of dependentFields) {
                toggleField(field, !firstValue);
            }
        } else {
            for(let field of dependentFields) {
                toggleField(field, firstValue);
            }
        }
    }

    for(let [conditionField, value] of conditionFieldMapping) {
        if(!conditionField) continue;

        conditionField.addEventListener("change", (e) => {
            for(let field of dependentFields) {
                toggleField(field, !value)
            }
        });
    }
}

defineConditionalFields(
    [
        ["#id_escalation_to_livechat_1", true],
        ["#id_escalation_to_livechat_2", false]
    ],
    ["#div_id_livechat_reason", "#div_id_successful_livechat_escalation"],
)

defineConditionalFields(
    [
        ["#id_successful_livechat_escalation_1", false],
        ["#id_successful_livechat_escalation_2", false],
        ["#id_successful_livechat_escalation_3", true],
    ],
    ["#div_id_failed_escalation_reason"],
)

defineConditionalFields(
    [
        ["#id_citizen_properly_assisted_1", false],
        ["#id_citizen_properly_assisted_2", true],
        ["#id_citizen_properly_assisted_3", false]
    ],
    ["#div_id_critical_error"],
)
