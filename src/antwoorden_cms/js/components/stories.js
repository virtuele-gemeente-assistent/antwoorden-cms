import {getCookie} from './dialogs.js';

let toggleStorySelects = document.querySelectorAll(".toggle-story");
let currentMunicipality = JSON.parse((document.querySelector("#municipality") || {}).textContent || '""');

const FIXED_VALUES = [
    "enabled",
    "disabled",
    "crisis_answer",
    "handover_livechat",
    "parking_answer"
];

function setStoryData(objectExists, municipality, storyId, enabled, handoverToOrganisation, crisis_answer, handover_livechat, parking_answer) {
    if(objectExists) {
        // DO PATCH
        fetch(`/api/${municipality}/stories/${storyId}/`, {
            method: "PATCH",
            body: JSON.stringify({enabled: enabled, handover_to_organisation: handoverToOrganisation, crisis_answer: crisis_answer, handover_livechat: handover_livechat, parking_answer:parking_answer}),
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': getCookie("csrftoken"),
            },
        })
            .then((response) => response.json())
            .then((data) => console.log(data));
    } else {
        // DO POST
        fetch(`/api/${municipality}/stories/${storyId}/`, {
            method: "POST",
            body: JSON.stringify({enabled: enabled, handover_to_organisation: handoverToOrganisation, crisis_answer: crisis_answer, handover_livechat: handover_livechat, parking_answer:parking_answer}),
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': getCookie("csrftoken"),
            },
        })
            .then((response) => response.json())
            .then((data) => console.log(data));
    }
}

for(let select of toggleStorySelects) {
    select.addEventListener("change", (e) => {
        let storyId = e.target.dataset.storyId;
        // make request
        let enabled = select.value !== "disabled";
        let handoverToOrganisation = "";
        let crisis_answer = select.value === "crisis_answer";
        let parking_answer = select.value === "parking_answer";
        let handover_livechat = select.value === "handover_livechat";

        if(!FIXED_VALUES.includes(select.value)) {
            handoverToOrganisation = select.value;
        }

        setStoryData(e.target.dataset.hasObj, currentMunicipality, storyId, enabled, handoverToOrganisation, crisis_answer, handover_livechat, parking_answer);

        // set hasObj
        select.dataset.hasObj = "true";

        select.parentElement.querySelector(".story-toggle-title").classList.remove("d-none");
        select.parentElement.querySelector(".story-toggle-title-undefined").classList.add("d-none");

        let emptyOption = select.querySelector("[value='empty']")
        if(emptyOption) {
            emptyOption.remove();
        }
    });
}

export {FIXED_VALUES};
