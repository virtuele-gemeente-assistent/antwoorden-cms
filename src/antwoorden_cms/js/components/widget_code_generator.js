import { copy } from './dialogs.js';

function createModal(data, modalId) {
    location.hash = modalId;

    const outerDiv = document.createElement("div");
    const closeButton = document.createElement("a");
    closeButton.href = "#close";
    closeButton.title = "Close"
    closeButton.className = "close";
    closeButton.textContent = "X";
    outerDiv.appendChild(closeButton);

    const copyHeadButton = document.createElement("button");
    copyHeadButton.id = "copy-head-button";
    copyHeadButton.className = "btn btn-primary m-3";
    copyHeadButton.textContent = "Kopieer widget styling";
    copyHeadButton.addEventListener("click", function() {
        copy("head-snippet");
    });

    // Only show headSnippet if stylesheets need to be included (loader script is not used)
    if(data[0]) {
        const headSnippet = document.createElement("pre");
        headSnippet.id = "head-snippet";
        headSnippet.className = "widget-snippet";
        headSnippet.textContent = data[0]

        const headDiv = document.createElement("div");
        headDiv.appendChild(copyHeadButton)
        const descriptionHead = document.createElement("h4")
        descriptionHead.textContent = "Kopieer deze code in de `<head>` van de website";
        headDiv.appendChild(descriptionHead);
        headDiv.appendChild(headSnippet);
        outerDiv.appendChild(headDiv);
    }

    const copyBodyButton = document.createElement("button");
    copyBodyButton.id = "copy-body-button";
    copyBodyButton.className = "btn btn-primary m-3";
    copyBodyButton.textContent = "Kopieer widget scripts";
    copyBodyButton.addEventListener("click", function() {
        copy("body-snippet");
    });

    const bodySnippet = document.createElement("pre");
    bodySnippet.id = "body-snippet";
    bodySnippet.className = "widget-snippet";
    bodySnippet.textContent = data[1]

    const bodyDiv = document.createElement("div");
    bodyDiv.appendChild(copyBodyButton)
    const descriptionBody = document.createElement("h4");
    descriptionBody.textContent = "Kopieer deze code onderaan de `<body>` van de website";
    bodyDiv.appendChild(descriptionBody);
    bodyDiv.appendChild(bodySnippet)

    outerDiv.appendChild(bodyDiv);

    document.getElementById(modalId).innerHTML = "";
    document.getElementById(modalId).appendChild(outerDiv);
}

window.addEventListener("load", function() {
    const generateWidgetCodeButton = document.getElementById("generate-widget-code");
    if(generateWidgetCodeButton) {
        generateWidgetCodeButton.addEventListener("click", function(e) {
            e.preventDefault();
            console.log(this.dataset.widgetBodyUrl)
            Promise.all([
                fetch(this.dataset.widgetHeadUrl).then(response => response.text()),
                fetch(this.dataset.widgetBodyUrl).then(response => response.text()),
            ]
            ).then(results => createModal(results, "openModal-widget-code"));
        });
    }
});
