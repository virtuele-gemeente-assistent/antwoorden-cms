import {getCookie} from './dialogs.js';

const deleteButtons = document.querySelectorAll(".delete-button");

for(let i = 0; i < deleteButtons.length; i++) {
    deleteButtons[i].addEventListener("click", (e) => {
        let confirmationText = "Weet je het zeker?";
        if(e.currentTarget.classList.contains("delete-response")) {
            confirmationText = "Je staat op het punt om je lokale variant te verwijderen. Hierdoor zal Gem voortaan het landelijke standaardantwoord geven. Weet je het zeker?";
        }
        if(!confirm(confirmationText)) e.preventDefault();
    });
}

const acceptButtons = document.querySelectorAll(".accept-button");

for(let i = 0; i < acceptButtons.length; i++) {
    acceptButtons[i].addEventListener("click", (e) => {
        if(confirm("Weet je het zeker?")) {
            let element = e.currentTarget;
            fetch(element.dataset.targetUrl, {
                method: 'POST',
                mode: 'same-origin',
                cache: 'no-cache',
                credentials: 'same-origin',
                headers: {
                  'Content-Type': 'application/x-www-form-urlencoded',
                  'X-CSRFToken': getCookie("csrftoken"),
                  "X-Requested-With": "XMLHttpRequest"
                },
                referrerPolicy: 'same-origin',
                body: "csrfmiddlewaretoken=" + encodeURIComponent(getCookie("csrftoken")) + "&response=" + encodeURIComponent(element.dataset.response)
            }).then(response => {
                if(response.ok) {
                    let warning = element.parentElement.parentElement.querySelector(".utter-changed-warning");
                    if(warning) {
                        warning.remove();
                        element.remove();
                    }
                }
            });
        }
    });
}

const toggleProdDataButtons = document.querySelectorAll(".toggle-prod-data");

for(let i = 0; i < toggleProdDataButtons.length; i++) {
    let currentButton = toggleProdDataButtons[i];
    currentButton.addEventListener("click", (e) => {
        let currentText = currentButton.textContent;
        currentButton.textContent = currentButton.dataset.untoggleText;
        currentButton.dataset.untoggleText = currentText;

        let target = document.querySelector(toggleProdDataButtons[i].dataset.target);
        target.classList.toggle("d-none");
    });
}