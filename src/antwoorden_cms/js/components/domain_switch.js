let domainSwitcher = document.querySelector("#domain-switch");

if(domainSwitcher) {
    domainSwitcher.addEventListener("change", (e) => {
        let url = new URL(window.location);
        url.searchParams.delete("page");
        url.searchParams.set("domain", e.target.value);
        window.location = url;
    })
}
