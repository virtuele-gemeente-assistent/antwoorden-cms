// https://docs.djangoproject.com/en/3.1/ref/csrf/#ajax
export function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
const csrftoken = getCookie('csrftoken');

export function copy(selector) {
    const element = document.getElementById(selector);
    var range = document.createRange();
    range.selectNodeContents(element);
    var sel = window.getSelection();
    sel.removeAllRanges();
    sel.addRange(range);
    document.execCommand("copy")
}

// Source: https://stackoverflow.com/a/6234804
function escapeHtml(unsafe) {
    return unsafe
         .replace(/&/g, "&amp;")
         .replace(/</g, "&lt;")
         .replace(/>/g, "&gt;")
         .replace(/"/g, "&quot;")
         .replace(/'/g, "&#039;");
 }

function createModal(data, modalId) {
    location.hash = modalId;
    const splitPath = window.location.pathname.split("/");
    const senderId = splitPath[splitPath.length - 1];
    const element = `<div><a href="#close" title="Close" class="close">X</a><div>
                    <button class="btn btn-primary m-3" id="copy-button">Kopieer data</button>
                    <a class="btn btn-success" href="https://gitlab.com/virtuele-gemeente-assistent/gem/-/issues/new?issue[title]=Antwoord%20CMS%20Issue%20${senderId}" target="_blank">Maak issue aan</a>
                    <pre id="exported_data">${data}</pre></div></div>`;
    document.getElementById(modalId).innerHTML = element;
    document.getElementById("copy-button").addEventListener("click", function() {
        copy("exported_data");
    });
}

async function checkTokenValidity() {

    try{
        const response = await fetch('/gitlab-token/check_valid');
        const data = await response.json();
        return Boolean(data.valid);
    }
    catch (Error){
        return false;
    }
}

/* Modal in which the user can edit the text, before creating a GitLab issue */
function createEditModal(data, modalId) {
    location.hash = modalId;
    const opening_element = `<div><a href="#close" title="Close" class="close">X</a><div>`;
    const closing_element = `<div class="form-group modal-form"><textarea id="gitlab-issue-data" class="textarea form-control" cols="100" rows="12">${data}</textarea></div></div></div>`;
    let middle_element = `<button class="btn btn-primary m-3" id="submit-button">Stuur issue naar GitLab</button>`;

    checkTokenValidity().then(valid =>{
        if (!valid) {
        middle_element =
            `<button class="btn btn-primary m-3" id="submit-button" disabled>Stuur issue naar GitLab</button>
            <div class="alert alert-danger" role="alert">
                Je Gitlab Access Token is niet (meer) geldig. Je kunt nu geen issues aanmaken in GitLab. <br>
                Hoe los je dit op?
            <ol>
                <li>Maak een nieuwe Gitlab Access Token aan. Zie de <a target="_blank" href="https://gitlab.com/virtuele-gemeente-assistent/gem/-/wikis/HANDLEIDING-Gitlab-Access-Token-vernieuwen">handleiding</a>.</li>
                <li>Voer deze nieuwe Gitlab Access Token in in je <a href="/gitlab-token/">profiel</a>.</li>
            </ol>
            </div>`;
        }

        document.getElementById(modalId).innerHTML = opening_element + middle_element+ closing_element;
        document.getElementById('submit-button').addEventListener('click', function() {
            postGitlabIssue();
        });
    });
}

function eventsToMarkdown() {
    var resultList = document.getElementById("dialog-list");
    var selectedRows = resultList.getElementsByClassName("selected");

    if(selectedRows.length == 0) {
        return
    }

    var res = `## **Korte beschrijving van het probleem:** ...\n\n**Chatlog:** ${document.URL}\n\n`;

    let chatStartedOnPage = document.querySelector("#chat-started-on-page-link");
    if(chatStartedOnPage) {
        res += `**Chat gestart op pagina:** ${chatStartedOnPage.href}\n\n`
    }

    res += `|Timestamp|Message Gem|Message User|Intent name (Confidence)|\n|---|---|---|---|`;
    for(var i = 0; i < selectedRows.length; i++) {
        var output = [];
        for(var j = 0; j < selectedRows[i].children.length; j++) {
            var currentChild = selectedRows[i].children[j];

            if(currentChild.textContent.replace(/\s/g, '').length) {
                output.push(currentChild.innerHTML.replace(/\n|\r|\t/g, "").replace(/^\s+|\s+$/g, ""))
            } else {
                output.push("");
            }
        }
        res += `\n|${output.join(" | ")}|`;
    }

    // Add expandable logging details to issue
    let loggingDetails = escapeHtml("\n\n<details><summary><b>Uitgebreide logging<b>:</summary>")
                         + `\n\n \`\`\`json\n${document.querySelector(".raw-event-data").innerHTML}\`\`\`\n`
                         + escapeHtml("</details>");

    res += loggingDetails;

    return res
    // const splitPath = window.location.pathname.split("/");
    // const senderId = splitPath[splitPath.length - 1];
    // const baseUrl = "https://gitlab.com/virtuele-gemeente-assistent/gem/-/issues/new";
    // const url = `${baseUrl}?issue[title]=Antwoord%20CMS%20Issue%20${senderId}&issue[description]=${encodeURIComponent(res)}`
    // window.open(url)
}

/* Send the text from the textarea to GitLab */
function postGitlabIssue() {
    let data = document.getElementById("gitlab-issue-data").value;

    data = data.replaceAll("<br>", "\n");
    data = data.replaceAll(/<\/?pre>/gi, "")

    const splitUrl = location.pathname.split("/");
    const senderId = splitUrl[splitUrl.length - 1]

    const request = new Request(
        window.location,
        {headers: {'X-CSRFToken': csrftoken}}
    );
    fetch(request, {
        method: 'POST',
        mode: 'same-origin',  // Do not send CSRF token to another domain.
        body: JSON.stringify({"description": data, "sender_id": senderId})
    }).then(response => response.text()).then(data => { window.open(JSON.parse(data).url) });

    location.hash = "close";
}

window.addEventListener("load", function() {
    const createIssueButton = document.getElementById("create_issue");
    const eventsToMarkdownButton = document.getElementById("events_to_markdown");
    if(createIssueButton) {
        createIssueButton.addEventListener("click", function() {
            let res = eventsToMarkdown();

            if(!res) {
                return
            }

            createEditModal(res, "openModal-export");
        });
    } else if(eventsToMarkdownButton) {
        eventsToMarkdownButton.addEventListener("click", function() {
            createModal(eventsToMarkdown(), "openModal-export");
        });
    }
});

window.addEventListener("load", function() {
    const checkBoxes = document.getElementsByClassName("event-select");
    for(var i=0; i<checkBoxes.length; i++) {
        checkBoxes[i].addEventListener("click", function(e) {
            e.target.parentNode.parentNode.classList.toggle("selected");
        });
    }

    const selectAll = document.getElementById("event-select-all");

    if(selectAll) {
        selectAll.addEventListener("click", function(){
            for(var i=0; i<checkBoxes.length; i++) {
                checkBoxes[i].click();
            }
        });
    }
});
