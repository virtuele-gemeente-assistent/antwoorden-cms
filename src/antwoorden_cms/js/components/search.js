const searchField = document.getElementById("search-field");

if(searchField) {
    const searchButton = document.getElementById("search-submit");

    searchButton.addEventListener("click", (e) => {
        let url = new URL(window.location);
        url.searchParams.delete("page");
        url.searchParams.set("search", searchField.value.toLowerCase());
        window.location = url;
    });

    searchField.addEventListener("keypress", (e) => {
        if(e.code === "Enter") {
            let url = new URL(window.location);
            url.searchParams.delete("page");
            url.searchParams.set("search", searchField.value.toLowerCase());
            window.location = url;
        }
    });

    const clearButton = document.getElementById("search-clear");

    clearButton.addEventListener("click", (e) => {
        let url = new URL(window.location);
        url.searchParams.delete("search");
        window.location = url;
    });
}
