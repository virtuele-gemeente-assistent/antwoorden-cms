"""
WSGI config for antwoorden_cms project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/
"""

import os  # noqa

from django.core.wsgi import get_wsgi_application

from antwoorden_cms.setup import setup_env

setup_env()

application = get_wsgi_application()
