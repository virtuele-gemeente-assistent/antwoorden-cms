from django.apps import apps
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, path

from ratelimit.decorators import ratelimit
from two_factor.urls import urlpatterns as tf_urls

from antwoorden_cms.revproxy.view import RevProxyView

from .accounts.admin import CustomAdminSiteOTPRequired
from .accounts.views import GitLabTokenValidView, GitLabTokenView
from .config.views import WidgetStylingView
from .registration_urls import urlpatterns as registration_urls

handler500 = "antwoorden_cms.utils.views.server_error"
admin.site.site_header = "antwoorden_cms admin"
admin.site.site_title = "antwoorden_cms admin"
admin.site.index_title = "Welcome to the antwoorden_cms admin"

admin.site.__class__ = CustomAdminSiteOTPRequired

urlpatterns = [
    path("", include(tf_urls)),
    path("gitlab-token/", GitLabTokenView.as_view(), name="gitlab_token"),
    path(
        "gitlab-token/check_valid",
        GitLabTokenValidView.as_view(),
        name="gitlab_token_check_valid",
    ),
    path(
        "password_change/",
        ratelimit(key="user", method="POST", rate="5/h", block=True)(
            auth_views.PasswordChangeView.as_view()
        ),
        name="password_change",
    ),
    path(
        "password_change/done/",
        auth_views.PasswordChangeDoneView.as_view(),
        name="password_change_done",
    ),
    path(
        "admin/password_reset/",
        auth_views.PasswordResetView.as_view(
            html_email_template_name="registration/password_reset_email.html"
        ),
        name="admin_password_reset",
    ),
    path(
        "admin/password_reset/done/",
        auth_views.PasswordResetDoneView.as_view(),
        name="password_reset_done",
    ),
    path("admin/hijack/", include("hijack.urls")),
    path("admin/", admin.site.urls),
    path(
        "reset/<uidb64>/<token>/",
        auth_views.PasswordResetConfirmView.as_view(),
        name="password_reset_confirm",
    ),
    path(
        "reset/done/",
        auth_views.PasswordResetCompleteView.as_view(),
        name="password_reset_complete",
    ),
    # Simply show the master template.
    # path("", RedirectView.as_view(url="/api/redoc/"), name="home"),
    path("api/", include("antwoorden_cms.utters.api.urls", namespace="antwoorden_cms")),
    path("", include("antwoorden_cms.utters.urls", namespace="gemeentes")),
    path("config/", include("antwoorden_cms.config.urls", namespace="config")),
    path("overview/", include("antwoorden_cms.dialogs.urls", namespace="dialogs")),
    path(
        "statistics/", include("antwoorden_cms.statistics.urls", namespace="statistics")
    ),
    path("accounts/", include(registration_urls)),
    path("i18n/", include("django.conf.urls.i18n")),
    # For backward compatibility
    path(
        "<slug:gemeente_slug>/_styling",
        WidgetStylingView.as_view(content_type="text/css"),
        name="widget_styling",
    ),
    path(
        "services/<str:host_and_port>/",
        RevProxyView.as_view(),
        name="revproxy",
    ),
    path(
        "services/<str:host_and_port>/<path:rest>",
        RevProxyView.as_view(),
        name="revproxy",
    ),
]

# NOTE: The staticfiles_urlpatterns also discovers static files (ie. no need to run collectstatic). Both the static
# folder and the media folder are only served via Django if DEBUG = True.
urlpatterns += staticfiles_urlpatterns() + static(
    settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
)

if settings.DEBUG and apps.is_installed("debug_toolbar"):
    import debug_toolbar

    urlpatterns = [
        path("__debug__/", include(debug_toolbar.urls)),
    ] + urlpatterns
