import os

# Django-hijack (and Django-hijack-admin)
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _

from log_outgoing_requests.formatters import HttpFormatter
from sentry_sdk.integrations import DidNotEnable, django, redis

from .utils import config

try:
    from sentry_sdk.integrations import celery
except (ImportError, DidNotEnable):  # no celery in this proejct
    celery = None

SITE_ID = 1

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
DJANGO_PROJECT_DIR = os.path.abspath(
    os.path.join(os.path.dirname(__file__), os.path.pardir)
)
BASE_DIR = os.path.abspath(
    os.path.join(DJANGO_PROJECT_DIR, os.path.pardir, os.path.pardir)
)

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.getenv("SECRET_KEY")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = []

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": os.getenv("DB_NAME", "antwoorden_cms"),
        "USER": os.getenv("DB_USER", "antwoorden_cms"),
        "PASSWORD": os.getenv("DB_PASSWORD", "antwoorden_cms"),
        "HOST": os.getenv("DB_HOST", "localhost"),
        "PORT": os.getenv("DB_PORT", 5432),
    }
}

# Application definition

INSTALLED_APPS = [
    # Note: contenttypes should be first, see Django ticket #10827
    "django.contrib.contenttypes",
    "django.contrib.auth",
    "django.contrib.sessions",
    # Note: If enabled, at least one Site object is required
    "django.contrib.sites",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    # django-admin-index
    "ordered_model",
    "django_admin_index",
    # Optional applications.
    "django.contrib.admin",
    # 'django.contrib.admindocs',
    # 'django.contrib.humanize',
    # 'django.contrib.sitemaps',
    # External applications.
    "axes",
    "auditlog",
    "corsheaders",
    "django_better_admin_arrayfield",
    "sniplates",
    "hijack",
    "compat",  # Part of hijack
    "hijack_admin",
    "elasticapm.contrib.django",
    "rest_framework",
    "drf_yasg",
    "registration",
    "ratelimit",
    "crispy_forms",
    "django_bootstrap_breadcrumbs",
    "view_breadcrumbs",
    "mathfilters",
    "markdownify",
    "django_otp",
    "django_otp.plugins.otp_static",
    "django_otp.plugins.otp_totp",
    "two_factor",
    "solo",
    "log_outgoing_requests",
    "revproxy",
    # Project applications.
    "antwoorden_cms.accounts",
    "antwoorden_cms.utils",
    "antwoorden_cms.utters",
    "antwoorden_cms.config",
    "antwoorden_cms.dialogs",
    "antwoorden_cms.statistics",
]

MIDDLEWARE = [
    "elasticapm.contrib.django.middleware.TracingMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    # 'django.middleware.locale.LocaleMiddleware',
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django_otp.middleware.OTPMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "antwoorden_cms.utils.middleware.SessionExpiredMiddleware",
    "antwoorden_cms.utils.middleware.ChatbotEnvironmentMiddleware",
    "auditlog.middleware.AuditlogMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "axes.middleware.AxesMiddleware",
]

ROOT_URLCONF = "antwoorden_cms.urls"

# List of callables that know how to import templates from various sources.
RAW_TEMPLATE_LOADERS = (
    "django.template.loaders.filesystem.Loader",
    "django.template.loaders.app_directories.Loader",
    # 'admin_tools.template_loaders.Loader',
)

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [
            os.path.join(DJANGO_PROJECT_DIR, "templates"),
        ],
        "APP_DIRS": False,  # conflicts with explicity specifying the loaders
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "antwoorden_cms.utils.context_processors.settings",
                "antwoorden_cms.utils.context_processors.current_gemeentes",
                "antwoorden_cms.utils.context_processors.current_gemeente",
                "antwoorden_cms.utils.context_processors.site_config",
            ],
            "loaders": RAW_TEMPLATE_LOADERS,
        },
    },
]

WSGI_APPLICATION = "antwoorden_cms.wsgi.application"

# Database: Defined in target specific settings files.
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

# Password validation
# https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]


# Internationalization
# https://docs.djangoproject.com/en/3.0/topics/i18n/

LANGUAGES = (
    ("nl", _("Nederlands")),
    ("en", _("English")),
)

LANGUAGE_CODE = "nl-nl"

TIME_ZONE = "Europe/Amsterdam"

USE_I18N = True

USE_L10N = True

USE_TZ = True

USE_THOUSAND_SEPARATOR = True

# Translations
LOCALE_PATHS = (os.path.join(DJANGO_PROJECT_DIR, "conf", "locale"),)

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.0/howto/static-files/

STATIC_URL = "/static/"

STATIC_ROOT = os.path.join(BASE_DIR, "static")

# Additional locations of static files
STATICFILES_DIRS = (os.path.join(DJANGO_PROJECT_DIR, "static"),)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
    # 'django.contrib.staticfiles.finders.DefaultStorageFinder',
]

MEDIA_ROOT = os.path.join(BASE_DIR, "media")
MEDIA_URL = "/media/"
FILE_UPLOAD_PERMISSIONS = 0o644

FIXTURE_DIRS = (os.path.join(DJANGO_PROJECT_DIR, "fixtures"),)

DEFAULT_FROM_EMAIL = "info@virtuele-gemeente-assistent.nl"
EMAIL_TIMEOUT = 10

LOGGING_DIR = os.path.join(BASE_DIR, "log")
LOG_REQUESTS = os.getenv("LOG_REQUESTS", False)
LOG_OUTGOING_REQUESTS_DB_SAVE = os.getenv("LOG_OUTGOING_REQUESTS_DB_SAVE", False)
LOG_OUTGOING_REQUESTS_DB_SAVE_BODY = True  # save request/response body
LOG_OUTGOING_REQUESTS_EMIT_BODY = True  # log request/response body

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "verbose": {
            "format": "%(asctime)s %(levelname)s %(name)s %(module)s %(process)d %(thread)d  %(message)s"
        },
        "timestamped": {"format": "%(asctime)s %(levelname)s %(name)s  %(message)s"},
        "simple": {"format": "%(levelname)s  %(message)s"},
        "performance": {
            "format": "%(asctime)s %(process)d | %(thread)d | %(message)s",
        },
        "outgoing_requests": {"()": HttpFormatter},
    },
    "filters": {
        "require_debug_false": {"()": "django.utils.log.RequireDebugFalse"},
    },
    "handlers": {
        "mail_admins": {
            "level": "ERROR",
            "filters": ["require_debug_false"],
            "class": "django.utils.log.AdminEmailHandler",
        },
        "null": {
            "level": "DEBUG",
            "class": "logging.NullHandler",
        },
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "timestamped",
        },
        "django": {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": os.path.join(LOGGING_DIR, "django.log"),
            "formatter": "verbose",
            "maxBytes": 1024 * 1024 * 10,  # 10 MB
            "backupCount": 10,
        },
        "project": {
            "level": "INFO",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": os.path.join(LOGGING_DIR, "antwoorden_cms.log"),
            "formatter": "verbose",
            "maxBytes": 1024 * 1024 * 10,  # 10 MB
            "backupCount": 10,
        },
        "performance": {
            "level": "INFO",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": os.path.join(LOGGING_DIR, "performance.log"),
            "formatter": "performance",
            "maxBytes": 1024 * 1024 * 10,  # 10 MB
            "backupCount": 10,
        },
        "log_outgoing_requests": {
            "level": "DEBUG",
            "formatter": "outgoing_requests",
            "class": "logging.StreamHandler",
        },
        "save_outgoing_requests": {
            "level": "DEBUG",
            "class": "log_outgoing_requests.handlers.DatabaseOutgoingRequestsHandler",
        },
    },
    "loggers": {
        "antwoorden_cms": {
            "handlers": ["project"],
            "level": "INFO",
            "propagate": True,
        },
        "django.request": {
            "handlers": ["django"],
            "level": "ERROR",
            "propagate": True,
        },
        "django.template": {
            "handlers": ["console"],
            "level": "INFO",
            "propagate": True,
        },
        "log_outgoing_requests": {
            "handlers": (
                ["log_outgoing_requests", "save_outgoing_requests"]
                if LOG_REQUESTS
                else []
            ),
            "level": "DEBUG",
            "propagate": True,
        },
        # "antwoorden_cms.utters": {
        #     "handlers": ["console"],
        #     "level": "DEBUG",
        #     "propagate": True,
        # },
    },
}

#
# Additional Django settings
#

# Custom user model
AUTH_USER_MODEL = "accounts.User"

# Allow logging in with both username+password and email+password
AUTHENTICATION_BACKENDS = [
    "axes.backends.AxesBackend",
    "antwoorden_cms.accounts.backends.UserModelEmailBackend",
    "django.contrib.auth.backends.ModelBackend",
]

# become the default in Django
DEFAULT_AUTO_FIELD = "django.db.models.AutoField"

# Expire sessions after 30 minutes
SESSION_COOKIE_AGE = 30 * 60

DATA_UPLOAD_MAX_NUMBER_FIELDS = 10000

#
# Custom settings
#
PROJECT_NAME = "Antwoorden CMS"
ENVIRONMENT = None
SHOW_ALERT = True

#
# Library settings
#


# Django-Admin-Index
ADMIN_INDEX_SHOW_REMAINING_APPS = True
ADMIN_INDEX_AUTO_CREATE_APP_GROUP = True

# Django-Axes (4.0+)
#
# The number of login attempts allowed before a record is created for the
# failed logins. Default: 3
AXES_FAILURE_LIMIT = 10
# If set, defines a period of inactivity after which old failed login attempts
# will be forgotten. Can be set to a python timedelta object or an integer. If
# an integer, will be interpreted as a number of hours. Default: None
AXES_COOLOFF_TIME = 1
# If True only locks based on user id and never locks by IP if attempts limit
# exceed, otherwise utilize the existing IP and user locking logic Default:
# False
AXES_ONLY_USER_FAILURES = True
# If set, specifies a template to render when a user is locked out. Template
# receives cooloff_time and failure_limit as context variables. Default: None
AXES_LOCKOUT_TEMPLATE = "account_blocked.html"
AXES_USE_USER_AGENT = True  # Default: False
AXES_LOCK_OUT_BY_COMBINATION_USER_AND_IP = True  # Default: False

# The default meta precedence order
IPWARE_META_PRECEDENCE_ORDER = (
    "HTTP_X_FORWARDED_FOR",
    "X_FORWARDED_FOR",  # <client>, <proxy1>, <proxy2>
    "HTTP_CLIENT_IP",
    "HTTP_X_REAL_IP",
    "HTTP_X_FORWARDED",
    "HTTP_X_CLUSTER_CLIENT_IP",
    "HTTP_FORWARDED_FOR",
    "HTTP_FORWARDED",
    "HTTP_VIA",
    "REMOTE_ADDR",
)

# Django-Hijack
HIJACK_LOGIN_REDIRECT_URL = "/"
HIJACK_LOGOUT_REDIRECT_URL = reverse_lazy("admin:accounts_user_changelist")
# The Admin mixin is used because we use a custom User-model.
HIJACK_REGISTER_ADMIN = False
# This is a CSRF-security risk.
# See: http://django-hijack.readthedocs.io/en/latest/configuration/#allowing-get-method-for-hijack-views
HIJACK_ALLOW_GET_REQUESTS = True

# User registration settings

ACCOUNT_ACTIVATION_DAYS = 7
LOGIN_REDIRECT_URL = "/"
LOGIN_URL = "two_factor:login"
LOGOUT_REDIRECT_URL = LOGIN_URL

# CRISPY FORMS

CRISPY_TEMPLATE_PACK = "bootstrap4"

# Sentry SDK
SENTRY_DSN = os.getenv("SENTRY_DSN")

SENTRY_SDK_INTEGRATIONS = [
    django.DjangoIntegration(),
    redis.RedisIntegration(),
]
if celery is not None:
    SENTRY_SDK_INTEGRATIONS.append(celery.CeleryIntegration())

if SENTRY_DSN:
    import sentry_sdk

    SENTRY_CONFIG = {
        "dsn": SENTRY_DSN,
        "release": os.getenv("VERSION_TAG", "VERSION_TAG not set"),
    }

    sentry_sdk.init(
        **SENTRY_CONFIG, integrations=SENTRY_SDK_INTEGRATIONS, send_default_pii=True
    )

# Elastic APM
ELASTIC_APM_SERVER_URL = os.getenv("ELASTIC_APM_SERVER_URL", None)
ELASTIC_APM = {
    "SERVICE_NAME": f"antwoorden_cms {ENVIRONMENT}",
    "SECRET_TOKEN": config("ELASTIC_APM_SECRET_TOKEN", "default"),
    "SERVER_URL": ELASTIC_APM_SERVER_URL,
}
if not ELASTIC_APM_SERVER_URL:
    ELASTIC_APM["ENABLED"] = False
    ELASTIC_APM["SERVER_URL"] = "http://localhost:8200"

# Settings for API schema generation

SWAGGER_SETTINGS = {"DEFAULT_INFO": "antwoorden_cms.utters.api.api.info"}

# CORS settings

# Allow API to be called from every origin
CORS_ALLOW_ALL_ORIGINS = True
CORS_URLS_REGEX = r"(^/api/.*$)|(^/(config/)?([-a-zA-Z0-9_]+)/_styling$)"


#
# Custom environment variables
#

# Indicates whether the local utter import should delete all pre existing utters
LOCAL_UTTER_IMPORT_OVERRIDE = os.getenv("LOCAL_UTTER_IMPORT_OVERRIDE", "no") in [
    "yes",
    "True",
    "1",
]

CHATBOT_URL = os.getenv("CHATBOT_URL", "https://virtuele-gemeente-assistent.nl")

MEDIA_CONTAINER_URL = os.getenv("MEDIA_CONTAINER_URL", "http://localhost:81")
MEDIA_CONTAINER_SUBPATH = os.getenv("MEDIA_CONTAINER_SUBPATH", "/gem/pre")
DOMAIN_URL = MEDIA_CONTAINER_URL + MEDIA_CONTAINER_SUBPATH + "/domain/responses.yml"

# URL to retrieve responses.yaml
LOCAL_YML_URL = MEDIA_CONTAINER_URL + MEDIA_CONTAINER_SUBPATH + "/local.yml"

STORIES_PATH = MEDIA_CONTAINER_SUBPATH + os.getenv(
    "STORIES_PATH", "/data/simple-stories/functional_tests/"
)

LOGGING_COMPONENT_URL = os.getenv("LOGGING_COMPONENT_URL")
STATISTICS_API_URL = os.getenv("STATISTICS_API_URL")
STATISTICS_COMPONENT_URL = os.getenv(
    "STATISTICS_COMPONENT_URL", "http://localhost:8111/api/v1"
)

# Gitlab API
GITLAB_GEM_PROJECT_ID = 12344345
GITLAB_PROJECT_BASE_URL = f"https://gitlab.com/api/v4/projects/{GITLAB_GEM_PROJECT_ID}"

# Prevent XSS
MARKDOWNIFY_BLEACH = True
MARKDOWNIFY_STRIP = False
MARKDOWNIFY_WHITELIST_TAGS = ["a", "br", "p", "ul", "ol", "li"]

# Default (connection timeout, read timeout) for the requests library (in seconds)
REQUESTS_DEFAULT_TIMEOUT = (10, 30)

# Timeouts for requests
REQUESTS_TIMEOUT_SHORT = 5
REQUESTS_TIMEOUT_LONG = 20

REST_FRAMEWORK = {
    "JSON_UNDERSCOREIZE": {
        "skip_next_fields": ("enabled_pages",),
    },
}

SCRAPY_URL = os.getenv("SCRAPY_URL", "http://localhost:6800")
ENABLE_SCRAPY_ENDPOINT = False


RASA_URL = os.getenv("RASA_URL", "http://localhost:5005")
