import logging

from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


class DialogEvaluation(models.Model):
    # Hidden fields
    evaluator = models.ForeignKey(
        "accounts.User",
        verbose_name=_("Naam turver"),
        max_length=512,
        help_text=_(
            "Gebruikersaccount van de persoon die het gespreek geëvalueerd heeft."
        ),
        null=True,
        blank=True,
        on_delete=models.DO_NOTHING,
    )
    gemeente = models.ForeignKey(
        "utters.Gemeente",
        help_text=_("Gemeente waarbij het gesprek gevoerd is"),
        on_delete=models.CASCADE,
    )
    timestamp = models.DateTimeField(verbose_name=_("Datum en tijd"))
    session_id = models.CharField(
        verbose_name=_("Sessie ID"),
        max_length=256,
        help_text=_("ID van het gesprek in het logging component/CMS."),
    )
    chat_started_on_page = models.URLField(
        verbose_name=_("Gesprek gestart op pagina"),
        max_length=2000,
        help_text=_("URL van de webpagina waarop het gesprek gestart is."),
        blank=True,
    )
    subjects = ArrayField(
        models.CharField(
            verbose_name=_("Onderwerp(en) van het gesprek"), max_length=256
        ),
        verbose_name=_("Onderwerp(en) van het gesprek"),
        help_text=_("Onderwerp(en) van het gesprek"),
        default=list,
        blank=True,
    )

    # Question recognition
    num_correct_answers = models.PositiveSmallIntegerField(
        verbose_name=_("Hoe vaak gaf Gem direct juiste antwoord?"),
        null=True,
        blank=True,
    )
    num_correct_answers_after_one_correction = models.PositiveSmallIntegerField(
        verbose_name=_("Hoe vaak gaf Gem een correct antwoord na 1 reparatie?"),
        null=True,
        blank=True,
    )
    num_correct_answers_after_two_corrections = models.PositiveSmallIntegerField(
        verbose_name=_("Hoe vaak gaf Gem een correct antwoord na 2 reparaties?"),
        null=True,
        blank=True,
    )
    num_failed_corrections = models.PositiveSmallIntegerField(
        verbose_name=_("Hoe vaak kon Gem niet goed repareren?"),
        null=True,
        blank=True,
    )
    num_incorrect_responses = models.PositiveSmallIntegerField(
        verbose_name=_("Hoe vaak gaf Gem een verkeerd antwoord (plank mis)?"),
        null=True,
        blank=True,
    )
    num_impossible_questions = models.PositiveSmallIntegerField(
        verbose_name=_("Hoe vaak stelde de inwoner een onmogelijke vraag?"),
        null=True,
        blank=True,
    )

    # KPI's
    citizen_properly_assisted = models.BooleanField(
        verbose_name=_("Heeft de inwoner correcte dienstverlening ontvangen?"),
        null=True,
    )
    critical_error = models.BooleanField(
        verbose_name=_("Kritische fout?"), default=False
    )
    escalation_to_livechat = models.BooleanField(
        verbose_name=_("Doorverwezen naar KCC/livechat?"), default=False
    )
    detected_escalation_to_livechat = models.CharField(
        verbose_name=_("Livechat status (automatisch afgeleid)"),
        blank=True,
        max_length=100,
    )
    livechat_reason = ArrayField(
        models.CharField(
            max_length=100, verbose_name=_("Doorverwijzing naar KCC/livechat na")
        ),
        verbose_name=_("Doorverwijzing naar KCC/livechat na"),
        default=list,
        blank=True,
    )
    successful_livechat_escalation = models.CharField(
        verbose_name=_("Doorschakeling naar livechat gelukt?"),
        max_length=100,
        blank=True,
    )
    failed_escalation_reason = models.CharField(
        verbose_name=_("Reden livechat niet gelukt"),
        max_length=100,
        blank=True,
    )

    # Other
    different_language = models.BooleanField(
        verbose_name=_("Andere taal?"),
        default=False,
    )
    personal_question = models.BooleanField(
        verbose_name=_("Persoonlijke vraag?"),
        default=False,
    )
    remarks = models.TextField(
        verbose_name=_("Opmerkingen"),
        blank=True,
    )

    classification = models.CharField(
        verbose_name=_("Classificatie"),
        max_length=100,
        blank=True,
    )
