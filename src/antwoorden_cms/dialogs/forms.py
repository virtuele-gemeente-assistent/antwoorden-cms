from typing import List, Union

from django import forms
from django.core.exceptions import ValidationError
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _

from crispy_forms.bootstrap import Field
from crispy_forms.helper import FormHelper
from crispy_forms.layout import HTML, Fieldset, Layout, Submit
from crispy_forms.utils import TEMPLATE_PACK

from antwoorden_cms.statistics.statistics_client import StatisticsClient

ESCALATE_MAPPING = {
    None: "Niet naar een medewerker",
    "": "Niet naar een medewerker",
    "unknown": "Na onbekende reden",
    "feedback": "N.a.v. feedback",
    "reparation": "N.a.v. reparatie",
    "offered": "Aangeboden",
    "requested": "Gevraagd",
    "direct": "Direct livechat",
}

ESCALATE_RESULT_MAPPING = {
    "no_livechat": "Gemeente heeft geen livechat",
    "closed": "Buiten openingstijden",
    "failed": "Technische fout",
    "no_agents": "Medewerkers niet online tijdens kantoortijden",
    "not_available": "Livechat niet beschikbaar (controleer configuratie)",
    "disconnected": "Doorschakeling gestopt na lange wachttijd (10 min)",
    "interrupted": "KCC uitgelogd terwijl inwoner in wachtrij stond",
    "cancelled": "Doorschakeling geannuleerd door inwoner",
    "success": "Doorschakeling succesvol",
    "unknown": "Doorschakeling gelukt onbekend",
}


class CustomField(Field):
    template = "widgets/custom_field.html"

    def __init__(self, *args, **kwargs):
        self.extra_context = kwargs.get("extra_context", {"field_template": None})
        super().__init__(*args, **kwargs)

    def render(self, form, form_style, context, template_pack=TEMPLATE_PACK, **kwargs):
        extra_context = {**self.extra_context, **kwargs.pop("extra_context", {})}
        return super().render(
            form,
            form_style,
            context,
            template_pack=template_pack,
            extra_context=extra_context,
            **kwargs,
        )


class ArrayFieldWidget(forms.TextInput):
    """
    Adapted from https://github.com/gradam/django-better-admin-arrayfield
    """

    template_name = "widgets/arrayfield_widget.html"

    def __init__(self, *args, **kwargs):
        self.subwidget_form = kwargs.pop("subwidget_form", forms.TextInput)
        self.subwidget_kwargs = kwargs.pop("subwidget_kwargs", {})
        super().__init__(*args, **kwargs)

    def get_context(self, name, value, attrs):
        context_value = value or [""]
        context = super().get_context(name, context_value, attrs)
        final_attrs = context["widget"]["attrs"]
        id_ = context["widget"]["attrs"].get("id")
        context["widget"]["is_none"] = value is None

        subwidgets = []
        widget_value = context["widget"]["value"]
        value = (
            widget_value if isinstance(widget_value, list) else widget_value.split(",")
        )

        for index, item in enumerate(value):
            widget_attrs = final_attrs.copy()
            if id_:
                widget_attrs["id"] = "{id_}_{index}".format(id_=id_, index=index)
            widget = self.subwidget_form(**self.subwidget_kwargs)
            widget.is_required = self.is_required
            subwidgets.append(widget.get_context(name, item, widget_attrs)["widget"])

        context["widget"]["subwidgets"] = subwidgets
        return context

    def value_from_datadict(self, data, files, name):
        try:
            getter = data.getlist
            return [value for value in getter(name) if value]
        except AttributeError:
            return data.get(name)

    def value_omitted_from_data(self, data, files, name):
        return False

    def format_value(self, value):
        return value or []


class DialogEvaluationForm(forms.Form):
    evaluator = forms.CharField(widget=forms.HiddenInput())
    organisation_id = forms.CharField(widget=forms.HiddenInput())
    session_id = forms.CharField(widget=forms.HiddenInput())
    source = forms.CharField(widget=forms.HiddenInput())
    timestamp = forms.DateTimeField(widget=forms.HiddenInput())
    subjects = forms.Field(
        label=_("Onderwerp(en)"),
        widget=ArrayFieldWidget(subwidget_form=forms.widgets.Select),
        required=False,
    )
    remarks = forms.CharField(
        label=_("Opmerking"), widget=forms.Textarea, required=False
    )
    classification = forms.ChoiceField(
        label=_("Kwaliteit van assistent"),
        choices=[
            ("assisted-good", "Klant geholpen - goed gesprek"),
            ("assisted-soso", "Klant geholpen - matig gesprek"),
            ("notassisted-inferior", "Klant niet geholpen - Niet kritische fout"),
            ("notassisted-critical", "Klant niet geholpen - Kritische fout"),
            ("invalid", "Onmogelijk gesprek of om onbekende reden gestopt"),
        ],
        required=True,
        initial=None,
    )

    class Meta:
        labels = {
            "subjects": mark_safe(
                "{label} <br><small>{sublabel}</small>".format(
                    label=_("Onderwerp(en)"),
                    sublabel=_("Type enkele letters om te zoeken in de lijst"),
                )
            )
        }
        help_texts = {
            "subjects": _("Lijst van onderwerp(en) van het gesprek (maximaal vijf)")
        }

    def __init__(
        self,
        escalate: Union[str, None],
        escalate_result: Union[str, None],
        escalate_wait: Union[int, None],
        upl: List[str],
        different_language: bool,
        no_evaluation_data: bool,
        *args,
        **kwargs,
    ):
        super().__init__(*args, **kwargs)

        self.fields["subjects"].widget.subwidget_kwargs = {
            "choices": StatisticsClient.get_subject_choices(
                self.initial["organisation_id"]
            )
        }

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"

        # Construct the HTML for automatische gegevens dynamically
        if no_evaluation_data:
            auto_data_html = """
                <strong class="text-warning">LET OP:</strong> De statistiekgegevens voor dit gesprek zijn nog niet verwerkt. Niet turven.<br/>
            """
        else:
            livechat = f"Livechat: {ESCALATE_MAPPING[escalate]}"
            if escalate and escalate_result and escalate_wait:
                escalate_wait_string = (
                    f"{escalate_wait} s"
                    if escalate_wait <= 60
                    else f"{escalate_wait // 60} m {escalate_wait % 60} s"
                )
                livechat += f", {ESCALATE_RESULT_MAPPING.get(escalate_result, escalate_result)}, {escalate_wait_string}"

            elif escalate and escalate_result:
                livechat += (
                    f", {ESCALATE_RESULT_MAPPING.get(escalate_result, escalate_result)}"
                )

            else:
                livechat += ""

            auto_data_html = f"""
                {livechat}<br/>
                Andere taal: {"Ja" if different_language else "Nee"}<br/>
                Gedetecteerde onderwerpen:<br/>
                <ul>{"".join([f"<li>{item}</li>" for item in upl])}</ul>
            """

        self.helper.layout = Layout(
            Fieldset(
                "",
                "evaluator",
                "organisation_id",
                "source",
                "session_id",
                "timestamp",
                "subjects",
                CustomField(
                    "classification",
                    extra_context={"field_template": "widgets/star_radio_select.html"},
                ),
                "remarks",
            ),
            HTML(
                f"""
                <div class="alert alert-info">
                    <p>
                        <strong>Automatische gegevens</strong><br/>
                        {auto_data_html}
                    </p>
                </div>
                """
            ),
            Submit("submit", _("Opslaan")),
        )

    def clean_subjects(self):
        subjects = self.cleaned_data["subjects"]

        for subject in subjects:
            if ";" in subject:
                raise ValidationError(
                    _(
                        "Gebruik komma's (,) in plaats van puntkomma's om meerdere onderwerpen in te voeren"
                    )
                )

        return subjects

    def clean(self):
        cleaned_data = super().clean()

        if len(set(cleaned_data["subjects"])) < len(cleaned_data["subjects"]):
            raise ValidationError(_("Kies niet meer dan één keer hetzelfde onderwerp"))

        return cleaned_data
