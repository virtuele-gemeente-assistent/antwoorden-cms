from django.utils.translation import gettext_lazy as _

from djchoices import ChoiceItem, DjangoChoices

HIGH_THRESHOLD = 0.8
LOW_THRESHOLD = 0.6

RASA_BUTTON_REGEX = r"(?:(?:^\/[\w\-]+)|(?:(?<=\s)\/[\w\-]+))(?:{\"?.*\"?})?"


class LivechatEscalationCauses(DjangoChoices):
    correctly_predicted = ChoiceItem("correctly_predicted", _("Correcte intentie"))
    uncertainty = ChoiceItem("uncertainty", _("Onzekerheid"))


class LivechatEscalationSuccessful(DjangoChoices):
    niet_van_toepassing = ChoiceItem("niet_van_toepassing", _("N.v.t."))
    ja = ChoiceItem("ja", _("Ja"))
    nee = ChoiceItem("nee", _("Nee"))


class LivechatEscalationFailedReasons(DjangoChoices):
    niet_van_toepassing = ChoiceItem("niet_van_toepassing", _("N.v.t."))
    buiten_kantoortijd = ChoiceItem("buiten_kantoortijd", _("Buiten kantoortijd"))
    bezet_of_niet_beschikbaar = ChoiceItem(
        "bezet_of_niet_beschikbaar", _("Bezet/niet beschikbaar binnen kantoortijd")
    )
    technische_fout = ChoiceItem("technische_fout", _("Technische fout"))
    user_declined = ChoiceItem("user_declined", _("Gebruiker weigerde"))
