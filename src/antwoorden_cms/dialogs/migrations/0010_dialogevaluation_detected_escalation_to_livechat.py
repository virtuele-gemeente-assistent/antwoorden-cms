# Generated by Django 3.2.13 on 2022-08-31 14:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("dialogs", "0009_auto_20220831_1552"),
    ]

    operations = [
        migrations.AddField(
            model_name="dialogevaluation",
            name="detected_escalation_to_livechat",
            field=models.CharField(
                blank=True,
                max_length=100,
                verbose_name="Livechat status (automatisch afgeleid)",
            ),
        ),
    ]
