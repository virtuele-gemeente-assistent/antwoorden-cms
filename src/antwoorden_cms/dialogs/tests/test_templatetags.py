from datetime import datetime

from django.template import Context, Template
from django.test import TestCase
from django.utils.timezone import make_aware

from freezegun import freeze_time
from pytz.exceptions import AmbiguousTimeError


class TemplatetagsTest(TestCase):
    @freeze_time("2023-09-16 16:00:00", tz_offset=+2)
    def test_convert_timestamp_with_daylight_savings(self):

        date = datetime(2023, 10, 29, 2, 7, 58, 599828)

        with self.assertRaises(AmbiguousTimeError):
            make_aware(date)

        rendered = Template(
            "{% load convert_timestamp %}{{ bad_date |convert_timestamp }}"
        ).render(Context({"bad_date": date.timestamp()}))

        self.assertEqual(rendered, "29 oktober 2023 02:07")
