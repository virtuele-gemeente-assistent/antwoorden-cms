import copy
import re

from django.conf import settings
from django.test import TestCase

from ..constants import RASA_BUTTON_REGEX
from ..utils import event_data_without_text

DOMAIN_URL = f"{settings.CHATBOT_URL}/domain/"
DOMAIN_RESPONSE = {
    "intents": [
        "oepsikbedoel+inform_denial",
        {
            "oepsikbedoel": {
                "suggestions": [
                    {"text": "Ik bedoelde iets anders", "entities": []},
                    {"text": "Ik bedoelde {product}", "entities": ["product"]},
                    {
                        "text": "Ik bedoelde {municipality}",
                        "entities": ["municipality"],
                    },
                    {"text": "Ik bedoelde {land}", "entities": ["land"]},
                ]
            }
        },
        {
            "waaromvraagjedat": {
                "suggestions": [
                    {"text": "Waarom vraag je dat?", "entities": []},
                ]
            }
        },
    ]
}


class ButtonMatchTestCase(TestCase):
    def test_match_buttons(self):
        text = """
        /product_brp-uittreksel{"product": "brp-uittreksel", "number": 1}
        /bla{"entity": "value", "bla": "bla"}
        /waaromvraagjedat
        """
        expected = [
            '/product_brp-uittreksel{"product": "brp-uittreksel", "number": 1}',
            '/bla{"entity": "value", "bla": "bla"}',
            "/waaromvraagjedat",
        ]
        result = re.findall(RASA_BUTTON_REGEX, text)

        self.assertEqual(result, expected)


event_data = {
    "event": "user",
    "timestamp": 1728241128.997736,
    "metadata": {
        "userAgent": "Mozilla/5.0",
        "username": "antwoorden-cms",
        "municipality": "Utrecht",
        "chatStartedOnPage": "#Gemeente",
        "input_channel": "router.socketio",
        "environment_url": "internal-environment",
        "feature_livechat": "niet_aanwezig",
    },
    "text": "Text Here",  # base dict
    "parse_data": {
        "intent": {"name": "greet", "confidence": 1.0},
        "entities": [
            {"entity": "municipality", "start": 6, "end": 32, "value": "Utrecht"}
        ],
        "text": "S'more text",  # nested dict
        "message_id": "message_ui",
        "metadata": {
            "chat_history_plain": "different text",  # different label, double nested
            "userAgent": "Mozilla/5.0",
            "username": "antwoorden-cms",
            "municipality": "Utrecht",
            "chatStartedOnPage": "#Gemeente",
            "input_channel": "router.socketio",
            "environment_url": "internal-environment",
            "feature_livechat": "niet_aanwezig",
        },
        "intent_ranking": [
            {
                "name": "greet",
                "confidence": 1.0,
                "chat_history_plain": "different text key in weird place",  # different label, dict in a double nested list
            }
        ],
    },
    "input_channel": "router.socketio",
    "message_id": "message_id",
}


# manually set values to empty
cleaned_event_data = copy.deepcopy(event_data)
cleaned_event_data["text"] = ""
cleaned_event_data["parse_data"]["text"] = ""
cleaned_event_data["parse_data"]["metadata"]["chat_history_plain"] = ""
cleaned_event_data["parse_data"]["intent_ranking"][0]["chat_history_plain"] = ""


class EventDataWithoutTextTEsts(TestCase):
    def test_event_data_without_text(self):

        copy_event = copy.deepcopy(event_data)
        removed_text = event_data_without_text(copy_event)
        self.maxDiff = None
        self.assertNotEqual(removed_text, event_data)
        self.assertEqual(removed_text, cleaned_event_data)
