openapi: 3.0.3
info:
  title: Logging component
  version: 0.1.0
  description: Your project description
paths:
  /api/config/:
    get:
      operationId: config_list
      tags:
      - config
      responses:
        '200':
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/MunicipalityConfiguration'
          description: ''
    post:
      operationId: config_create
      tags:
      - config
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/MunicipalityConfiguration'
          application/x-www-form-urlencoded:
            schema:
              $ref: '#/components/schemas/MunicipalityConfiguration'
          multipart/form-data:
            schema:
              $ref: '#/components/schemas/MunicipalityConfiguration'
        required: true
      responses:
        '201':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/MunicipalityConfiguration'
          description: ''
  /api/config/{municipality_slug}/:
    get:
      operationId: config_retrieve
      parameters:
      - in: path
        name: municipality_slug
        schema:
          type: string
        required: true
      tags:
      - config
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/MunicipalityConfiguration'
          description: ''
    put:
      operationId: config_update
      parameters:
      - in: path
        name: municipality_slug
        schema:
          type: string
        required: true
      tags:
      - config
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/MunicipalityConfiguration'
          application/x-www-form-urlencoded:
            schema:
              $ref: '#/components/schemas/MunicipalityConfiguration'
          multipart/form-data:
            schema:
              $ref: '#/components/schemas/MunicipalityConfiguration'
        required: true
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/MunicipalityConfiguration'
          description: ''
    patch:
      operationId: config_partial_update
      parameters:
      - in: path
        name: municipality_slug
        schema:
          type: string
        required: true
      tags:
      - config
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/PatchedMunicipalityConfiguration'
          application/x-www-form-urlencoded:
            schema:
              $ref: '#/components/schemas/PatchedMunicipalityConfiguration'
          multipart/form-data:
            schema:
              $ref: '#/components/schemas/PatchedMunicipalityConfiguration'
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/MunicipalityConfiguration'
          description: ''
  /api/events/:
    get:
      operationId: events_list
      parameters:
      - in: query
        name: agent_type
        schema:
          type: array
          items:
            type: string
        description: Multiple values may be separated by commas.
        explode: false
        style: form
      - in: query
        name: channel
        schema:
          type: array
          items:
            type: string
        description: Multiple values may be separated by commas.
        explode: false
        style: form
      - in: query
        name: event_type
        schema:
          type: array
          items:
            type: string
        description: Multiple values may be separated by commas.
        explode: false
        style: form
      - name: ordering
        required: false
        in: query
        description: Which field to use when ordering the results.
        schema:
          type: string
      - in: query
        name: organisation_id
        schema:
          type: array
          items:
            type: string
        description: Multiple values may be separated by commas.
        explode: false
        style: form
      - in: query
        name: session_id
        schema:
          type: array
          items:
            type: string
        description: Multiple values may be separated by commas.
        explode: false
        style: form
      - in: query
        name: source
        schema:
          type: array
          items:
            type: string
        description: Multiple values may be separated by commas.
        explode: false
        style: form
      - in: query
        name: test_session
        schema:
          type: boolean
      tags:
      - events
      responses:
        '200':
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Event'
          description: ''
    post:
      operationId: events_create
      tags:
      - events
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Event'
          application/x-www-form-urlencoded:
            schema:
              $ref: '#/components/schemas/Event'
          multipart/form-data:
            schema:
              $ref: '#/components/schemas/Event'
        required: true
      responses:
        '201':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Event'
          description: ''
  /api/events/{event_id}/:
    get:
      operationId: events_retrieve
      parameters:
      - in: path
        name: event_id
        schema:
          type: string
          format: uuid
          description: Unieke resource identifier van het event (UUID4).
        required: true
      tags:
      - events
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Event'
          description: ''
    delete:
      operationId: events_destroy
      parameters:
      - in: path
        name: event_id
        schema:
          type: string
          format: uuid
          description: Unieke resource identifier van het event (UUID4).
        required: true
      tags:
      - events
      responses:
        '204':
          description: No response body
components:
  schemas:
    Event:
      type: object
      properties:
        event_id:
          type: string
          format: uuid
          readOnly: true
          description: Unieke resource identifier van het event (UUID4).
        session_id:
          type: string
          description: Unieke resource identifier van de sessie (UUID4).
          maxLength: 256
        timestamp:
          type: string
          format: date-time
          readOnly: true
          description: De datumtijd in ISO8601 formaat waarop dit event geregistreerd
            is.
        event_type:
          type: string
          description: Het type van dit event.
          maxLength: 256
        agent_type:
          type: string
          description: Het type agent waartoe dit event behoort.
          maxLength: 256
        sender_agent_id:
          type: string
          description: De unieke identificatie van de uitzendende agent.
          maxLength: 256
        recipient_agent_id:
          type: string
          description: De unieke identificatie van de ontvangende agent.
          maxLength: 256
        message:
          type: string
          description: Het inkomende bericht (mogelijk vertaald).
        buttons:
          type: array
          items:
            type: string
            maxLength: 512
          description: De inkomende knoppen (mogelijk vertaald).
        incoming_data:
          type: object
          additionalProperties: {}
          description: De originele data zoals de router het ontvangen heeft.
        outgoing_data:
          type: object
          additionalProperties: {}
          description: De originele data zoals de router het doorgestuurd heeft.
        translated_from:
          type: string
          description: De taal van het originele bericht (ISO 639-1).
          maxLength: 2
        translated_by:
          type: string
          description: De plugin waarmee het bericht vertaald is.
          maxLength: 256
        translated_to:
          type: string
          description: De taal van het vertaalde bericht (ISO 639-1).
          maxLength: 2
        organisation_id:
          type: string
          description: De unieke identificatie van de organisatie waarmee het gesprek
            gevoerd is.
          maxLength: 256
        source:
          type: string
          description: De pagina waarop het gesprek gestart is.
          maxLength: 2048
        channel:
          type: string
          description: Het kanaal waarmee het gesprek gevoerd is.
          maxLength: 256
        test_session:
          type: boolean
          description: Geeft aan of de sessie een testsessie is.
      required:
      - channel
      - event_id
      - event_type
      - session_id
      - timestamp
    MunicipalityConfiguration:
      type: object
      properties:
        municipality_slug:
          type: string
          maxLength: 512
        archiving_period:
          type: integer
          maximum: 32767
          minimum: 0
          description: The number of days events will be retained in the database
      required:
      - municipality_slug
    PatchedMunicipalityConfiguration:
      type: object
      properties:
        municipality_slug:
          type: string
          maxLength: 512
        archiving_period:
          type: integer
          maximum: 32767
          minimum: 0
          description: The number of days events will be retained in the database
