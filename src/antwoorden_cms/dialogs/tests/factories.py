from factory.django import DjangoModelFactory

from ..models import DialogEvaluation


class DialogEvaluationFactory(DjangoModelFactory):
    class Meta:
        model = DialogEvaluation
