from django.apps import AppConfig


class DialogConfig(AppConfig):
    name = "antwoorden_cms.dialogs"
