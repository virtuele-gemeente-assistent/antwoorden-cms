import csv
import json
import logging
from collections import Counter
from dataclasses import field, fields, make_dataclass
from datetime import datetime
from io import StringIO
from typing import Any, List, Optional, Sequence, Tuple

from django.conf import settings
from django.http import FileResponse, JsonResponse
from django.urls import reverse
from django.views.generic import FormView, TemplateView, View

import requests
import yaml
from two_factor.views import OTPRequiredMixin
from view_breadcrumbs import BaseBreadcrumbMixin

from antwoorden_cms.statistics.statistics_client import StatisticsClient

from ...utters.mixins import ObjectPermissionMixin
from ...utters.models import Gemeente
from ..forms import DialogEvaluationForm
from ..utils import event_data_without_text

logger = logging.getLogger(__name__)


class SwaggerTypeMapper:
    mapping = {
        "string": {"": str, "date-time": datetime},
        "number": {"": float},
        "integer": {"": int},
        "boolean": {"": bool},
        "array": {"": Sequence},
        "object": {"": dict},
    }
    cast_mapping = {
        datetime: lambda value: datetime.fromisoformat(value),
    }

    @classmethod
    def map_type(cls, swagger_type: str, format: str = ""):
        python_type = cls.mapping.get(swagger_type).get(format)
        if not python_type:
            return cls.mapping.get(swagger_type).get("")
        return python_type

    @classmethod
    def cast(cls, cast_to: type, value: Any):
        cast_function = cls.cast_mapping.get(cast_to)
        if not cast_function:
            if value:
                return cast_to(value)
            return value
        return cast_function(value)


def swagger_dataclass_factory(resource_name: str, resource_schema: dict):
    required_props = resource_schema.get("required", [])
    properties = []
    for property_name, property_schema in resource_schema["properties"].items():
        property_type = SwaggerTypeMapper.map_type(
            property_schema["type"], property_schema.get("format", "")
        )
        if property_name in required_props:
            properties.append(
                (
                    property_name,
                    property_type,
                )
            )
        else:
            properties.append(
                (
                    property_name,
                    property_type,
                    field(default=None),
                )
            )

    def cast_properties(resource_class):
        for resource_field in fields(resource_class):
            value = getattr(resource_class, resource_field.name)
            if not isinstance(value, resource_field.type):
                setattr(
                    resource_class,
                    resource_field.name,
                    SwaggerTypeMapper.cast(resource_field.type, value),
                )

    resource_class = make_dataclass(
        resource_name,
        sorted(properties, key=lambda x: len(x)),
        namespace={"__post_init__": lambda self: cast_properties(self)},
    )
    return resource_class


class LoggingComponentClient:
    @property
    def events_endpoint(self):
        return f"{settings.LOGGING_COMPONENT_URL}/api/events/"

    @property
    def statistics_endpoint(self):
        return f"{settings.LOGGING_COMPONENT_URL}/api/statistics/"

    def parse_oas(self, url: str) -> None:
        logger.info("Retrieving OAS for logging component: %s", url)
        response = requests.get(url, timeout=settings.REQUESTS_TIMEOUT_SHORT)
        schema = yaml.safe_load(response.content)

        for resource_name, resource_schema in schema["components"]["schemas"].items():
            resource_class = swagger_dataclass_factory(resource_name, resource_schema)
            setattr(self, resource_name, resource_class)

    def fetch_events(
        self, url=None, params: Optional[dict] = None
    ) -> Tuple[List, int, int]:
        self.parse_oas(f"{settings.LOGGING_COMPONENT_URL}/api/schema")

        if url is None:
            url = self.events_endpoint

        params = params or {}
        data = requests.get(
            url, params=params, timeout=settings.REQUESTS_TIMEOUT_LONG
        ).json()
        return (
            [self.Event(**event_data) for event_data in data.get("results", [])],
            data.get("total_pages", 0),
            data.get("count"),
        )

    def fetch_initial_user_events(
        self, params: Optional[dict] = None
    ) -> Tuple[List, int, int]:
        params = params or {}
        all_user_events = self.fetch_events(
            self.events_endpoint + "list_first/", params=params
        )
        return all_user_events

    def _fetch_all_events(self, url, params):
        events, total_pages, count = self.fetch_events(url, params=params)

        # fetch all if more than 100 events
        for i in range(1, total_pages):
            params["page"] = i + 1
            events += client.fetch_events(self.events_endpoint, params=params)[0]

        return events

    def fetch_all_events(self, params):
        return self._fetch_all_events(self.events_endpoint, params)

    def fetch_all_initial_user_events(
        self, params: Optional[dict] = None
    ) -> Tuple[List, int, int]:
        params = params or {}
        return self._fetch_all_events(self.events_endpoint + "list_first/", params)

    def fetch_dialog(self, session_id: str, params: Optional[dict] = None):
        params = params or {}
        params.update(
            {
                "session_id": session_id,
                "ordering": "timestamp",
                "page_size": 100,
            }
        )
        events = self.fetch_all_events(params=params)

        return [event for event in events if not event.message.startswith("/greet")]


client = LoggingComponentClient()


class RasaClient:
    @staticmethod
    def tracker_endpoint(session_id):
        return f"{settings.RASA_URL}/conversations/{session_id}/tracker"

    def get_raw_event_data(self, session_id):
        try:
            data = requests.get(
                self.tracker_endpoint(session_id),
                {"include_events": "ALL"},
                timeout=settings.REQUESTS_TIMEOUT_SHORT,
            )
        except requests.exceptions.RequestException:
            return [{"error": "failed to retrieve event data"}]

        events = data.json().get("events") or []

        for i in range(len(events)):
            events[i] = event_data_without_text(events[i])
        sorted(events, key=lambda x: x["timestamp"])

        return events


rase_client = RasaClient()


class DialogEventListView(
    BaseBreadcrumbMixin, OTPRequiredMixin, ObjectPermissionMixin, TemplateView
):
    template_name = "dialogs/dialog_list_logging_component.html"
    only_test_sessions = False
    page_size = 20
    permission_required = "utters.dialog_read"

    default_sort_param = "date"
    default_sort_order = "desc"

    @property
    def crumbs(self):
        gemeente = Gemeente.objects.get(slug=self.kwargs["gemeente_slug"])
        return [
            (gemeente.name, reverse("gemeentes:gemeente_detail", args=[gemeente.slug])),
            (
                "Dialogen",
                reverse("dialogs:dialog_list_logging_component", args=[gemeente.slug]),
            ),
        ]

    def get(self, request, *args, **kwargs):
        kwargs["page"] = int(request.GET.get("page")) if request.GET.get("page") else 1
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        page_num = kwargs["page"]
        context["only_test_sessions"] = self.only_test_sessions
        dialogs, total_pages, count = self.get_dialogs(
            page=page_num, page_size=self.page_size
        )
        session_ids = [dialog.session_id for dialog in dialogs]

        context["dialog_list"] = dialogs
        context["dialog_list_count"] = count
        gemeente = Gemeente.objects.get(slug=self.kwargs["gemeente_slug"])
        evaluations = StatisticsClient.fetch_statistics(
            organisation_id=gemeente.name,
            session_id=session_ids,
            params={},
            type_="json",
        )

        if not isinstance(evaluations, list):
            raise ValueError(
                f"Evaluations are not a list (current value: {evaluations})"
            )

        evaluation_mapping = (
            {
                evaluation["session_id"]: evaluation["is_evaluated"]
                for evaluation in evaluations
            }
            if evaluations
            else {}
        )

        # if session_id in session_ids list is not in evaluation_mapping. Update evaluation mapping to include it with None
        evaluation_mapping.update(
            {
                session_id: None
                for session_id in session_ids
                if session_id not in evaluation_mapping
            }
        )

        context["evaluation_mapping"] = evaluation_mapping

        context["is_paginated"] = "true"
        context["page_obj"] = {
            "has_previous": page_num > 1,
            "has_next": page_num < total_pages,
            "previous_page_number": page_num - 1,
            "next_page_number": page_num + 1,
            "number": page_num,
        }
        context["paginator"] = {
            "page_range": range(1, total_pages + 1),
        }

        context["sort"] = self.request.GET.get("sort", self.default_sort_param)
        context["order"] = self.request.GET.get("order", self.default_sort_order)
        context["hide_direct"] = self.request.GET.get("hide_direct", "true")

        return context

    def get_dialogs(self, page=1, page_size=20):
        gemeente = Gemeente.objects.get(slug=self.kwargs["gemeente_slug"])

        order = self.request.GET.get("order")

        if order is not None and order == "asc":
            ordering = "timestamp"
        else:
            ordering = "-timestamp"

        params = {
            "organisation_id": gemeente.name,
            "test_session": self.only_test_sessions,
            "page": page,
            "page_size": page_size,
            "ordering": ordering,
        }

        if self.request.GET.get("hide_direct") != "false":
            params["exclude_messages"] = "/direct_escalatie"

        return client.fetch_initial_user_events(params=params)


class DialogEventDetailView(
    BaseBreadcrumbMixin, OTPRequiredMixin, ObjectPermissionMixin, FormView
):
    template_name = "dialogs/dialog_detail_logging_component.html"
    only_test_sessions = False
    form_class = DialogEvaluationForm
    permission_required = "utters.dialog_read"

    @property
    def crumbs(self):
        gemeente = Gemeente.objects.get(slug=self.kwargs["gemeente_slug"])
        session_id = self.kwargs["session_id"]

        list_label = "Dialogen"
        list_url = reverse(
            "dialogs:dialog_list_logging_component",
            args=[gemeente.slug],
        )
        previous_page = self.kwargs.get("previous_page")

        if previous_page:
            list_label += f" Pagina {previous_page}"
            list_url += f"?page={previous_page}"

        return [
            (gemeente.name, reverse("gemeentes:gemeente_detail", args=[gemeente.slug])),
            (list_label, list_url),
            (
                "Dialoog",
                reverse(
                    "dialogs:dialog_detail_logging_component",
                    args=[gemeente.slug, session_id],
                ),
            ),
        ]

    def get_event_metadata(self, events):
        for event in events:
            if event.event_type == "user_message":
                return event.incoming_data.get("customData", {}).get(
                    "chatStartedOnPage", ""
                )

    def get_raw_event_data(self):
        return rase_client.get_raw_event_data(self.kwargs["session_id"])

    def get(self, request, *args, **kwargs):
        previous_page = self.request.GET.get("previous_page")
        if previous_page:
            self.kwargs["previous_page"] = previous_page

        context = self.get_context_data()
        status = 200 if len(context["event_list"]) > 0 else 404
        return self.render_to_response(context, status=status)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["only_test_sessions"] = self.only_test_sessions
        context["event_list"] = self.events
        context["chat_started_on_page"] = self.get_event_metadata(self.events)
        context["all_events"] = self.get_raw_event_data()
        return context

    def get_success_url(self):
        view_name = "dialog_detail_logging_component"
        if self.only_test_sessions:
            view_name = f"test_{view_name}"

        previous_page = self.request.GET.get("previous_page")

        if previous_page is not None:
            previous_page = "?previous_page=" + previous_page
        else:
            previous_page = ""
        return (
            reverse(
                f"dialogs:{view_name}",
                kwargs={
                    "gemeente_slug": self.kwargs["gemeente_slug"],
                    "session_id": self.kwargs["session_id"],
                },
            )
            + previous_page
        )

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()

        self.events = self.get_events()

        session_id = self.kwargs["session_id"]
        gemeente = Gemeente.objects.get(slug=self.kwargs["gemeente_slug"])
        instance = StatisticsClient.fetch_statistics(
            organisation_id=gemeente.name,
            session_id=session_id,
            params={},
            type_="json",
        )

        kwargs["upl"] = []
        kwargs["different_language"] = False
        kwargs["escalate"] = None
        kwargs["escalate_result"] = None
        kwargs["escalate_wait"] = None
        kwargs["no_evaluation_data"] = False
        kwargs["initial"].update(
            {
                "evaluator": self.request.user,
                "organisation_id": gemeente.name,
                "session_id": session_id,
            }
        )

        if not instance:
            kwargs["no_evaluation_data"] = True

        if not instance and len(self.events) > 0:
            timestamp = self.events[0].timestamp
            url = self.get_event_metadata(self.events)
            kwargs["initial"].update(
                {
                    "source": url,
                    "timestamp": timestamp,
                }
            )

        elif instance:
            instance = instance[0]
            kwargs["initial"].update(
                {
                    "evaluator": self.request.user,
                    "organisation_id": gemeente.name,
                    "session_id": instance["session_id"],
                    "source": instance["source"],
                    "timestamp": instance["start"],
                    "subjects": instance["subjects"],
                    "remarks": instance["remarks"],
                    "classification": instance.get("classification", None),
                }
            )
            kwargs["upl"] = instance.get("upl", [])
            kwargs["different_language"] = instance.get("performance", {}).get(
                "unsupported_language", False
            )
            kwargs["escalate"] = instance.get("escalate", None)
            kwargs["escalate_result"] = instance.get("escalate_result", None)
            kwargs["escalate_wait"] = instance.get("escalate_wait", None)

        return kwargs

    def form_valid(self, form):
        gemeente = Gemeente.objects.get(slug=self.kwargs["gemeente_slug"])
        is_success = StatisticsClient.save_evaluation(
            organisation_id=gemeente.name,
            session_id=self.kwargs["session_id"],
            evaluation=form.cleaned_data,
        )

        if not is_success:
            logger.error(
                "Failed to save evaluation for session %s", self.kwargs["session_id"]
            )

        return super().form_valid(form)

    def post(self, request, *args, **kwargs):
        # Evaluation form
        if "evaluator" in request.POST:
            return super().post(request, *args, **kwargs)

        data = json.loads(request.body)
        gemeente_name = Gemeente.objects.get(slug=kwargs["gemeente_slug"]).name
        response = requests.post(
            f"https://gitlab.com/api/v4/projects/{settings.GITLAB_GEM_PROJECT_ID}/issues",
            json={
                "confidential": True,
                "description": data["description"],
                "labels": f"Content,Gem {gemeente_name}",
                "title": f"Antwoorden CMS issue {data['sender_id']}",
            },
            headers={"PRIVATE-TOKEN": request.user.gitlab_access_token},
            timeout=settings.REQUESTS_TIMEOUT_SHORT,
        )
        return JsonResponse({"url": response.json()["web_url"]})

    def get_events(self):
        gemeente = Gemeente.objects.get(slug=self.kwargs["gemeente_slug"])

        messages = client.fetch_dialog(
            self.kwargs["session_id"],
            params={
                "organisation_id": gemeente.name,
                "test_session": self.only_test_sessions,
            },
        )
        return messages


class TestDialogEventListView(DialogEventListView):
    only_test_sessions = True

    @property
    def crumbs(self):
        gemeente = Gemeente.objects.get(slug=self.kwargs["gemeente_slug"])
        return [
            (gemeente.name, reverse("gemeentes:gemeente_detail", args=[gemeente.slug])),
            (
                "Test dialogen",
                reverse(
                    "dialogs:test_dialog_list_logging_component", args=[gemeente.slug]
                ),
            ),
        ]


class TestDialogEventDetailView(DialogEventDetailView):
    only_test_sessions = True

    @property
    def crumbs(self):
        gemeente = Gemeente.objects.get(slug=self.kwargs["gemeente_slug"])
        session_id = self.kwargs["session_id"]

        list_label = "Test dialogen"
        list_url = reverse(
            "dialogs:test_dialog_list_logging_component",
            args=[gemeente.slug],
        )
        previous_page = self.kwargs.get("previous_page")

        if previous_page:
            list_label += f" Pagina {previous_page}"
            list_url += f"?page={previous_page}"

        return [
            (gemeente.name, reverse("gemeentes:gemeente_detail", args=[gemeente.slug])),
            (list_label, list_url),
            (
                "Test dialoog",
                reverse(
                    "dialogs:test_dialog_detail_logging_component",
                    args=[gemeente.slug, session_id],
                ),
            ),
        ]


class DialogExportView(OTPRequiredMixin, ObjectPermissionMixin, View):
    permission_required = "utters.dialog_read"

    only_test_sessions = False

    def has_permission(self):
        """
        Only allow access if the user has the correct permissions and is
        assigned to the current gemeente
        """
        has_permission = super().has_permission()
        if not has_permission:
            return False

        return self.request.user.can_export_dialogs

    def get(self, request, *args, **kwargs):
        gemeente = Gemeente.objects.get(slug=self.kwargs["gemeente_slug"])

        extra_params = {}
        if "timestamp__gte" in self.request.GET:
            extra_params["timestamp__gte"] = self.request.GET.get("timestamp__gte")
        if "timestamp__lt" in self.request.GET:
            extra_params["timestamp__lt"] = self.request.GET.get("timestamp__lt")
        params = {
            "organisation_id": gemeente.name,
            "ordering": "timestamp",
            "test_session": self.only_test_sessions,
            "page_size": 1000,
        }
        params.update(extra_params)
        events = client.fetch_all_events(params=params)

        # Ignore sessions with only 1 user input
        # TODO should be filter option on API?
        ids_to_use = set()
        user_events_per_id = Counter()
        for event in events:
            if event.event_type == "user_message":
                user_events_per_id[event.session_id] += 1

        for k, v in user_events_per_id.items():
            if v > 1:
                ids_to_use.add(k)

        columns = [
            "Datum",
            "Gespreks ID",
            "Zender",
            "Ontvanger",
            "Bericht",
        ]

        with StringIO() as f:
            csvwriter = csv.writer(f, delimiter=";", quoting=csv.QUOTE_ALL)
            csvwriter.writerow(columns)

            for event in events:
                if event.session_id not in ids_to_use:
                    continue

                csvwriter.writerow(
                    [
                        event.timestamp.strftime("%d-%m-%Y %H:%M:%S"),
                        event.session_id,
                        event.sender_agent_id or "bezoeker",
                        event.recipient_agent_id or "bezoeker",
                        event.message,
                    ]
                )
            content = f.getvalue()

        response = FileResponse(content)
        response["Content-Disposition"] = "attachment; filename=export.csv"

        return response


class TestDialogExportView(DialogExportView):
    only_test_sessions = True
