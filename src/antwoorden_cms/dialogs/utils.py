from typing import Any, Iterable, Union

DIALOG_EXPORT_FIELDS = [
    "session_id",
    "timestamp",
    "chat_started_on_page",
    "evaluator",
    "Onderwerp 1",
    "Onderwerp 2",
    "Onderwerp 3",
    "Onderwerp 4",
    "Onderwerp 5",
    # "gemeente",
    "num_correct_answers",
    "num_correct_answers_after_one_correction",
    "num_correct_answers_after_two_corrections",
    "num_failed_corrections",
    "num_incorrect_responses",
    "num_impossible_questions",
    "citizen_properly_assisted",
    "critical_error",
    "escalation_to_livechat",
    "detected_escalation_to_livechat",
    "livechat_reason",
    "successful_livechat_escalation",
    "failed_escalation_reason",
    "different_language",
    "personal_question",
    "remarks",
]


EMPTY_DIALOG_EXPORT_FIELDS = [
    "session_id",
    "timestamp",
    "chat_started_on_page",
    "Link naar de website",
]


def recursive_remove(dictionary: Union[dict, Any], key: str, replace_value: str):
    if not isinstance(dictionary, dict):
        return dictionary

    for k, v in dictionary.items():
        if isinstance(v, dict):
            recursive_remove(v, key, replace_value)
        elif isinstance(v, Iterable) and not isinstance(v, str):
            for i in v:
                recursive_remove(i, key, replace_value)
    if key in dictionary:
        dictionary[key] = replace_value


def event_data_without_text(data: dict) -> dict:
    recursive_remove(data, "text", "")
    recursive_remove(data, "chat_history_plain", "")
    return data
