from django.urls import path

from .apps import AppConfig
from .views.logging_component import (
    DialogEventDetailView,
    DialogEventListView,
    DialogExportView,
    TestDialogEventDetailView,
    TestDialogEventListView,
    TestDialogExportView,
)

app_name = AppConfig.__name__

urlpatterns = [
    # Logging-component dialogs
    path(
        "dialog-list/<slug:gemeente_slug>/",
        DialogEventListView.as_view(),
        name="dialog_list_logging_component",
    ),
    path(
        "dialog-list/<slug:gemeente_slug>/export",
        DialogExportView.as_view(),
        name="dialog_export",
    ),
    path(
        "test-dialog-list/<slug:gemeente_slug>/export",
        TestDialogExportView.as_view(),
        name="test_dialog_export",
    ),
    path(
        "dialog-detail/<slug:gemeente_slug>/<str:session_id>",
        DialogEventDetailView.as_view(),
        name="dialog_detail_logging_component",
    ),
    path(
        "test-dialog-list/<slug:gemeente_slug>/",
        TestDialogEventListView.as_view(),
        name="test_dialog_list_logging_component",
    ),
    path(
        "test-dialog-detail/<slug:gemeente_slug>/<str:session_id>",
        TestDialogEventDetailView.as_view(),
        name="test_dialog_detail_logging_component",
    ),
]
