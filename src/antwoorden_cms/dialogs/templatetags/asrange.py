from django import template

register = template.Library()


@register.filter
def asrange(x):
    return range(x)
