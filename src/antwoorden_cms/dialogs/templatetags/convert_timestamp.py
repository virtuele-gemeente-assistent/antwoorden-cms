from datetime import datetime

from django import template
from django.utils.timezone import make_aware

register = template.Library()


@register.filter
def convert_timestamp(timestamp):
    return make_aware(datetime.fromtimestamp(timestamp), is_dst=True)
