from django import template

from ..constants import HIGH_THRESHOLD, LOW_THRESHOLD

register = template.Library()


@register.filter
def confidence_class(confidence):
    segment = "low"
    if confidence >= HIGH_THRESHOLD:
        segment = "high"
    elif confidence >= LOW_THRESHOLD:
        segment = "middle"
    return f"confidence__{segment}"
