from django import template

register = template.Library()


@register.filter
def get_text(obj, key):
    return obj.get_text(key)
