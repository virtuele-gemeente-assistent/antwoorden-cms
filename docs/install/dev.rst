.. _install_development:

=======================
Development environment
=======================

Quick start
===========

#. Navigate to the location where you want to place your project.

#. Get the code::

    git clone git@gitlab.com:virtuele-gemeente-assistent/antwoorden-cms.git
    cd antwoorden_cms

#. Bootstrap the virtual environment and install all required libraries. The
   ``bootstrap.py`` script basically sets the proper Django settings file to be
   used::

    python bootstrap.py <production|staging|test|dev>

#. Activate your virtual environment and create the statics and database::

    source env/bin/activate  # or, workon <env> if you use virtualenvwrapper
    npm install
    npm run build
    python src/manage.py collectstatic --link
    python src/manage.py migrate


Next steps
----------

You can now run your installation and point your browser to the address given
by this command::

    python src/manage.py runserver

Dependencies
==================================
Dependencies are listed in the requirements directory in the ``<environment>.in`` files
and pinned in the ``<environment>.txt`` files.

Installing
----------

Requirements can be installed manually from one of these files::

    pip install -r <production|staging|test|dev>.txt

Adding a new dependency
-----------------------

Dependencies can be placed inside their respective environment file i.e. if used everywhere, place in `base.in`,
if only used in testing, place in 'test.in'

You'll need `uv <https://astral.sh/uv>`_ to generate the pinned versions of the
requirements, either system-wide or install in your virtualenv::

    ./bin/compile_dependencies.sh

You can use `pip-compile <https://pypi.org/project/pip-tools/>`_ arguments with this script::

    ./bin/compile_dependencies.sh --upgrade-package django

Connecting with Other Components
================================

Logging Component
-----------------

The `CMS` mainly interacts with the `logging component <https://gitlab.com/virtuele-gemeente-assistent/logging-component>`_:

* In the dialog list ``/overview/dialog-list/<gemeente>/`` and detail view ``/overview/dialog-detail/<gemeente>/<session_id>/``
* In the statics dashboard ``/statistics/<slug:gemeente_slug>/dashboard/`` and individual statistics views.

Many of these pages will cause exceptions if the `logging` component is not available.


The easiest way is to have the `logging component` running locally on another port e.g. ``8008`` and in your ``.env`` or
``docker-compose.yml`` set these values:

* ``LOGGING_COMPONENT_URL = "http://127.0.0.1:8008"``



Gem Media Container
-------------------

The `CMS` makes use of `GEM's <https://gitlab.com/virtuele-gemeente-assistent/gem>`_ media container component from the
``docker-compose.yml``. It is used with most ``UtterResponse`` related things, such as the stories views
``/<gemeente>/stories/<category>/``. In the UtterResponse admin (``/admin/utters/utterresponse/``), the `Import default responses` button will sync the
default responses from the `media container`.

The `CMS` will handle situations where the `media container` is not available by returning empty data without raising an exception.

You can run this by using the docker compose commands in the gem repository::

    docker compose pull # get the latest version
    docker compose up media # only start the media container

By default, the media container is hosted on port ``81`` and the ``MEDIA_CONTAINER_URL`` is set to ``"http://localhost:81"``,
so it should work without configuration. ``MEDIA_CONTAINER_URL`` can also be set in ``.env`` and ``docker-compose.yml``.

Other
-----

* ``CHATBOT_URL``: defaults to ``"https://virtuele-gemeente-assistent.nl"``. Does not need to be changed.


Testing
=======

VCR.py
------

Many tests that connect to the external APIs use `VCR.py <https://vcrpy.readthedocs.io/en/latest/>`_ that records
and replays responses from these external APIs.

The 'media container' is simple and required no configuration, just records the default responses.

The 'logging component' setup is quite complex. The test were preformed with a local logging component using the
staging database and return test sessions as if they were non-test sessions (by removing the filter).

.. note ::
    In the future, it might make sense to create a docker-compose for the logging component with preloaded data that
    is used to record VCRs.

By default, VCR is set to run once and will only record a new cassette once. You can rerecord a cassette by deleting the
cassette ``.yaml`` file in the ``tests/cassettes`` directory. The cassette files are named after the test they come from.
