Apache + mod-wsgi configuration
===============================

An example Apache2 vhost configuration follows::

    WSGIDaemonProcess antwoorden_cms-<target> threads=5 maximum-requests=1000 user=<user> group=staff
    WSGIRestrictStdout Off

    <VirtualHost *:80>
        ServerName my.domain.name

        ErrorLog "/srv/sites/antwoorden_cms/log/apache2/error.log"
        CustomLog "/srv/sites/antwoorden_cms/log/apache2/access.log" common

        WSGIProcessGroup antwoorden_cms-<target>

        Alias /media "/srv/sites/antwoorden_cms/media/"
        Alias /static "/srv/sites/antwoorden_cms/static/"

        WSGIScriptAlias / "/srv/sites/antwoorden_cms/src/antwoorden_cms/wsgi/wsgi_<target>.py"
    </VirtualHost>


Nginx + uwsgi + supervisor configuration
========================================

Supervisor/uwsgi:
-----------------

.. code::

    [program:uwsgi-antwoorden_cms-<target>]
    user = <user>
    command = /srv/sites/antwoorden_cms/env/bin/uwsgi --socket 127.0.0.1:8001 --wsgi-file /srv/sites/antwoorden_cms/src/antwoorden_cms/wsgi/wsgi_<target>.py
    home = /srv/sites/antwoorden_cms/env
    master = true
    processes = 8
    harakiri = 600
    autostart = true
    autorestart = true
    stderr_logfile = /srv/sites/antwoorden_cms/log/uwsgi_err.log
    stdout_logfile = /srv/sites/antwoorden_cms/log/uwsgi_out.log
    stopsignal = QUIT

Nginx
-----

.. code::

    upstream django_antwoorden_cms_<target> {
      ip_hash;
      server 127.0.0.1:8001;
    }

    server {
      listen :80;
      server_name  my.domain.name;

      access_log /srv/sites/antwoorden_cms/log/nginx-access.log;
      error_log /srv/sites/antwoorden_cms/log/nginx-error.log;

      location /500.html {
        root /srv/sites/antwoorden_cms/src/antwoorden_cms/templates/;
      }
      error_page 500 502 503 504 /500.html;

      location /static/ {
        alias /srv/sites/antwoorden_cms/static/;
        expires 30d;
      }

      location /media/ {
        alias /srv/sites/antwoorden_cms/media/;
        expires 30d;
      }

      location / {
        uwsgi_pass django_antwoorden_cms_<target>;
      }
    }
